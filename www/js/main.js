function char2Diacritic(transDiacritic) {
    var charDiacritic = "ÁČĎÉÍĹĽŇÓÔŔŘŠŤÚŮÝŽ";
    var numDiacritic = "ACDEILLNOORRSTUUYZ";
    var tmpDiacritic = "";
    var newDiacritic = "";

    transDiacritic = transDiacritic.toUpperCase();
    for (i = 0; i < transDiacritic.length; i++) {
        if (charDiacritic.indexOf(transDiacritic.charAt(i)) != -1)
            tmpDiacritic += numDiacritic.charAt(charDiacritic.indexOf(transDiacritic.charAt(i))) + '|';
        else
            tmpDiacritic += transDiacritic.charAt(i);
    }

    return tmpDiacritic;
}

jQuery.fn.dataTableExt.oSort['czechString-asc'] = function (a, b) {

    x = char2Diacritic(a);
    y = char2Diacritic(b);
    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
}

jQuery.fn.dataTableExt.oSort['czechString-desc'] = function (a, b) {

    x = char2Diacritic(a);
    y = char2Diacritic(b);
    return ((x < y) ? 1 : ((x > y) ? -1 : 0));
}

var confirmationConfig = {
    btnOkLabel: 'Ano',
    btnCancelLabel: 'Ne',
    btnCancelClass: 'btn btn-small btn-danger',
    placement: 'top',
    title: 'Opravdu smazat?'
};

$(function () {
    $('[data-toggle="confirmation"]').click(function () {
            if (!$(this).hasClass("noAlert")) {
                alert("POZOR!! Přistupujete k mazací akci! Tato akce může být nevratná!");
            }
        }
    );
    $.nette.ext('spinner', {
        start: function (jqXHR, settings) {
            $('#spinner').show();
        },
        complete: function () {
            $('#spinner').hide();
        }
    });

    $.nette.init();
    $(".tomodal").click(function () {
        var what = $(this).data("target");
        $('#' + what).modal();
    });

    $(".button").click(function () {
        $(".modal").modal("hide");
    });

    $("#myModal").on('shown.bs.modal', function (e) {
        $(".closing").click(function () {
            $(".modal").modal("hide");
        });
    });
});

function initModals() {
    $(".tomodal").click(function () {
        var what = $(this).data("target");
        $('#' + what).modal();
    });

    $(".closing").click(function () {
        $(".modal").modal("hide");
    });
}

function update() {
    $('#myTable').DataTable({

        "fnDrawCallback": function (nRow, aData, iDataIndex) {
            $.nette.load();
            initConfirmation();

            $(".tomodal").click(function () {
                var what = $(this).data("target");
                $('#' + what).modal();
            });
        },
        "initComplete": function (settings, json) {
            $("#spinner").fadeOut();
        }
    });
}

function initConfirmation() {
    $('[data-toggle="confirmation"]').confirmation(confirmationConfig);
}

function updateChosen() {
    $(".chosen-select").chosen();
}

function del(id) {
    var url = $("#del" + id).data("url");
    $.get(url, {}, function (data) {
        if (data.status == "ok") {
            $("#row_" + id).hide("slow");
        } else {
            alert('Došlo k neznámé FATÁLNÍ chybě. Kontaktujte administrátora.');
        }

        if ($("#del" + id).hasClass("notable")) {
            location.reload();
        }
    });
}


function delClass(id) {
    var url = $("#delClass" + id).data("url");
    $.get(url, {}, function (data) {

        if (data.status == "ok") {
            $(".row_" + id).hide("slow");
        }

        if ($(".del" + id).hasClass("notable")) {
            location.reload();
        }
    });
}

function delDep(id) {
    var url = $("#dep" + id).data("url");
    $.get(url, {}, function (data) {
        if (data.status == "ok") {
            $("#rowDep_" + id).hide("slow");
        }

        if ($("#del" + id).hasClass("notable")) {
            location.reload();
        }
    });
}


function rc() {
    var obj = $(".rc");

    if (obj.val().length >= 6) {
        var fail = false;
        var text = obj.val();
        var ar = text.match(/.{1,2}/g);
        var year_rc = ar[0];
        var month_rc = ar[1];
        var day_rc = ar[2];

        var year = "19" + year_rc;
        if (year_rc == "00") {
            year = "2000";
        }

        var month = 0;
        if ((month_rc > 0 && month_rc <= 12)) {
            month = month_rc;
        } else if (month_rc >= 51 && month_rc <= 62) {
            month = month_rc - 50;
        } else {
            fail = true;
        }

        if (day_rc <= 31 && day_rc > 0) {
            var day = day_rc
        } else {
            fail = true;
        }

        if (!fail && text.length <= 10) {
            $("input[name=birthDate]").val(day + "." + month + "." + year);
        } else {
            $("input[name=birthDate]").val("neplatne RČ");
        }
    } else {
        $("input[name=birthDate]").val("--.--.----");
    }
}

function rc_update() {
    $(".rc").keyup(function () {
        rc();
    });
}

function integer(number) {
    return Math[number < 0 ? 'ceil' : 'floor'](number);
}

function datepickerStart() {
    $.fn.datepicker.dates['cs'] = {
        days: ["Neděle", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota"],
        daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        daysMin: ["Ne", "Po", "Ut", "St", "Čt", "Pá", "So"],
        months: ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"],
        monthsShort: ["Led", "Únr", "Bře", "Dub", "Kvě", "Čer", "Čvc", "Srp", "Zář", "Říj", "Lis", "Pro"],
        today: "Dnes",
        clear: "Clear",
        format: "dd.mm.yyyy",
        titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
        weekStart: 1
    };

    $("#to").datepicker({language: 'cs'});
    $("#from").datepicker({language: 'cs'}).on("change", function () {
        var minValue = $(this).val();

        $("#to").datepicker("remove");
        $("#to").datepicker({startDate: minValue, language: 'cs'});

    })
}

function plus(obj, step) {
    if (!Array.prototype.getKey) {
        Array.prototype.getKey = function (value) {
            for (var key in this) {
                if (this[key] == value) {
                    return key;
                }
            }
            return null;
        };
    }

    var target = $(obj).data("target");
    var type = $(obj).data("type");
    var current_value = $("#" + target).val();

    var max = $(obj).data("max");

    if (current_value == max) {
        alert("Bylo dosaženo maxima");
        // na serveru se to kontroluje rucne
        return false;
    }
    var series;
    if (step == 0) {
        if ($("#velikostidavky").data("velikostidavky") == "ctvrtiny") {
            series = [0, 0.25, 0.5, 0.75, 1,
                1.25, 1.5, 1.75, 2,
                2.25, 2.5, 2.75, 3,
                3.25, 3.5, 3.75, 4,
                4.25, 4.5, 4.75, 5,
                5.25, 5.5, 5.75, 6,
                6.25, 6.5, 6.75, 7,
                7.25, 7.5, 7.75, 8
            ];
        } else {
            series = [0, 0.33, 0.66, 1, 1.33, 1.66, 2, 2.33, 2.6, 3, 3.33, 3.66, 4, 4.33, 4.66, 5, 5.33, 5.66, 6,
                6.33, 6.66, 7, 7.33, 7.66, 8];
        }

        var index = series.getKey(current_value);
        var index_new = index;
        if (type == "minus") {
            index_new--;
            if (index_new < 0) {
                index_new = 0;
            }
        } else {
            index_new++;
        }

        var kopirovani = $("#kopirovani").data("kopirovani");

        if (kopirovani == 'all') {
            var davka = $("#" + target).data("davka");
            $("." + davka).val(series[index_new]);
            $("." + davka).css("background", "#FFB0B0");
            setTimeout(function () {
                $("." + davka).css("background", "white");
            }, 500);

        } else if (kopirovani == 'sl') {
            var davka = $("#" + target).data("davka");
            var sl = $("#" + target).data("sl");
            $("." + davka + "." + sl).val(series[index_new]);

            $("." + davka + "." + sl).css("background", "#FFB0B0");
            setTimeout(function () {
                $("." + davka + "." + sl).css("background", "white");
            }, 500);
        } else if (kopirovani == 'weeks') {
            var den = $("#" + target).data("den");
            var davka = $("#" + target).data("davka");

            $("." + davka + "." + den).val(series[index_new]);

            $("." + davka + "." + den).css("background", "#FFB0B0");
            setTimeout(function () {
                $("." + davka + "." + den).css("background", "white");
            }, 500);
        } else if (kopirovani == 'none') {

            $("#" + target).val(series[index_new]);
            $("#" + target).css("background", "#FFB0B0");
            setTimeout(function () {
                $("#" + target).css("background", "white");
            }, 500);
        }
    } else {

        if (type == "minus") {
            if (current_value != step) {
                $("#" + target).val(parseInt(current_value) - step);
            }
        } else {
            $("#" + target).val(parseInt(current_value) + step);
        }
    }
}

function changeWeek(show) {

    var classes = ["yy", "druhy_tyden", "treti_tyden", "ctvrty_tyden", "paty_tyden", "sesty_tyden", "sedmy_tyden", "osmy_tyden", "devaty_tyden", "desaty_tyden", "jedenacty_tyden", "dvanacty_tyden"];

    var weeks = $(".weeks-select").val();
    var showSelector = ".xx";

    while (weeks > 0) {
        showSelector += ", ." + classes[weeks - 1];
        weeks--;
    }

    if (show == "prvni") {
        $(showSelector).hide();
        $("#druhy_tab").removeClass("active");

        $("#prvni_tab").addClass("active");
    } else {
        $(showSelector).show();
        $("#druhy_tab").addClass("active");

        $("#prvni_tab").removeClass("active");
    }

    adjustActiveWeek();
}

/**
 * controls the selectBox of activeWeek in medical card form
 */
function adjustActiveWeek() {
    $("#activeWeekAlert").hide();

    var selectedWeeksNumber = $(".weeks-select").val();
    var currentActiveWeek = $(".activeWeekSelect").val();

    if (currentActiveWeek > selectedWeeksNumber) {
        $(".activeWeekSelect").val(1);
        $("#activeWeekAlert").show('fast');
        // resetting the value to first option
    }
    $(".activeWeekSelect option").removeAttr("disabled")
    $(".activeWeekSelect option:gt(" + (selectedWeeksNumber - 1) + ")").attr('disabled', 'disabled');
}

function removeActiveWeekAlert() {
    $("#activeWeekAlert").hide();

}


function velikostidavky(what) {
    $("#velikostidavky").data("velikostidavky", what);
    $(".velikostidavky").removeClass("active");
    $("." + what).addClass("active");

    $(".input-number").val(0);
}

function changeSD(what) {

    $("#kopirovani").data("kopirovani", what);
    $(".kopirovani").removeClass("active");
    $("." + what).addClass("active");

    if (what == "sl" || what == "none") {
        $("#showTwoWeeks").val(1);
    } else {
        $("#showTwoWeeks").val(0);
    }

    if (what == "none") {
        $("#help").show();
        $("#help").removeClass("user-info");
        $("#help").addClass("user-warning");
        $("#help").text("Pozor, musíte správně zadat i data druhého týdne! Data se nekopírují.");
    } else {
        var msgs = {
            all: "Data se duplikují do všech řádků a týdnů.",
            sl: "Data se duplikují ob řádek. Použijte pro sudé/liché dávky.",
            weeks: "Zadávané dávky se zrcadlí do druhého týdne."
        }
        $("#help").show();
        $("#help").removeClass("user-warning");
        $("#help").addClass("user-info");
        $("#help").text(msgs[what]);
    }

}

function upravVelikost(what) {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="confirmation"]').confirmation(confirmationConfig);
    if (what == 'big') {
        $(".modal-dialog").addClass("big_modal");
    } else if (what == 'medium') {
        $(".modal-dialog").addClass("medium_modal");
    } else {
        $(".modal-dialog").removeClass("big_modal");
        $(".modal-dialog").removeClass("medium_modal");
    }
}

function reinitClosing() {
    // reinit closing operation
    $(".closing").click(function () {
        $(".modal").modal("hide");
    });
}

function closeModal(){
    $(".modal").modal("hide");

}


function initNextOption() {
    $("#next_option").click(function () {
        // todo - asi by to chtelo prepsat nejak hezky...
        var int = $(this).data("num");
        int++;

        $("#frm-medicineForm-options1").data("num", int);
        //id="frm-medicineForm-options1"
        var data = "<div class='form-group'>" +
            "<input type='text' name='options" + int + "' class='hiddenOption form-control medium-text' placeholder='Balení " + int + "' style='display: block;' id='frm-medicineForm-options" + int + "'   data-lfv-initialized='true' " +
            "data-lfv-message-id='frm-medicineForm-options" + int + "_message'><span " +
            "id='frm-medicineForm-options" + int + "_message'></span></div>";
        $(this).data("num", int);
        var int2 = int - 1;
        $("#frm-medicineForm-options" + int2).parent().after(data);

    });
}

function defColumns() {
    var table_name = $('#myTable').data("name");

    if (table_name == "patient-admin") {
        var columns = [{"sType": "czechString"}, {"sType": "de_date"}, null, null, null, null];
    } else if (table_name == "patient-view") {
        var columns = [{"sType": "czechString"}, {"sType": "de_date"}, null, null];
    } else if (table_name == "patient-view-secret") {
        var columns = [{"sType": "czechString"}, {"sType": "de_date"}, null, null, null];

    } else if (table_name == "prescription9") {
        var columns = [{"sType": "czechString"}, null, null, null, null, null, {"sType": "de_date"}, null, null, ];
    } else if (table_name == "prescription10") {
        var columns = [{"sType": "czechString"}, null, null, null, null, null, {"sType": "de_date"}, null, null, null, ];
    } else if (table_name == "prescription11") {
        var columns = [{"sType": "czechString"}, null, null, null, null, null, {"sType": "de_date"}, null, null, null, null];
    } else if (table_name == "prescription12") {
        var columns = [{"sType": "czechString"}, null, null, null, null, null, {"sType": "de_date"}, null, null, null, null, null];
    } else if (table_name == "prescription13") {
        var columns = [{"sType": "czechString"}, null, null, null, null, null, {"sType": "de_date"}, null, null, null, null, null, null];
    } else if (table_name == "branche") {
        var columns = [null, null, null, null];
    } else if (table_name == "history") {
        var columns = [null, null, null, null, {"sType": "de_datetime"}, null];
    } else if (table_name == "batching") {
        var columns = [{"sType": "czechString"}, {"sType": "czechString"}, {"sType": "czechString"}, null, null, null, null];
    }

    return columns;
}

function update_filter(order, asc) {

    order = typeof order !== 'undefined' ? order : 0;
    asc = typeof asc !== 'undefined' ? asc : 'asc';
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="confirmation"]').confirmation(confirmationConfig);


    $('#myTable').DataTable({

        "order": [[order, asc]],
        "aoColumns": defColumns(),


        initComplete: function () {
            $("#spinner").fadeOut();

            this.api().columns(".filter").every(function () {
                var column = this;
                var select = $('<select class="form-control chosen small-chosen"><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        },
        "fnDrawCallback": function (nRow, aData, iDataIndex) {

            $.nette.load();
            $(".tomodal").click(function () {
                var what = $(this).data("target");
                $('#' + what).modal();
            });
        }
    });
}

function sendReport(obj) {
    var url = $(obj).data("url");
    if (url == "#") {
        alert("Pokud chcete odeslat email znovu, obnovte stránku");
    } else {
        $.get(url, {}, function (data) {
                if (data == "ok") {
                    $(obj).removeClass("btn-info");
                    $(obj).addClass("btn-default");
                    $(obj).text("Odesláno");
                    $(obj).data("url", "#");
                    alert("Email s reportem byl odeslán");
                } else {
                    alert("Došlo k chybě, kontaktujte prosím administrátora");
                }
            }
        );
    }
}

function changePck(obj) {

    var size = $(obj).data("size");
    var sum = $(obj).data("sum");
    var target = $(obj).data("target");
    var ret = Math.round((sum / size) * 100) / 100;

    $(obj).siblings().removeClass("btn-success");
    $(obj).addClass("btn-success");

    $("#" + target).text(ret);
}

function addToCardDuplicate() {
    // duplikovat form
    // v kazdem policku zmenit prefix [x]inputYY
    // skryt puvodni
    // zobrazit novy
}

function modifyLink(obj, noLoading) {
    var from = $("#from").val();
    var to = $("#to").val();

    var link_src = $(obj).attr("href");
    if ($(obj).data("tmphref") != undefined && !noLoading) {
        link_src = $(obj).data("tmphref");
        $("#spinner").fadeIn();
    }
    var link = link_src + "&from=" + from + "&to=" + to;

    $(obj).attr("href", link);

}

function showLoading() {
    $("#spinner").fadeIn();
}

function newMCcomment(obj) {
    var name = $(obj).find(":selected").text();
    var currentVal = $(".comment-input").val();

    var parts = currentVal.split(": ");
    if (currentVal.slice(-1) == ":" || (parts.length == 2 && $(obj).data("lastName") == parts[1])) {
        $(".comment-input").val($(".comment-input").data("bcktitle") + " " + name);

        $(obj).data("lastName", name);

    }
}

function renderOrderSlots() {
    var last;
    var inputElements = $(".orderHash");
    inputElements.each(function (index) {
        var element = inputElements[index];
        $(element).parent().addClass("orderHashLabel");
        var parsedValue = $(element).attr("value").split("#");
        if (parsedValue[1] == "") {
            $(element).parent().addClass("noOrder");
        }

        if ($(element).is(':checked')) {
            $(this).parent().addClass("orderActive");
        }

        $(element).on("click", toggleActiveClass);

        last = element;

    });

    $(last).parent().parent().children("br").remove();
}

function toggleActiveClass() {
    $(".orderHashLabel").removeClass("orderActive");
    $(this).parent().addClass("orderActive");
}

function changePackages() {
    $(".medicine-select").change(function () {
        $.get($("#system").data("url") + "/" + $(".medicine-select").val(), function (data) {
            // data = {values: [ {ID: 1, value: }, {ID: VAL}]}
            var out = "";
            for (var i = 0; i < data.length; i++) {
                out += "<option value='" + data[i].id + "'>" + data[i].size + "</option>";
            }
            $(".medicine_package_chose").html(out);
        }, "json");
    });
}

function setCustomSystemOrderItem(option) {
    $(".customOrSystem").val(option);
}

function setActiveMedicineSource() {
    var target = $(".customOrSystem").val();
    $("#tabMarkup_" + target).click();
}

function expandOrder(obj, id) {
    $("#expand-overlay" + id).hide();
    $("#collapseButton" + id).show();
    $("#order" + id).removeClass("table-order-items");

    $(obj).hide();
    var openOrders = JSON.parse(window.localStorage.getItem("openOrders"));
    if (!openOrders) {
        openOrders = [];
    }
    // openOrders = openOrders.splice( $.inArray(id, openOrders), 1 ); // just to avoid from duplicates
    openOrders = jQuery.grep(openOrders, function (value) {
        return value != id;
    });
    openOrders.push(id);
    window.localStorage.setItem("openOrders", JSON.stringify(openOrders));
}

function collapseOrder(obj, id) {
    $("#expand-overlay" + id).show();
    $("#expandButton" + id).show();
    $("#order" + id).addClass("table-order-items");

    $(obj).hide();

    var openOrders = JSON.parse(window.localStorage.getItem("openOrders"));
    openOrders = jQuery.grep(openOrders, function (value) {
        return value != id;
    });
    window.localStorage.setItem("openOrders", JSON.stringify(openOrders));
}

function initCountdown() {
    $(".countdown-markup").each(function () {
        var datum = $(this).data('targetdate');
        var clock = $(this).countdown(new Date(datum), function (event) {
            $(this).html(event.strftime('%D dní , %H:%M:%S'));
        }).on('finish.countdown', finishCountDown);
    });

    function finishCountDown() {
        var orderId = $(this).data('orderid');
        var url = $("#closeOrderDatTime" + orderId).data('url');
        $.get(url, function (responseData) {
            if (responseData.status == 'ok') {
                location.reload();
            }
        });
    }
}

function initArchivedFilterButtons() {
    var remove, add;

    $("#checkbox-extra").change(function () {
        var extra = $("#checkbox-extra").is(":checked");
        if (!extra) {
            remove = "fa-check-square-o";
            add = "fa-square-o";
            $(".order_extra").fadeOut("fast");
        } else {

            add = "fa-check-square-o";
            remove = "fa-square-o";
            $(".order_extra").fadeIn("fast");
        }
        $("label[for=checkbox-extra] > span > i").removeClass(remove).addClass(add);

    });
}

function initOrderFilterButtons() {
    $(".filter-button-checkbox").change(function () {
        var id = $(this).attr("id");
        var val = $(this).val();
        var remove, add;
        var extra = $("#checkbox-extra").is(":checked");

        if (id == "checkbox-extra")  // operation for extra
        {
            if (!extra) {
                remove = "fa-check-square-o";
                add = "fa-square-o";
                $(".order_extra").fadeOut("fast");
            } else {
                remove = "fa-square-o";
                add = "fa-check-square-o";
                $(".filter-button-checkbox:checked").each(function (index, element) {
                    if ($(element).val() != "extra") {
                        $(".order_extra.order_" + $(element).val()).fadeIn("fast");
                    }
                });
            }
        } else  // normal operation
        {
            if ($(this).is(':checked')) {
                remove = "fa-square-o";
                add = "fa-check-square-o";
                $(".order_" + val).fadeIn("fast");
            } else {
                remove = "fa-check-square-o";
                add = "fa-square-o";
                $(".order_" + val).fadeOut("fast");
            }

            if (extra)  // removing or showing the extra
            {
                $(".order_" + val + ".order_extra").show();
            } else {
                $(".order_" + val + ".order_extra").hide();
            }

        }

        // changing icon
        $("label[for=" + id + "] > span > i").removeClass(remove).addClass(add);
        updateFilterStorage();
    });

    // applying stored filters from localStorage
    var activeFilters = JSON.parse(window.localStorage.getItem("checkedFilters")) || []; // active filters from localStorage
    var allFilterElements = $(".filter-button-checkbox"); // all filter elements

    // setting filters (prop checked) and generating the change event which is registered above
    allFilterElements.each(function (index, value) {
        var shouldBeChecked = (activeFilters.indexOf($(value).attr("id")) >= 0);
        $("#" + $(value).attr("id")).prop("checked", shouldBeChecked);
        $("#" + $(value).attr("id")).change();
    });
}

/**
 * based on the localStorage info - opens the orders
 */
function initExpandButtons() {
    var openOrders = JSON.parse(window.localStorage.getItem('openOrders'));
    if (openOrders) {
        openOrders.forEach(function (item) {
            expandOrder($("#expandButton" + item), item);
        });
    }

}

/**
 * collects checked filters and stores it in the localStorage
 */
function updateFilterStorage() {
    var checkedElements = [];
    var elements = $(".filter-button-checkbox");
    elements.each(function (index, el) {
        if ($(el).is(":checked"))
            checkedElements.push($(el).attr("id"));
    });

    window.localStorage.setItem("checkedFilters", JSON.stringify(checkedElements));
}

function expandAll() {
    $(".expand-button").click();
}

function collapseAll() {
    $(".collapse-button").click();
}

/**
 * rendering the radio list items (when moving) with color and border regarding the status of the order
 */
function renderLabels() {
    var elements = $(".orderListLabels");
    for (var i = 0; i < elements.length; i++) {
        var itemParent = $(elements[i]).parent();

        if (itemParent.text().indexOf("Odeslaná") > 0) {
            $(itemParent).addClass('orderMoveCheckbox');
            $(itemParent).addClass('orderMoveCheckboxSent');
            if (itemParent.text().indexOf("Extra") > 0) {
                $(itemParent).addClass('orderMoveCheckboxExtra');
            }
        }

        if (itemParent.text().indexOf("Neotevřená") > 0) {
            $(itemParent).addClass('orderMoveCheckbox');
            $(itemParent).addClass('orderMoveCheckboxNotOpen');
        }

        if (itemParent.text().indexOf("Nová") > 0) {
            $(itemParent).addClass('orderMoveCheckbox');
            $(itemParent).addClass('orderMoveCheckboxOpen');
        }
    }
}

function initPermissionTree() {
    $("#tree").fancytree({
        checkbox: true,
        selectMode: 2,
        source: {
            url:
                $("#tree").data('url')
        },
        lazyLoad: function (event, data) {
            data.result = {url: $("#tree").data('url')};
        },
    });
}

function saveTreeData() {
    var data = $.ui.fancytree.getTree().toDict();
    var url = $("#tree").data('saveurl');
    $.post(url, {data}, function (ret) {
        if(ret.status === 'ok'){
            $("#flashes.custom .alert").fadeIn(3000,function () {
                $("#flashes.custom .alert").fadeOut(3000);
            });
        }
    });
}