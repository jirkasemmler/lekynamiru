<?php
if ($file = file_get_contents(__DIR__ . "/../setting/settings.json"))
{
	$parsed_file = json_decode($file, true);
	if(!array_key_exists("mode", $parsed_file) || !in_array($parsed_file['mode'], ["debug", "down", "normal"]))
	{
		killThatShit();
	}

	if ($parsed_file["mode"] == "down" && $parsed_file["ip"] != $_SERVER['REMOTE_ADDR'])
	{
		if ($page = file_get_contents("maintenance/index.html"))
		{
			echo $page;
		}
		else
		{
			killThatShit();
		}
		exit();
	}
	else
	{
		$container = require __DIR__ . '/../app/bootstrap.php';
		$container->getByType('Nette\Application\Application')->run();
	}
}
else
{
	killThatShit();
}


function killThatShit()
{
	echo "<html><h1>Doslo k neocekavane chybe.</h1><h4>Kontaktujte prosim adminitratora na +420 734 417 421 nebo jirka.semmler@gmail.com</h4></html>";
	exit();
}
