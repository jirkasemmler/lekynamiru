<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>LnM debug</title>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<style>
	.btn {margin: 2px}
</style>
<body>

<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<a class="navbar-brand" href="#">Debug tool</a>
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link"  target='_blank' href="renderTime.php">Cron time</a>
			</li>
			<li class="nav-item">
				<a class="nav-link"  target='_blank' href="https://lekynamiru.cz/debug/report.html">GoAccess</a>
			</li>
		</ul>
	</div>
</nav>

<br>
<?php
$tracyForm = '
<form action="index.php" method="post" class="form-inline">
	Heslo: <input class="form-control mr-sm-2" type="password" name="heslo"><br>
	<input class="form-control btn btn-info" type="submit" name="odeslat" value="zapnout"> <input name="odeslat"  class="form-control btn btn-info" type="submit" value="vypnout">
</form>
';
// TODO batch and bootstrap form
$tracyOp = "";
$tracyStat = "";
if(isset($_POST["odeslat"]) && isset($_POST["heslo"]) && $_POST["heslo"] == "xx")
{
	if($_POST["odeslat"] == "zapnout")
	{
		$data = ["mode"=>"debug", "ip"=>$_SERVER['REMOTE_ADDR']];
		$tracyOp = "<i>zapinam</i><br>";

	}
	elseif($_POST["odeslat"] == "vypnout")
	{
		$tracyOp = "<i>vypinam</i><br>";

		$data = ["mode"=>"normal", "ip"=>null];
	}

	$to_write = json_encode($data);
	$data = file_put_contents("../../setting/settings.json", $to_write);

}
elseif(isset($_POST["odeslat"]))
{
	$tracyOp = "<i>neplatne udaje</i><br>";
}
else
{
	$tracyOp = "<i>nothing to do</i><br>";
}

$data = file_get_contents("../../setting/settings.json");
$parsed_file = json_decode($data, true);

$tracyStat = (isset($parsed_file["mode"]) && $parsed_file["mode"] == "debug") ? "<span style='color: darkgreen'>zapnuty</span>" : "<span style='color: red'>vypnuty</span>";

?>


<?php
$downForm = '
<form action="index.php" method="post" class="form-inline">
	Heslo: <input class="form-control mr-sm-2" type="password" name="heslo"><br>
	<input class="form-control btn btn-info" type="submit" name="odeslatDown" value="zapnout"> <input name="odeslatDown"  class="form-control btn btn-info" type="submit" value="vypnout">
</form>
';
// TODO batch and bootstrap form
$downOp = "";
$downStat = "";
if(isset($_POST["odeslatDown"]) && isset($_POST["heslo"]) && $_POST["heslo"] == "xx")
{
	if($_POST["odeslatDown"] == "zapnout")
	{
		$data = ["mode"=>"down", "ip"=>$_SERVER['REMOTE_ADDR']];
		$downOp = "<i>zapinam</i><br>";

	}
	elseif($_POST["odeslatDown"] == "vypnout")
	{
		$downOp = "<i>vypinam</i><br>";

		$data = ["mode"=>"normal", "ip"=>null];
	}

	$to_write = json_encode($data);
	$data = file_put_contents("../../setting/settings.json", $to_write);

}
elseif(isset($_POST["odeslat"]))
{
	$downOp = "<i>neplatne udaje</i><br>";
}
else
{
	$downOp = "<i>nothing to do</i><br>";
}

$data = file_get_contents("../../setting/settings.json");
$parsed_file = json_decode($data, true);

$downStat = (isset($parsed_file["mode"]) && $parsed_file["mode"] == "down") ? "<span style='color: darkgreen'>zapnuty</span>" : "<span style='color: red'>vypnuty</span>";

?>

<?php

$goForm = '
<form action="index.php" method="post" class="form-inline">
	Heslo: <input class="form-control mr-sm-2" type="password" name="heslo"><br>
	<input class="form-control btn btn-info" type="submit" name="odeslatgo" value="Restart"> 
</form>
';
$goOp = "";
$goStat = "";
if(isset($_POST["odeslatgo"]) && isset($_POST["heslo"]) && $_POST["heslo"] == "xx")
{
	if($_POST["odeslatgo"] == "Restart")
	{
		$goOp = "<i>Resetuju monitor</i><br>";
		system("sh ../../scripts/goaccess.sh");
	}
}
elseif(isset($_POST["odeslat"]))
{
	$goOp = "<i>neplatne udaje</i><br>";
}
else
{
	$goOp = "<i>nothing to do</i><br>";
}
?>

<?php
$logsForm = '
<form action="index.php" method="post" class="form-inline">
	Heslo: <input class="form-control mr-sm-2" type="password" name="heslo"><br>
	<input class="form-control btn btn-info" type="submit" name="odeslatLogy" value="smazat">
</form>';


if(isset($_POST["odeslatLogy"]) && isset($_POST["heslo"]) && $_POST["heslo"] == "xx")
{
	$files_to_remove = scandir("../../log");

	$logOp = "<i> mazu logy </i>";
	foreach($files_to_remove as $file_to_remove){ // iterate files
		if(is_file("../../log/".$file_to_remove))
			unlink("../../log/".$file_to_remove); // delete file
	}
}
else
{
	$logOp =  "<i> nothing to do </i>";
}

$files = scandir("../../log/");
$logFiles = "";
foreach($files as $file)
{
	if($file == "." || $file == "..")
	{
		continue;
	}
	$logFiles .= "<li><a href='log.php?type=log&file=".$file."' target='_blank'>$file</a></li>";
}
?>

<?php

$cacheForm = '
<form action="index.php" method="post" class="form-inline">
	Heslo: <input class="form-control mr-sm-2" type="password" name="heslo"><br>
	<input class="form-control btn btn-info" type="submit" name="odeslatCache" value="smazat">
</form>';


if(isset($_POST["odeslatCache"]) && isset($_POST["heslo"]) && $_POST["heslo"] == "xx")
{
	echo system("rm -rf ../../temp/cache");
	echo system("rm -rf ../../temp/*.php");
	$cacheOp = "cache smazana, je tam ?";
}
else
{
	$cacheOp = "<i> nothing to do </i>";
}
$files = scandir("../../temp");
$cacheFiles = "";
foreach($files as $file)
{
	if($file == "." || $file == "..")
	{
		continue;
	}
	$cacheFiles .= "<li>$file</li>";
}
?>

<div class="container">
	<div class="row">
		<div class="col-md-3">

			<div class="card">
				<div class="card-block">
					<h4 class="card-title">Tracy</h4>
					<h6 class="card-subtitle mb-2 text-muted"><?php echo $tracyOp ?></h6>
					<h6 class="card-subtitle mb-2 text-muted"><?php echo $tracyStat ?></h6>
					<p class="card-text"><?php echo $tracyForm?></p>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card">
				<div class="card-block">
					<h4 class="card-title">Down</h4>
					<h6 class="card-subtitle mb-2 text-muted"><?php echo $downOp ?></h6>
					<h6 class="card-subtitle mb-2 text-muted"><?php echo $downStat ?></h6>
					<p class="card-text"><?php echo $downForm?></p>
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="card">
				<div class="card-block">
					<h4 class="card-title">GoAccess</h4>
					<h6 class="card-subtitle mb-2 text-muted"><?php echo $goOp ?></h6>
					<p class="card-text"><?php echo $goForm ?></p>
					<a href="http://lekynamiru.cz/debug/report.html"  target='_blank' class="card-link">Link</a>
				</div>
			</div>

		</div>
		<div class="col-md-3">
			<div class="card">
				<div class="card-block">
					<h4 class="card-title">Logs</h4>
					<h6 class="card-subtitle mb-2 text-muted"><?php echo $logOp ?></h6>
					<p class="card-text"><?php echo $logsForm ?></p>
					<a href="#" class="card-link" data-toggle="modal" data-target="#logsModal">Vypsat</a>
				</div>
			</div>

		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-3">

			<div class="card">
				<div class="card-block">
					<h4 class="card-title">Orders</h4>
					<a href="log.php?type=report&file=sendOrders.json" target='_blank' class="card-link">Send</a>
					<a href="log.php?type=report&file=archiveOrders.json" target='_blank' class="card-link">Archived</a>
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="card">
				<div class="card-block">
					<h4 class="card-title">Recount</h4>
					<h6 class="card-subtitle mb-2 text-muted">run </h6>
					<a href="#"  data-toggle="modal" data-target="#recountModal" class="card-link">Link</a>
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="card">
				<div class="card-block">
					<h4 class="card-title">CronLogs</h4>
					<h6 class="card-subtitle mb-2 text-muted">reports </h6>
					<a href="#"  data-toggle="modal" data-target="#cronModal" class="card-link">Link</a>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card">
				<div class="card-block">
					<h4 class="card-title">Cache</h4>
					<h6 class="card-subtitle mb-2 text-muted"><?php echo $cacheOp ?></h6>
					<p class="card-text"><?php echo $cacheForm ?></p>
					<a href="#"  data-toggle="modal" data-target="#cacheModal" class="card-link">Vypsat</a>
				</div>
			</div>

		</div>
	</div>
</div>



<?php
$files = scandir("../../reports");
$cronFiles = "";
foreach($files as $file)
{
	if($file == "." || $file == "..")
	{
		continue;
	}
	$cronFiles .= "<li><a href='log.php?type=report&file=".$file."' target='_blank'>$file</a></li>";
}
?>


<?php
$files = scandir("../../run");
$runFiles = "";
foreach($files as $file)
{
	if($file == "." || $file == "..")
	{
		continue;
	}
	$runFiles .= "<li><a href='log.php?type=run&file=".$file."' target='_blank'>$file</a></li>";
}
?>


<div class="modal fade" id="logsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Logs</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<ul>
					<?php echo $logFiles; ?>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="cacheModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Cache</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<ul>
					<?php echo $cacheFiles; ?>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="cronModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Reports from cron</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<ul>
					<?php echo $cronFiles; ?>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="recountModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Recounts</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<ul>
					<?php echo $runFiles; ?>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.3/js/tether.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
</body>
</html>
