<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Recount stats</title>
	<link rel="stylesheet" href="../css/bootstrap.css">
</head>
<script src="../js/bootstrap.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<body>

<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Recount stats</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="renderTime.php">All</a></li>
				<li><a href="renderTime.php?limit=last-month">Last month</a></li>
				<li><a href="renderTime.php?limit=18-01">2018-01</a></li>
				<li><a href="renderTime.php?limit=18-02">2018-02</a></li>
				<li><a href="renderTime.php?limit=18*">2018</a></li>
				<li><a href="renderTime.php?limit=17*">2017</a></li>
				<li><a href="renderTime.php?limit=16*">2016</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>

<div class="container">
	<div class="row">
		<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	</div>
	<div class="row">
		<div id="container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

	</div>
</div>


<?php
function align($date, $value){
	$limit1 = new Datetime('2017-08-08');
	$limit2 = new Datetime('2017-09-04');
	$limit3 = new Datetime('2017-11-10');
	$limit4 = new Datetime('2018-03-01');

	$out = $value;

	if((new Datetime($date)) < $limit4){
		$out = 10000 - $value;
	}
	if((new Datetime($date)) < $limit3){
		$out = 9700 - $value;
	}
	if((new Datetime($date)) < $limit2){
		$out = 5200 - $value;
	}
	if((new Datetime($date)) < $limit1){
		$out = 2300 - $value;
	}

	return round($out);

}
$pattern = "../../run/recount20*.json";
if(isset($_GET['limit'])){
	if($_GET['limit'] == "last-month"){
		$ym = (new DateTime())->format("y-m");
		$pattern = "../../run/recount20".$ym."*.json";
	}
	else{
		$pattern = "../../run/recount20".$_GET['limit']."*.json";
	}
}
$files = glob($pattern);
$out = [];
$dates = [];
$elements = [];
$times = [];
foreach($files as $file)
{
	$string = file_get_contents($file);
	$json_a = json_decode($string, true);
	if(!isset($json_a['date'])){
		$json_a["date"] = substr($file, 17,10);
	}

	$dates[] = $json_a["date"];
	$elements[] = $json_a["id"];
	$times[] = align($json_a["date"], $json_a["time"]);
	$out[] = $json_a;
}
?>
<script>
    Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Amount of IDs in time'
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        xAxis: {
            categories: ['<?php echo implode("', '", $dates)?>']
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'amount of elements',
            data: [<?php echo implode(", ", $elements)?>]
        }]
    });

    Highcharts.chart('container2', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Recount time'
        },
        xAxis: {
            categories: ['<?php echo implode("', '", $dates)?>']
        },
        yAxis: {
            title: {
                text: 'time [s]'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Time',
            data: [<?php echo implode(", ", $times)?>]
        }]
    });
</script>
<div class="container">
	<div class="row">


<table class="table table-striped">
	<tr>
		<th>Datum</th>
		<th>IDs</th>
		<th>Time</th>
		<th>Link</th>
	</tr>

<?php

foreach($out as $item){
	echo "<tr>
<td>".$item['date']."</td>
<td>".$item['id']."</td>
<td>".$item['time']."</td>
<td><a href='log.php?type=run&file=recount".$item['date'].".json'>Link</a></td>
</tr>";
}

echo "</table>"


?>
	</div>
</div>
</body>
</html>