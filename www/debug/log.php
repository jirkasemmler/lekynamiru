<?php

if (isset($_GET["type"]) && isset($_GET["file"])) {
    $fileNameLog = "../../log/" . $_GET["file"];
    $fileNameReport = "../../reports/" . $_GET["file"];
    $fileNameRun = "../../run/" . $_GET["file"];

    if ($_GET["type"] === "log" && file_exists($fileNameLog)) {
        $a = file_get_contents($fileNameLog);
        echo $a;
    } elseif ($_GET["type"] === "report" && file_exists($fileNameReport)) {
        $a = file_get_contents($fileNameReport);
        echo $a;
    } elseif ($_GET["type"] === "run" && file_exists($fileNameRun)) {
        $a = file_get_contents($fileNameRun);
        echo $a;
    } else {
        echo "nothing to do";
    }
} else {
    echo "nothing to do";
}
