README for maintenance purposes

There are three stages of app status
1) NORMAL
2) DEBUG
3) DOWN

NORMAL - app works normally, no maintenance page or Tracy is displayed
settings.json MUST BE : {"mode":"normal, "ip": null}

DEBUG - app works normally but displays the Tracy for selected IP
settings.json MUST BE : {"mode":"debug", "ip": "<your IP>"}

DOWN - app shows the maintenance page without any functionality. There is working app for selected IP with Tracy
settings.json MUST BE : {"mode":"down", "ip": "<your IP>"}