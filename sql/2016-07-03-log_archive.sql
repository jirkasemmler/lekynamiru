CREATE TABLE `log_archive` (
  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `file` varchar(255) COLLATE 'utf8_czech_ci' NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE='InnoDB' COLLATE 'utf8_czech_ci';

ALTER TABLE `log`
ADD `archive_id` int(11) NULL,
ADD `show` tinyint(1) NOT NULL DEFAULT '1' AFTER `archive_id`;

ALTER TABLE `log`
CHANGE `archive_id` `archive_id` int(10) NULL AFTER `usage_meta_id`,
ADD FOREIGN KEY (`archive_id`) REFERENCES `log_archive` (`id`) ON DELETE RESTRICT;