update `log`
set patient_id=422
where data LIKE "%Polívková%";
UPDATE log set patient_id=85
where data REGEXP "Cejpková(.*)Kalium|Cejpková(.*)Furorese|Cejpková(.*)Calcichew|Cejpková(.*)Furon|Cejpková(.*)Allopurinol|Cejpková(.*)Furorese|Cejpková(.*)Helicid|
Cejpková(.*)Rocaltrol|Cejpková(.*)Apo-Amlo";

UPDATE log set patient_id=310 where patient_id IS NULL AND data LIKE "%Cejpková%";

UPDATE log set patient_id=90
WHERE data like "%Minaříková%";

UPDATE log set patient_id=869
WHERE data like "%Nechvátalová%";

UPDATE log set patient_id=472
WHERE data like "%Bejkovská%";

UPDATE log set patient_id=384
WHERE data like "%Tupá%";

UPDATE log set patient_id=477
WHERE data like "%Dočkalová%";

UPDATE log set patient_id=425
WHERE data like "%Poláchová%";

UPDATE log set patient_id=491 WHERE data like "%Missbachová%";


UPDATE log set patient_id=628 WHERE data like "%Kourková%";

UPDATE log set patient_id=642 WHERE data like "%Šustáček%";

UPDATE log set patient_id=622
WHERE data like "%Křikavová%";

UPDATE log set patient_id=485
WHERE data like "%Koudelková%";

UPDATE log set patient_id=436
WHERE data like "%Jochová%";

UPDATE log set patient_id=25
WHERE data like "%Šanderová%";

UPDATE log set patient_id=116
WHERE data like "%Kučerová%";

UPDATE log set patient_id=724
WHERE data like "%Vaverková%";

UPDATE log set patient_id=662
WHERE data like "%Vyroubal%";

UPDATE log set patient_id=800
WHERE data like "%Maláčová%";

UPDATE log set patient_id=94
WHERE data like "%Vlčková%";


UPDATE log set patient_id=184 where data REGEXP "Novotná(.*)Marixino|Novotná(.*)Verospiron|Novotná(.*)Prenessa|Novotná(.*)Letrox|Novotná(.*)Buronil|Novotná(.*)Tiapralan";

UPDATE `log` SET
`id` = '10139',
`date` = '2016-06-08 17:29:14',
`event` = 'patient_edited',
`user_id` = '1',
`data` = 'Uživatel <b>Jan Novák</b> editoval pacienta <b>Věra Novotná</b> do stavu <b>Aktivní</b> a oddělení <b>1</b>',
`usage_meta_id` = NULL,
`deleted` = '0',
`medical_card_usage_meta_id` = NULL,
`patient_id` = '184'
WHERE `id` = '10139';

UPDATE log set patient_id=492 where data LIKE "%Novotná%" AND patient_id IS NULL;

update log set patient_id=279 where data regexp "Vondráková(.*)Betaxa|Vondráková(.*)Milurit|Vondráková(.*)Pentomer|Vondráková(.*)Apo-Ome|Vondráková(.*)Indapamid Stada|Vondráková(.*)Mega Lecithin|Vondráková(.*)Torvacard 10mg|Vondráková(.*)Sertralin actavis|Vondráková(.*)Tiapra";

update log set patient_id=732 where data LIKE "%Vondráková%" AND patient_id IS NULL;

update log set patient_id=332 where data LIKE "%Dvořáková%" AND patient_id IS NULL;

UPDATE log set patient_id=707 where data REGEXP  "Sedláková(.*)Prestarium|Sedláková(.*)Godasal|Sedláková(.*)Swiss";

UPDATE log set patient_id=500 where data LIKE  "%Sedláková%" AND patient_id IS NULL;

UPDATE log set patient_id=723 where data LIKE  "%šulcová%" AND patient_id IS NULL;

UPDATE log set patient_id=916 where data LIKE  "%Maštalířová%" AND patient_id IS NULL;

delete  from log where data LIKE "%karel omacka%";

delete from log where data LIKE "%iří test%";

delete from log where patient_id IS NULL and event in ("card_created", "card_changed") order by date;

delete from log where patient_id IS NULL and data like "%Michal Marek%";

delete from log where patient_id IS NULL and data like "%Aleš Zeman%";

delete from log where patient_id IS NULL and data like "%Info TEST%";

UPDATE `log` SET
`id` = '3',
`date` = '2016-06-16 23:59:07',
`event` = 'patient_edited',
`user_id` = '1',
`data` = 'Uživatel <b>Jan Novák</b> editoval pacienta <b>Jaroslav Potůček</b> do stavu <b>Zesnulý</b> a oddělení <b>II</b>',
`usage_meta_id` = NULL,
`deleted` = '1',
`medical_card_usage_meta_id` = NULL,
`patient_id` = '651'
WHERE `id` = '3';

UPDATE `log` SET
`id` = '96',
`date` = '2016-06-16 23:59:07',
`event` = 'patient_created',
`user_id` = '1',
`data` = 'Uživatel <b>Jan Novák</b> vytvořil pacienta <b>Mišková Erika</b> ve stavu <b>Aktivní</b> a oddělení <b>1C</b>',
`usage_meta_id` = NULL,
`deleted` = '1',
`medical_card_usage_meta_id` = NULL,
`patient_id` = '697'
WHERE `id` = '96';

UPDATE `log` SET
`id` = '1364',
`date` = '2016-06-16 23:59:07',
`event` = 'patient_created',
`user_id` = '24',
`data` = 'Uživatel <b>sestra druhé oddělení</b> vytvořil pacienta <b>NEZBEDOVÁ ANNA</b> ve stavu <b>Aktivní</b> a oddělení <b>II</b>',
`usage_meta_id` = NULL,
`deleted` = '1',
`medical_card_usage_meta_id` = NULL,
`patient_id` = '715'
WHERE `id` = '1364';

update log set patient_id = 725 WHERE patient_id IS NULL and data LIKE "%Vaverka%";

DELETE FROM log where id IN (14) OR data REGEXP "Test EBox|KATALOG - Třebíč";

update log set patient_id = 891 WHERE patient_id IS NULL and data LIKE "%Přiby%";

DELETE from log where patient_id IS NULL LIMIT 51;
