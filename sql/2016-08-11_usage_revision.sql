ALTER TABLE `usage_meta`
ADD `revision` int(11) NULL;

ALTER TABLE `usage_meta`
ADD `revision_source` int(11) NULL;

ALTER TABLE `usage_meta`
ADD INDEX `revision_source` (`revision_source`);

ALTER TABLE `usage_meta`
ADD `deleted` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `usage_meta`
ADD FOREIGN KEY (`revision_source`) REFERENCES `usage_meta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE