CREATE TABLE `order_setting` (
  `id` int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `day_order` tinyint unsigned NOT NULL DEFAULT '0' COMMENT 'day of the week of ddl for order, 0=Mon, 6=Sun',
  `hour_closing` tinyint unsigned NOT NULL DEFAULT '1' COMMENT 'hour of ddl in selected day. 1 = 1am',
  `day_delivery` tinyint unsigned NOT NULL DEFAULT '0' COMMENT 'when the order should be delivered. day of the week',
  `hour_delivery` tinyint unsigned NOT NULL DEFAULT '1',
  `extra_limit` tinyint unsigned NOT NULL DEFAULT '2' COMMENT 'hours as limit. delivery_time-extra_limit = deadline for extra order'
) ENGINE='InnoDB' COLLATE 'utf8_czech_ci';

CREATE TABLE `order` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `branche_id` int(11) NOT NULL,
  `date_send` datetime NULL,
  `is_extra` tinyint(1) NOT NULL DEFAULT '0',
  `state` set('new','send','planned','archived') NOT NULL DEFAULT 'new',
  `order_setting_id` int(10) unsigned NOT NULL,
  FOREIGN KEY (`branche_id`) REFERENCES `branche` (`id`) ON DELETE RESTRICT,
  FOREIGN KEY (`order_setting_id`) REFERENCES `order_setting` (`id`) ON DELETE RESTRICT
) ENGINE='InnoDB' COLLATE 'utf8_czech_ci';


CREATE TABLE `order_item` (
  `id` int(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `patient_id` int(11) NOT NULL,
  `medicine_id` int(11) NULL,
  `package_id` int(11) NULL,
  `packages` int(12) NULL COMMENT 'number of requested pckgs; if the medicine is in the system',
  `medicine_name` varchar(255) COLLATE 'utf8_czech_ci' NULL COMMENT 'if the medicine is NOT in the system',
  `real_amount` int(12) NULL COMMENT 'number of pills, if the medicine is NOT in the system',
  `state` enum('new','accepted','changed','canceled','postponed','deleted') COLLATE 'utf8_czech_ci' NOT NULL DEFAULT 'new',
  `is_urgent` tinyint(1) NOT NULL DEFAULT '0',
  `is_prescription` tinyint(1) NOT NULL DEFAULT '0',
  FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`) ON DELETE RESTRICT,
  FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`id`) ON DELETE RESTRICT,
  FOREIGN KEY (`package_id`) REFERENCES `package` (`id`) ON DELETE RESTRICT
) ENGINE='InnoDB';


CREATE TABLE `order_item_change` (
  `id` int(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `date` datetime NOT NULL,
  `type` enum('comment,medicine_change,postponed,canceled') COLLATE 'utf8_czech_ci' NOT NULL,
  `order_id_from` int(11) NOT NULL,
  `order_id_to` int(11) NOT NULL,
  `medicine_id_from` int(11) NULL,
  `medicine_id_to` int(11) NULL,
  `medicine_name_from` varchar(255) COLLATE 'utf8_czech_ci' NULL,
  `medicine_name_to` varchar(255) COLLATE 'utf8_czech_ci' NULL,
  `user_id` int(11) NOT NULL,
  `comment` text(12) COLLATE 'utf8_czech_ci' NOT NULL,
  FOREIGN KEY (`order_id_from`) REFERENCES `order` (`id`) ON DELETE RESTRICT,
  FOREIGN KEY (`order_id_to`) REFERENCES `order` (`id`) ON DELETE RESTRICT,
  FOREIGN KEY (`medicine_id_from`) REFERENCES `medicine` (`id`) ON DELETE RESTRICT,
  FOREIGN KEY (`medicine_id_to`) REFERENCES `medicine` (`id`) ON DELETE RESTRICT,
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT
) ENGINE='InnoDB';

ALTER TABLE `order_setting`
ADD `branche_id` int(11) NOT NULL AFTER `id`,
CHANGE `hour_closing` `hour_order` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'hour of ddl in selected day. 1 = 1am' AFTER `day_order`,
ADD FOREIGN KEY (`branche_id`) REFERENCES `branche` (`id`) ON DELETE CASCADE;


ALTER TABLE `order`
CHANGE `date_send` `closing_date` datetime NULL COMMENT 'real/absolute closing date. today is Monday 1.9. and setting is Wed -> closing date = 3.9.' AFTER `branche_id`,
CHANGE `state` `state` set('new','sent','closed','archived') COLLATE 'utf8_czech_ci' NOT NULL DEFAULT 'new' AFTER `is_extra`;

ALTER TABLE `order`
ADD `delivery_date` datetime NULL COMMENT 'the same as for closing_date' AFTER `closing_date`;

ALTER TABLE `order_item`
ADD `order_id` int(11) NULL AFTER `id`,
ADD FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE RESTRICT;

ALTER TABLE `order_item`
CHANGE `is_prescription` `target_delivery` enum('branch','pharmacy') COLLATE 'utf8_czech_ci' NOT NULL DEFAULT 'branch' AFTER `is_urgent`;

update order_item set target_delivery='branch' where target_delivery='';

ALTER TABLE `branche` ADD `orders` tinyint(1) NOT NULL AFTER `manage_mc`;

ALTER TABLE `branche`
CHANGE `orders` `orders` tinyint(1) NOT NULL DEFAULT '1' AFTER `manage_mc`;

ALTER TABLE `department`
ADD `orders` tinyint(1) NOT NULL DEFAULT '1';


ALTER TABLE `order_item`
CHANGE `real_amount` `real_amount` varchar(255) COLLATE 'utf8_czech_ci' NULL COMMENT 'number (string) of pills, if the medicine is NOT in the system' AFTER `medicine_name`;

ALTER TABLE `order_item`
ADD `medicine_source` enum('custom','system') COLLATE 'utf8_czech_ci' NOT NULL DEFAULT 'system' AFTER `real_amount`;

ALTER TABLE `order_item`
CHANGE `medicine_name` `custom_medicine_name` varchar(255) COLLATE 'utf8_czech_ci' NULL COMMENT 'if the medicine is NOT in the system' AFTER `packages`,
CHANGE `real_amount` `custom_medicine_amount` varchar(255) COLLATE 'utf8_czech_ci' NULL COMMENT 'number (string) of pills, if the medicine is NOT in the system' AFTER `custom_medicine_name`;

ALTER TABLE `order`
CHANGE `order_setting_id` `order_setting_id` int(10) unsigned NULL AFTER `state`;

ALTER TABLE `order`
  DROP FOREIGN KEY `order_ibfk_2`,
  ADD FOREIGN KEY (`order_setting_id`) REFERENCES `order_setting` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `order`
  ADD `opened_by_admin` tinyint(1) NOT NULL DEFAULT '0' AFTER `delivery_date`;

ALTER TABLE `order_item_change`
  ADD `order_item_id` int(12) NOT NULL AFTER `id`,
  CHANGE `comment` `comment` text COLLATE 'utf8_czech_ci' NOT NULL AFTER `user_id`,
  ADD FOREIGN KEY (`order_item_id`) REFERENCES `order_item` (`id`) ON DELETE CASCADE;

ALTER TABLE `order_item_change`
  CHANGE `order_id_from` `order_id_from` int(11) NULL AFTER `type`,
  CHANGE `order_id_to` `order_id_to` int(11) NULL AFTER `order_id_from`;

ALTER TABLE `order_item_change`
  ADD `medicine_custom_amount_from` varchar(255) COLLATE 'utf8_czech_ci' NULL AFTER `medicine_name_to`,
  ADD `medicine_custom_amount_to` varchar(255) COLLATE 'utf8_czech_ci' NULL AFTER `medicine_custom_amount_from`;

ALTER TABLE `order_item_change`
  CHANGE `type` `type` enum('comment','medicine_change','postponed','canceled') COLLATE 'utf8_czech_ci' NOT NULL AFTER `date`;

ALTER TABLE `order_item_change`
  CHANGE `medicine_custom_amount_from` `medicine_amount_from` varchar(255) COLLATE 'utf8_czech_ci' NULL COMMENT 'used for DB medicines and for custom as well' AFTER `medicine_name_to`,
  CHANGE `medicine_custom_amount_to` `medicine_amount_to` varchar(255) COLLATE 'utf8_czech_ci' NULL AFTER `medicine_amount_from`;

ALTER TABLE `order_item`
  CHANGE `state` `state` enum('new','accepted','changed','canceled','postponed','deleted','archived') COLLATE 'utf8_czech_ci' NOT NULL DEFAULT 'new' COMMENT 'archived = just for history purposes - live is the follower' AFTER `medicine_source`,
  ADD `follower_id` int(10) NULL COMMENT 'follower order_item';

ALTER TABLE `order_item`
  ADD FOREIGN KEY (`follower_id`) REFERENCES `order_item` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `order_item_change`
  CHANGE `type` `type` enum('comment','medicine_change','postponed_from','canceled','postponed_to') COLLATE 'utf8_czech_ci' NOT NULL AFTER `date`;

ALTER TABLE `branche`
  ADD `order_weekend` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'can be order delivered on weekends';

CREATE TABLE `order_note` (
  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `note_type` enum('postpone','cancel','change') NULL,
  `rank` int NOT NULL COMMENT 'like order, just another naming because of order table',
  `note_text` text COLLATE 'utf8_czech_ci' NOT NULL
) ENGINE='InnoDB' COLLATE 'utf8_czech_ci';

INSERT INTO `order_note` (`id`, `note_type`, `rank`, `note_text`) VALUES
  (1,	'cancel',	1,	'Není dostupné na trhu'),
  (2,	'cancel',	2,	'Po konzultaci zrušeno'),
  (3,	'cancel',	3,	'Pacient zemřel'),
  (4,	'postpone',	1,	'Není dostupné na trhu'),
  (5,	'postpone',	2,	'Problém s doručením do domova'),
  (6,	'postpone',	3,	'Není ještě potřeba'),
  (7,	'change',	1,	'Originál není dostupný, nahrazeno'),
  (8,	'change',	2,	'Došlo k chybě při zadání'),
  (9,	'change',	3,	'Nalezena kontraindikace s ostatními léky');