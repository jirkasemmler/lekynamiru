CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) COLLATE 'utf8_czech_ci' NOT NULL,
  `description` text COLLATE 'utf8_czech_ci' NULL
) ENGINE='InnoDB' COLLATE 'utf8_czech_ci';

ALTER TABLE `user`
ADD `role_id` int(11) NULL,
ADD FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE SET NULL;

CREATE TABLE `permission` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) COLLATE 'utf8_czech_ci' NOT NULL,
  `presenter` varchar(255) COLLATE 'utf8_czech_ci' NOT NULL,
  `action` varchar(255) COLLATE 'utf8_czech_ci' NOT NULL,
  `params` text COLLATE 'utf8_czech_ci' NULL,
  `parent_id` int NULL
) ENGINE='InnoDB';

ALTER TABLE `permission`
ADD FOREIGN KEY (`parent_id`) REFERENCES `permission` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

CREATE TABLE `role_permission` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE CASCADE
) ENGINE='InnoDB';

ALTER TABLE `role_permission`
ADD `params` text COLLATE 'utf8_czech_ci' NULL;
ALTER TABLE `permission`
ADD `do` varchar(255) COLLATE 'utf8_czech_ci' NOT NULL AFTER `action`;

ALTER TABLE `permission`
CHANGE `do` `do` varchar(255) COLLATE 'utf8_czech_ci' NULL AFTER `action`;

alter table permission
	add data_order int null;
