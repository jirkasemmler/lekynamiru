
CREATE TABLE `usage_stats` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `department_id` int(11) NOT NULL,
  `medicine_id` int(11) NOT NULL,
  `half` int NOT NULL,
  `quater` int NOT NULL,
  `third` int NOT NULL,
  `full` int NOT NULL,
  `week_delta` int NOT NULL,
  FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`id`) ON DELETE CASCADE
) ENGINE='InnoDB' COLLATE 'utf8_czech_ci';

