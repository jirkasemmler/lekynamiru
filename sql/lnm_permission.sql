INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (1, 'Léky - vyberte alespoň jednu sekci', 'medicine', '#', null, null, null, 1);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (15, 'Domovy', 'branche', 'default', null, null, null, 2);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (28, 'Pacienti', 'patient', 'default', null, null, null, 3);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (41, 'Mrtví pacienti', 'patient', 'dead', null, null, null, 4);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (42, 'Stavy', 'patient', 'prescription', null, null, null, 5);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (43, 'Report', 'report', 'default', null, null, null, 6);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (44, 'Spotřeba', 'usage', 'default', null, null, null, 7);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (45, 'Objednávky', 'order', 'default', null, null, null, 8);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (46, 'Systém - vyberte alespoň jednu sekci', 'settings', '#', null, null, null, 9);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (3, 'Editovat lék', 'medicine', 'default', 'ManageMedicine', 'id', 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (4, 'Smazat lék', 'medicine', 'default', 'DelMedicine', 'id', 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (5, 'Exportovat léky', 'api', 'medicine', null, null, 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (6, 'Kanystry', 'medicine', 'cans', null, null, 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (7, 'Přidat kanystr', 'medicine', 'cans', 'ManageCan', null, 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (8, 'Editovat kanystr', 'medicine', 'cans', 'ManageCan', 'id', 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (9, 'Smazat kanystr', 'medicine', 'cans', 'DelCan', 'id', 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (10, 'Exportovat kanystry', 'api', 'can', null, null, 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (11, 'Odbornosti', 'medicine', 'speciality', null, null, 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (12, 'Přidat odbornost', 'medicine', 'speciality', 'ManageSpeciality', null, 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (13, 'Editovat odbornost', 'medicine', 'speciality', 'ManageSpeciality', 'id', 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (14, 'Smazat odbornost', 'medicine', 'speciality', 'DelSpeciality', 'id', 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (53, 'Léky
', 'medicine', 'default', null, null, 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (70, 'Přidat lék', 'medicine', 'default', 'ManageMedicine', null, 1, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (17, 'Exportovat domovy', 'api', 'branche', '', null, 15, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (18, 'Přidat domov', 'branche', 'default', 'ManageBranche', null, 15, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (19, 'Smazat domov', 'branche', 'default', 'DelBranche', 'id', 15, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (20, 'Editovat domov', 'branche', 'default', 'ManageBranche', 'id', 15, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (21, 'Přidat oddělení', 'branche', 'default', 'ManageDepartment', 'idBranche', 15, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (22, 'Editovat oddělení', 'branche', 'default', 'ManageDepartment', 'idBranche,idDepartment', 15, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (23, 'Smazat oddělení', 'branche', 'default', 'DelDepartment', 'id', 15, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (24, 'Editovat odbornosti', 'branche', 'default', 'ManageSpec', 'id', 15, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (25, 'Přidat doručovací lhůtu', 'branche', 'default', 'AddOrderSetting', 'id', 15, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (26, 'Editovat doručovací lhůtu', 'branche', 'default', 'AddOrderSetting', 'id,orderSettingId', 15, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (27, 'Smazat doručovací lhůtu', 'branche', 'default', 'DelOrderSetting', 'id', 15, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (29, 'Export pacienty', 'api', 'patients', '', null, 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (30, 'Přidat pacienta', 'patient', 'default', 'ManagePatient', null, 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (31, 'Editovat', 'patient', 'default', 'ManagePatient', 'id', 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (32, 'Smazat', 'patient', 'default', 'DelPatient', 'id', 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (33, 'Zobrazit RČ', 'patient', 'default', 'show_rc', null, 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (34, 'Naplánovat změnu plánu', 'patient', 'default', 'Plan', 'id', 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (35, 'Dávkově přidat tablety', 'patient', 'default', 'MultiPills', 'id', 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (36, 'Zobrazit medikaci', 'patient', 'default', 'ShowCard', 'id', 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (37, 'Přidat medikaci', 'patient', 'default', 'AddToCard', 'id', 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (38, 'Editovat medikaci', 'patient', 'default', 'AddToCard', 'id,id_card', 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (39, 'Smazat medikaci', 'patient', 'default', 'DelUsage', 'id_usage', 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (40, 'Exportovat MK', 'api', 'm-k', '', null, 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (77, 'Přepínat domovy', 'login', 'changeBranche', '', 'id', 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (78, 'Nastavovat stav medikační karty', 'Patient', 'default', 'changeNumber', 'changeNumber', 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (64, 'Přidat tablety', 'patient', 'prescription', 'AddPills', 'id_usage', 42, 99);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (65, 'Expotovat stavy', 'api', 'prescription', '', null, 42, 99);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (66, 'Přepočítat stavy', 'patient', 'prescription', 'changeBatchingRange', 'from,to', 42, 99);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (57, 'Změnit domov', 'usage', 'default', 'changeUsage', 'branche', 44, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (67, 'Exportovat spotřebu', 'api', 'usage', '', null, 44, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (68, 'Exportovat výrobu', 'api', 'production', '', null, 44, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (69, 'Změnit týden', 'usage', 'default', 'changeUsage', 'branche,period', 44, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (71, 'Změnit oddělení', 'usage', 'default', 'changeUsage', 'branche,department', 44, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (47, 'Uživatelé', 'user', 'default', null, null, 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (48, 'Historie', 'log', 'default', null, null, 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (49, 'Nastavení', 'log', 'setting', null, null, 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (50, 'Nastavení exportu Eureky', 'log', 'eurekaExport', null, null, 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (51, 'Role', 'role', 'default', null, null, 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (52, 'Oprávnění', 'permission', 'default', null, '', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (54, 'Uložit oprávnění', 'role', 'savePermission', null, 'id,data', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (55, 'Nastavit oprávnění role', 'role', 'permissions', null, 'id', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (56, 'Získat oprávnění', 'role', 'permissionsForRole', null, 'id', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (58, 'Přidat uživatele', 'user', 'default', 'ManageUser', '', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (59, 'Editovat uživatele', 'user', 'default', 'ManageUser', 'id', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (60, 'Smazat uživatele', 'user', 'default', 'DelUser', 'id', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (62, 'Nastavit hodnotu nastavení', 'log', 'setting', 'Edit', 'id', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (63, 'Oprávnění', 'permission', 'getPermissionData', '', '', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (72, 'Editovat roli', 'role', 'default', 'ManageRole', 'id', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (73, 'Přidat roli', 'role', 'default', 'ManageRole', '', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (74, 'Smazat roli', 'role', 'default', 'DelRole', 'id', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (75, 'Změnit rozsah historie', 'log', 'default', 'changeRange', 'from,to', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (76, 'Exportovat historii', 'api', 'history', null, 'from,to', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (79, 'Zobrazit historii medikace', 'patient', 'default', 'ShowCard', 'id,mc_id', 28, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (80, 'Přístup do všech domovů', 'api', 'admin',null, null, 46, 999);

INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (81, 'Vidět pojišťovnu v XLS stavech', 'api', 'patient', 'prescription', 'show_ins', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (82, 'Vidět datum narození v XLS stavech', 'api', 'patient', 'prescription', 'show_bd', 46, 999);

INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (83, 'Exportovat všechny pacienty', 'api', 'patient-data',null, 'what', 46, 999);

INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (84, 'Vidět pojišťovnu v XLS mailovém přehledu léků', 'api', 'mailer', 'usage', 'show_ins', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (85, 'Vidět datum narození v XLS mailovém přehledu léků', 'api', 'mailer', 'usage', 'show_bd', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (86, 'Dostávat kompletní přehledy stavů celé aplikace', 'api', 'full-export', null, null, 46, 999);

INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (87, 'Spravovat urgentní', 'order', 'default', 'ManageOrderItem', 'orderId,orderItemId,dateTime,urgent', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (88, 'Spravovat', 'order', 'default', 'ManageOrderItem', 'orderId,orderItemId,dateTime', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (89, 'Sumarizovat', 'order', 'default', 'SummarizeOrder', 'orderId', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (90, 'Odeslat extra', 'order', 'sendOrder', null, 'orderId,isAndSetAsExtra', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (91, 'Odeslat', 'order', 'sendOrder', null, 'orderId', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (92, 'Smazat položku', 'order', 'default', 'DelOrderItem','orderItemId', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (93, 'Spracovat položku a změnit stav', 'order', 'default', 'ProceedOrderItem', 'id,orderItemId,operation', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (94, 'Schválit položku', 'order', 'default', 'ApproveOrderItem', 'id,orderItemId', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (95, 'Změnit stav', 'order', 'default', 'ChangeOrderState','[id[,orderId,newStatus', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (96, 'Změnit stav - dialog', 'order', 'default', 'ChangeOrderStateOpenDialog','[id],orderId,newStatus', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (97, 'Uzavřít', 'order', 'default', 'CloseOrder','orderId', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (98, 'Extra objednávka - dialog', 'order', 'default', 'ExtraOrderOpenDialog',null, 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (99, 'Vytvořit extra objednávku', 'order', 'default', 'CreateExtraOrder',null, 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (100, 'Smazat extra objednávku', 'order', 'default', 'DelExtraOrder','orderId', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (101, 'Zobrazit změny', 'order', 'default', 'ShowChanges','id,orderItemId', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (103, 'Objednávky domovů', 'order', 'default', null,'id', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (102, 'Archivované objednávky', 'order', 'default', null,'archived', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (104, 'Spracovat položku', 'order', 'default', 'ProceedOrderItem', 'id,orderItemId', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (105, 'Soubory', 'order', 'default', 'OrderFile', '[id],orderId', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (106, 'Smazat soubor', 'order', 'default', 'DelOrderFile', '[id],orderItem', 45, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (107, 'Stahnout soubor', 'api', 'orderFile', null, 'id', 45, 999);

INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (108, 'Vidět rodné číslo v XLS stavech', 'api', 'patient', 'prescription', 'show_rc', 46, 999);
INSERT INTO permission (id, name, presenter, action, do, params, parent_id, data_order) VALUES (109, 'Vidět rodné číslo v XLS mailovém přehledu léků', 'api', 'mailer', 'usage', 'show_rc', 46, 999);


#
# ManageOrderItem('orderId,orderItemId,dateTime,urgent') {
# ManageOrderItem('orderId', 'orderItemId', 'dateTime') {
# SummarizeOrder('orderId') {
# SendOrder('orderId,isAndSetAsExtra') {
# SendOrder('orderId') {
# DelOrderItem('orderItemId) {
# ProceedOrderItem('orderItemId', 'operation = "approve") {
# ApproveOrderItem('orderItemId) {
# ChangeOrderState('orderId', 'newStatus) {
# ChangeOrderStateOpenDialog('orderId', 'newStatus) {
# CloseOrder('orderId) {
# ExtraOrderOpenDialog() {
# CreateExtraOrder() {
# DelExtraOrder('orderId) {
# ShowChanges('orderItemId) {
