ALTER TABLE `log`
ADD `patient_id` int(11) NULL,
ADD FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`) ON DELETE CASCADE;

ALTER TABLE `log`
CHANGE `date` `date` timestamp NULL AFTER `id`;



INSERT INTO `settings` (`key`, `value`)
VALUES ('staging', 'no');

INSERT INTO `settings` (`key`, `value`)
VALUES ('test_email', 'test@lekynamiru.cz');

-- AFTER EVERYTHING DONE

ALTER TABLE `log`
CHANGE `date` `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP AFTER `id`;