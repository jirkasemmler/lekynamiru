ALTER TABLE `log`
ADD `medical_card_usage_meta_id` int(11) NULL,
ADD FOREIGN KEY (`medical_card_usage_meta_id`) REFERENCES `medical_card_usage_meta` (`id`) ON DELETE CASCADE;