CREATE TABLE `medical_card` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `patient_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text COLLATE 'utf8_czech_ci' NOT NULL,
  FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE='InnoDB' COLLATE 'utf8_czech_ci';



-- ALTER TABLE `usage_meta`
-- DROP `revision`;

ALTER TABLE `usage_meta`
ADD `active` tinyint(1) NOT NULL DEFAULT '1';

ALTER TABLE `medical_card`
ADD `revision_number` int(11) NOT NULL;

CREATE TABLE `medical_card_usage_meta` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `usage_meta_id` int(11) NOT NULL,
  `medical_card_id` int(11) NOT NULL,
  FOREIGN KEY (`usage_meta_id`) REFERENCES `usage_meta` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`medical_card_id`) REFERENCES `medical_card` (`id`) ON DELETE CASCADE
) ENGINE='InnoDB' COLLATE 'utf8_czech_ci';

ALTER TABLE `medical_card`
ADD `active` tinyint(1) NOT NULL DEFAULT '1';