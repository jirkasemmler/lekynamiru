CREATE TABLE `order_file` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_order` int(11) NOT NULL,
  `hash` varchar(255) COLLATE 'utf16_czech_ci' NOT NULL,
  `filename` varchar(255) COLLATE 'utf16_czech_ci' NOT NULL,
  `title` varchar(255) COLLATE 'utf8_czech_ci' NULL,
  `created_at` datetime NULL,
  FOREIGN KEY (`id_order`) REFERENCES `order` (`id`)
) ENGINE='InnoDB' COLLATE 'utf16_czech_ci';