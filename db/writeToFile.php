<?php
if(php_sapi_name() == "cli" || $_GET["token"] == "dbtoken")
{

	$file = file_get_contents(realpath(dirname(__FILE__))."/config.json");
	$data = json_decode($file);
	set_time_limit(0);
	ignore_user_abort(TRUE);
	require __DIR__ . '/MySQLDump.php';
	$time = -microtime(TRUE);
	$dump = new MySQLDump(new mysqli($data->server, $data->user, $data->pass , $data->db));
	$dump->save(realpath(dirname(__FILE__)).'/dump/dump_' . date('Y-m-d_H-i-s') . '.sql.gz');
	$time += microtime(TRUE);
	echo "FINISHED (in $time s)";
}
else
{
	echo "fail";
	exit();
}