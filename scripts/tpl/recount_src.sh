#/bin/bash
echo "

"

echo -n " ---- Recount script staring  ----   "
date +%D' '%H:%M:%S
echo ""

echo "
DB backup before: START"
php7.3 PATH/db/writeToFile.php
if [ $? -eq 0 ] ; then
	echo "
DB backup before: DONE"
else
	echo "
DB backup before: FAILED"
fi


echo "
Recount PREPARE: START"
php7.3 PATH/www/index.php Api:prepare
if [ $? -eq 0 ] ; then
	echo "Recount PREPARE: DONE"
else
	echo "Recount PREPARE: FAILED"
fi

echo "
Recount change-status: START"
php7.3  -d max_execution_time=900 PATH/www/index.php Api:changeStatus
if [ $? -eq 0 ] ; then
	echo "Recount change-status: DONE"
else
	echo "Recount change-status: FAILED"
fi


echo "
Recount non-active: START"
php7.3  -d max_execution_time=900 PATH/www/index.php Api:nonActive
if [ $? -eq 0 ] ; then
	echo "Recount non-active: DONE"
else
	echo "Recount non-active: FAILED"
fi



echo "
Recount change-week: START"
php7.3  -d max_execution_time=1300 PATH/www/index.php Api:changeWeek
if [ $? -eq 0 ] ; then
	echo "Recount change-week: DONE"
else
	echo "Recount change-week: FAILED"
fi

echo "
Recount recount: START"
php7.3 -d max_execution_time=2000 PATH/www/index.php Api:recount
if [ $? -eq 0 ] ; then
	echo "Recount recount: DONE"
else
	echo "Recount recount: FAILED"
fi

echo "
Recount mailer: START"
php7.3 -d max_execution_time=1300 PATH/www/index.php Api:mailer
if [ $? -eq 0 ] ; then
	echo "Recount mailer: DONE"
else
	echo "Recount mailer: FAILED"
fi


echo "
Recount usage-stat: START"
php7.3 -d max_execution_time=2000 PATH/www/index.php Api:usageStat
if [ $? -eq 0 ] ; then
	echo "Recount usage-stat: DONE"
else
	echo "Recount usage-stat: FAILED"
fi




echo "
Recount check: START"
php7.3 PATH/www/index.php Api:check
if [ $? -eq 0 ] ; then
	echo "Recount check: DONE"
else
	echo "Recount check: FAILED"
fi

### ORDERS

#echo "
#Orders : START"
#php7.3 PATH/www/index.php Api:Orders
#if [ $? -eq 0 ] ; then
#	echo "Orders: DONE"
#else
#	echo "Orders: FAILED"
#fi

### END ORDERS




echo "
DB backup AFTER: START"
php7.3 PATH/db/writeToFile.php
if [ $? -eq 0 ] ; then
	echo "
DB backup AFTER: DONE"
else
	echo "
DB backup AFTER: FAILED"
fi

echo "
Send DB: START"
php7.3 PATH/www/index.php Api:sendDB
if [ $? -eq 0 ] ; then
	echo "Send DB: DONE"
else
	echo "Send DB: FAILED"
fi

echo " ------ Recount script FINISHING ----

"