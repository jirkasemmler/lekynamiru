#/bin/bash

echo "
Check orders to archive: START"
php PATH/www/index.php Api:archiveOrders
if [ $? -eq 0 ] ; then
	echo "ArchiveOrders: DONE"
else
	echo "ArchiveOrders: FAILED"
fi
