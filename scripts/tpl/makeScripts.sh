#!/usr/bin/env bash
cp recount_src.sh ../recount.sh
cp start_src.sh ../startRecount.sh
cp send_orders_src.sh ../sendOrders.sh
cp archive_orders_src.sh ../archiveOrders.sh
SCRIPTPATH=`cat path`
sed -i 's/PATH/'"$SCRIPTPATH"'/g' ../recount.sh
sed -i 's/PATH/'"$SCRIPTPATH"'/g' ../startRecount.sh
sed -i 's/PATH/'"$SCRIPTPATH"'/g' ../sendOrders.sh
sed -i 's/PATH/'"$SCRIPTPATH"'/g' ../archiveOrders.sh