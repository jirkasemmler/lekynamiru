#!/usr/bin/env bash

sh PATH/scripts/recount.sh >> PATH/reports/`date +%Y-%m-%d`-report.log
if [ $? -eq 0 ] ; then
	echo "ok"
else
	echo "failed"
fi
