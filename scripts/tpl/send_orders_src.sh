#/bin/bash

echo "
Check orders to send: START"
php7.3 PATH/www/index.php Api:sendOrders
if [ $? -eq 0 ] ; then
	echo "SendOrders: DONE"
else
	echo "SendOrders: FAILED"
fi
