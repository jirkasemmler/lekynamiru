#!/usr/bin/env bash

zcat /var/log/apache2/lekynamiru.cz/access*gz > /tmp/tmp.log
cat /var/log/apache2/lekynamiru.cz/access-lekynamiru.cz.log* >> /tmp/tmp.log

goaccess /tmp/tmp.log  --config-file=/etc/goaccess_custom.conf -o  /www/hosting/lekynamiru.cz/www/lekynamiru/www/debug/report.html

rm /tmp/tmp.log