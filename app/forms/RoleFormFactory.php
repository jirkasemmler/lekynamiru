<?php

namespace App\Forms;


use App\Model\Entities\Role;
use App\Model\Repositories\RoleRepository;
use Instante\Bootstrap3Renderer\BootstrapRenderer;
use Nette\Application\UI\Form;

class RoleFormFactory
{
    /**
     * @var RoleRepository $roleRepository
     */
    protected $roleRepository;

    public function __construct(RoleRepository $r)
    {
        $this->roleRepository = $r;
    }

    public function createRole($id = null): Form
    {
        if (isset($_POST["id"])) {
            $id = $_POST["id"];
        }


        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());
        $form->addHidden("id", $id);

        $form->addText('name', 'Název');
        $form->addText('description', 'Popis');
        $form->addCheckbox('send_notifications', 'Zasílat notifikace');
        $form->addSubmit('send', 'Odeslat')
             ->setAttribute("class", "btn btn-success closing");


        if ($id) {
            /**
             * @var $role Role
             */
            $role = $this->roleRepository->getByID($id);
            $form->setDefaults(['name' => $role->name, 'description' => $role->description, 'send_notifications' => $role->sendNotifications]);
        }

        return $form;
    }
}
