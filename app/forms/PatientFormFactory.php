<?php

namespace App\Forms;


use App\Model\Entities\Usage;
use App\Model\Repositories\PatientRepository;
use Instante\Bootstrap3Renderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\BrancheRepository;
use App\Model\Repositories\MedicineRepository;
use App\Model\Repositories\UsageRepository;

class PatientFormFactory
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository,
                                PatientRepository $patientRepository,
                                BrancheRepository $brancheRepository,
                                UsageRepository $usageRepository,
                                MedicineRepository $medicineRepository)
    {
        $this->userRepository = $userRepository;
        $this->usageRepository = $usageRepository;
        $this->brancheRepository = $brancheRepository;
        $this->patientRepository = $patientRepository;
        $this->medicineRepository = $medicineRepository;
    }

    /**
     * form for login user to system
     *
     * @param int  $id
     *
     * @return Form
     */
    public function create($id = 0)
    {
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());
        $form->addHidden("id", $id);

        $form->addText('firstName', 'Jméno:')
             ->setAttribute("class", "medium-text")
             ->setRequired('Zadejte křestní jméno.');

        $form->addText('lastName', 'Příjmení:')
             ->setAttribute("class", "medium-text")
             ->setRequired('Zadejte příjmení.');

        $form->addText('rc', 'RČ:')
             ->setAttribute("class", "medium-text rc")
             ->addRule(Form::MIN_LENGTH, 'RČ musí mít minimálně 10 znaků', '10')
             ->setRequired('Zadejte rodné číslo ve validním tvaru bez lomítka.');

        $form->addText('pojistovna', 'Pojišťovna:')
             ->setAttribute("class", "medium-text")
             ->addCondition(Form::LENGTH, 3)// pak bude muset obsahovat číslici
             ->addRule(Form::PATTERN, 'Musí být složeno z tří číslic', '[0-9]{3}')
             ->setRequired('Zadejte tříčíselný kod pojišťovny.');

        $form->addText('birthDate', 'Datum narození:')
             ->setAttribute("class", "medium-text")
             ->setAttribute("readonly", "readonly");

        $form->addSelect("department", "Oddělení", $this->brancheRepository->getDepartments())
             ->setAttribute("class", "medium-text");
        $state_ar = [
            "running" => "Aktivní",
            "sleeping" => "Pasivní",
            "stop" => "Zcela pozastaveno",
            "dead" => "Zesnulý/á",
        ];
        $form->addSelect("state", "Stav", $state_ar)
             ->setAttribute("class", "medium-text");

        if ($id != 0) {
            $patient = $this->patientRepository->getByID($id);
            $date = date_create_from_format("Y-m-d H:i:s", $patient->birthDate);

            if ($date != false) {
                $date_form = $date->format("d.m.Y");
            } else {
                $date_form = "-.-.----";
            }
            $defaults = [
                "firstName" => $patient->firstName,
                "lastName" => $patient->lastName,
                "rc" => $patient->rc,
                "pojistovna" => $patient->pojistovna,
                "birthDate" => $date_form,
                "state" => $patient->state,
                "id" => $patient->id,
            ];


            if ($patient->departmentId != null) {
                $defaults["department"] = $patient->departmentId;
            }
            $form->setDefaults($defaults);
        }
        $form->addSubmit('send', 'Odeslat')
             ->setAttribute("class", "btn btn-success closing");
        return $form;
    }

    public function createPlan($id_patient)
    {
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());
        $form->getElementPrototype()
             ->class('ajax');


        $form->addHidden("id_patient", $id_patient);
        $form->addText("date", "Datum")
             ->setAttribute("class", "medium-text datepicker")
             ->setAttribute("data-date-autoclose", "true");

        $state_ar = [
            "running" => "Aktivní",
            "sleeping" => "Pasivní",
            "stop" => "Zcela pozastaveno",
            "dead" => "Zesnulý/á",
        ];
        $form->addSelect("target", "Stav", $state_ar)
             ->setAttribute("class", "medium-text");
        $form->addSubmit('send', 'Odeslat')
             ->setAttribute("class", "btn btn-success closing");

        return $form;
    }

    public function createCard($id_patient, $id_card = 0, $canChaneState = true)
    {
        $form = new Form;
        $form->getElementPrototype()
             ->class('ajax');

        $form->setRenderer(new BootstrapRenderer());
        $form->addHidden("id_usage", $id_card);
        $form->addHidden("id_patient", $id_patient);
        $form->addText("showTwoWeeks", 0)
             ->setAttribute("class", "hidden")
             ->setAttribute("id", "showTwoWeeks")
             ->setDefaultValue("0");

        if ($canChaneState) {
            $form->addText("stav", 0)
                 ->setRequired(false)
                 ->setAttribute("class", "stav miniFormInputField medium-text form-control")
                 ->setDefaultValue("0")
                 ->addRule(Form::FLOAT, 'Stav musí být celé nebo desetinné (oddělené tečkou) číslo');
            //musi byt text aby se do nej dalo zapisovat pres JS
        }

        $leky = $this->medicineRepository->getCardMedicine($id_patient);
        //ziskam leky ve tvaru [ "<specializace>-<ID leku>" => <nazev>

        $weeks = [2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12];
        $form->addSelect("weeks", "Týdnů", $weeks)
             ->setDefaultValue(2)
             ->setAttribute("class", "miniFormInputField weeks-select form-control")
             ->setAttribute("onchange", "changeWeek('druhy');");

        $possibleActiveWeeks = [
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
            6 => 6,
            7 => 7,
            8 => 8,
            9 => 9,
            10 => 10,
            11 => 11,
            12 => 12,
        ];
        $form->addSelect("activeWeekSelect", "Akt. týden", $possibleActiveWeeks)
             ->setDefaultValue(1)
             ->setAttribute("class", "activeWeekSelect miniFormInputField form-control")
             ->setAttribute('onchange', 'removeActiveWeekAlert();');

        if ($id_card != 0) {
            $defaultLek = $this->usageRepository->getDefMedicine($id_card);
            // uz vybrany (definovany) lek ve tvaru [<speciality>, "<speciality>-<ID-leku>", <nazev>]
            $leky[$defaultLek[0]][$defaultLek[1]] = $defaultLek[2];
            $form->addSelect("medicineSelect", "Lék", $leky)
                 ->setAttribute("class", "chosen-select medicine-select")
                 ->setAttribute("data-placeholder", "Vyberte lék...");

            $usage = $this->usageRepository->getByID($id_card);
            $activeWeekIndex = array_search($usage->activeWeek, Usage::$weeks) + 1;
            $form->setDefaults([
                                   "medicineSelect" => $defaultLek[1],
                                   "stav" => $usage->number,
                                   "weeks" => $usage->weeks,
                                   "activeWeekSelect" => $activeWeekIndex,
                               ]);
            $data = $this->usageRepository->getOneUsage($id_card); // jednotlive spotreby
            $comment = "Úprava medikace - " . $defaultLek[2] . "";
        } else {
            $form->addSelect("medicineSelect", "Lék", $leky)
                 ->setAttribute("class", "chosen-select medicine-select")
                 ->setAttribute("data-placeholder", "Vyberte lék...")
                 ->setAttribute("onchange", "newMCcomment(this);");
            $comment = "Zadání nové medikace:";
        }

        $davky = ["morning", "noon", "evening", "night"];
        $dny = [
            "mon" => "Pondělí",
            "tue" => "Úterý",
            "wed" => "Středa",
            "thu" => "Čtvrtek",
            "fri" => "Pátek",
            "sat" => "Sobota",
            "sun" => "Neděle",
        ];

        $sd_ar = [
            'first',
            'second',
            'third',
            'fourth',
            'fifth',
            'sixth',
            'seventh',
            'eighth',
            'ninth',
            'tenth',
            'eleventh',
            'twelfth',
            'zarazka',
        ];
        $sd_count = 0; //pro even-odd

        foreach ($sd_ar as $tyden) {
            foreach ($dny as $denK => $denV) {
                foreach ($davky as $davka) {
                    if ($id_card == 0 || !isset($data[$tyden][$denK][$davka])) {
                        $def = 0; //default pocet leku
                    } else {
                        $def = $data[$tyden][$denK][$davka];
                    }

                    $form->addText($denK . "0" . $davka . "0" . $tyden)
                         ->setAttribute("id", $denK . "0" . $davka . "0" . $tyden)
                         ->setAttribute("class", "input-number " . $davka . " " . $denK . " " . $sd_ar[$sd_count % 2])
                         ->setAttribute("data-sl", $sd_ar[$sd_count % 2])
                         ->setAttribute("data-den", $denK)
                         ->setAttribute("data-davka", $davka)
                         ->setAttribute("readonly", "readonly")
                         ->setDefaultValue($def);
                }
                $sd_count++;
            }
        }

        $form->addText("comment", "Komentář")
             ->setDefaultValue($comment)
             ->setAttribute("class", " medium-text form-control comment-input")
             ->setAttribute("data-bcktitle", $comment);
        $form->addSubmit('send', 'Odeslat')
             ->setAttribute("class", "btn btn-success closing");

        return $form;
    }

    public function createPills($id_usage)
    {

        if ($id_usage == null) {
            $id_usage = $_POST["id_usage"];
        }

        $usage = $this->usageRepository->getByID($id_usage);
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());
        //$form->getElementPrototype()
        //     ->class('ajax');
        $form->addHidden("id_usage", $id_usage);

        $form->addText('pills', "")
             ->setAttribute("size", "5")
             ->setAttribute("class", "mini-text form-control small-chosen")
             ->setRequired('Zadejte počet tablet.')
             ->setDefaultValue(0)
             ->addRule(Form::INTEGER, 'Počet tablet musí být číslo (i záporné)');

        $form->addText('pckgs', "")
             ->setAttribute("size", "5")
             ->setAttribute("class", "mini-text form-control small-chosen")
             ->setDefaultValue(0)
             ->setRequired('Zadejte počet balení.')
             ->addRule(Form::INTEGER, 'Počet balení musí být číslo (i záporné)');

        $pckgs = $this->medicineRepository->getPackagesArray($usage->medicineId);

        $form->addSelect("pcksize", "", $pckgs)
             ->setAttribute("class", "form-control mini-select small-chosen");

        $form->addSubmit('send', 'Odeslat')
             ->setAttribute("class", "btn btn-success closing");
        return $form;
    }

    public function createMultiPills($id_patient)
    {
        if ($id_patient == null) {
            $id_patient = $_POST["id_patient"];
        }

        $form = new Form;
        $form->getElementPrototype()
             ->class('ajax');
        $form->setRenderer(new BootstrapRenderer());
        $form->addHidden("id_patient", $id_patient);

        $usages = $this->usageRepository->findByPatientId($id_patient);
        $pills = $form->addContainer("pills");
        foreach ($usages as $usage) {
            $tmp = $pills->addContainer($usage->id);

            $tmp->addText("medicine_name")
                ->setDisabled()
                ->setDefaultValue($usage->getMedicine()->name)
                ->setAttribute("class", "fakeInput");

            $tmp->addText("number")
                ->setDisabled()
                ->setDefaultValue($usage->number)
                ->setAttribute("class", "mini-text fakeInput number");

            $tmp->addHidden("id_usage", $usage->id);

            $tmp->addText('pills', "")
                ->setAttribute("size", "5")
                ->setAttribute("class", "mini-text form-control small-chosen")
                ->setRequired('Zadejte počet tablet.')
                ->setDefaultValue(0)
                ->addRule(Form::INTEGER, 'Počet tablet musí být číslo (i záporné)');

            $tmp->addText('pckgs', "")
                ->setAttribute("size", "5")
                ->setAttribute("class", "mini-text form-control small-chosen")
                ->setDefaultValue(0)
                ->setRequired('Zadejte počet balení.')
                ->addRule(Form::INTEGER, 'Počet balení musí být číslo (i záporné)');

            $pckgs = $this->medicineRepository->getPackagesArray($usage->medicineId);

            $tmp->addSelect("pcksize", "", $pckgs)
                ->setAttribute("class", "form-control mini-select small-chosen");
        }

        $form->addSubmit("submit", "Odeslat")
             ->setAttribute("class", "btn btn-success closing");

        return $form;
    }
}
