<?php

namespace App\Forms;

use App\Model\Repositories\CanRepository;
use App\Model\Repositories\MedicineRepository;
use App\Model\Repositories\SpecialityRepository;
use App\Model\Repositories\UsageRepository;
use Instante\Bootstrap3Renderer\BootstrapRenderer;
use Nette\Application\UI\Form;

class MedicineFormFactory {


    protected $canRepository;
    protected $medicineRepository;
    protected $specialityRepository;
    protected $usageRepository;

    public function __construct(
        CanRepository $canRepository,
        MedicineRepository $medicineRepository,
        SpecialityRepository $specialityRepository,
        UsageRepository $usageRepository
    ) {
        $this->canRepository = $canRepository;
        $this->specialityRepository = $specialityRepository;
        $this->medicineRepository = $medicineRepository;
        $this->usageRepository = $usageRepository;
    }

    /**
     * form for login user to system
     * @return Form
     */
    public function create($id) {
        if (isset($_POST["id"])) {
            $id = $_POST["id"];
        }
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());
        $form->addHidden("id", $id);

        $form->addText('name', 'Název:')
            ->setAttribute("class", "medium-text")
            ->setRequired('Zadejte název léku.');

        $form->addText('sukl', 'SUKL:')
            ->setAttribute("class", "medium-text")
            ->setRequired('Zadejte SUKL kód léku.')
            ->addRule(Form::PATTERN, 'Musí být složen z číslic', '[0-9]*');
        $form->addSelect("can", "Kanystr", $this->canRepository->getCansWithMedicines())
            ->setAttribute("class", "medium-text");

        $form->addCheckbox("dangerous", "Nebezpečný");

        $form->addCheckboxList('specialities', 'Odbornosti:',
            $this->specialityRepository->getAll(true))->setDefaultValue($this->medicineRepository->getSpecialites($id));

        $i = 1; //start counting for "volba"

        if ($id != 0) {
            $packages = $this->medicineRepository->getPackages($id);
            foreach ($packages as $package) //for all the options...
            {
                if ($i == 1) {
                    $label = "Balení";
                } else {
                    $label = "";
                }
                $form->addText("options" . $i, $label)
                    ->setAttribute("class", "medium-text")
                    ->setAttribute("placeholder", "Balení " . $i)
                    ->setDefaultValue($package->size)
                    ->setAttribute("data-num", $i);
                $i++;
            }
            $form->addText("options" . $i)
                ->setAttribute("class", "hiddenOption medium-text")
                ->setAttribute("placeholder", "Balení " . $i)
                ->setAttribute("data-num", $i);
        } else //it is new att -> than show the first option
        {
            $form->addText("options1", "Balení")
                ->setAttribute("class", "hiddenOption medium-text")
                ->setAttribute("placeholder", "Balení 1")
                ->setAttribute("data-num", 1);
        }

        $form->addButton("next_option", "Další")
            ->setAttribute("class", "hiddenOption")
            ->setAttribute("id", "next_option")
            ->setAttribute("data-num", $i);

        if ($id != 0) {
            $data = $this->medicineRepository->getByID($id);
            $form->setDefaults(array(
                "name" => $data->name,
                "sukl" => $data->sukl,
                "id" => $data->id,
                "can" => $data->getCan(true),
                "dangerous" => $data->dangerous
            ));
        }

        $form->addSubmit('send', 'Odeslat')
            ->setAttribute("class", "btn btn-success closing");
        return $form;
    }

    public function createCan($id) {

        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());
        $form->getElementPrototype()->class('ajax');
        $form->addHidden("id", $id);
        $form->addText('name', 'Název:')
            ->setAttribute("class", "medium-text")
            ->setRequired('Zadejte název kanystru.');
        $form->addSelect("medicine", "Lék", $this->medicineRepository->getMedicinesWithCans())->setAttribute("class",
            "chosen-select medicine-select medium-text")->setAttribute("data-placeholder", "Vyberte lék...");
        $form->addHidden("medicineOld", 0);

        if ($id != 0) {
            $can = $this->canRepository->getByID($id);
            $form->setDefaults(array(
                "name" => $can->name,
                "medicine" => $can->medicineId,
                "medicineOld" => $can->medicineId,
                "id" => $can->id
            ));
        }

        $form->addSubmit('send', 'Odeslat')
            ->setAttribute("class", "btn btn-success");
        return $form;
    }

    public function createSpeciality($id) {

        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());
        $form->getElementPrototype()->class('ajax');
        $form->addHidden("id", $id);
        $form->addText('name', 'Název:')
            ->setAttribute("class", "medium-text")
            ->setRequired('Zadejte název odbornosti.');

        if ($id != 0) {
            $speciality = $this->specialityRepository->getByID($id);
            $form->setDefaults(array("name" => $speciality->name, "id" => $speciality->id));
        }

        $form->addSubmit('send', 'Odeslat')
            ->setAttribute("class", "btn btn-success");
        return $form;
    }

    public function createImportForm() {
        $form = new Form;
        $form->addUpload('file', 'Soubor:');
        $form->addSubmit('Upload');
        return $form;
    }

}
