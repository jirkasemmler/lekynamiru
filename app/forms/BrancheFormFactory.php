<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 27.6.15
 * Time: 16:00
 */

namespace App\Forms;

use App\Model\Entities\Branche;
use App\Model\Entities\Predikce;
use App\Model\Repositories\BrancheRepository;
use App\Model\Repositories\PredikceRepository;
use App\Model\Repositories\SpecialityRepository;
use App\Model\Repositories\UserRepository;
use Instante\Bootstrap3Renderer\BootstrapRenderer;
use Nette\Application\UI\Form;

class BrancheFormFactory
{

    /**
     * @param BrancheRepository $brancheRepository
     */
    protected $brancheRepository;

    /**
     * @param SpecialityRepository $specialityRepository
     */
    protected $specialityRepository;
    /**
     * @param PredikceRepository $predikceRepository
     */
    protected $predikceRepository;

    /**
     * @param UserRepository $userRepository
     */
    protected $userRepository;

    public function __construct(
        BrancheRepository $brancheRepository,
        UserRepository $userRepository,
        PredikceRepository $predikceRepository,
        SpecialityRepository $specialityRepository
    ) {
        $this->brancheRepository = $brancheRepository;
        $this->userRepository = $userRepository;
        $this->predikceRepository = $predikceRepository;
        $this->specialityRepository = $specialityRepository;
    }

    public function createBranchEurekaExport()
    {
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());

        $branches = $this->brancheRepository->findAll();

        /**
         * @var $branch Branche
         */
        foreach ($branches as $branch) {
            $list = [];
            $vals = [];
            $deps = $branch->getDepartmentsAll();
            foreach ($deps as $dep) {
                $list[$dep['id']] = $dep['name'];
                if ($dep['export_to_eureka']) {
                    $vals[] = $dep['id'];
                }
            }
            $form->addCheckboxList('export_branch_' . $branch->id, $branch->name, $list);
            $form['export_branch_' . $branch->id]->setValue($vals);
        }


        $form->addSubmit('send', 'Odeslat');

        return $form;
    }

    public function create($id = 0)
    {
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());
        $form->addHidden("id", $id);
        $form->addHidden("userOld", 0);
        $form->addText('name', 'Název domova:')
            ->setAttribute("class", "medium-text")
            ->setRequired('Zadejte název domova.');
        $form->addCheckbox("export", "Exportovat");
        $form->addCheckbox("orders", "Objednávky");
        $oldUsers = [];
        $oldAssocUsers = [];
        if ($id != 0) // insert new
        {
            $form->addCheckbox("changeUser", "Změnit vrchní sestru");
            /**
             * @var Branche $branche
             */
            $branche = $this->brancheRepository->getByID($id);
            $oldUsers = $branche->getUsers(true);
            $oldAssocUsers = $branche->getAssociatedUsers(true);
        }

        $users = $this->userRepository->getFreeUsers(["department"], $id);
        $users2 = $users + $oldUsers;

        $allUsers = $this->userRepository->getAllUsersAsArray();

        $form->addMultiSelect("users", "Vrchní sestra", $users2)
            ->setAttribute("class", "medium-text chosen-select")
            ->setAttribute("data-placeholder", "Přiřaďte uživatele")
            ->setAttribute("disabled", "disabled");


        $form->addMultiSelect("associated_users", "Asociovaní uživatelé", $allUsers)
            ->setAttribute("class", "medium-text chosen-select")
            ->setAttribute("data-placeholder", "Přiřaďte uživatele")
            ->setAttribute("disabled", "disabled");


        $form->addHidden("oldUsers", json_encode(array_keys($oldUsers)))->setAttribute("class", "hidden");
        $form->addHidden("oldAssocUsers", json_encode(array_keys($oldAssocUsers)))->setAttribute("class", "hidden");

        if ($id != 0) {
            $form->setDefaults(array(
                "name" => $branche->name,
                "id" => $branche->id,
                "changeUser" => false,
                "users" => array_keys($oldUsers),
                "associated_users" => array_keys($oldAssocUsers),
                "export" => $branche->export,
                "orders" => $branche->orders
            ));
        }

        $form->addSubmit('send', 'Odeslat')
            ->setAttribute("class", "btn btn-success closing");
        return $form;
    }

    public function createDepartment($idBranche, $idDepartment = 0)
    {
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());
        $form->addHidden("id", $idDepartment);
        $form->addHidden("idBranche", $idBranche);
        $form->addHidden("userOld", 0);

        $form->addText('name', 'Název oddělení:')
            ->setAttribute("class", "medium-text")
            ->setRequired('Zadejte název oddělení.');

        $form->addCheckbox("export", "Exportovat");
        $form->addCheckbox("orders", "Objednávky");
        $form->addText("osoba", "Staniční sestra")->setAttribute("class", "medium-text")->setDisabled(true);

        if ($idDepartment != 0) // insert new
        {
            $form->addCheckbox("changeDepUser", "Změnit staniční sestru");
        }


        $form->addSelect("user", "Staniční sestra",
            $this->userRepository->getFreeUsers(["department", "branche", "branche_department"],
                $idBranche))->setAttribute("class", "medium-text")->setOption('id', 'userSelect');
        if ($idDepartment != 0) {
            $department = $this->brancheRepository->getDepartment($idDepartment);

            if ($department->user_id != 0) {
                $osoba = $this->userRepository->getByID($department->user_id);
                $user_name = $osoba->name;
            } else {
                $user_name = "-nepřiřazeno-";
            }

            $form->setDefaults(
                array(
                    "name" => $department->name,
                    "id" => $idDepartment,
                    "userOld" => $department->user_id,
                    "changeUser" => false,
                    "osoba" => $user_name,
                    "export" => $department->export,
                    "orders" => $department->orders
                ));
        }

        $form->addSubmit('send', 'Odeslat')
            ->setAttribute("class", "btn btn-success closing");
        return $form;
    }

    public function createSpecialities($brancheId)
    {
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());

        $form->addHidden("brancheId", $brancheId);

        $specialities = $this->specialityRepository->findAll();
        if ($brancheId == 0) {
            $brancheId = $_POST["brancheId"];
        }

        $this->predikceRepository->checkBranche($brancheId);


        foreach ($specialities as $speciality) {
            $predikce = $this->predikceRepository->getBy([
                "speciality_id" => $speciality->id,
                "branche_id" => $brancheId
            ]);

            $form->addText("days" . $predikce->id, $speciality->name)
                ->setDefaultValue($predikce->days)
                ->setAttribute("class", "form-control input-number")
                ->setAttribute("id", "days" . $predikce->id);

            $form->addSelect("user" . $predikce->id, $speciality->name,
                $this->prepareUsers($predikce))->setDefaultValue($predikce->userId)->setAttribute("class",
                "form-control");

        }

        $form->addSubmit('send', 'Odeslat')
            ->setAttribute("class", "btn btn-success closing");

        return $form;
    }

    /**
     * @param $predikce Predikce
     * @return array
     */
    private function prepareUsers($predikce)
    {
        $free = $this->userRepository->getFreeUsers();
        if ($predikce->userId != null) {
            $free[$predikce->toRecord()->user->id] = $predikce->toRecord()->user->name . " (zvoleno)";
        }

        return $free;

    }
}
