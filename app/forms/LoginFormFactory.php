<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 20.6.15
 * Time: 23:45
 */

namespace App\Forms;


use App\Model\Entities\User;
use App\Model\Repositories\RoleRepository;
use Instante\Bootstrap3Renderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\SettingRepository;

class LoginFormFactory
{
    /**
     * @var UserRepository
     */
    protected $userRepository;
    /**
     * @var SettingRepository
     */
    protected $settingRepository;
    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    public function __construct(UserRepository $userRepository, SettingRepository $settingRepository, RoleRepository $r)
    {
        $this->userRepository = $userRepository;
        $this->settingRepository = $settingRepository;
        $this->roleRepository = $r;
    }

    public function createResetPass($token)
    {
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());

        $form->addText('email', 'Email:')
             ->setAttribute("class", "medium-text")
             ->setRequired('Zadejte prosím přihlašovací email.');

        $form->addPassword('password1', "Heslo", 20)
             ->setRequired('Prosím zadejte Vaše tajné heslo.')
             ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 6)
             ->setAttribute("class", "medium-text");

        $form->addPassword('password2', 'Ověření hesla:', 20)
             ->setRequired('Prosím zadejte ověření Vašeho hesla.')
             ->setAttribute("class", "medium-text")
             ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 6)
             ->addConditionOn($form['password1'], Form::VALID)
             ->addRule(Form::EQUAL, 'Zadaná hesla se neshodují.', $form['password1']);

        $form->addHidden("token", $token);

        $form->addSubmit('send', 'Odeslat')
             ->setAttribute("class", "btn btn-success closing");
        return $form;
    }

    /**
     * form for login user to system
     * @return Form
     */
    public function create()
    {
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());

        $form->addText('email', 'Email:')
             ->setAttribute("class", "medium-text")
             ->setRequired('Zadejte prosím přihlašovací email.');

        $form->addPassword('password', 'Heslo:')
             ->setAttribute("class", "medium-text")
             ->setRequired('Prosím zadejte Vaše tajné heslo.');

        $form->addCheckbox('remember', 'Neodhlašovat');

        $form->addSubmit('send', 'Přihlásit se!')
             ->setAttribute("class", "btn btn-success closing");
        return $form;
    }

    /**
     * form for lost pass
     * @return Form
     */
    public function createLostPass()
    {
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());

        $form->addText('email', 'Email:')
             ->setAttribute("class", "medium-text")
             ->setRequired('Zadejte prosím přihlašovací email.');

        $form->addSubmit('send', 'Resetovat heslo')
             ->setAttribute("class", "btn btn-success closing");
        return $form;
    }

    /**
     * form for registration (or update user)
     * @return Form
     */
    public function createReg($id = 0, $admin = false)
    {

        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());

        $form->addText('name', 'Jméno a příjmení:')
             ->setRequired('Zadejte prosím jméno a přijmení.')
             ->setAttribute("class", "medium-text")
             ->setAttribute("placeholder", "Jan Novák");

        $form->addText('email', 'Email:')
             ->setRequired('Zadejte prosím přihlašovací email.')
             ->addRule(Form::EMAIL, "Zadejte email ve tvaru name@example.com")
             ->setAttribute("class", "medium-text")
             ->setAttribute("placeholder", "jan.novak@example.com");
        $form->addCheckbox("sendReports", "Zasílat reporty");
        $form->addCheckbox("showSums", "Zobrazovat součty léků");

        $heslo_nazev = "Heslo";

        if ($id != 0 && $admin == false) //user change on its own
        {
            $form->addPassword('password_edit', 'Kontrolní heslo:', 20)
                 ->setAttribute("class", "medium-text");

            $form->addCheckbox("changePass", "Změnit heslo");
            $heslo_nazev = "Nové heslo";
            $form->addHidden("ownEdit", "true");
        } elseif ($id != 0 && $admin != false) //admin changes
        {
            $form->addHidden("ownEdit", "false");
            $form->addCheckbox("changePass", "Změnit heslo");
            $heslo_nazev = "Nové heslo";


        } else //reg
        {
            $form->addHidden("ownEdit", "false");
        }

        // TODO move this role
        $roles = $this->roleRepository->findAll();
        $rolesAr = [];
        foreach($roles as $role){
            $rolesAr[$role->id] = $role->name;
        }
        $form->addSelect('role', 'Role', $rolesAr);

        if (($id != 0 && $admin == false) || ($id != 0 && $admin == true)) {

            $form->addPassword('password1', $heslo_nazev, 20)
                 ->setAttribute("class", "medium-text");

            $form->addPassword('password2', 'Ověření hesla:', 20)
                 ->setAttribute("class", "medium-text")
                 ->setRequired('Prosím zadejte ověření Vašeho hesla.')
                 ->addConditionOn($form['password1'], Form::VALID)
                 ->addRule(Form::EQUAL, 'Zadaná hesla se neshodují.', $form['password1']);
        } else {
            $form->addHidden("password1", "tmp");
            $form->addHidden("password2", "tmp");
        }

        if ($id == 0 || $admin != false) {

            $form->addCheckbox("showSecretData", "Zobrazovat citlivé údaje");
        }

        $form->addHidden("id", $id);

        if ($id != 0) {
            /**
             * @var User
             */
            $user = $this->userRepository->getByID($id);
            $form->setDefaults([
                                   "name" => $user->name,
                                   "email" => $user->email,
                                   "showSecretData" => $user->showSecretData,
                                   "showSums" => $user->showSums,
                                   "sendReports" => $user->sendReports,
                                   "role" => $user->roleId,
                               ]);
        }


        $form->getElementPrototype()->id("password1")->setRequired('Prosím zadejte Vaše tajné heslo.')
             ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 6);
        $form->getElementPrototype()->id("password2")->setRequired('Prosím zadejte Vaše tajné heslo.');

        $form->addSubmit('send', 'Odeslat')
             ->setAttribute("class", "btn  btn-success");;

        return $form;
    }

    /**
     * form for registration (or update user)
     *
     * @param int $id
     *
     * @return Form
     */
    public function createSetting($id = 0)
    {

        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());

        if ($id == 0) {
            $id = $_POST["id"];
        }
        $setting = $this->settingRepository->getByID($id);
        $form->addHidden("id", $id);
        $form->addText("key", "Klíč", $setting->key)
             ->setDisabled()
             ->setAttribute("class",
                            "medium-text")
             ->setDefaultValue($setting->key);

        $form->addText("value", "Hodnota")->setAttribute("class", "medium-text")->setDefaultValue($setting->value);

        $form->addSubmit("send", "Odeslat")->setAttribute("class", "btn  btn-success");

        return $form;
    }
}
