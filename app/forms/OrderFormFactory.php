<?php
/**
 * Created by PhpStorm.
 * User: stet
 * Date: 23.9.17
 * Time: 14:12
 */

namespace App\Forms;

use App\Model\Entities\Medicine;
use App\Model\Entities\Order;
use App\Model\Entities\OrderItem;
use App\Model\Entities\OrderSetting;
use App\Model\Entities\Patient;
use App\Model\Entities\User;
use App\Model\Repositories\MedicineRepository;
use App\Model\Repositories\OrderItemRepository;
use App\Model\Repositories\OrderNoteRepository;
use App\Model\Repositories\OrderRepository;
use App\Model\Repositories\OrderSettingRepository;
use App\Model\Repositories\PatientRepository;
use Instante\Bootstrap3Renderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;


class OrderFormFactory
{
	/** @var MedicineRepository */
	protected $medicineRepository;

	/** @var PatientRepository */
	protected $patientRepository;

	/** @var OrderSettingRepository */
	protected $orderSettingRepository;

	/** @var OrderItemRepository */
	protected $orderItemRepository;

	/** @var OrderRepository */
	protected $orderRepository;

	/** @var OrderNoteRepository */
	protected $orderNoteRepository;

	public function __construct(
		MedicineRepository $medicineRepository,
		PatientRepository $patientRepository,
		OrderSettingRepository $orderSettingRepository,
		OrderItemRepository $orderItemRepository,
		OrderRepository $orderRepository,
		OrderNoteRepository $orderNoteRepository
	)
	{
		$this->medicineRepository = $medicineRepository;
		$this->patientRepository = $patientRepository;
		$this->orderSettingRepository = $orderSettingRepository;
		$this->orderItemRepository = $orderItemRepository;
		$this->orderRepository = $orderRepository;
		$this->orderNoteRepository = $orderNoteRepository;
	}


	/**
	 * creates a form for order items (not the ORDER but ORDER_ITEM!)
	 * @param $orderId
	 * @param $orderItemId
	 * @param $dateTime
	 * @param $user User
	 * @param $branche
	 * @param $urgent
	 * @return Form
	 */
	public function createOrderItem($orderId, $orderItemId, $dateTime, $user, $branche, $urgent)
	{
		/**
		 * @var $order Order
		 */
		$order = $this->orderRepository->getByID($orderId);

		if($user->isDepartment())
		{
			$deps = [];
			$depsTmp = $user->getDepartments();
			foreach($depsTmp as $temp)
			{
				if($temp->orders)  // only the departments with allowed orders
				{
					$deps[] = $temp;
				}
			}
		}
		else
		{
			$deps = null;
		}

		$patients = $this->patientRepository->getPatients($branche, $deps, Patient::NON_DEAD_STATES);

		$medicines = $this->medicineRepository->getMedicines();
		$slots = $this->orderSettingRepository->getFollowingSlots($branche, true);
		/**
		 * @var Medicine $firstMedicine
		 */
		if($orderItemId != 0)
		{
			/** @var $item OrderItem */
			$item = $this->orderItemRepository->getByID($orderItemId);
			if($item->_getPackageId() and $item->_getMedicineId())
			{
				$selectedMedicine = $item->getMedicine();
			}
			else
			{
				$selectedMedicine = $medicines->toArray()[0];
			}
		}
		else
		{
			$selectedMedicine = $medicines->toArray()[0];
		}

		$packages = $selectedMedicine->getPackageList();
		$packagesAr = $this->dbToSelect($packages, 'size', false);
		if(isset($_POST['medicinePackageChose'])) // so awful... but the input is changed dynamically via Ajax between the component is created and sent
		{
			$packagesAr = [$_POST['medicinePackageChose'] => $_POST['medicinePackageChose']];
		}
		$orderList = [];
		$activeKey = null;
		foreach($slots as $slot)
		{
			if($slot['setting'] == null)
			{
				continue;
			}
			$formattedDateTimeHash = $slot['orderDateTime']->format("Y-m-d_H:i");
			$formattedDateTime = $slot['orderDateTime']->format("d.m.Y H:i");
			$orderIdTmp = ($slot['order']) ? $slot['order']->id : "";
			$key = $slot['setting']->id . "#" . $orderIdTmp . "#" . $formattedDateTimeHash;
			if($formattedDateTimeHash == $dateTime)
			{
				$activeKey = $key;
			}
			$orderList[$key] = $formattedDateTime;
		}
		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());
		$form->getElementPrototype()->class('ajax');

		$form->addHidden("urgent", $urgent);
		$form->addHidden("orderId", $orderId);
		$form->addHidden("orderItemId", $orderItemId);
		$form->addSelect("patient", "Pacient", $this->dbToSelect($patients, "getFullName"))
			->setAttribute("class", "chosen-select patient-select")
			->setAttribute("data-placeholder", "Vyberte pacienta...");

		$form->addSelect("medicine", "Přípravek", $this->dbToSelect($medicines, "getFullName"))
			->setAttribute("class", "chosen-select medicine-select")
			->setAttribute("data-placeholder", "Vyberte lék...");

		$form->addRadioList("targetDelivery", "Doručení", ['branch' => "Do domova", 'pharmacy' => "Do lékárny"])->setDefaultValue('branch');

		$form->addText("customMedicine", "Název přípravku")->setAttribute("class", "full-text form-control");
		$form->addText("customMedicineAmount", "Množství")->setAttribute("class", "medium-text form-control")->setDefaultValue(1);

		$form->addText("medicinePackageNumber", "Počet balení")->setAttribute("class", "form-control small-line  input-number")->setDefaultValue(1);
		$form->addSelect("medicinePackageChose", "Velikost balení", $packagesAr)->setAttribute("class", "medicine_package_chose form-control small-line");

		if(!$order || ($order && !$order->isIsExtra() && !$urgent))
		{
			$form->addRadioList("orderHash", "Objednávka", $orderList)
				->setDefaultValue($activeKey)
				->setAttribute("class", "orderHash ");
		}
		else
		{
			$form->addHidden("orderHash", ""); // just faking the order hash... not sure if good idea
		}

		$form->addSelect("customOrSystem", "", [OrderItem::MS_CUSTOM => OrderItem::MS_CUSTOM, OrderItem::MS_SYSTEM => OrderItem::MS_SYSTEM])
			->setDefaultValue("system")
			->setAttribute("class", "customOrSystem hidden");

		$form->addSubmit('send', 'Odeslat')
			->setAttribute("class", "btn btn-success closing");

		$form->addSubmit('sendAndAdd', 'Odeslat a přidat další')
			->setAttribute("class", "btn btn-primary".(($orderItemId != 0 || !$order) ? " hidden" : ""));

		if($orderItemId != 0)
		{
			/** @var $item OrderItem */
			$item = $this->orderItemRepository->getByID($orderItemId);
			$form->setDefaults(array(
				"patient" => $item->_getPatientId(),
				"medicine" => $item->_getMedicineId(),
				"customMedicine" => $item->_getCustomMedicineName(),
				"customMedicineAmount" => $item->_getCustomMedicineAmount(),
				"medicinePackageNumber" => $item->_getPackages(),
				"medicinePackageChose" => $item->_getPackageId(),
				"targetDelivery" => $item->_getTargetDelivery(),
				"customOrSystem" => $item->_getMedicineSource()
			));
		}

		return $form;
	}

	public function createOrder($orderId)
	{
		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());
	}

	public function createOrderSetting($brancheId, $orderSettingId = 0)
	{
		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());
		$form->getElementPrototype()->class('ajax');


		$days = ["Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota", "Neděle"];
		$hours = range(0, 23);
		$form->addHidden('brancheId', $brancheId);
		$form->addHidden('orderSettingId', $orderSettingId);
		$form->addSelect('dayOrder', "Den uzavření objednávky", $days)->setAttribute("class", "medium-text form-control");
		$form->addSelect('hourOrder', "Hodina uzavření objednávky", $hours)->setAttribute("class", "medium-text form-control");
		$form->addSelect('dayDelivery', "Den doručení", $days)->setAttribute("class", "medium-text form-control");
		$form->addSelect('hourDelivery', "Hodina doručení", $hours)->setAttribute("class", "medium-text form-control");
		$form->addText('extraLimit', "Limit pro zadání vyjímky");

		$form->addSubmit('send', 'Odeslat')
			->setAttribute("class", "btn btn-success closing");

		//edit -> fill the data
		if($orderSettingId != 0)
		{
			/** @var $data OrderSetting */
			$data = $this->orderSettingRepository->getByID($orderSettingId);
			$form->setDefaults(array(
					"dayOrder" => $data->_getDayOrder(),
					"hourOrder" => $data->_getHourOrder(),
					"dayDelivery" => $data->_getDayDelivery(),
					"hourDelivery" => $data->_getHourDelivery(),
					"extraLimit" => $data->_getExtraLimit())
			);
		}

		return $form;
	}

	/**
	 * form for canceling order item
	 * @param $orderItemId
	 * @return Form
	 */
	public function createCancelOrderItem($orderItemId)
	{
		$form = new Form();
		$form->setRenderer(new BootstrapRenderer());
		$form->getElementPrototype()->class('ajax');

		$form->addHidden("id", $orderItemId);
		$form->addHidden("target_status", "canceled");

		$notes = $this->orderNoteRepository->findStringNotesFor('cancel');
		$form->addRadioList("comment", "Odůvodnění odmítnutí", $notes)->setDefaultValue(array_keys($notes)[0]);

		$form->addSubmit('send', 'Odeslat')
			->setAttribute("class", "btn btn-success closing");

		return $form;
	}

	public function createChangeOrderItem($orderItemId)
	{
		if(!$orderItemId and isset($_POST['order_item_id']))
		{
			$orderItemId = $_POST['order_item_id'];
		}
		/**
		 * @var $orderItem OrderItem
		 */
		$orderItem = $this->orderItemRepository->getByID($orderItemId);

		$form = new Form();
		$form->setRenderer(new BootstrapRenderer());
		$form->getElementPrototype()->class('ajax');

		$form->addHidden("order_item_id", $orderItemId);

		if($orderItem->isCustom())
		{
			$form->addHidden("medicine_type", "custom");

			$form->addHidden("old_custom_medicine_name", $orderItem->_getCustomMedicineName());
			$form->addHidden("old_custom_medicine_amount", $orderItem->_getCustomMedicineAmount());

			// these ones are just for rendering in the form. disabled values are not send in the POST
			$form->addText("old_custom_medicine_name_label", "Původní přípravek")
				->setDisabled()
				->setDefaultValue($orderItem->_getCustomMedicineName());
			$form->addText("old_custom_medicine_amount_label", "Původní objem")
				->setDisabled()
				->setDefaultValue($orderItem->_getCustomMedicineAmount());


			$form->addText("new_custom_medicine_name", "Nový přípravek")->setRequired();
			$form->addText("new_custom_medicine_amount", "Nový objem")->setRequired();
		}
		else
		{
			$form->addHidden("medicine_type", "system");

			$medicines = $this->medicineRepository->getMedicines();
			$medicine = $orderItem->getMedicine();
			$packages = $medicine->getPackageList();
			$packagesAr = $this->dbToSelect($packages, 'size', false);
			if(isset($_POST['medicinePackageChose'])) // so awful... but the input is changed dynamically via Ajax between the component is created and sent
			{
				$packagesAr = [$_POST['medicinePackageChose'] => $_POST['medicinePackageChose']];
			}

			$form->addText("medicine_from", "Původní přípravek")->setAttribute("readonly", "readonly")->setDefaultValue($orderItem->getMedicineName());
			$form->addText("medicine_amount", "Původní objem")->setAttribute("readonly", "readonly")->setDefaultValue($orderItem->getAmount());
			// old ones

			// new ones
			$form->addSelect("medicine", "Nový přípravek", $this->dbToSelect($medicines, "getFullName"))
				->setAttribute("class", "chosen-select medicine-select")
				->setAttribute("data-placeholder", "Vyberte lék...");

			$form->addText("medicinePackageNumber", "Počet balení")->setAttribute("class", "form-control small-line  input-number")->setDefaultValue(1);
			$form->addSelect("medicinePackageChose", "Velikost balení", $packagesAr)->setAttribute("class", "medicine_package_chose form-control small-line");

		}

		$notes = $this->orderNoteRepository->findStringNotesFor('change');
		$form->addRadioList("comment", "Poznámka", $notes)->setDefaultValue(array_keys($notes)[0]);

		$form->addSubmit('send', 'Odeslat')
			->setAttribute("class", "btn btn-success closing");

		return $form;
	}

	public function createCloseOrder($orderId)
	{
		if(!$orderId and isset($_POST['order_id']))
		{
			$orderId = $_POST['order_id'];
		}
		/**
		 * @var Order $order
		 */
		$order = $this->orderRepository->getByID($orderId);
		$form = new Form();
		$form->addHidden('order_id', $order->_getId());
		$tmpClosingDate = $order->_getClosingDate() ? : new DateTime();
		$form->addHidden('startDateInput', $tmpClosingDate->format('d/m/Y'))
		->setAttribute("class", 'startDateInput');
		$form->addHidden('canDeliverOnWeekend', $order->getBranche()->canDeliverOnWeekend() ? "true" :"false")
		->setAttribute("class", 'canDeliverOnWeekend');
		$form->setRenderer(new BootstrapRenderer());

		$form->getElementPrototype()->class('ajax');
		$form->addText('delivery_date', 'Datum doručení')
			->setAttribute("class", 'datepicker')
			->setDefaultValue($order->getDeliveryDateTimeFormatted(OrderSettingRepository::DATETIME_USER_FORMAT_PICKER));
		$form->addSubmit("submit", "ANO, uzavřít")
			->setAttribute("class", 'btn btn-success closing');


		return $form;
	}

	public function createPostponeOrder($orderItemId)
	{
		if(!$orderItemId and isset($_POST['order_item_id']))
		{
			$orderItemId = $_POST['order_item_id'];
		}
		/**
		 * @var $orderItem OrderItem
		 */
		$orderItem = $this->orderItemRepository->getByID($orderItemId);
		$orderId = $orderItem->getOrder()->_getId();
		$form = new Form();
		$form->setRenderer(new BootstrapRenderer());
		$form->getElementPrototype()->class('ajax');

		$form->addHidden("order_item_id", $orderItemId);

		$brancheId = $orderItem->getOrder()->getBranche()->id;

		$slots = $this->orderSettingRepository->getFollowingSlots($brancheId, false);

		$orderList = [];
		$activeKey = null;
		foreach($slots as $slot)
		{
			$formattedDateTime = null; // just to find out if the $formattedDateTime was set or not
			$formattedDateTimeHash = null;
			$settingID = null;

			// the "new" order is allowed because after the move, it has flag that it was moved by pharmacy so branch can not edit it
			if($slot['order'])
			{
				$orderObj = $slot['order'];
				/**
				 * @var $orderObj Order
				 */
				if($orderObj->isSent())
				{
					$formattedDateTimeHash = $orderObj->_getClosingDate()->format("Y-m-d_H:i");
					$formattedDateTime = $orderObj->_getClosingDate()->format("d.m.Y H:i")." Odeslaná".($orderObj->isIsExtra() ? " Extra" : "");
					$settingID = 0;
				}
				elseif($orderObj->isNew()){
					$formattedDateTimeHash = $orderObj->_getClosingDate()->format("Y-m-d_H:i");
					$formattedDateTime = $orderObj->_getClosingDate()->format("d.m.Y H:i")." Nová";
					$settingID = 0;
				}
			}
			else // not opened order yet
			{
				$formattedDateTimeHash = $slot['orderDateTime']->format("Y-m-d_H:i");
				$formattedDateTime = $slot['orderDateTime']->format("d.m.Y H:i")." Neotevřená";
				$settingID =  $slot['setting']->id;
			}

			if($formattedDateTime && $formattedDateTimeHash){
				$orderIdTmp = ($slot['order']) ? $slot['order']->id : "";
				$key = $settingID. "#" . $orderIdTmp . "#" . $formattedDateTimeHash;
				if($slot['order'] and $slot['order']->_getId() == $orderId)
				{
					$activeKey = $key;
				}
				$orderList[$key] = $formattedDateTime;
			}
		}

		$form->addRadioList("orderHash", "Objednávka", $orderList)
			->setAttribute("class", "orderHash orderListLabels")
			->setDefaultValue($activeKey);

		$notes = $this->orderNoteRepository->findStringNotesFor('postpone');
		$form->addRadioList("comment", "Poznámka", $notes)->setDefaultValue(array_keys($notes)[0]);

		$form->addSubmit('send', 'Odeslat')
			->setAttribute("class", "btn btn-success closing");

		return $form;

	}
	public function dbToSelect($collection, $property, $method = true)
	{
		$out = [];
		foreach($collection as $item)
		{
			$out[$item->id] = ($method) ? $item->$property() : $item->$property;
		}
		return $out;
	}

    public function createOrderFile($orderId)
    {
        /**
         * @var Order $order
         */
        $form = new Form();
        $form->setRenderer(new BootstrapRenderer());

        $form->addText('title', 'Titulek')->setRequired();;
        $form->addUpload('file', 'Soubor')->setRequired();;

        $form->addHidden('id_order')->setValue($orderId);

        $form->addSubmit('send', 'Odeslat')
            ->setAttribute("class", "btn btn-success closing");

        return $form;
    }
}