<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;


if ($file = file_get_contents(__DIR__ . "/../setting/settings.json")) {
    $parsed_file = json_decode($file, true);

    if ($parsed_file["mode"] == 'debug' && filter_var($parsed_file["ip"], FILTER_VALIDATE_IP)) {
        $configurator->setDebugMode($parsed_file["ip"]); // Jirkova domaci IP
    }
}


$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
             ->addDirectory(__DIR__)
             ->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
