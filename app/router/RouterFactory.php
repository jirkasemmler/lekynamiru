<?php

namespace App;

use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route;
use Nette\Application\Routers\CliRouter;

class RouterFactory
{

	private $container;

	public function __construct(Nette\DI\Container $container)
	{
		$this->container = $container;
	}

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{

		$router = new RouteList();
		$router[] = new CliRouter(array('action' => 'Api:mailer'));
		$router[] = new CliRouter(array('action' => 'Api:recount'));
		$router[] = new CliRouter(array('action' => 'Api:usageStat'));
		$router[] = new CliRouter(array('action' => 'Api:changeWeek'));
		$router[] = new CliRouter(array('action' => 'Api:nonActive'));
		$router[] = new CliRouter(array('action' => 'Api:changeStatus'));
		$router[] = new CliRouter(array('action' => 'Api:test'));
		$router[] = new CliRouter(array('action' => 'Api:sendDB'));

		$router[] = new CliRouter(array('action' => 'Api:sendOrders'));
		$router[] = new CliRouter(array('action' => 'Api:archiveOrders'));


		$router[] = new Route('api/eureka/<token>', 'Api:Eureka');
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Login:');
		$router[] = new Route('', 'Login:');

		return $router;
	}

}
