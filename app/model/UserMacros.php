<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 21.7.15
 * Time: 14:20
 */

namespace App\Model;

use Latte;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;
use Latte\Compiler;
//	,
//	Nette\Latte\,
//	Nette\Latte\MacroNode,
//	Nette\Latte\PhpWriter;

class UserMacros extends MacroSet {

	public static function install(Compiler $compiler)
	{
		$me = new static($compiler);

		$me->addMacro('moje', NULL, NULL, array($me, 'macroMoje'));
	}


	/**
	 * Finishes template parsing.
	 * @return array(prolog, epilog)
	 */
	public function finalize()
	{
		return array('list($_b, $_g, $_l) = $template->initialize('
			. var_export($this->getCompiler()->getTemplateId(), TRUE) . ', '
			. var_export($this->getCompiler()->getContentType(), TRUE)
			. ')');
	}


	/********************* macros ****************d*g**/

	/**
	 * n:attr="..."
	 */
	public function macroMoje(MacroNode $node, PhpWriter $writer)
	{
		return $writer->write('echo "hello"');
	}

}