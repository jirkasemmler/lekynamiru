<?php
/**
 * Created by PhpStorm.
 * User: stet
 * Date: 13.8.17
 * Time: 19:10
 */

namespace App\Model\Entities;
use Latte\Engine;
use YetORM\Entity;

/**
 * Class OrderItemChange
 * @package App\Model\Entities
 * @property-read int $id
 * @property int|NULL $orderItemId -> order_item_id
 * @property \DateTime $date -> date
 * @property string $type -> type
 * @property int|NULL $orderIdFrom -> order_id_from
 * @property int|NULL $orderIdTo -> order_id_to
 * @property int|NULL $medicineIdFrom -> medicine_id_from
 * @property int|NULL $medicineIdTo -> medicine_id_to
 * @property string|NULL $medicineNameFrom -> medicine_name_from
 * @property string|NULL $medicineNameTo -> medicine_name_to
 * @property string|NULL $medicineAmountFrom -> medicine_amount_from
 * @property string|NULL $medicineAmountTo -> medicine_amount_to
 * @property int $userId -> user_id
 * @property string|NULL $comment -> comment
 */
class OrderItemChange extends Entity
{
	const TYPE_COMMENT = 'comment';
	const TYPE_MEDICINE_CHANGE = 'medicine_change';
	const TYPE_POSTPONED_FROM = 'postponed_from';
	const TYPE_POSTPONED_TO = 'postponed_to';
	const TYPE_CANCELED = 'canceled';
    /**
     * @return int
     */
    public function _getOrderItemId()
    {
        return $this->orderItemId;
    }

    /**
     * @param int $orderItemId
     */
    public function _setOrderItemId($orderItemId)
    {
        $this->orderItemId = $orderItemId;
    }
    /**
     * @return int
     */
    public function _getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function _setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function _getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function _setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function _getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function _setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int|NULL
     */
    public function _getOrderIdFrom()
    {
        return $this->orderIdFrom;
    }

    /**
     * @param int|NULL $orderIdFrom
     */
    public function _setOrderIdFrom($orderIdFrom)
    {
        $this->orderIdFrom = $orderIdFrom;
    }

    /**
     * @return int|NULL
     */
    public function _getOrderIdTo()
    {
        return $this->orderIdTo;
    }

    /**
     * @param int|NULL $orderIdTo
     */
    public function _setOrderIdTo($orderIdTo)
    {
        $this->orderIdTo = $orderIdTo;
    }

    /**
     * @return int|NULL
     */
    public function _getMedicineIdFrom()
    {
        return $this->medicineIdFrom;
    }

    /**
     * @param int|NULL $medicineIdFrom
     */
    public function _setMedicineIdFrom($medicineIdFrom)
    {
        $this->medicineIdFrom = $medicineIdFrom;
    }

    /**
     * @return int|NULL
     */
    public function _getMedicineIdTo()
    {
        return $this->medicineIdTo;
    }

    /**
     * @param int|NULL $medicineIdTo
     */
    public function _setMedicineIdTo($medicineIdTo)
    {
        $this->medicineIdTo = $medicineIdTo;
    }

    /**
     * @return NULL|string
     */
    public function _getMedicineNameFrom()
    {
        return $this->medicineNameFrom;
    }

    /**
     * @param NULL|string $medicineNameFrom
     */
    public function _setMedicineNameFrom($medicineNameFrom)
    {
        $this->medicineNameFrom = $medicineNameFrom;
    }


    /**
     * @return NULL|string
     */
    public function _getMedicineNameTo()
    {
        return $this->medicineNameTo;
    }

    /**
     * @param NULL|string $medicineNameTo
     */
    public function _setMedicineNameTo($medicineNameTo)
    {
        $this->medicineNameTo = $medicineNameTo;
    }


	/**
	 * @return NULL|string
	 */
	public function _getMedicineAmountFrom()
	{
		return $this->medicineAmountFrom;
	}

	/**
	 * @param NULL|string $medicineAmountFrom
	 */
	public function _setMedicineAmountFrom($medicineAmountFrom)
	{
		$this->medicineAmountFrom = $medicineAmountFrom;
	}
	/**
	 * @return NULL|string
	 */
	public function _getMedicineAmountTo()
	{
		return $this->medicineAmountTo;
	}

	/**
	 * @param NULL|string $medicineAmountTo
	 */
	public function _setMedicineAmountTo($medicineAmountTo)
	{
		$this->medicineAmountTo = $medicineAmountTo;
	}

    /**
     * @return int
     */
    public function _getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function _setUserId($userId)
    {
        $this->userId = $userId;
    }

	public function getUserName()
	{
		$user = new User($this->record->user);
		return $user->name;
    }

    /**
     * @return NULL|string
     */
    public function _getComment()
    {
        return $this->comment;
    }

    /**
     * @param NULL|string $comment
     */
    public function _setComment($comment)
    {
        $this->comment = $comment;
    }

	public function printChange()
	{
		$latte = new Engine;

		$latte->setTempDirectory(__DIR__ . '/../../../temp');

		$html = $latte->renderToString(__DIR__ . '/../../../app/presenters/templates/Order/Components/orderItem_historyEvent.latte', ["change" => $this]);

		return $html;
    }

	public function getOrderFrom()
	{
		/**
		 * @var Order $order
		 */
		$order = new Order($this->record->ref('order','order_id_from'));

		return $order->getFormattedName();
    }
	public function getOrderTo()
	{
		/**
		 * @var Order $order
		 */
		$order = new Order($this->record->ref('order','order_id_to'));

		return $order->getFormattedName();
    }

	public function getMedicineFromName()
	{
		if($this->_getMedicineNameFrom())
		{
			return $this->_getMedicineNameFrom();
		}
		else
		{
			$medicine = new Medicine($this->record->ref('medicine','medicine_id_from'));
			return $medicine->getFullName();
		}
    }
	public function getMedicineToName()
	{
		if($this->_getMedicineNameTo())
		{
			return $this->_getMedicineNameTo();
		}
		else
		{
			$medicine = new Medicine($this->record->ref('medicine','medicine_id_to'));
			return $medicine->getFullName();
		}
    }

//	public function getAncestorName()
//	{
//
//    }
//
//	public function getFollowerName()
//	{
//		$this->getOrderTo()
//    }

}