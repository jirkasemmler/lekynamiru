<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 27.6.15
 * Time: 15:53
 */

namespace App\Model\Entities;

use YetORM\Entity;
use YetORM\EntityCollection;

/**
 * Class Branche
 * @package App\Model
 * @property-read int $id
 * @property string $name
 * @property bool $export
 * @property bool $eureka_export
 * @property bool $orders
 * @property bool $orderWeekend -> order_weekend
 */
class Branche extends Entity
{

	public function canDeliverOnWeekend()
	{
		return $this->orderWeekend;
	}

	public function findOrders()
	{
		return new EntityCollection($this->toRecord()->related("order"), "App\Model\Entities\Order");
	}

	public function getUsers($array = false)
	{
		$data = $this->toRecord()->related("branche_user");
		if (!$array)
		{
			return $data;
		}
		else
		{
			$ret = [];
			foreach ($data as $item)
			{
				$ret[$item->user->id] = $item->user->name;
			}
			return $ret;
		}
	}
	public function getAssociatedUsers($array = false)
	{
		$data = $this->toRecord()->related("associated_users");
		if (!$array)
		{
			return $data;
		}
		else
		{
			$ret = [];
			foreach ($data as $item)
			{
				$ret[$item->user->id] = $item->user->name;
			}
			return $ret;
		}
	}

	public function isToExport()
	{
		if ($this->export == true)
		{
			return '<span class="fa fa-check"></span>';
		}
		else
		{
			return '<span class="fa fa-times"></span>';
		}
	}

	public function ordersMarkup()
	{
		if ($this->orders == true)
		{
			return '<span class="fa fa-check"></span>';
		}
		else
		{
			return '<span class="fa fa-times"></span>';
		}
	}

	public function getOrderSettings()
	{
		return new EntityCollection($this->toRecord()->related("order_setting"), "App\Model\Entities\OrderSetting");
	}

	//get department for template. show branche -> show all the departments in the branche
	public function getDepartments($addUser = True)
	{
		$ret = [];
		$data = $this->toRecord()->related("department");
		if (!$addUser)
		{
//			$ret[NULL] = " - nepřiřazeno -";
		}
		foreach ($data as $item)
		{
			if ($addUser)
			{
				$tmp = ["name" => $item->name, "id" => $item->id];
//				print_r($item->user_id)
				if ($item->user_id != NULL)
				{
					$tmp["user"] = $item->user->name;
					$tmp["export"] = $item->export;
					$tmp["orders"] = $item->orders;
					$tmp["userid"] = $item->user->id;
				}
				else
				{
					$tmp["export"] = $item->export;
					$tmp["orders"] = $item->orders;
					$tmp["user"] = "<i>nepřiřazeno</i>";
					$tmp["userid"] = 0;
				}
				$ret[] = $tmp;
			}
			else
			{
				$ret[$item->id] = $item->name;
			}

		}

		return $ret;
	}

	public function getUsersForReport()
	{
		if ($this->export)
		{
			$to_ret = [];
			$users = $this->toRecord()->related("branche_user")->fetchAll();
			foreach ($users as $user)
			{
				if ($user->user->send_reports)
				{
					$to_ret[] = $user->user->email;
				}
			}

			return $to_ret;
		}
		else
		{
			return [];
		}
	}
	public function getUsersObjectForReport()
	{
		if ($this->export)
		{
			$to_ret = [];
			$users = $this->toRecord()->related("branche_user")->fetchAll();
			foreach ($users as $user)
			{
			    $to_ret[] = new User($user->user);
			}

			return $to_ret;
		}
		else
		{
			return false;
		}
	}

	public function getDepartmentsReport()
	{
		$deps = $this->toRecord()->related("department")->fetchAll();
		$ret = [];
		foreach ($deps as $dep)
		{
			if ($dep->export && $dep->user_id && $dep->user->send_reports)
			{
				$ret[] = ["id" => $dep->id, "mail" => $dep->user->email, "name" => $dep->name, "user" => new User($dep->user)];
			}
		}

		return $ret;
	}


	public function getDepartmentsAll()
	{
		$deps = $this->toRecord()->related("department")->fetchAll();
		$ret = [];
		foreach ($deps as $dep)
		{
			$ret[] = ["id" => $dep->id, "name" => $dep->name, "export_to_eureka" => $dep->eureka_export];
		}

		return $ret;
	}

	public function getFullName()
	{
		return $this->name;
	}
}
