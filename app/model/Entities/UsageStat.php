<?php
/**
 * Created by PhpStorm.
 * Jiri Semmler, Mathesio 2016
 * User: Jiri Semmler (semmler@mathesio.cz)
 * Date: 2.6.16
 * Time: 22:19
 */

namespace App\Model\Entities;
use YetORM\Entity;

/**
 * Class UsageStat
 * @property-read int $id
 * @property int $departmentId -> department_id
 * @property int $medicineId -> medicine_id
 * @property int $half
 * @property int $quater
 * @property int $third
 * @property int $full
 * @property int $week_delta
 * @property int|NULL $halfs
 * @property int|NULL $thirds
 * @property int|NULL $quaters
 * @property int|NULL $fulls
 */
class UsageStat extends Entity
{
	public function setValues($full, $half, $quater, $third)
	{
		$this->half     = (int) $half;
		$this->third    = (int) $third;
		$this->quater   = (int) $quater;
		$this->full     = (int) $full;
	}

	public function setMedicine($medicine)
	{
		$this->medicineId = $medicine->id;
	}

	public function setDepartment($department_id)
	{
		$this->departmentId = (int) $department_id;
	}

	public function setWeekDelta($week)
	{
		$this->week_delta = (int) $week;
	}

	public function getSum()
	{
		return ($this->fulls+($this->halfs*0.5) + ($this->quaters*0.25) + round($this->thirds/3,2));
	}
	public function getPackageList()
	{
		return $this->toRecord()->medicine->related("package");
	}
	public function getCan()
	{
		$medicine = $this->toRecord()->medicine;

		if($medicine->place == "can")
		{
			$can = $medicine->related("can")->fetch();
			if($can == NULL)
			{
				return "-";
			}

			return $can->name;
		}
		else
		{
			$placeAr = ["fspmdu"=>"FPS/MDU","fsp"=>"FSP", "mdu"=>"MDU"];
			return $placeAr[$medicine->place];
		}
	}

	public function getMedicineName()
	{
		return $this->toRecord()->medicine->name;
	}
}