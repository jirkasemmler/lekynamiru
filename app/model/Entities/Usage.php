<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 18.7.15
 * Time: 18:51
 */

namespace App\Model\Entities;
use Nette\Utils\DateTime;
use YetORM\Entity;
/**
 * Class Usage
 * @package App\Model
 * @property-read int $id
 * @property string $startDate -> start_date
 * @property int $medicineId -> medicine_id
 * @property int $specialityId -> speciality_id
 * @property int $patientId -> patient_id
 * @property double $doubleweekly
 * @property double $number
 * @property bool $showTwoWeeks
 * @property string $activeWeek -> active_week
 * @property int $weeks
 * @property int $remaining -> remaining_days
 * @property double|NULL $dailyAverage -> daily_average
 * @property string $state
 * @property int|NULL $revisionSrc -> revision_source
 * @property bool $deleted
 * @property bool $active
 */
use Week;
class Usage extends Entity{

	// just to have the array of weeks on one place
	// BUT this array is unfortunately on more places. if wanted to edit -> look for all of them. have fun

	public $need_to_date;
	private $weeks_data = [];
	private $index_of_active_week = 0;

	public static $weeks = ['first','second','third','fourth','fifth','sixth','seventh','eighth','ninth','tenth','eleventh','twelfth'];

	public function defineWeeks()
	{
		$number_of_weeks = $this->weeks;
		$weeks_names = $this->getWeeksAr();
		for($i = 0; $i< $this->weeks; $i++)
		{
			$week_data = $this->getWeek($weeks_names[$i]);
			$tmp_week = new Week($week_data, array_search($this->activeWeek, $weeks_names), $this->weeks, $i);
			$tmp_week->countPillsByType();
			$this->weeks_data[] = $tmp_week;
			if($tmp_week->isActive())
			{
				$this->index_of_active_week = $i;
			}
		}
	}

	public function getMedicalCard($revision = "last")
	{
			$card = $this->toRecord()->related("medical_card_usage_meta")->order("medical_card.revision_number DESC")->fetch();

		return $card["medical_card_id"];
	}



	public function getActiveWeekData($offset = 0, $by_offset = true, $hard_index = 0)
	{
		if($by_offset)
		{
			if(($this->index_of_active_week + $offset) >= $this->weeks)
			{
				$tmp_index = ($this->index_of_active_week + $offset) - $this->weeks;
			}
			else
			{
				$tmp_index = $this->index_of_active_week+$offset;
			}
		}
		else
		{
			$tmp_index = $hard_index;
		}

		return $this->weeks_data[$tmp_index];
	}


	public function getWeeksAr()
	{
		return self::$weeks;
	}

	// what is the INT index of active week? because in DB it is as ENUM and MySQL returns it as string
	public function getActiveWeekNumber()
	{
		$ar = $this->getWeeksAr();
		return array_search($this->activeWeek,$ar);
	}

	/**
	 * returns number of usage for one day set by $day , $setWeeks. e.g. countDay("sun","first") -> how many pills is the usage for Sunday of the first week? thats the question :D
	 * @param      $day
	 * @param null $setWeek
	 *
	 * @return mixed
	 */
	public function countDay($day, $setWeek = NULL)
	{
		$week = ($setWeek == NULL) ? $this->activeWeek : $setWeek; // if not set, use the active
		$dayUsage = $this->toRecord()->related("usage")->where(["week"=>$week, "day"=>$day])->fetch();
		$sum = $dayUsage["morning"]+$dayUsage["noon"]+$dayUsage["evening"]+$dayUsage["night"];
		// just sum it
		return $sum;
	}

	/**
	 * returns patient name
	 * TODO : make it as a link
	 * @return string
	 */
	public function getPatient()
	{
		return $this->toRecord()->patient->last_name." ".$this->toRecord()->patient->first_name;
	}

    public function getPatientIns()
    {
        return $this->toRecord()->patient->pojistovna;
    }
    public function getPatientRc()
    {
        return $this->toRecord()->patient->rc;
    }

    /**
     * returns patient BD
     * @return string
     */
    public function getPatientDate()
    {
        return date("d.m.Y", strtotime($this->toRecord()->patient->birth_date));
    }

	/**
	 * returns patient name
	 * TODO : make it as a link
	 * @return string
	 */
	public function getPatientDepartment()
	{
		$dep = $this->toRecord()->patient->department;

		return $dep == NULL ? "nezařazeno" : $dep->name;

	}

	/**
	 * change the active week for the next one in circual array
	 */
	public function changeActiveWeek()
	{
		$week_ar = $this->getWeeksAr();
		$thisWeekIndex = array_search($this->activeWeek,$week_ar);
		if($thisWeekIndex == ($this->weeks-1)) //end of the loop, move to begining
		{
			$this->activeWeek = "first";
		}
		else
		{
			$this->activeWeek = $week_ar[++$thisWeekIndex]; //move to next one
		}
		//not saved. you need to save it at the place you call it
	}

	public function getMedicine()
	{
		return new Medicine($this->record->medicine);
	}

	public function getMedicineName()
	{
		return $this->record->medicine->name;
	}

	public function getSpeciality()
	{
		return $this->toRecord()->speciality->name;
	}

	/**
	 * DOES NOT count the remaingn days. just formate the output
	 * @param bool $formatToString  - TRUE (default) = return in string with formatted suffix ; FALSE = return in INT
	 * @return string
	 */
	public function getRemainingDays($formatToString = true)
	{
		if($formatToString) {
			switch (abs($this->remaining)) {
				case 0:
					$days = "dní";
					break;
				case 1:
					$days = "den";
					break;
				case (abs($this->remaining) >= 2 && abs($this->remaining) <= 4):
					$days = "dny";
					break;
				case (abs($this->remaining) == 0 || abs($this->remaining) >= 5):
					$days = "dní";
					break;
			}

			$ret_str = $this->remaining . " " . $days;
			return $ret_str;
		}
		else
		{
			return $this->remaining;
		}
	}

	/**
	 * @param $formatToString - see $this->getRemainingDays()
	 * @return float
	 */
	public function getPills($formatToString = true)
	{
		$numb = round($this->number,2);
		return ($formatToString) ? $numb." tbl" : $numb;
	}

	/**
	 * formats the date of end of usage
	 * @return bool|string
	 */
	public function getRemainingDate()
	{
		$date = date("d.m.Y");
		$returnDate = date('d.m.Y', strtotime($date. ' '.$this->remaining.' days'));
		return $returnDate;
	}


	public function getWeek($week = "first")
	{
		return $this->toRecord()->related("usage")->where(["week"=>$week])->order("day");
	}

	public function countToDate($targetDate)
	{
		$sum = 0;
		$tmpDay = strtolower(date("D")); //start couting from today
		$tmpWeek = $this->activeWeek;
		$weeks = $this->getWeeksAr();

		$today = date('Y-m-d');


		while($today < $targetDate)
		{
			$sum += $this->countDay($tmpDay,$tmpWeek);

			$tmpDay = $this->nextDay($tmpDay);

			if(array_search($tmpWeek,$weeks) == ($this->weeks - 1)) //na konci
			{
				$tmpWeek = $weeks[0];
			}
			else
			{
				$tmpWeek = $this->nextWeek($tmpWeek);
			}
			$today = date('Y-m-d', strtotime($today . ' +1 day'));
		}

//		echo "before: ".$this->number." after: ".($this->number - $sum);
//		echo "<br>";
		return $this->number - $sum;
	}
	/**
	 * counts the number of remaining days. should be called after each change of usage.
	 * @return int
	 *
	 */
	public function countRemainingDays()
	{
		$weeks = $this->getWeeksAr();

		$sum = 0;
		$remaining = 0;
		$tmpDay = strtolower(date("D")); //start couting from today
		$tmpWeek = $this->activeWeek;

		$zeros = 0;
		$max_zeros = $this->weeks*7;
		if($this->number >= 0) //gonna count to future? or to past
		{
			while($sum < $this->number)
			{
				$today_usage = $this->countDay($tmpDay,$tmpWeek);
				if($today_usage == 0)
				{
					$zeros++;
					if($zeros >= $max_zeros)
					{
//						echo "break1";
						return 0;
						break;
					}
				}
				else
				{
					$zeros = 0;
				}
				$tmpDay = $this->nextDay($tmpDay);

				if(array_search($tmpWeek,$weeks) == ($this->weeks - 1)) //na konci
				{
					$tmpWeek = $weeks[0];
				}
				else
				{
					$tmpWeek = $this->nextWeek($tmpWeek);
				}

				$sum += $today_usage;
				if($sum <= $this->number)
				{
					$remaining++;
				}
			}
		}
		else // the past. output e.g. "před -5 dny"
		{
			while($sum >= $this->number)
			{
				$today_usage = $this->countDay($tmpDay,$tmpWeek);

				if($today_usage == 0)
				{
					$zeros++;
					if($zeros >= $max_zeros)
					{
						echo "break2";
						return 0;
						break;
					}
				}
				else
				{
					$zeros = 0;
				}

				$tmpDay = $this->previousDay($tmpDay);

				if($tmpWeek == 'first') //na konci
				{
					$tmpWeek = $weeks[$this->weeks-1];
				}
				else
				{
					$tmpWeek = $this->previousWeek($tmpWeek);
				}

				$sum -= $today_usage;
				if($sum >= $this->number)
				{
					$remaining--;
				}
			}
		}

		return $remaining;

	}

	// what is the next week? because each usage has different size of loop
	private function nextWeek($thisWeek)
	{
		$weeks = $this->getWeeksAr();
		$index = array_search($thisWeek,$weeks);
		return $weeks[++$index];
	}

	// the same as ^^^ but for previous
	private function previousWeek($thisWeek)
	{
		$weeks = $this->getWeeksAr();
		$index = array_search($thisWeek,$weeks);
		return $weeks[--$index];
	}

	// next day
	private function nextDay($thisDay)
	{
		$days = ["mon","tue","wed","thu","fri","sat","sun"];
		if($thisDay == "sun")
		{
			return "mon";
		}
		else
		{
			$index = array_search($thisDay,$days);
			return $days[++$index];
		}
	}

	private function previousDay($thisDay)
	{
		$days = ["mon","tue","wed","thu","fri","sat","sun"];
		if($thisDay == "mon")
		{
			return "sun";
		}
		else
		{
			$index = array_search($thisDay,$days);
			return $days[--$index];
		}
	}

	/**
	 * if the medicine of the usage is dangerous, it should by highligeted
	 * @return string - CSS class name
	 */
	public function getDangerousClass()
	{
		if($this->toRecord()->medicine->dangerous)
		{
			return "dangerous";
		}
		else
		{
			return "";
		}
	}


	/**
	 * returns average daily usage. Average = SUM(all days) / weeks
	 * @return float|int
	 */
	public function countDailyAverage() {
		$query = $this->toRecord()->related("usage");
		$sum = $query->sum("morning") + $query->sum("noon") + $query->sum("evening") + $query->sum("night");
		$this->dailyAverage = ($sum / ($this->weeks * 7));
		return $this->dailyAverage;
	}

	/**
	 * getter for dailyAverage . dailyAverage = dailyUsage because of the getter restriction
	 * @param bool $stringFormat
	 * @return float|mixed
	 */
	public function getDailyUsage($stringFormat = false)
	{
		if($this->dailyAverage == null) // just in case...
		{
			$this->countDailyAverage();
		}
		return ($stringFormat)? round($this->dailyAverage, 2): $this->dailyAverage;
	}


    /**
     * counting predection (needed pills) to selected days
     * @param $toDate
     * @return float
     */
	public function getPrediction($toDate)
	{
		$dailyUsage = $this->getDailyUsage(false);
		$toDateObj = new DateTime($toDate);
		$diffDays = (int) $toDateObj->diff(new DateTime(), true)->format("%a")+1;
		return round(($dailyUsage * $diffDays)-$this->getPills(false), 2);
	}

}
