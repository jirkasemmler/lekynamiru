<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system 
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 20.6.15
 * Time: 23:26
 */

namespace App\Model\Entities;

use Nette\Database\NotNullConstraintViolationException;
use YetORM\Entity;
use YetORM\EntityCollection;

/**
 * Class Speciality
 * @package App\Model
 * @property-read int $id
 * @property string $name
 */
class Speciality extends Entity {


}
