<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 31.8.15
 * Time: 22:22
 */

namespace App\Model\Entities;

use YetORM\Entity;
use YetORM\EntityCollection;

/**
 * Class Log
 * @package App\Model
 * @property-read int $id
 * @property string $date
 * @property int|NULL $userId -> user_id
 * @property string $event
 * @property string $data
 * @property int|NULL $usageMetaID -> usage_meta_id
 * @property int|NULL $mc -> medical_card_usage_meta_id
 * @property int|NULL $patient -> patient_id
 */
class Log extends Entity {
	public function translate()
	{
		$trans_array = [
						"card_created"=>"Vytvořena medikační karta",
						"card_changed"=>"Upravena medikační karta",

						"added_pills"=>"Doplněna zásoba tablet",
						"added_pck"=>"Doplněna zásoba balení a tablet",

						"changed_patient_state"=>"Změna stavu pacienta",
						"plan_created"=>"Naplánována změna stavu pacienta",

						"patient_created"=>"Vytvoření pacienta",
						"patient_edited"=>"Změna pacienta",

						"usage_created"=>"Nová medikace",
						"usage_changed"=>"Úprava medikace",

						"card_deleted"=>"Smazání medikace"
		];
		return $trans_array[$this->event];
	}

	public function getUser()
	{
		return $this->toRecord()->user;
	}

	public function getUserName()
	{
		$tmp = $this->toRecord()->user;
		return $tmp == NULL ? " - neznamo - " : $tmp->name;
	}


	/**
	 * @return Patient
	 */
	public function getPatientEntity()
	{
		return new Patient($this->record->ref('patient', 'patient_id'));
	}


	public function getPatientName()
	{
		return $this->getPatientEntity()->getFullName();
	}

	public function getBranchName()
	{
		return ($this->patient == NULL) ? " - " : $this->toRecord()->patient->branche->name;
	}


	public function dataTranslate($useTags = true)
	{
		switch($this->event)
		{
			case "card_created":
			case "card_changed":
		{
			$usage = $this->toRecord()->usage_meta;
			if($usage == NULL)
			{
				$patient = " - neznamo - ";
				$medicine = " - neznamo - ";
			}
			else
			{
				$patient = $usage->patient->first_name." ".$usage->patient->last_name;
				$medicine = $usage->medicine->name;
			}


			return $useTags ? "Změna karty pacienta <b>".$patient."</b> u léku <b>".$medicine."</b>" : "Změna karty pacienta ".$patient." u léku ".$medicine."";
		;break;
		};
			case "usage_created":
			case "usage_changed":
		{


			return $this->mc == NULL ? "Úprava medikační karty" :
				($useTags) ?
					"<a href='/patient/default/".$this->toRecord()->medical_card_usage_meta->medical_card->patient_id."?mc_id=".$this->toRecord()->medical_card_usage_meta->medical_card_id."&do=ShowCard' class='btn btn-default btn-sm'><i class='fa fa-search'></i></a> ".$this->translate()." v kartě pacienta <b>".$this->toRecord()->medical_card_usage_meta->medical_card->patient->last_name." ".$this->toRecord()->medical_card_usage_meta->medical_card->patient->first_name."</b> lék: <b>".$this->toRecord()->medical_card_usage_meta->usage_meta->medicine->name."</b> "
					:
					$this->translate()." v kartě pacienta ".$this->toRecord()->medical_card_usage_meta->medical_card->patient->last_name." ".$this->toRecord()->medical_card_usage_meta->medical_card->patient->first_name." lék: ".$this->toRecord()->medical_card_usage_meta->usage_meta->medicine->name;

//			http://lnm.dev/patient/default/842?mc_id=606&do=ShowCard
		;break;
		};
			case "card_deleted":
			{
				if($useTags)
				{
					return "<a href='/patient/default/".$this->toRecord()->medical_card_usage_meta->medical_card->patient_id."?mc_id=".$this->toRecord()->medical_card_usage_meta->medical_card_id."&do=ShowCard' class='btn btn-default btn-sm'><i class='fa fa-search'></i></a> ".$this->toRecord()->medical_card_usage_meta->medical_card->comment;
				}
				else
				{
					$msg = $this->toRecord()->medical_card_usage_meta->medical_card->comment;
					$tmp = str_replace("<b>","", $msg);
					return str_replace("</b>","", $tmp);

				}
			};
			case "added_pills":
			case "added_pck":
			case "changed_patient_state":
			case "plan_created":
			case "patient_edited":
			case "patient_created":
		{
			if($useTags)
			{
				return $this->data;
			}
			else
			{
				$tmp = str_replace("<b>","", $this->data);
				return str_replace("</b>","", $tmp);

			}
		}

		}
	}
}