<?php

namespace App\Model\Entities;

use YetORM\Entity;

/**
 * Class Role
 * @package App\Model
 * @property-read int $id
 * @property string $name
 * @property string $presenter
 * @property string $action
 * @property string|NULL $do
 * @property string|NULL $params
 * @property int|NULL $parentId -> parent_id
 * @property int|NULL $order -> data_order
 */
class Permission extends Entity
{
    public $enabled = false;

    public function getParentKey()
    {
        if ($this->parentId) {
//            $tmp = $this->toRecord()->related('permission', 'parent_id')->fetchAll();
            $parent = $this->record->parent;
            return ucfirst($parent->presenter) . ':' . $parent->action;
        }
        return '';
    }

    public function getKey($removeOptional = false)
    {
        $params = $this->params;
        if ($params && $removeOptional) {
            $params = preg_replace('/\[.*\],/', '', $params);
        }

        if ( $params && strpos($params, '[') !== false) {
            $params = str_replace('[', '', $params);
            $params = str_replace(']', '', $params);
        }
        return ucfirst($this->presenter) . ':' . $this->action . (($this->do) ? 'do=' . $this->do : '') . $params;
    }

    public static function buildKey($presenter, $action, $do = null, $params = '')
    {
        if (strpos($do, 'submit') !== false) {
            // ignoring forms
            $do = null;
        }
        return ucfirst($presenter) . ':' . $action . (($do) ? 'do=' . $do : '') . (($params) ? $params : '');
    }
}
