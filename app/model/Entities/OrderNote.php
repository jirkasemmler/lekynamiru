<?php
/**
 * Created by PhpStorm.
 * User: stet
 */

namespace App\Model\Entities;

use YetORM\Entity;

/**
 * Class OrderNote
 * @package App\Model\Entities
 * @property-read int $id
 * @property int $noteType -> note_type
 * @property int $rank -> rank
 * @property string $noteText -> note_text
 */
class OrderNote extends Entity{

}