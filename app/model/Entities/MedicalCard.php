<?php
/**
 * Created by PhpStorm.
 * Jiri Semmler, Mathesio 2016
 * User: Jiri Semmler (semmler@mathesio.cz)
 * Date: 25.8.16
 * Time: 0:12
 */
namespace App\Model\Entities;

use YetORM\Entity;
use YetORM\EntityCollection;


/**
 * Class MedicalCard
 * @package App\Model
 * @property-read int $id
 * @property string $date
 * @property int $patientId -> patient_id
 * @property int|NULL $userId -> user_id
 * @property string $comment
 * @property int|NULL $revisionNumber -> revision_number
 * @property bool $active
 */
class MedicalCard extends Entity
{

	public function getOneUsage()
	{
		return $this->toRecord()->related("medical_card_usage_meta")->fetch();
	}
}