<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 20.6.15
 * Time: 23:31
 */

namespace App\Model\Entities;

use YetORM\Entity;

/**
 * Class User
 * @package App\Model
 * @property-read int    $id
 * @property string      $email
 * @property string      $name
 * @property string      $type
 * @property string      $pass
 * @property bool        $showSecretData -> show_secret_data
 * @property bool        $sendReports    -> send_reports
 * @property string|NULL $token
 * @property bool        $superAdmin     -> super_admin
 * @property bool        $showSums       -> show_sums
 * @property int|NULL    $roleId         -> role_id
 */
class User extends Entity
{
    const TYPE_ADMIN = "admin";
    const TYPE_DEPARTMENT = "department";
    const TYPE_VIEW = "view";
    const TYPE_BRANCHE = "branche";
    const TYPE_SPECIALITY = "speciality";
    const TYPE_BRANCHE_AND_DEP = "branche_department";
    private $perms;

    public function getRole()
    {
        return $this->type;
    }

    public function getUserNameRole(): string
    {
        $role = $this->toRecord()->role;
        return $role ? $role->name : '';
    }

    public function isAdmin()
    {
        return $this->isAllowed('api', ['action' => 'admin']);
//        return ($this->type == self::TYPE_ADMIN);
    }

    public function isSuperAdmin()
    {
        return ($this->superAdmin == true);
    }

    public function showSums()
    {
        return ($this->isAdmin() || $this->showSums == true);
    }

    /**
     * @return bool
     */
    public function isDepartment()
    {
        return ($this->getRole() == self::TYPE_DEPARTMENT || $this->getRole() == self::TYPE_BRANCHE_AND_DEP);
    }

    /**
     * @return bool
     */
    public function isBranche()
    {
        return ($this->getRole() == self::TYPE_BRANCHE);
    }

    public function getBranche()
    {
        return $this->toRecord()->related("branche_user");
    }

    public function getDepartments($returnArrayOfIds = false)
    {
        $records = $this->toRecord()->related("department");
        if (!$records) {
            return null;
        }
        if ($returnArrayOfIds) {
            $out = [];
            /** @var ActiveRow $records */
            foreach ($records as $dep) {
                $out[] = $dep->id;
            }
            return $out;
        } else {
            return $records;
        }
    }

    public function getUserType()
    {
        $arTrans = [
            "admin" => "Administrátor",
            "branche" => "Vrchní sestra",
            "department" => "Staniční sestra",
            "view" => "Pozorovatel",
            "speciality" => "Lékař",
            "branche_department" => "Vrchní a staniční sestra",
        ];
        return $arTrans[$this->type];
    }

    public function getUserPlaces()
    {
        $ret = [];
        switch ($this->type) {
            case "branche":
                {
                    $data = $this->toRecord()->related("branche_user");
                    foreach ($data as $item) {
                        $ret[] = $item->branche->name;
                    }
                    return $ret;
                    break;
                }

            case "department":
                {
                    $data = $this->toRecord()->related("department");
                    foreach ($data as $item) {
                        $branche = $item->branche->name;
                        $ret[] = $item->name . " (" . $branche . ")";
                    }
                    return $ret;
                    break;
                }
        }
        return $ret;
    }

    // to show in profile
    public function sendReportsTpl()
    {
        if ($this->sendReports) {
            return "<span class='fa fa-check success-color'></span> Ano";
        } else {
            return "<span class='fa fa-times warning-color'></span> Ne";
        }
    }

    /**
     * can user see the orders in menu?
     * @return bool
     */
    public function allowOrders()
    {
        return $this->isAllowed('Order', ['action' => 'default']);
    }

    public function getDepartmentsIdsForOrders()
    {
        $out = [];
        if ($this->isDepartment()) {
            foreach ($this->getDepartments() as $dep) {
                if ($dep->orders) {
                    $out[] = $dep->id;
                }
            }
        }

        return $out;
    }

    /**
     * @return Role
     */
    public function getUserRole()
    {
        return (new Role($this->record->role));
    }

    public function getPermissions()
    {
        $out = [];
        $perms = $this->getUserRole()->getPermissions();
        /**
         * @var Permission $perm
         */
        foreach ($perms as $perm) {


            if ($perm->params && strpos($perm->params, '[') !== false) {
                $key = $perm->getKey(true);
                $out[] = $key;
            }
            $key = $perm->getKey();
            $out[] = $key;

        }

        return $out;
    }

    public function isAllowed(string $presenter, $params)
    {
        if($this->superAdmin){
            return true;
        }

        if($presenter == 'Default' && $params['action'] == 'default'){
            return true;
        }

        if(!$this->perms){
            $this->perms = $this->getPermissions();
        }

        if($presenter == 'Patient' && $params['action'] == 'dead' && isset($params['do']) && $params['do'] != ''){
            $params['action'] = 'default';
        }
        
        $key = Permission::buildKey($presenter, $params['action'], isset($params['do']) ? $params['do'] : null, isset($params['params']) ? $params['params'] : '');
        return in_array($key, $this->perms);
    }
}
