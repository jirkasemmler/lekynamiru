<?php

namespace App\Model\Entities;

use YetORM\Entity;

/**
 * Class Role
 * @package App\Model
 * @property-read int $id
 * @property string $name
 * @property boolean $sendNotifications -> send_notifications
 * @property string|null $description
 */
class Role extends Entity
{
    public function getPermissions()
    {
        $permIds = [];
        $perms = $this->toRecord()->related('permission')->order('permission.data_order');
        foreach ($perms as $perm) {
            $permIds[] = new Permission($perm->permission);
        }

        return $permIds;
    }

    /**
     * @param $config
     * @param array $perms - objects indexed by ID
     *
     * @return array
     */
    public static function generateMenu($config, $perms)
    {
        $out = [];
        uasort($perms, function ($a, $b)
        {
            return $a->order > $b->order;
        });
        $configKeys = array_keys($config);
        /**
         * @var Permission $perm
         */
        foreach ($perms as $perm) {
            if (!$perm->do) {
                $key = ucfirst($perm->presenter) . ":" . $perm->action;
                if (in_array($key, $configKeys)) {
                    $tmp = $config[$key];
                    $tmp['sub'] = [];
                    if ($perm->parentId) {
                        if (isset($out[$perm->getParentKey()])) {
                            $out[$perm->getParentKey()]['sub'][] = $tmp;
                        } else {
                            $out[$perm->getParentKey()] = ['sub' => [$tmp]];
                        }
                    } else {

                        if (isset($out[$key])) {
                            $tmp2 = array_merge($tmp, $out[$key]);
                            $out[$key] = $tmp2;
                        } else {
                            $out[$key] = $tmp;
                        }
                    }
                }
            }
        }

        return $out;
    }
}
