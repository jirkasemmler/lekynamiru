<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system 
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 20.6.15
 * Time: 23:26
 */

namespace App\Model\Entities;

use YetORM\Entity;
use YetORM\EntityCollection;

/**
 * Class Can
 * @package App\Model
 * @property-read int $id
 * @property string $name
 * @property int|NULL $medicineId -> medicine_id
 */
class Can extends Entity {

	/**
	 * get medicine name
	 * @return string
	 */
	public function getMedicine()
	{

		$ret = $this->record->medicine;
		if($ret == NULL)
		{
			return "-";
		}
		else
		{
			return $ret->name;
		}
	}
}
