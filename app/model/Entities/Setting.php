<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 2.9.15
 * Time: 22:13
 */

namespace App\Model\Entities;
use Nette\Database\NotNullConstraintViolationException;
use YetORM\Entity;
use YetORM\EntityCollection;
/**
 * Class Setting
 * @package App\Model
 * @property-read int $id
 * @property string $key
 * @property string $value
 */
class Setting extends Entity {

	public function _getValue()
	{
		return $this->value;
	}
}