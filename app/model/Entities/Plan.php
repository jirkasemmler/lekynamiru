<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system 
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 15.7.15
 * Time: 23:04
 */

namespace App\Model\Entities;


use YetORM\Entity;
use YetORM\EntityCollection;

/**
 * Class Patient
 * @package App\Model
 * @property-read int $id
 * @property string $date
 * @property int $patientId -> patient_id
 * @property string $target -> target_state
 */
class Plan extends Entity {


}
