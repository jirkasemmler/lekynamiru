<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system 
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 12.7.15
 * Time: 13:43
 */

namespace App\Model\Entities;


namespace App\Model\Entities;

use YetORM\Entity;
use YetORM\EntityCollection;

/**
 * Class Patient
 * @package App\Model
 * @property-read int $id
 * @property string $firstName -> first_name
 * @property string $lastName -> last_name
 * @property string $birthDate -> birth_date
 * @property string $rc
 * @property string $pojistovna
 * @property string $state
 * @property int $brancheId -> branche_id
 * @property int|NULL $departmentId -> department_id
 */
class Patient extends Entity {

    const STATE_RUNNING = "running";
    const STATE_SLEEPING = "sleeping";
    const STATE_STOPED = "stop";
    const STATE_DEAD = "dead";

    const NON_DEAD_STATES = [self::STATE_RUNNING, self::STATE_SLEEPING, self::STATE_STOPED];

	public function getActiveMC()
	{
		return $this->toRecord()->related("medical_card")->where(["active" => true])->fetch();
	}
	/**
	 * returns name of department where is the patient. if no department -> "--"
	 * @return string
	 */
	public function getPatientsDepartment(){
		if($this->toRecord()->department == NULL)
		{
			return "--";
		}
		else
		{
			return $this->record->department->name;
		}

	}

	public function getFullName()
	{
		return $this->lastName." ".$this->firstName;
	}

	public function getDate()
	{
		return date("d.m.Y", strtotime($this->birthDate));
	}

	public function getBranche()
	{
		return $this->toRecord()->branche->name;
	}

	public function getUsages()
	{
		return $this->toRecord()->related("usage_meta");
	}
	public function getDepartment()
	{
		$dep = $this->toRecord()->department;
		if($dep != NULL)
		{
			return $dep->name;
		}
		else
		{
			return "";
		}
	}

	public function translate_state()
	{
		$dict = ["running"=>"Aktivní","sleeping"=>"Pasivní", "stop"=>"Zcela pozastaveno", "dead"=>"Zesnulý/á"];

		return $dict[$this->state];
	}
}
