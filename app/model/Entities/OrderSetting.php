<?php
/**
 * Created by PhpStorm.
 * User: stet
 * Date: 13.8.17
 * Time: 19:11
 */

namespace App\Model\Entities;

use YetORM\Entity;

/**
 * Class OrderSetting
 * @package App\Model\Entities
 * @property-read int $id
 * @property int $brancheId -> branche_id
 * @property int $dayOrder -> day_order
 * @property int $hourOrder -> hour_order
 * @property int $dayDelivery -> day_delivery
 * @property int $hourDelivery -> hour_delivery
 * @property int $extraLimit -> extra_limit
 */
class OrderSetting extends Entity
{
	private $days = ["Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota", "Neděle"];
	const ENG_DAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
	//<editor-fold desc="setter-getter">
	/**
	 * @return Usage
	 */
	public function _getBranche()
	{
		return new Usage($this->toRecord()->ref("branche_id"));
	}

	/**
	 * @param string|int $brancheId
	 */
	public function _setBranche($brancheId)
	{
		$this->brancheId = (int) $brancheId;
	}

	/**
	 * @return int|NULL
	 */
	public function _getExtraLimit()
	{
		return $this->extraLimit;
	}

	/**
	 * @param int|string $extraLimit
	 */
	public function _setExtraLimit($extraLimit)
	{
		$this->extraLimit = (int) $extraLimit;
	}

	/**
	 * @return int
	 */
	public function _getHourDelivery()
	{
		return $this->hourDelivery;
	}

	/**
	 * @param int $hourDelivery
	 */
	public function _setHourDelivery($hourDelivery)
	{
		$this->hourDelivery = $hourDelivery;
	}

	/**
	 * @return int
	 */
	public function _getDayDelivery()
	{
		return $this->dayDelivery;
	}

	/**
	 * @param int $dayDelivery
	 */
	public function _setDayDelivery($dayDelivery)
	{
		$this->dayDelivery = $dayDelivery;
	}

	/**
	 * @return int
	 */
	public function _getDayOrder()
	{
		return $this->dayOrder;
	}

	/**
	 * @param int $dayOrder
	 */
	public function _setDayOrder($dayOrder)
	{
		$this->dayOrder = $dayOrder;
	}

	/**
	 * @return int
	 */
	public function _getId()
	{
		return $this->id;
	}

	/**
	 * @return int
	 */
	public function _getHourOrder()
	{
		return $this->hourOrder;
	}

	/**
	 * @param int $hourOrder
	 */
	public function _setHourOrder($hourOrder)
	{
		$this->hourOrder = $hourOrder;
	}
	//</editor-fold>

	public function translateDay($day)
	{
		return $this->days[$day];
	}

	/**
	 * @return string
	 */
	public function getDayOrderName()
	{
		return $this->translateDay($this->_getDayOrder());
	}

	/**
	 * @return string
	 */
	public function getDayDeliveryName()
	{
		return $this->translateDay($this->_getDayDelivery());
	}
}