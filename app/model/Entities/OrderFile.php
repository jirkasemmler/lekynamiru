<?php

namespace App\Model\Entities;

use YetORM\Entity;

/**
 * Class OrderFile
 * @package App\Model
 * @property-read int $id
 * @property string $title
 * @property string $hash
 * @property string $filename
 * @property \DateTime $date -> created_at
 * @property integer $id_order
 */
class OrderFile extends Entity
{

}
