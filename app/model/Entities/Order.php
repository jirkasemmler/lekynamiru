<?php
/**
 * Created by PhpStorm.
 * User: stet
 * Date: 13.8.17
 * Time: 19:10
 */

namespace App\Model\Entities;

use App\Model\Repositories\OrderSettingRepository;
use YetORM\Entity;
use YetORM\EntityCollection;
use Latte\Engine;

/**
 * Class Order
 * @package App\Model\Entities
 * @property-read int $id
 * @property int $brancheId -> branche_id
 * @property \DateTime|NULL $closingDate -> closing_date
 * @property \DateTime|NULL $deliveryDate -> delivery_date
 * @property boolean $openedByAdmin -> opened_by_admin
 * @property boolean $isExtra -> is_extra
 * @property string $state -> state
 * @property int|NULL $orderSettingId -> order_setting_id
 */
class Order extends Entity
{
	const STATE_NEW = "new";
	const STATE_SENT = "sent";
	const STATE_CLOSED = "closed";
	const STATE_ARCHIVED = "archived";

	const STATES = [self::STATE_NEW, self::STATE_SENT, self::STATE_CLOSED, self::STATE_ARCHIVED];

//	protected $id, $brancheId, $dateSend, $isExtra, $state,
//		$deliverySetting;

	//<editor-fold desc="getter/setter">
	/**
	 * @return int
	 */
	public function _getId()
	{
		return $this->id;
	}

	/**
	 * @return int
	 */
	public function _getBrancheId()
	{
		return $this->brancheId;
	}

	/**
	 * @param int $brancheId
	 * @return Order
	 */
	public function _setBrancheId($brancheId)
	{
		$this->brancheId = $brancheId;
		return $this;
	}

	/**
	 * @return \DateTime|NULL
	 */
	public function _getClosingDate()
	{
		return $this->closingDate;
	}

	/**
	 * @param \DateTime|NULL $closingDate
	 * @return Order
	 */
	public function _setClosingDate($closingDate)
	{
		$this->closingDate = $closingDate;
		return $this;
	}

	/**
	 * @return \DateTime|NULL
	 */
	public function _getDeliveryDate()
	{
		return $this->deliveryDate;
	}

	/**
	 * @param \DateTime|NULL $deliveryDate
	 * @return Order
	 */
	public function _setDeliveryDate($deliveryDate)
	{
		$this->deliveryDate = $deliveryDate;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isIsExtra()
	{
		return $this->isExtra;
	}

	/**
	 * @param boolean $isExtra
	 * @return Order
	 */
	public function _setIsExtra($isExtra)
	{
		$this->isExtra = $isExtra;
		return $this;
	}
	/**
	 * @return boolean
	 */
	public function _getOpenedByAdmin()
	{
		return $this->openedByAdmin;
	}

	/**
	 * @param boolean $openedByAdmin
	 * @return Order
	 */
	public function _setOpenedByAdmin($openedByAdmin)
	{
		$this->openedByAdmin = $openedByAdmin;
		return $this;
	}

	/**
	 * @return string
	 */
	public function _getState()
	{
		return $this->state;
	}

	/**
	 * @param string $state
	 * @return Order
	 */
	public function _setState($state)
	{
		$this->state = $state;
		return $this;
	}

	/**
	 * @return int
	 */
	public function _getOrderSettingId()
	{
		return $this->orderSettingId;
	}

	/**
	 * @param int|string|NULL $orderSettingId
	 * @return Order
	 */
	public function _setOrderSettingId($orderSettingId)
	{
		$this->orderSettingId = (int)$orderSettingId;
		return $this;
	}
	//</editor-fold>

	/**
	 * @return OrderSetting
	 */
	public function getOrderSetting()
	{
		return new OrderSetting($this->record->order_setting);
	}

	public function getOrderItems($states = [], $brancheOnly = false)
	{
		$items = $this->toRecord()->related("order_item");
		$paramAr = [];
		if($states != [])
		{
			$paramAr['state'] = $states;
		}

		if($brancheOnly === true)
		{
			$paramAr['target_delivery'] = "branch";
		}
		elseif($brancheOnly === "pharmacy")
		{
			$paramAr['target_delivery'] = "pharmacy";
		}

		if($paramAr != [])
		{
			$items->where($paramAr);
		}

		return new EntityCollection($items, 'App\Model\Entities\OrderItem');
	}

    public function hasFiles()
    {
        return $this->toRecord()->related("order_file")->count() > 0;
	}

	/**
	 * @return Branche
	 */
	public function getBranche()
	{
		return new Branche($this->record->branche);
	}

	/**
	 * @return string
	 */
	public function getOrderClosingTimeString()
	{
		$date = new \DateTime($this->_getClosingDate());
		return $date->format(OrderSettingRepository::DATETIME_FORMAT);
	}

	public function getOrderTimeTemplate()
	{
		$latte = new Engine;
		$latte->setTempDirectory(__DIR__ . '/../../../temp');

		$parameters = [
			'hide' => ($this->isIsExtra() and $this->isNew()),
			'orderid' => $this->_getId(),
			'datetime' => new \DateTime($this->_getClosingDate()),
			'showCountDown' => ($this->isNew() && !$this->isIsExtra())
		];

		return $latte->renderToString(__DIR__ . '/../../../app/presenters/templates/Order/Components/order_closingDatetime.latte', $parameters);
	}

	/**
	 * formats and returns the delivery date for usage in templates
	 * @param string $format
	 * @return string
	 */
	public function getDeliveryDateTimeFormatted($format = OrderSettingRepository::DATETIME_USER_FORMAT_LABEL)
	{
		if($format == OrderSettingRepository::DATETIME_USER_FORMAT_LABEL && !$this->_getDeliveryDate() && $this->isIsExtra()){
			return '<i> extra</i>';
		}
		else{
			$date = new \DateTime($this->_getDeliveryDate());
			return $date->format($format);
		}
	}

	/**
	 * @return \DateTime|null
	 */
	public function getDeliveryDateTime()
	{
		$tmp = $this->_getDeliveryDate();
		return ($tmp) ? new \DateTime($this->_getDeliveryDate()) : null;
	}

	public function isSent()
	{
		return ($this->_getState() == self::STATE_SENT);
	}

	public function isNew()
	{
		return ($this->_getState() == self::STATE_NEW);
	}

	public function isClosed()
	{
		return ($this->_getState() == self::STATE_CLOSED);
	}

	public function isArchived()
	{
		return ($this->_getState() == self::STATE_ARCHIVED);
	}

	public function send()
	{
		$this->_setState(self::STATE_SENT);
	}

	public function archive()
	{
		$this->_setState(self::STATE_ARCHIVED);
	}

	public function canAddUrgent()
	{
		$closing = $this->getClosingDateWithLimit();
		$now = new \DateTime();
		$now->setTime((int)$now->format("H"),
			(int)$closing->format("i"),
			(int)$closing->format("s")
		);
		return ($now < $closing);
	}

	public function getClosingDateWithLimit()
	{
		if($this->_getOrderSettingId())
		{
			$limit = $this->getOrderSetting()->_getExtraLimit();
		}
		else
		{
			$limit = 0;
		}
		return $this->_getClosingDate()->modify('+' . $limit . ' hours');
	}

	public function getAutoArchiveDate()
	{
		return $this->_getDeliveryDate()->modify('+5 days');
	}

	/**
	 * checks if all the orderItems are solved -> can be closed
	 */
	public function canBeClosed()
	{
		$unfinishedItems = $this->record->related("order_item")->where(["state" => OrderItem::STATE_WAITING]);

		return ($this->_getState() == self::STATE_SENT and $unfinishedItems->count() == 0);
	}

	/**
	 * checks if it is before delivery -> can be opened for new entries
	 */
	public function canBeMarkedAsSent()
	{
		return ($this->_getState() == self::STATE_CLOSED and $this->getDeliveryDateTime() > new \DateTime());
	}

	public function getStatusLabel()
	{
		$latte = new Engine;

		$latte->setTempDirectory(__DIR__ . '/../../../temp');

		$html = $latte->renderToString(__DIR__ . '/../../../app/presenters/templates/Order/Components/order_label.latte', ["order" => $this]);

		return $html;
	}

	public function getFormattedName()
	{
		$date = new \DateTime($this->_getClosingDate());
		return $date->format(OrderSettingRepository::DATETIME_USER_FORMAT_PICKER);
	}
}