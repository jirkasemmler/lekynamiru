<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system 
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 20.6.15
 * Time: 23:26
 */

namespace App\Model\Entities;

use Nette\Database\NotNullConstraintViolationException;
use YetORM\Entity;
use YetORM\EntityCollection;

/**
 * Class Medicine
 * @package App\Model
 * @property-read int $id
 * @property string $name
 * @property string $sukl
 * @property string $place
 * @property bool $dangerous
 */
class Medicine extends Entity {

	// variables used for counting. do not change
	public $tretiny;
	public $pulky;
	public $cele;
	public $ctvrtky;
	public $sum;


	/**
	 * get list of packges joined by " - " -> "50 - 30 - 20"
	 * @return string
	 */
	public function getPackage()
	{
		$pckgs = $this->toRecord()->related("package");
		$ar = [];
		foreach($pckgs as $pck)
		{
			$ar[] = $pck->size;
		}
		return join(" - ",$ar);
	}

	public function getSinglePackage($packageId)
	{
		return $this->getPackageList()->where(["id"=>$packageId]);
	}

    public function getFullName()
    {
        return $this->name;
	}


	/**
	 * return list of packages which the Medicine has
	 * @return \Nette\Database\Table\GroupedSelection
	 */
	public function getPackageList()
	{
		return $this->toRecord()->related("package");

	}

	/**
	 * returns can where is the medicine. if $int -> return as INT (to forms etc) or return as a name
	 * @param bool $int
	 *
	 * @return int|string
	 */
	public function getCan($int = false)
	{
		if($this->place == "can")
		{
			$ret = $this->toRecord()->related("can")->fetch();
			if($ret == NULL)
			{
				if($int)
				{
					return 0;
				}
				return "-";
			}
			else if($int)
			{
				return $ret->id;
			}
			else
			{
				return $ret->name;
			}
		}
		else
		{
//			$placeAr = [,"FSP", "MDU"];
			if($int)
			{
				$placeAr = ["fspmdu"=>-1,"fsp"=>-2, "mdu"=>-3];
			}
			else
			{
				$placeAr = ["fspmdu"=>"FPS/MDU","fsp"=>"FSP", "mdu"=>"MDU"];
			}
			return $placeAr[$this->place];
		}

	}

	/**
	 * returns list of speciality names
	 * TODO do it as a link to one speciality
	 * @return string
	 */
	public function getSpecialities()
	{
		$data = $this->toRecord()->related("speciality_medicine");
		$ar = [];
		foreach($data as $item)
		{
			$ar[] = $item->speciality["name"];
		}
		$str = join(", ",$ar);
		return $str;
	}

	/**
	 * returns HTML element to show. if dangerous - check, if not cross
	 * @return string
	 */
	public function isDangerous($export = false)
	{
		if($export)
		{
			if($this->dangerous == true)
			{
				return 'Ano';
			}
			else
			{
				return '';
			}
		}
		else
		{
			if($this->dangerous == true)
			{
				return '<span class="fa fa-check"></span>';
			}
			else
			{
				return '<span class="fa fa-times"></span>';
			}
		}


	}
}
