<?php
/**
 * Created by PhpStorm.
 * User: stet
 * Date: 13.8.17
 * Time: 19:10
 */

namespace App\Model\Entities;

use YetORM\Entity;
use YetORM\EntityCollection;
use YetORM\Record;

/**
 * Class OrderItem
 * @package App\Model\Entities
 * @property-read int $id
 * @property int|NULL $patientId -> patient_id
 * @property int|NULL $medicineId -> medicine_id
 * @property int|NULL $packageId -> package_id
 * @property int|NULL $orderId -> order_id
 * @property int|NULL $packages
 * @property string|NULL $customMedicineName -> custom_medicine_name
 * @property string|NULL $customMedicineAmount -> custom_medicine_amount
 * @property string $medicineSource -> medicine_source
 * @property string $state
 * @property boolean $isUrgent -> is_urgent
 * @property string $targetDelivery -> target_delivery
 * @property int|NULL $followerId -> follower_id
 */
class OrderItem extends Entity
{
	const STATE_WAITING = "new";
	const STATE_APPROVED = "accepted";
	const STATE_CHANGED = "changed";
	const STATE_CANCELED = "canceled";
	const STATE_POSTPONED = "postponed";
	const STATE_DELETED = "deleted";

	const DELIVERY_BRANCH = "branch";
	const DELIVERY_PHARMACY = "pharmacy";

	const MS_CUSTOM = "custom";  // MC = Medicine Source
	const MS_SYSTEM = "system";

	public function isWaiting()
	{
		return $this->_getState() == self::STATE_WAITING;
	}
	public function isChanged()
	{
		return $this->_getState() == self::STATE_CHANGED;
	}
	public function isCanceled()
	{
		return $this->_getState() == self::STATE_CANCELED;
	}
	public function isPostponed()
	{
		return $this->_getState() == self::STATE_POSTPONED;
	}
	public function isApproved()
	{
		return $this->_getState() == self::STATE_APPROVED;
	}

	//<editor-fold desc="setter/getter">
	/**
	 * @return int
	 */
	public function _getId()
	{
		return $this->id;
	}

	/**
	 * @return int
	 */
	public function _getOrderId()
	{
		return $this->orderId;
	}

	/**
	 * @param int $orderId
	 */
	public function _setOrderId($orderId)
	{
		$this->orderId = $orderId;
	}

	/**
	 * @return int|NULL
	 */
	public function _getPatientId()
	{
		return $this->patientId;
	}

	/**
	 * @param int|NULL $patientId
	 */
	public function _setPatientId($patientId)
	{
		$this->patientId = $patientId;
	}

	/**
	 * @return int|NULL
	 */
	public function _getMedicineId()
	{
		return $this->medicineId;
	}

	/**
	 * @param int|NULL $medicineId
	 */
	public function _setMedicineId($medicineId)
	{
		$this->medicineId = $medicineId;
	}

	/**
	 * @return int|NULL
	 */
	public function _getPackageId()
	{
		return $this->packageId;
	}

	/**
	 * @param int|NULL $packageId
	 */
	public function _setPackageId($packageId)
	{
		$this->packageId = $packageId;
	}

	/**
	 * @return int|NULL
	 */
	public function _getFollowerId()
	{
		return $this->followerId;
	}

	/**
	 * @param int|NULL $followerId
	 */
	public function _setFollowerId($followerId)
	{
		$this->followerId = $followerId;
	}

	/**
	 * @return int|NULL
	 */
	public function _getPackages()
	{
		return $this->packages;
	}

	/**
	 * @param int|NULL $packages
	 */
	public function _setPackages($packages)
	{
		$this->packages = $packages;
	}

	/**
	 * @return NULL|string
	 */
	public function _getCustomMedicineName()
	{
		return $this->customMedicineName;
	}

	/**
	 * @param NULL|string $customMedicineName
	 */
	public function _setCustomMedicineName($customMedicineName)
	{
		$this->customMedicineName = $customMedicineName;
	}

	/**
	 * @return string|NULL
	 */
	public function _getCustomMedicineAmount()
	{
		return $this->customMedicineAmount;
	}

	/**
	 * @param string $customMedicineAmount
	 */
	public function _setCustomMedicineAmount($customMedicineAmount)
	{
		$this->customMedicineAmount = $customMedicineAmount;
	}

	/**
	 * @return string
	 */
	public function _getState()
	{
		return $this->state;
	}

	/**
	 * @param string $state
	 */
	public function _setState($state)
	{
		$this->state = $state;
	}

	/**
	 * @return string
	 */
	public function _getMedicineSource()
	{
		return $this->medicineSource;
	}

	/**
	 * @param string $medicineSource
	 */
	public function _setMedicineSource($medicineSource)
	{
		$this->medicineSource = $medicineSource;
	}

	/**
	 * @return boolean
	 */
	public function _isUrgent()
	{
		return $this->isUrgent;
	}

	/**
	 * @param boolean $isUrgent
	 */
	public function _setIsUrgent($isUrgent)
	{
		$this->isUrgent = $isUrgent;
	}

	/**
	 * @return string
	 */
	public function _getTargetDelivery()
	{
		return $this->targetDelivery;
	}

	/**
	 * @param string $targetDelivery
	 */
	public function _setTargetDelivery($targetDelivery)
	{
		$this->targetDelivery = $targetDelivery;
	}

	/**
	 * @return Record
	 */
	public function _getRecord()
	{
		return $this->record;
	}

	/**
	 * @param Record $record
	 */
	public function _setRecord($record)
	{
		$this->record = $record;
	}

	//</editor-fold>

	/**
	 * @return Patient
	 */
	public function getPatient()
	{
		return new Patient($this->record->patient);
	}

	/**
	 * @return Medicine
	 */
	public function getMedicine()
	{
		return new Medicine($this->record->medicine);
	}

	/**
	 * @return string
	 */
	public function getMedicineName()
	{
		if($this->_getMedicineSource() == self::MS_SYSTEM)
		{
			return $this->getMedicine()->getFullName();
		}
		else
		{
			return $this->_getCustomMedicineName();
		}
	}

	public function isCustom()
	{
		return ($this->_getMedicineSource() == self::MS_CUSTOM);
	}

	/**
	 * @return string
	 */
	public function getPatientName()
	{
		return $this->getPatient()->getFullName();
	}

	public function getOrder()
	{
		return new Order($this->record->order);
	}

	/**
	 * @param $user User
	 * @return bool
	 */
	public function canUserEdit($user)
	{
		if($user->isBranche() || $user->isAdmin())
		{
			return true;
		}
		elseif($user->isDepartment())
		{
			$availableDeps = $user->getDepartmentsIdsForOrders();
			return in_array($this->getPatient()->departmentId, $availableDeps);
		}
		else
		{
			return false;
		}
	}

	public function isToPharmacy()
	{
		return ($this->_getTargetDelivery() == self::DELIVERY_PHARMACY);
	}

	public function getPackageSize()
	{
		return ($this->toRecord()->package) ? $this->toRecord()->package->size : null;
	}
	public function getAmount()
	{
		if($this->_getMedicineSource() == self::MS_SYSTEM)
		{
			return $this->_getPackages()."x ".$this->getPackageSize();
		}
		else
		{
			return $this->_getCustomMedicineAmount();
		}
	}

	public function isCustomMedicine()
	{
		return ($this->_getMedicineSource() == self::MS_CUSTOM);
	}

	public function getFormattedState()
	{
		switch($this->_getState())
		{
			case self::STATE_APPROVED:
				$out = "Schváleno";
				break;
			case self::STATE_CANCELED:
				$out = "Zrušeno";
				break;
			case self::STATE_CHANGED:
				$out = "Zaměněno";
				break;
			case self::STATE_DELETED:
				$out = "Smazáno";
				break;
			case self::STATE_POSTPONED:
				$out = "Přesunuto";
				break;
			default:
				$out = "Čeká";
		}
		return $out;
	}


	public function getChanges()
	{
//		$omg = new EntityCollection($this->record->related("order_item_change"), 'App\Model\Entities\OrderItemChange');
//		return $omg->toArray();
		return new EntityCollection($this->record->related("order_item_change"), 'App\Model\Entities\OrderItemChange');
		// TODO muze byt prepsano s kompletni historii
	}

	public function getLastChange()
	{
		return new OrderItemChange($this->record->related("order_item_change")->order("id DESC")->limit(1)->fetch());
	}

	public function getFollower()
	{
		if($this->_getFollowerId())
			return new OrderItem($this->record->ref("order_item","follower_id"));
		else
			return null;
	}

	public function getFollowerOrderName()
	{
		$follower = $this->getFollower();
		if($follower)
			return $follower->getOrder()->getFormattedName();
		else
			return "";
	}

	public function wasPostponed()
	{
		return $this->record->related('order_item_change')->where(['type' => OrderItemChange::TYPE_POSTPONED_TO])->count() > 0;
	}
}