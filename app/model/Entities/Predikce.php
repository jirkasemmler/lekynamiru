<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system 
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 19.7.15
 * Time: 13:24
 */

namespace App\Model\Entities;

use YetORM\Entity;
use YetORM\EntityCollection;

/**
 * Class Predikce
 * @package App\Model
 * @property-read int $id
 * @property int $days
 * @property int|NULL $userId -> user_id
 * @property int $specialityId -> speciality_id
 * @property int $brancheId -> branche_id
 */
class Predikce extends Entity {


	public function getUser()
	{
		if($this->userId == NULL)
		{
			return "-";
		}
		else
		{
			return $this->record->user->name;
		}
	}


	public function getUserMail()
	{
		if($this->userId == NULL)
		{
			return "-";
		}
		else
		{
			return $this->record->user->email;
		}
	}
	public function getUserSendreport()
	{
		if($this->userId == NULL)
		{
			return false;
		}
		else
		{
			return $this->record->user->send_reports;
		}
	}

	public function getSpeciality()
	{
		if($this->specialityId == NULL)
		{
			return "-";
		}
		else
		{
			return $this->record->speciality->name;
		}
	}
}
