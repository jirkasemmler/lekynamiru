<?php
/**
 * Created by PhpStorm.
 * Jiri Semmler, FIT VUT 2015
 * User: stet (semmler@mathesio.cz)
 * Date: 12.12.15
 * Time: 13:46
 */


class SumUsage
{
	private $week_offset; // int 0 1 2 -> this , next week , next next week;


	public $data = [];

	public function __construct()
	{

		$days = ["mon", "tue","wed","thu","fri","sat","sun"];
		$parts = ["morning", "noon", "evening", "night"];
		$pills = ["one"=>0, "half"=>0, "third"=>0, "quater"=>0];
		foreach($days as $day)
		{
			$this->data[$day] = [];
			foreach($parts as $part)
			{
				$this->data[$day][$part] = $pills;
			}
		}
	}

	public function setOffset($offset)
	{
//		echo "set offset ".$offset."\n\n";
		$this->week_offset = $offset;
	}

	private function countPills(&$array, $value)
	{

		if(fmod($value, 1) == 0)
		{
			$array["one"] += $value/1;
			return $array;
		}
		elseif(fmod($value, 0.5) == 0)
		{
			$array["one"] += ($value-0.5)/1;
			$array["half"] += 1;
		}
		elseif(fmod($value, 0.25) == 0)
		{
			//it is x.75
			if(fmod($value, 0.75) == 0)
			{
				$array["one"] = ($value-0.75)/1;
				$array["half"] += 1;
				$array["quater"] += 1;
			}
			//it is x.25
			else
			{
				$array["one"] = ($value-0.25)/1;
				$array["quater"] += 1;
			}
			return $array;
		}
		elseif(fmod($value, 0.33) == 0)
		{
			if(fmod($value, 0.66) == 0)
			{
				$array["one"] += ($value-0.66)/1;
				$array["third"] += 2;
			}
			//it is x.25
			else
			{
				$array["one"] += ($value-0.33)/1;
				$array["third"] += 1;
			}
			return $array;
		}
	}

	public function setDayPart($usage_record)
	{
		$day = $usage_record->day;
//		$this->week_offset = $usage_record->day;

		$this->countPills($this->data[$day]["morning"], $usage_record->morning);
		$this->countPills($this->data[$day]["noon"], $usage_record->noon);
		$this->countPills($this->data[$day]["evening"], $usage_record->evening);
		$this->countPills($this->data[$day]["night"], $usage_record->night);
	}

	public function getDayPart($day, $dayPart)
	{
		try
		{
			return $this->data[$day][$dayPart];
		}
		catch(Exception $e)
		{
			return [];
		}
	}

	/**
	 * adds the $in to this object. what does it mean/why? I can merge (sum) more weeks to one SumUsage object and render it
	 * e.g.:
	 *    Usage 1(praktik-paralen) [ .... data .... ]
	 *    Usage 2(praktik-brufen) [.... data .... ]
	 *    create object for praktik ($praktikSumUsage)
	 *    $praktikSumUsage->mergeWith($usage1->sumUsage);
	 *    $praktikSumUsage->mergeWith($usage2->sumUsage);
	 *    in
	 * @param \SumUsage $in
	 */
	public function mergeWith(SumUsage $in)
	{
//
		$this->week_offset = $in->week_offset;
		foreach($this->data as $dayK => $dayV)
		{
			foreach($dayV as $partK => $partV)
			{
				foreach($partV as $typeK => $typeV)
				{
					$this->data[$dayK][$partK][$typeK] += $in->data[$dayK][$partK][$typeK];
				}
			}
		}
	}

	public function translateWeekOffset()
	{
		$names_of_offsets = ["tento týden", "příští týden", "za %N% týdny", "za %N% týdnů"];
		switch($this->week_offset)
		{
			case 0:
			{
				return $names_of_offsets[0];
			}
			case 1:
			{
				return $names_of_offsets[1];
			}
			case $this->week_offset > 1 && $this->week_offset < 5:
			{
				$tmp_name = str_replace("%N%", $this->week_offset, $names_of_offsets[2]);
				return $tmp_name;
			}
			default:
			{
				$tmp_name = str_replace("%N%", $this->week_offset, $names_of_offsets[3]);
				return $tmp_name;
			}
		}
	}
}
