<?php
/**
 * Created by PhpStorm.
 * Jiri Semmler, FIT VUT 2015
 * User: Jiri Semmler (semmler@mathesio.cz)
 * Date: 12.12.15
 * Time: 13:46
 */
use App\Model\Entities\Usage;

/**
 * Class Week
 * class reperezenting one week of usage
 */
class Week extends Usage
{
	private $number_of_this_week;
	private $active_week = false; //boolean is this week active or not
	public $data = [];
	public $sumUsage;
	private $number_of_weeks;
	private $number_of_active_week;
	/**
	 * @param \Nette\Database\Table\ActiveRow|null|\YetORM\Record $data
	 * @param                                                     $active_week - ID of active week of whole usage
	 * @param                                                     $weeks -> number of weeks which this usage contains
	 * @param                                                     $this_week - ID of this week. If $this_week == $active_week of usage -> then this week is active
	 */
	public function __construct($data, $active_week, $weeks, $this_week)
	{
		$this->data = $data;
		if($this_week == $active_week)
		{
			$this->active_week = true;
		}
		$this->number_of_this_week = $this_week;

		$this->number_of_weeks = $weeks;
		$this->number_of_active_week = $active_week;
	}

	public function isActive()
	{
		return $this->active_week;
	}

	/**
	 * counts pills by its type (one, half, quater, third) for this usage by selected week (active / active+1 / active+n)
	 * @used in MK (bottom lines)
	 * this has only this week but SumUsage can merge more of them together any based on these data make the final number
	 */
	public function countPillsByType()
	{
		$offset = $this->number_of_this_week - $this->number_of_active_week;
		if($offset < 0)
		{
			$offset = $this->number_of_weeks + $offset;
		}
		$this->sumUsage = new SumUsage();
		$this->sumUsage->setOffset($offset);

		foreach($this->data as $item)
		{
			$this->sumUsage->setDayPart($item);
		}
	}


}