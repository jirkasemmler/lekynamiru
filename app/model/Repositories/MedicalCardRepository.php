<?php
/**
 * Created by PhpStorm.
 * Jiri Semmler, Mathesio 2016
 * User: Jiri Semmler (semmler@mathesio.cz)
 * Date: 23.8.16
 * Time: 23:41
 */

namespace App\Model\Repositories;


use App\Model\Entities\MedicalCard;
use App\Model\Entities\Usage;
use YetORM\Repository;
use App\Model\Entities\Log;

/**
 * Class MedicalCardRepository
 * @table medical_card
 * @entity MedicalCard
 */
class MedicalCardRepository extends Repository
{

	public function createFirstCard($patient_id, $usages, $userID = 1)
	{
		/** @var MedicalCard $card */
		$card = $this->createEntity();
		$card->revisionNumber = 0;
		$card->patientId = $patient_id;
		$card->date = date('Y-m-d H:i:s');
		$card->userId = $userID;
		$card->comment = "Inicializace";

		$this->persist($card);

		foreach($usages as $usage)
		{
			$this->getTable("medical_card_usage_meta")->insert(["usage_meta_id" => $usage->id, "medical_card_id" => $card->id]);

		}
	}
	/**
	 * returns last active medical card for selected patient
	 *
	 * @param $patientID
	 *
	 * @return MedicalCard $card
	 */
	public function getLastCard($patientID)
	{
		$cards = $this->findBy(["patient_id" => $patientID])->orderBy("id DESC")->limit(1);

		foreach($cards as $card)
		{
			return $card;
		}
	}

	public function addUsage($karta, $usage)
	{
//		add selected usageID to this MedicalCard
		return $this->getTable("medical_card_usage_meta")->insert(["usage_meta_id" => $usage->id, "medical_card_id" => $karta->id]);

	}

	/**
	 * creates new revision of card and copies previous usages to new card
	 *
	 * @param MedicalCard $lastCard - MedicalCard Entity
	 *
	 * @param             $userID
	 *
	 * @return \App\Model\Entities\MedicalCard
	 */
	public function makeNewCard($lastCard, $userID, $usage_revision_src, $comment = "", $exclude = null)
	{
//		get list of usages for last card
//		create new card object
//		persist new card
//		exclude $usageID
//		join previous usages to new card

		$where_ar = ["usage_meta.revision_source != ?"=> $usage_revision_src];
		if($exclude != NULL)
		{
			$where_ar = ["usage_meta.id != ?"=> $exclude]; // for deleting
		}

		@$lastCard->active = false;
		$this->persist($lastCard);
		$connections = $lastCard->toRecord()
			->related("medical_card_usage_meta")
			->where($where_ar);

		/**
		 * @var MedicalCard $karta
		 * @var Usage $item
		 * @var
		 */
		$karta = $this->createEntity();

		$karta->date = date('Y-m-d H:i:s');
		$karta->comment = $comment;
		$karta->patientId = $lastCard->patientId;
		$karta->revisionNumber = $lastCard->revisionNumber + 1;
		$karta->userId = $userID;
		$karta->active = true;

		$this->persist($karta);

		// vazani starych usage
		foreach($connections as $item)
		{
			$this->getTable("medical_card_usage_meta")->insert(["usage_meta_id" => $item->usage_meta_id, "medical_card_id" => $karta->id]);
		}

		return $karta;
	}


}