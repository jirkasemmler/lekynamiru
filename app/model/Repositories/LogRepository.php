<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 31.8.15
 * Time: 22:27
 */

namespace App\Model\Repositories;

use YetORM\Repository;
use App\Model\Entities\Log;

/**
 * Class LogRepository
 * @table log
 * @entity Log
 */
class LogRepository extends Repository{

	/**
	 * used in API only
	 */
	public function setMedicalCards()
	{
		$data = $this->findBy(["event"=>["usage_created", "usage_changed"]]);
		foreach($data as $item)
		{
			if(preg_match("/([0-9]+)-([0-9]+)/", $item->data, $output_array))
			{
//				$ret["mc_id"]."-".$ret["usage_id"]
				$usage = $output_array[2];
				$card = $output_array[1];

				$id = $this->getTable("medical_card_usage_meta")->where(["medical_card_id"=>$card, "usage_meta_id"=>$usage])->fetch()->id;

				$item->mc = $id;
				$this->persist($item);

			}
		}

	}

	public function create_edit_patient($type, $user_id, $user_name, $patient_name, $target_type, $department_name, $patient_id)
	{
		/**
		 * @var $log Log
		 */
		$log = $this->createEntity();
		$target_ar = ['running'=>"Aktivní",'sleeping'=>"Pasivní",'stop'=>"Pozastaveno",'dead'=>"Zesnulý"];
		if(!array_key_exists($target_type, $target_ar))
		{
			echo "nenalezen cil<br>";
			return false;
		}

		$log->event = $type;
		$log->userId = $user_id;
		if($type == "patient_edited")
		{
			$log->data = "Uživatel <b>".$user_name."</b> editoval pacienta <b>".$patient_name."</b> do stavu <b>".$target_ar[$target_type]."</b> a oddělení <b>".$department_name."</b>";
		}
		else
		{
			$log->data = "Uživatel <b>".$user_name."</b> vytvořil pacienta <b>".$patient_name."</b> ve stavu <b>".$target_ar[$target_type]."</b> a oddělení <b>".$department_name."</b>";

		}
		$log->date = date("Y-m-d H:i:s");
		$log->mc = NULL;
		$log->patient = $patient_id;
		$this->persist($log);
		return true;
	}

	public function translate_state($state)
	{
		$target_ar = ['running'=>"Aktivní",'sleeping'=>"Pasivní",'stop'=>"Pozastaveno",'dead'=>"Zesnulý"];
		return $target_ar[$state];
	}

	public function create_log_plan($type, $user_id, $user_name, $patient_name, $change_date, $target_type, $patient_id)
	{
		/**
		 * @var $log Log
		 */
		$log = $this->createEntity();
		$target_ar = ['running'=>"Aktivní",'sleeping'=>"Pasivní",'stop'=>"Pozastaveno",'dead'=>"Zesnulý"];
		if(!array_key_exists($target_type, $target_ar))
		{
			echo "nenalezen cil<br>";
			return false;
		}

		$log->event = $type;
		$log->userId = $user_id;
		$log->data = "Uživatel <b>".$user_name."</b> naplánoval pacientovi <b>".$patient_name."</b> změnu stavu na  na <b>".$target_ar[$target_type]."</b> dne ".$change_date;
		$log->date = date("Y-m-d H:i:s");
		$log->mc = NULL;
		$log->patient = $patient_id;
		$this->persist($log);
	}
	public function changedByPlan($patient, $target, $patient_id)
	{
		/**
		 * @var $log Log
		 */
		$log = $this->createEntity();
		$target_ar = ['running'=>"Aktivní",'sleeping'=>"Pasivní",'stop'=>"Pozastaveno",'dead'=>"Zesnulý"];
		if(!array_key_exists($target, $target_ar))
		{
			echo "nenalezen cil<br>";
			return false;
		}

		$log->event = "changed_patient_state";
		$log->userId = NULL;
		$log->data = "Stav pacienta <b>".$patient."</b> byl změněn na <b>".$target_ar[$target]."</b>.";
		$log->date = date("Y-m-d H:i:s");
		$log->mc = NULL;
		$log->patient = $patient_id;
		$this->persist($log);
	}

	public function create_log_mc($log_type, $user, $data, $patient_id, $msg = "")
	{
		/**
		 * @var $log Log
		 */
		$log = $this->createEntity();

		$log->event = $log_type;
		$log->userId = $user;
		$log->data = (string) $msg;
		$log->date = date("Y-m-d H:i:s");
		$log->mc = (int) $data->id;
		$log->patient = $patient_id;
		$this->persist($log);
	}
	public function create_log_addpills($log_type, $user, $data , $patient_id)
	{
		/** @var Log $log */
		$log = $this->createEntity();

		$log->event = $log_type;
		$log->userId = $user;
		$log->data = (string) $data;
		$log->date = date("Y-m-d H:i:s");
		$log->patient = $patient_id;
		$this->persist($log);
	}
}