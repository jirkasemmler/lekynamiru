<?php
/**
 */


namespace App\Model\Repositories;


use YetORM\Repository;
use App\Model\Entities\OrderFile;

/**
 * Class OrderFileRepository
 * @table order_file
 * @entity OrderFile
 */
class OrderFileRepository extends Repository
{
    public function del($orderFileID)
    {
        $orderFile = $this->getByID($orderFileID);

        try {
            $this->delete($orderFile);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function findForOrder($orderFileId)
    {
        return $this->findBy(['id_order' => $orderFileId]);
    }
}
