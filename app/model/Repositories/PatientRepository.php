<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system 
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 12.7.15
 * Time: 13:43
 */


namespace App\Model\Repositories;

use App\Model\Entities\Patient;
use Nette\Neon\Exception;
use YetORM\Repository;
use YetORM\Entity;

/**
 * Class PatientRepository
 * @table patient
 * @entity Patient
 */
class PatientRepository extends Repository
{

	public function manage($values, $branch = null)
	{
		$usageStatRepository = new UsageStatRepository($this->database);
		$MCRepository = new MedicalCardRepository($this->database);


		if($values->id == 0) //new
		{
			$patient = $this->createEntity();
		}
		else
		{
			$patient = $this->getByID($values->id);
		}
//		if($values->full == "true")
//		{
			try
			{
				$date = date_create_from_format("d.m.Y", $values->birthDate);
				if($date == false)
				{
					return false;
				}
				$patient->birthDate = $date->format("Y-m-d");
			}
			catch(\Exception $e)
			{
				return false;
			}

			$patient->firstName = $values->firstName;
			$patient->lastName = $values->lastName;
			$patient->rc = $values->rc;
			$patient->pojistovna = $values->pojistovna;
//		}

		$patient->brancheId = $branch ? : (int) $_SESSION["branche"];
		$patient->state = $values->state;

		if($values->department == "")
		{
			$department = NULL;
		}
		else
		{
			$department = $values->department;
		}
		$patient->departmentId = $department;
		try {
			$this->persist($patient);

			$department = $patient->departmentId;
			foreach($patient->getUsages() as $usage)
			{
				$usageStatRepository->updateUsageStat($usage->medicine_id, $department);
			}

			if($values->id == 0)
			{
				$MCRepository->createFirstCard($patient->id, [], $values->userID);
			}
//			$values->id =
		}
		catch(\Exception $e)
		{
			return false;
		}
		return $patient->id;
	}

	public function getPatients($idBranche, $idDepartment = NULL, $state = null)
	{
		$dep_filter = [];
		if($idDepartment != NULL)
		{
			$dep_filter["department_id"] = $idDepartment;
		}
		$filter_array = ["branche_id"=>$idBranche];
		$filter_array = array_merge($filter_array,$dep_filter);
		if($state != NULL)
		{
			$filter_array["state"] = $state;
		}
		return $this->findBy($filter_array);
	}

	public function del($id)
	{
		$usageStatRepository = new UsageStatRepository($this->database);

		$patient = $this->getByID($id);

		try{
			$department = $patient->departmentId;
			$medicines = [];
			foreach($patient->getUsages() as $usage)
			{
				$medicines[] = $usage->medicine_id;
			}
			$this->delete($patient);

			foreach($medicines as $medicine)
			{
				$usageStatRepository->updateUsageStat($medicine, $department);
			}

			return true;
		}
		catch(\Exception $e)
		{
			return false;
		}

	}

	public function changePatientState($id, $state)
	{
		$usageStatRepository = new UsageStatRepository($this->database);

		$patient = $this->getByID($id);
		$patient->state = $state;

		try
		{
			$this->persist($patient);

			$department = $patient->departmentId;
			foreach($patient->getUsages() as $usage)
			{
				$usageStatRepository->updateUsageStat($usage->medicine_id, $department);
			}

			return true;
		}
		catch(\Exception $e)
		{
			return false;
		}
	}

	/**
	 * @return \YetORM\EntityCollection|Patient[]
	 */
	function findBy(array $criteria)
	{
		$selection = $this->getTable()->where($criteria);
		return $this->createCollection($selection);
	}

	/**
	 *
	 * @return \YetORM\EntityCollection|Patient[]
	 */
	function findAll()
	{
		return $this->findBy(array());
	}

	/**
	 * @param array $criteria
	 * @return NULL|Patient
	 */
	function getBy(array $criteria)
	{
		return parent::getBy($criteria);
	}

	/**
	 * @param mixed $id
	 * @return Patient
	 */
	public function getByID($id)
	{
		return parent::getByID($id);
	}

	// ------------------------------------ import!!
	public function zabPacienti(){
		$this->getTable("patient")->delete();
	}
	public function tmpInsert($id, $first, $last, $datum, $rc, $poj, $oddeleni,$stav)
	{
		if($oddeleni == "")
		{
			echo "$first - $last<br>";
			return false;
		}
		$odd = $this->getTable("department")->where(["name"=>$oddeleni])->fetch();
echo "odd '".$oddeleni."' ".$odd["branche_id"]." <br>";
		$this->getTable("patient")->insert([
			"id"=>$id,
			"first_name"=>$first,
			"last_name"=>$last,
			"birth_date"=>$datum,
			"rc"=>$rc,
			"pojistovna"=>$poj,
			"branche_id"=>$odd["branche_id"],
			"department_id"=>$odd["id"],
			"state"=>$stav,
		]);
	}

	public function zabUsage()
	{
		$this->getTable("usage_meta")->delete();
		$this->getTable("usage")->delete();
	}
	public function insertUsage($id, $pac, $lek , $odb , $number,$start, $leky)
	{
//		* @property-read int $id
//	* @property string $startDate -> start_date
//	* @property int $medicineId -> medicine_id
//	* @property int $specialityId -> speciality_id
//	* @property int $patientId -> patient_id
//	* @property double $doubleweekly
//	* @property double $number
//	* @property bool $showTwoWeeks
//	* @property string $activeWeek -> active_week
//	* @property int $weeks
//	* @property int $remaining -> remaining_days
//	* @property string $state

		$odbornost = $this->getTable("speciality")->select("*")->where(["name"=>$odb])->fetch();

		$data = ["id"=>$id,
		"start_date"=>$start,
		"medicine_id"=>$lek,
		"speciality_id"=>$odbornost["id"],
		"patient_id"=>$pac,
		"number"=>$number,
		"showTwoWeeks"=>false,
		"active_week"=>"first",
		"weeks"=>2,
		];
		try{
			$usg = $this->getTable("usage_meta")->insert($data);
		}
		catch(\Exception $e)
		{
			echo $e->getCode()."<br>";
			print_r($data);
			echo "----------------------<br>";
			return false;
		}


		$weeks = ["first", "second"];
		foreach($weeks as $week)
		{
			foreach($leky as $key => $item)
			{
				$dat = ["day"=>$key, "week"=>$week, "morning"=>$item[0], "noon"=>$item[1], "evening"=>$item[2], "night"=>$item[3], "usage_meta_id"=>$usg["id"]];

				$this->getTable("usage")->insert($dat);
			}
		}
		return $usg["id"];
	}
}
