<?php
/**
 * Created by PhpStorm.
 * Jiri Semmler, Mathesio 2016
 * User: Jiri Semmler (semmler@mathesio.cz)
 * Date: 2.6.16
 * Time: 22:20
 */

namespace App\Model\Repositories;

use App\Model\Entities\Medicine;
use YetORM\Repository;
use App\Model\Entities\UsageStat;

/**
 * Class UsageStatRepository
 * @table usage_stats
 * @entity UsageStat
 */
class UsageStatRepository extends Repository
{
    // todo presununout vypocet spotreby z medicine repository sem a pouzit u toho tabulku usage_stat
    /** @var UsageRepository @inject */
    protected $usageRepository;

    /** @var MedicineRepository @inject */
    protected $medicineRepository;

    public function setDB() {
        $this->usageRepository = new UsageRepository($this->database);
        $this->medicineRepository = new MedicineRepository($this->database);

    }

    public function countDepartment($department, $period) {
        if ($this->usageRepository == null) {
            $this->setDB();
        }
        $leky = $this->medicineRepository->findAll();

//        $this->begin();
        foreach ($leky as $lek) {
            $this->medicineRecount($lek, $department, $period);
        }

//        $this->commit();
    }

    public function updateUsageStat($medicine_id, $department_id) {
        if ($this->usageRepository == null) {
            $this->setDB();
        }

        $periods = [2, 4, 6, 8, 10, 12, 16];

        $lek = $this->medicineRepository->getByID($medicine_id);
        foreach ($periods as $period) {
            $this->medicineRecount($lek, $department_id, $period);
        }
    }

    public function flushTable() {
        $this->getTable()->select("*")->delete();
    }

    public function medicineRecount($lek, $department, $period) {
        $davky = ["morning", "noon", "evening", "night"];

        $ctvrt_sum = 0;
        $tret_sum = 0;
        $pul_sum = 0;
        $cele_sum = 0;

        $ar_filter = ["medicine_id" => $lek->id, "patient.state" => "running"];

        if ($department != 0) {
            $ar_filter["patient.department_id"] = $department;
        }
        // "pocitam" i smazane aby se pregenerovaly i tyto spotreby. pokud je to deleted tak to ve vysledku preskocim
        $spotreby_meta = $this->usageRepository->findSrcBy($ar_filter);
        $pole = [
            'first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth', 'eleventh',
            'twelfth'
        ];

        foreach ($spotreby_meta as $spotreba_meta) {
            if ($spotreba_meta->deleted == true || $spotreba_meta->active == false) {
                continue;
            }
            $active = $spotreba_meta->activeWeek;
            $index = array_search($active, $pole);

            for ($n = 0; $n < $period; $n++) {
                $countedWeek = $index + $n;

                $countedWeekEffective = $countedWeek % $spotreba_meta->weeks;

                $spotreby = $spotreba_meta->toRecord()->related("usage")
                    ->where(["week" => $pole[$countedWeekEffective]]);

                foreach ($spotreby as $spotreba) {
                    foreach ($davky as $davka) {
                        if (fmod($spotreba[$davka], 0.25) == 0) {
                            $cele = ($spotreba[$davka] - fmod($spotreba[$davka], 1)) / 1;
                            $cele_sum += $cele;
                            $zbytek = $spotreba[$davka] - $cele;

                            $pulky = ($zbytek - fmod($zbytek, 0.5)) / 0.5;
                            $pul_sum += $pulky;
                            $zbytek = $zbytek - $pulky * 0.5;

                            if ($zbytek != 0) {
                                $ctvrt_sum++;
                            }
                        }
                        else {
                            $zbytek = $spotreba[$davka] % 1;
                            $cele = ($spotreba[$davka] - $zbytek) / 1;
                            if ($zbytek < 0.5) {
                                $tretin = 1;
                            }
                            else {
                                $tretin = 2;
                            }
                            $tret_sum += $tretin;
                        }
                    }
                }
            }

        }

        $lek->cele = $cele_sum;
        $lek->pulky = $pul_sum;
        $lek->ctvrtky = $ctvrt_sum;
        $lek->tretiny = $tret_sum;

        $this->setRecord($department, $period, $lek);

    }

    protected function setRecord($department, $weekDelta, $medicine) {
        /**
         * @var $record UsageStat
         */
        $record = $this->getBy([
            "week_delta" => $weekDelta, "department_id" => $department, "medicine_id" => $medicine->id
        ]);
        if ($this->shouldWriteStat($medicine, $record)) {

            if ($record == null) {
                // doesn't exist yet
                $record = $this->createEntity();
                $record->setMedicine($medicine);
                $record->setDepartment($department);
                $record->setWeekDelta($weekDelta);
            }

            $record->setValues($medicine->cele, $medicine->pulky, $medicine->ctvrtky, $medicine->tretiny);

            $this->persist($record);
        }
        else {
            // it is empty but it exists, so it should be deleted
            if ($record != null) {
                $this->delete($record);
            }
        }
    }

    /**
     * checks if the object of UsageStat should be persisted
     * true - stat changed (new or changed)
     * false - values are 0 or didnt change from the previous obj
     * @param Medicine $medicine
     * @param UsageStat|null $usageStat
     * @return bool
     */
    private function shouldWriteStat(Medicine $medicine, $usageStat) {
        if ($this->usageStatInMedicineIsNotEmpty($medicine)
            && ($usageStat === null
                || ($medicine->cele != $usageStat->full || $medicine->pulky != $usageStat->half
                    || $medicine->ctvrtky != $usageStat->quater || $medicine->tretiny != $usageStat->third)
            )
        ) {
            return true;
        }
        else {
            return false;
        }
    }

    private function usageStatInMedicineIsNotEmpty(Medicine $medicine) {
        return (array_sum([$medicine->cele, $medicine->pulky, $medicine->ctvrtky, $medicine->tretiny]) > 0);
    }

    public function getUsageStat($branche, $department, $period) {

        if ($branche == 0 and $department == null) {
            $selection = $this->getTable("usage_stats")->select("usage_stats.id,
			SUM(half) AS halfs,
			SUM(quater) AS quaters,
			sum(third) AS thirds,
			SUM(full) AS fulls,
			usage_stats.medicine_id")->group("medicine_id")->where("week_delta", $period);
        }
        elseif ($branche != 0 and $department == null) {
            $selection = $this->getTable("usage_stats")->select("usage_stats.id,
			SUM(half) AS halfs,
			SUM(quater) AS quaters,
			sum(third) AS thirds,
			SUM(full) AS fulls,
			usage_stats.medicine_id")->group("medicine_id")->where([
                "week_delta" => $period, "department.branche_id" => $branche
            ]);
        }
        else {
            $selection = $this->getTable("usage_stats")->select("usage_stats.id,
			SUM(half) AS halfs,
			SUM(quater) AS quaters,
			sum(third) AS thirds,
			SUM(full) AS fulls,
			usage_stats.medicine_id")->group("medicine_id")->where([
                "week_delta" => $period, "department_id" => $department
            ]);

        }
        $data = $this->createCollection($selection);

        return $data;
    }
}