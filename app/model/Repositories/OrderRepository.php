<?php
/**
 * Created by PhpStorm.
 * User: stet
 * Date: 13.8.17
 * Time: 19:58
 */

namespace App\Model\Repositories;

use App\Model\Entities\Order;
use App\Model\Entities\OrderItem;
use App\Model\Entities\OrderItemChange;
use App\Model\Entities\OrderNote;
use App\Model\Entities\OrderSetting;
use model\Exceptions\DataException;
use Nette\Neon\Exception;
use YetORM\EntityCollection;
use YetORM\Repository;
use YetORM\Entity;

/**
 * Class OrderRepository
 * @table order
 * @entity Order
 */
class OrderRepository extends Repository
{

	public function del($orderId)
	{
		$order = $this->getByID($orderId);

		try
		{
			$this->delete($order);
			return true;
		}
		catch(\Exception $e)
		{
			return false;
		}
	}

	/**
	 * called by cron, auto closing
	 * @return EntityCollection
	 */
	public function sendNewOrdersAuto()
	{
		$now = new \DateTime();
		$ordersToClose = $this->findBy(['state' => Order::STATE_NEW,
			"closing_date < '".$now->format("Y-m-d H:i:s")."'",
			'is_extra' => false,
			'opened_by_admin' => false]);

		// filtering orders which are to be closed by time. extra and opened orders should not be closed automatically
		/**
		 * @var $order Order
		 */
		foreach($ordersToClose as $order)
		{
			$this->sendOrder($order);
			// notification is triggered from api presenter
		}

		return $ordersToClose;
	}

	public function archiveClosedOrders()
	{
		$now = new \DateTime();
		$ordersToArchive = $this->findBy(['state' => Order::STATE_CLOSED, 'delivery_date' < $now]);

		$out = [];
		/**
		 * @var $order Order
		 */
		foreach($ordersToArchive as $order)
		{
			if($order->getAutoArchiveDate() < $now)
			{
				$this->archiveOrder($order);
				$out[] = $order;
			}
		}
		return $out;
	}


	public function findExtra($brancheId)
	{
		return $this->findBy(['is_extra' => true, 'branche_id' => $brancheId])->orderBy('id', 'desc');
	}

	public function createOrder($orderSettingId, $closingDate, $brancheId)
	{

		/** @var $order Order */
		$order = $this->createEntity();
		$order->_setBrancheId((int)$brancheId);
		if($orderSettingId)
		{
			$order->_setOrderSettingId($orderSettingId);
		}
		$order->_setState(Order::STATE_NEW);


		$this->persist($order);

		if($closingDate)
		{
			$closingDateObj = \DateTime::createFromFormat(OrderSettingRepository::DATETIME_FORMAT, $closingDate);
			$order->_setClosingDate($closingDateObj);
			$this->persist($order);
			$closingDateObj->modify('next ' . OrderSetting::ENG_DAYS[$order->getOrderSetting()->_getDayDelivery()]);
			$closingDateObj->setTime($order->getOrderSetting()->_getHourDelivery(), 0);

			$order->_setDeliveryDate($closingDateObj);
			$this->persist($order);
		}

		return $order;
	}

	/**
	 * @param $order Order
	 * @return Order
	 */
	public function sendOrder($order)
	{
		$order->send();
		$this->persist($order);
		return $order;
	}

	/**
	 * @param $order Order
	 * @return Order
	 */
	public function archiveOrder($order)
	{
		$order->archive();
		$this->persist($order);
		return $order;
	}

	/**
	 * @param $order Order
	 * @return Order
	 */
	public function setAsExtra($order)
	{
		$order->_setIsExtra(true);
		$order->_setClosingDate(new \DateTime());
		$this->persist($order);
		return $order;
	}

	/**
	 * returns EntityCollection of orders for all branches or for the selected one. returns the open ones only from admin perspective ( = SENT)
	 * @param int|null $brancheId
	 * @param bool $archived
	 * @return \YetORM\EntityCollection
	 */
	public function findOrdersForAdmin($brancheId = null, $archived = false)
	{
		if($archived)
		{
			$states = [Order::STATE_ARCHIVED];
		}
		else
		{
			$states = [Order::STATE_SENT, Order::STATE_CLOSED];
		}
		$filterOptions = ['state' => $states];
		if($brancheId)
		{
			$filterOptions['branche_id'] = $brancheId;
		}
		return $this->findBy($filterOptions)->orderBy("closing_date", EntityCollection::DESC);
	}

	/**
	 * setting opened_by_admin to true
	 * @param $orderId
	 */
	public function openByAdmin($orderId)
	{
		/**
		 * @var $order Order
		 */
		$order = $this->getByID($orderId);
		$order->_setOpenedByAdmin(true);
		$this->persist($order);
	}

	/**
	 * @param $orderId
	 * @param $newStatus
	 * @param null $deliveryDate
	 * @return Order
	 * @throws DataException
	 */
	public function changeState($orderId, $newStatus, $deliveryDate = null)
	{
		if(!in_array($newStatus, Order::STATES))
		{
			throw new DataException("Unknown state of order to change to");
		}

		/**
		 * @var $order Order
		 */
		$order = $this->getByID($orderId);

		// TODO doresit. zatim jsem to zakomentoval. puvodni byl ten druhy, pak jsem zkusil 1
//		if(($newStatus == Order::STATE_SENT and !$order->canBeMarkedAsSent() and $order->isClosed()) or ($newStatus == Order::STATE_CLOSED and !$order->canBeClosed()))
////		if(($newStatus == Order::STATE_SENT and !$order->canBeMarkedAsSent()) or ($newStatus == Order::STATE_CLOSED and !$order->canBeClosed()))
//		{
//			throw new DataException("Can not change order to state of '" . $newStatus . "'");
//		}

		if($deliveryDate)
		{
			$deliveryDateObj = \DateTime::createFromFormat(OrderSettingRepository::DATETIME_USER_FORMAT_PICKER, $deliveryDate);
			$order->_setDeliveryDate($deliveryDateObj);
		}

		// ok, change
		$order->_setState($newStatus);
		$this->persist($order);
		return $order;

	}

}

/**
 * Class OrderItemRepository
 * @table order_item
 * @entity OrderItem
 */
class OrderItemRepository extends Repository
{
	public function manage($data, $order)
	{
		/**
		 * @var OrderItem $orderItem
		 */
		$orderItem = ($data->orderItemId == 0) ? $this->createEntity() : $this->getByID($data->orderItemId);

		$orderItem->_setPatientId($data->patient);
		$orderItem->_setOrderId($order->id);
		$orderItem->_setTargetDelivery($data->targetDelivery);
		$orderItem->_setIsUrgent($data->urgent);

		$orderItem->_setMedicineSource($data->customOrSystem);
		if($data->customOrSystem == OrderItem::MS_SYSTEM)
		{
			$orderItem->_setMedicineId($data->medicine);
			$orderItem->_setPackageId((int)$data->medicinePackageChose);
			$orderItem->_setPackages((int)$data->medicinePackageNumber);
		}
		else
		{
			$orderItem->_setCustomMedicineName($data->customMedicine);
			$orderItem->_setCustomMedicineAmount($data->customMedicineAmount);
		}


		$this->persist($orderItem);

		return $orderItem;
	}

	public function del($orderItemId)
	{
		$orderItem = $this->getByID($orderItemId);

		try
		{
			$this->delete($orderItem);
			return true;
		}
		catch(\Exception $e)
		{
			return false;
		}
	}

	public function approve($orderItemId)
	{
		/**
		 * @var OrderItem $orderItem
		 */
		$orderItem = $this->getByID($orderItemId);

		$orderItem->_setState(OrderItem::STATE_APPROVED);
		$this->persist($orderItem);

		return $orderItem;
	}

	public function cancelOrderItem($values)
	{
		/**
		 * @var OrderItem $orderItem
		 */
		$orderItem = $this->getByID($values->id);
		$orderItem->_setState(OrderItem::STATE_CANCELED);
		$this->persist($orderItem);
		return $orderItem;
	}

	/**
	 * @param $values
	 * @return OrderItem
	 * @throws DataException
	 */
	public function changeOrderItem($values)
	{
		/** @var OrderItem $orderItem */
		$orderItem = $this->getByID($values->order_item_id);
		$orderItem->_setState(OrderItem::STATE_CHANGED);
		if($values->medicine_type == OrderItem::MS_CUSTOM)
		{
			$orderItem->_setCustomMedicineAmount($values->new_custom_medicine_amount);
			$orderItem->_setCustomMedicineName($values->new_custom_medicine_name);
			$this->persist($orderItem);

			return $orderItem;
		}
		elseif($values->medicine_type == OrderItem::MS_SYSTEM)
		{
			$orderItem->_setMedicineId($values->medicine);
			$orderItem->_setPackageId((int)$values->medicinePackageChose);
			$orderItem->_setPackages((int)$values->medicinePackageNumber);
			$this->persist($orderItem);

			return $orderItem;
		}
		else
		{
			throw new DataException("unknown type of medicine_type");
		}
	}

	/**
	 * @param $orderItem OrderItem
	 * @param $order Order
	 * @return OrderItem follower
	 */
	public function postponeOrderItem($orderItem, $order)
	{
		$tmpState = $orderItem->_getState();
		$orderItem->_setState(OrderItem::STATE_POSTPONED);
		/**
		 * @var OrderItem $follower
		 */
		$follower = $this->createEntity();
		$follower->_setState($tmpState);
		$follower->_setOrderId($order->_getId());
		$follower->_setPatientId($orderItem->_getPatientId());
		$follower->_setMedicineId($orderItem->_getMedicineId());
		$follower->_setPackageId($orderItem->_getPackageId());
		$follower->_setPackages($orderItem->_getPackages());
		$follower->_setCustomMedicineName($orderItem->_getCustomMedicineName());
		$follower->_setCustomMedicineAmount($orderItem->_getCustomMedicineAmount());
		$follower->_setMedicineSource($orderItem->_getMedicineSource());
		$follower->_setIsUrgent($orderItem->_isUrgent());
		$follower->_setTargetDelivery($orderItem->_getTargetDelivery());
		$this->persist($follower);

		$orderItem->_setFollowerId($follower->_getId());
		$this->persist($orderItem);

		return $follower;
	}



	public function getUrgentItems($orderID)
	{
		return $this->findBy(['order_id' => $orderID, 'is_urgent' => true]);
	}

	public function getAcceptedItems($orderID)
	{
		return $this->findBy(['order_id' => $orderID, 'state' => OrderItem::STATE_APPROVED]);
	}

	public function getChangedItems($orderID)
	{
		return $this->findBy(['order_id' => $orderID, 'state' => OrderItem::STATE_CHANGED]);

	}

	public function getCanceledItems($orderID)
	{
		return $this->findBy(['order_id' => $orderID, 'state' => OrderItem::STATE_CANCELED]);
	}

	public function getPharmacyItems($orderID)
	{
		return $this->findBy(['order_id' => $orderID, 'target_delivery' => OrderItem::DELIVERY_PHARMACY]);
	}

	public function getBranchItems($orderID)
	{
		return $this->findBy(['order_id' => $orderID, 'target_delivery' => OrderItem::DELIVERY_BRANCH]);
	}
}

/**
 * Class OrderSettingRepository
 * @table order_setting
 * @entity OrderSetting
 */
class OrderSettingRepository extends Repository
{
	const DATETIME_FORMAT = "Y-m-d_H:i";
	const DATETIME_USER_FORMAT_PICKER = "d/m/Y";
	const DATETIME_USER_FORMAT_LABEL = "d. m. Y H:i";

//	const DATETIME_USER_FORMAT = "d/m/Y H:i";  // TODO implement datetimepicker

	public function manage($data)
	{
		$sameSetting = $this->findBy(['branche_id' => $data->brancheId,
			'day_order' => $data->dayOrder,
			'hour_order' => $data->hourOrder
		]);
		if($data->orderSettingId == 0 and $sameSetting->count() > 0)
		{
			throw new DataException("multiple settings");
		}
		/**
		 * @var OrderSetting $orderItem
		 */
		$orderItem = ($data->orderSettingId == 0) ? $this->createEntity() : $this->getByID($data->orderSettingId);

		$orderItem->_setBranche($data->brancheId);
		$orderItem->_setDayOrder($data->dayOrder);
		$orderItem->_setHourOrder($data->hourOrder);
		$orderItem->_setDayDelivery($data->dayDelivery);
		$orderItem->_setHourDelivery($data->hourDelivery);
		$orderItem->_setExtraLimit($data->extraLimit);

		$this->persist($orderItem);

		return $orderItem;
	}

	public function del($id)
	{
		$setting = $this->getByID($id);
		$this->delete($setting);

		return true;
	}

	public function getFollowingSlots($brancheId, $openOnly = false)
	{
		$slots = $this->findBy(['branche_id' => $brancheId]);
		$slotsArray = [];
		$slotsDayArray = [0 => [], 1 => [], 2 => [], 3 => [], 4 => [], 5 => [], 6 => []];  // Mon-Sun. Mon = 0
		$existingOrders = [];
		$usedOrders = [];
		/** @var OrderSetting $slot */
		foreach($slots as $slot)
		{
			$slotsArray[$slot->_getDayOrder() . "#" . $slot->_getHourOrder()] = $slot;
			$slotsDayArray[$slot->_getDayOrder()][] = $slot;

			$existingOrdersData = $slot->toRecord()->related("order");
			foreach($existingOrdersData as $order)
			{
				$order = new Order($order);
				$existingOrders[$order->getOrderClosingTimeString()] = $order;

			}
		}

		$out = [];

		$date = new \DateTime();

		foreach(range(0, 20) as $deltaDay)  // such pythonic
		{
			$keyOfDay = $date->format("N") - 1;

			if(array_key_exists($keyOfDay, $slotsDayArray))
			{
				$slotsOfDay = $slotsDayArray[$keyOfDay];

				foreach($slotsOfDay as $slot)
				{
					$date->setTime($slot->_getHourOrder(), 0);
					$dateString = $date->format(self::DATETIME_FORMAT);
					$tmp = ["setting" => $slot,
						"order" => null,
						"orderDateTime" => clone $date
					];

					if(array_key_exists($dateString, $existingOrders))
					{
						$tmp['order'] = $existingOrders[$dateString];
						$usedOrders[] = $existingOrders[$dateString]->id;
						if($openOnly and !$existingOrders[$dateString]->isNew())
						{
							continue;
						}

					}
					$out[$dateString] = $tmp;
				}
			}
			$date->modify('+1 day');
		}

		$orderRepository = new OrderRepository($this->database);

		$extras = $orderRepository->findExtra($brancheId);

		/**
		 * @var $extra Order
		 */
		foreach($extras as $extra)
		{
			$key = $extra->_getClosingDate()->format(self::DATETIME_FORMAT) . $extra->id;
			$tmp = ["setting" => null,
				"order" => $extra,
				"orderDateTime" => null
			];
			$out[$key] = $tmp;
		}

		$notUsedFilter = ['branche_id' => $brancheId];
		if(count($usedOrders) > 0)
		{
			$notUsedFilter['id NOT IN'] = $usedOrders;
		}
		$notUsed = $orderRepository->findBy($notUsedFilter);

		/**
		 * @var $notUsedOrder Order
		 */
		foreach($notUsed as $notUsedOrder)
		{
			// this and extra key must be the same
			$key = $notUsedOrder->_getClosingDate()->format(self::DATETIME_FORMAT) . $notUsedOrder->id;
			$tmp = ["setting" => null,
				"order" => $notUsedOrder,
				"orderDateTime" => null
			];
			$out[$key] = $tmp;
		}
		krsort($out);  // sorting by key to get the latest first
		return $out;
	}

}

/**
 * Class OrderItemChangeRepository
 * @table order_item_change
 * @entity OrderItemChange
 */
class OrderItemChangeRepository extends Repository
{
	/**
	 * @param $orderItem OrderItem
	 * @param $comment string
	 * @param $userId
	 * @return OrderItemChange
	 */
	public function createChangeForCancelItem($orderItem, $comment, $userId)
	{
		/**
		 * @var OrderItemChange $orderItemChange
		 */
		$orderItemChange = $this->createEntity();
		$orderItemChange->_setOrderItemId($orderItem->_getId());
		$orderItemChange->_setType(OrderItemChange::TYPE_CANCELED);
		$orderItemChange->_setComment($comment);
		$orderItemChange->_setUserId($userId);
		$orderItemChange->_setDate(new \DateTime());

		$this->persist($orderItemChange);

		return $orderItemChange;
	}

	/**
	 * @param $orderItem OrderItem
	 * @param $values
	 * @param $userId
	 * @param null|OrderItem $orderItemBeforeModification
	 * @return OrderItemChange
	 */
	public function createChangeForChangeOrderItem($orderItem, $values, $userId, $orderItemBeforeModification = null)
	{
		/**
		 * @var OrderItemChange $orderItemChange
		 */
		$orderItemChange = $this->createEntity();

		$orderItemChange->_setOrderItemId($orderItem->_getId());
		$orderItemChange->_setType(OrderItemChange::TYPE_MEDICINE_CHANGE);
		$orderItemChange->_setComment($values->comment);
		$orderItemChange->_setUserId($userId);
		$orderItemChange->_setDate(new \DateTime());

		if($values->medicine_type == "custom")
		{
			$orderItemChange->_setMedicineNameFrom($values->old_custom_medicine_name);
			$orderItemChange->_setMedicineNameTo($values->new_custom_medicine_name);

			$orderItemChange->_setMedicineAmountFrom($values->old_custom_medicine_amount);
			$orderItemChange->_setMedicineAmountTo($values->new_custom_medicine_amount);

		}
		else
		{
			$orderItemChange->_setMedicineIdFrom($orderItemBeforeModification->_getMedicineId());
			$orderItemChange->_setMedicineAmountFrom($orderItemBeforeModification->getAmount());

			$orderItemChange->_setMedicineIdTo($orderItem->_getMedicineId());
			$orderItemChange->_setMedicineAmountTo($orderItem->getAmount());
		}


		$this->persist($orderItemChange);
		return $orderItemChange;
	}

	/**
	 * @param $orderItem OrderItem
	 * @param $followerOrderItem OrderItem
	 * @param $newOrder Order
	 * @param $values
	 * @param $userId
	 * @return OrderItemChange
	 */
	public function createChangeForPostponeOrderItem($orderItem, $followerOrderItem, $newOrder, $values, $userId)
	{
		/**
		 * @var OrderItemChange $orderItemChange
		 */
		$orderItemChange = $this->createEntity();

		$orderItemChange->_setOrderItemId($orderItem->_getId());
		$orderItemChange->_setType(OrderItemChange::TYPE_POSTPONED_FROM);
		$orderItemChange->_setComment($values->comment);
		$orderItemChange->_setUserId($userId);
		$orderItemChange->_setDate(new \DateTime());

		$orderItemChange->_setOrderIdTo($newOrder->_getId());
		$orderItemChange->_setOrderIdFrom($orderItem->_getOrderId());

		$this->persist($orderItemChange);

		$orderItemChange = $this->createEntity();

		$orderItemChange->_setOrderItemId($followerOrderItem->_getId());
		$orderItemChange->_setType(OrderItemChange::TYPE_POSTPONED_TO);
		$orderItemChange->_setComment($values->comment);
		$orderItemChange->_setUserId($userId);
		$orderItemChange->_setDate(new \DateTime());

		$orderItemChange->_setOrderIdTo($newOrder->_getId());
		$orderItemChange->_setOrderIdFrom($orderItem->_getOrderId());

		$this->persist($orderItemChange);

		return $orderItemChange;
	}
}

/**
 * Class OrderNoteRepository
 * @table order_note
 * @entity OrderNote
 */
class OrderNoteRepository extends Repository
{
	private $availableTypes = ['change', 'cancel', 'postpone'];

	/**
	 * @param $type
	 * @return EntityCollection
	 * @throws \Exception
	 */
	public function findNotesFor($type)
	{
		if(in_array($type, $this->availableTypes)){
			return $this->findBy(['note_type' => $type])->orderBy('rank');
		}
		else{
			throw new Exception('unknown type '.$type);
		}
	}

	/**
	 * @param $type
	 * @return string[]
	 */
	public function findStringNotesFor($type)
	{
		$notes = $this->findNotesFor($type);
		$out = [];
		/**
		 * @var $note OrderNote
		 */
		foreach($notes as $note){
			$out[$note->noteText] = $note->noteText;
		}
		return $out;
	}
}