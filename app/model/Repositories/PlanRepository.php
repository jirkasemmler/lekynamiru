<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system 
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 15.7.15
 * Time: 23:04
 */

namespace App\Model\Repositories;


use App\Model\Entities\Patient;
use App\Model\Entities\Plan;
use Nette\Neon\Exception;
use YetORM\Repository;
use YetORM\Entity;
use App\Model\Repositories\PatientRepository;
use App\Model\Repositories\LogRepository;
/**
 * Class PlanRepository
 * @table plan
 * @entity Plan
 */
class PlanRepository extends Repository
{
	public function translate($target){

	}
	public function runTodaysPlan()
	{
		$patientRepository = new PatientRepository($this->database);
		$logRepository = new LogRepository($this->database);
		$today = date("Y-m-d");
		$plans = $this->findBy(["date"=>$today]);
		foreach($plans as $plan)
		{
			if($patientRepository->changePatientState($plan->patientId, $plan->target))
			{
				$patient = $patientRepository->getByID($plan->patientId);
				$logRepository->changedByPlan($patient->getFullname(), $plan->target, $patient->id);
				$this->delete($plan);
			}
			else
			{
				return false;
			}
		}
		return true;
	}
	public function planAdd($values)
	{
		$date = date_create_from_format("d.m.Y", $values->date);
		if($date == false)
		{
			return false;
		}

		$plan = $this->createEntity();
		$plan->patientId = (int) $values->id_patient;
		$plan->target = $values->target;
		$plan->date = $date->format("Y-m-d");
		try
		{
			$this->persist($plan);
			return true;
		}
		catch(\Exception $e)
		{
			return false;
		}
	}

	public function del($id)
	{
		$item = $this->getByID($id);
		try
		{
			$this->delete($item);
			return true;
		}
		catch(\Exception $e){
			return false;
		}
	}
}
