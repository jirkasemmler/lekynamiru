<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 20.6.15
 * Time: 23:28
 */

namespace App\Model\Repositories;

use App\Model\Entities\Medicine;
use Nette\Neon\Exception;
use YetORM\Repository;
use YetORM\Entity;
use App\Model\Repositories\UsageRepository;

/**
 * Class MedicineRepository
 * @table medicine
 * @entity Medicine
 */
class MedicineRepository extends Repository {


    public function magic($filters) {
        return $this->getTable('medicine')->where($filters);
    }

    public function countPillsMagic($branche = 0, $department = 0, $period) {
        $usageRepository = new UsageRepository($this->database);
        $davky = ["morning", "noon", "evening", "night"];

        $leky = $this->findAll();
        $ret = [];

        foreach ($leky as $lek) {
            $ctvrt_sum = 0;
            $tret_sum = 0;
            $pul_sum = 0;
            $cele_sum = 0;

            $ar_filter = ["medicine_id" => $lek->id, "patient.state" => "running"];
            if ($branche != 0) {
                $ar_filter["patient.branche_id"] = $branche;
            }

            if ($department != 0) {
                $ar_filter["patient.department_id"] = $department;
            }
            $spotreby_meta = $usageRepository->findBy($ar_filter);

            if ($spotreby_meta->count() == 0) {
                continue;
            }

            $pole = [
                'first',
                'second',
                'third',
                'fourth',
                'fifth',
                'sixth',
                'seventh',
                'eighth',
                'ninth',
                'tenth',
                'eleventh',
                'twelfth'
            ];
            foreach ($spotreby_meta as $spotreba_meta) {
                $active = $spotreba_meta->activeWeek;
                $index = array_search($active, $pole);

                for ($n = 0; $n < $period; $n++) {
                    $countedWeek = $index + $n;

                    $countedWeekEffective = $countedWeek % $spotreba_meta->weeks;

                    $spotreby = $spotreba_meta->toRecord()->related("usage")->where(["week" => $pole[$countedWeekEffective]]);

                    foreach ($spotreby as $spotreba) {
                        foreach ($davky as $davka) {
                            if (fmod($spotreba[$davka], 0.25) == 0) {
                                $cele = ($spotreba[$davka] - fmod($spotreba[$davka], 1)) / 1;
                                $cele_sum += $cele;
                                $zbytek = $spotreba[$davka] - $cele;

                                $pulky = ($zbytek - fmod($zbytek, 0.5)) / 0.5;
                                $pul_sum += $pulky;
                                $zbytek = $zbytek - $pulky * 0.5;

                                if ($zbytek != 0) {
                                    $ctvrt_sum++;
                                }
                            } else {
                                $zbytek = $spotreba[$davka] % 1;
                                $cele = ($spotreba[$davka] - $zbytek) / 1;
                                if ($zbytek < 0.5) {
                                    $tretin = 1;
                                } else {
                                    $tretin = 2;
                                }
                                $tret_sum += $tretin;
                            }
                        }
                    }
                }

            }
            $lek->cele = $cele_sum;
            $lek->pulky = $pul_sum;
            $lek->ctvrtky = $ctvrt_sum;
            $lek->tretiny = $tret_sum;

            $lek->sum = ($cele_sum + ($pul_sum * 0.5) + ($ctvrt_sum * 0.25) + round($tret_sum / 3, 2));
            $ret[] = $lek;
        }
        return $ret;
    }

    public function getPackages($id) {

        return $this->getByID($id)->toRecord()->related("package")->select("size");
    }

    public function getPackagesArray($id) {
        $ret = [];
        $data = $this->getByID($id)->toRecord()->related("package")->select("size");
        foreach ($data as $item) {
            $ret[$item->size] = $item->size;

        }

        return $ret;

    }

    public function setPackages($medId, $sizes) {
        foreach ($sizes as $val) {
            $this->getTable("package")->insert(["size" => $val, "medicine_id" => $medId]);
        }
    }

    /**
     * get list of medicines which you can add to Medical Card
     * returns list of all medicines grouped by speciality but medicines which are already used (in speciality) are
     * excluded
     * @used in patient -> add to medical card
     * @return array
     */
    public function getCardMedicine($id_patient) {
        $ret = [];
        $usageRepository = new UsageRepository($this->database);

        $usages = $usageRepository->findBy(["patient_id" => $id_patient]);

        /*
         * prepare array usage_ar (medicines which patient uses) which is [<speciality_id>:[<medicineId>,<medicineId>,<medicineId>...],...]
         *
         */
        $usages_ar = [];
        foreach ($usages as $usage) {
            if (in_array($usage->specialityId, array_keys($usages_ar))) {
                $usages_ar[$usage->specialityId][] = $usage->medicineId;
            } else {
                $usages_ar[$usage->specialityId] = [$usage->medicineId];
            }
        }


        $specialities = $this->getTable("speciality")->select("*");

        foreach ($specialities as $speciality) {
            $tmpAr = [];
            $medicines = $this->getTable("speciality_medicine")->select("*")->where(["speciality_id" => $speciality->id])->order("medicine.name");
            foreach ($medicines as $medicine) {

                //does patient use this medicine in this peciality? if yes, skip it
                if (in_array($speciality->id, array_keys($usages_ar)) and in_array($medicine->medicine->id,
                        $usages_ar[$speciality->id])) {
                    continue;
                }

                $key = $medicine->speciality_id . "-" . $medicine->medicine->id;
                $tmpAr[$key] = $medicine->medicine->name;
            }

            $ret[$speciality->name] = $tmpAr;

        }

        return $ret;
    }

    public function manage($data) {
        if ($data->id == 0) {
            return $this->insert($data);
        } else {
            return $this->update($data);
        }

    }

    public function insert($data) {
        $medicine = $this->createEntity();
        $medicine->name = $data->name;
        $medicine->sukl = $data->sukl;
        $medicine->place = "can";

        if (isset($_POST["dangerous"])) {
            $medicine->dangerous = true;
        }

        try {
            $this->persist($medicine, $data->can, $data->specialities);
            $this->getTable("package")->where(["medicine_id" => $medicine->id])->delete();
            foreach ($_POST as $key => $val) {
                if (strpos($key, "options") !== false && $val > 0) {
                    $this->getTable("package")->insert(["size" => $val, "medicine_id" => $medicine->id]);
                }
            }
            return $medicine;
        } catch (\Exception $e) {
            return 23000;
        }
    }

    public function update($data) {
        $medicine = $this->getByID($data->id);
        $medicine->name = $data->name;
        $medicine->sukl = $data->sukl;
        $medicine->place = "can";


        if (isset($_POST["dangerous"])) {
            $medicine->dangerous = true;
        } else {
            $medicine->dangerous = false;
        }
        try {
            $this->persist($medicine, $data->can, $data->specialities);
            $this->getTable("package")->where(["medicine_id" => $medicine->id])->delete();

            foreach ($_POST as $key => $val) {
                if (strpos($key, "options") !== false && $val > 0) {
                    $this->getTable("package")->insert(["size" => $val, "medicine_id" => $medicine->id]);
                }
            }

            return $medicine;
        } catch (\Exception $e) {
            return 23000;
        }
    }

    public function getMedicines() {
        return $this->findAll();
    }


    public function getMedicinesWithCans() {
        $all = $this->findAll()->orderBy("name");
        $ret = [];
        $ret[0] = "-";
        foreach ($all as $item) {
            $ret[$item->id] = $item->name . " (" . $item->getCan() . ")";
        }

        return $ret;
    }

    public function persist(Entity $medicine, $canId = null, $specialities = null) {
        $this->transaction(function () use ($medicine, $canId, $specialities) {
            parent::persist($medicine); //write just the user

            if ($canId > 0) {
                $this->getTable("can")->where(["medicine_id" => $medicine->id])->update(["medicine_id" => null]);
                $this->getTable("can")->where(["id" => $canId])->update(["medicine_id" => $medicine->id]);
            } else {
                $place = '';
                switch ($canId) {
                    case "-1": {
                        $place = "fspmdu";
                    };
                        break;
                    case "-2": {
                        $place = "fsp";
                    };
                        break;
                    case "-3": {
                        $place = "mdu";
                    };
                        break;
                }

                $medicine->place = $place;
                parent::persist($medicine);
                $this->getTable("can")->where(["medicine_id" => $medicine->id])->update(["medicine_id" => null]);
            }

            $this->getTable("speciality_medicine")->where(["medicine_id" => $medicine->id])->delete();
            foreach ($specialities as $speciality) {
                $this->getTable("speciality_medicine")->insert([
                    "speciality_id" => $speciality,
                    "medicine_id" => $medicine->id
                ]);

            }
        });
        return $medicine;
    }

    public function delMedicine($id) {
        $medicine = $this->getByID($id);

        try {
            $this->delete($medicine);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getSpecialites($id) {
        $ret = [];
        $data = $this->getTable("speciality_medicine")
            ->select("speciality_id")
            ->where("medicine_id", $id);
        foreach ($data as $item) {
            $ret[] = $item->speciality_id;
        }
        return $ret;
    }

    public function changeSpecialityOfUsages($medineId, $specialities)
    {
        $specId = null;
        if(count($specialities) == 1){
            $specId = $specialities[0]->id;
        }

        if(count($specialities) > 1){
            foreach($specialities as $s){
                if($s->name == 'obecná'){
                    $specId = $s->id;
                    break;
                }
            }
        }
        if($specId){
            $this->getTable('usage_meta')->where(['medicine_id' => $medineId])->update(['speciality_id' => $specId]);
        }
    }

}
