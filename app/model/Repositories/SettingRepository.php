<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 2.9.15
 * Time: 22:14
 */

namespace App\Model\Repositories;
use YetORM\Repository;
use App\Model\Entities\Setting;
/**
 * Class SettingRepository
 * @table settings
 * @entity Setting
 */
class SettingRepository extends Repository
{
	const KEY_NOTIFICATION_EMAIL = "notifikacni_email";

	public function manage($values)
	{
		$set = $this->getByID($values->id);
		$set->value = $values->value;

		$this->persist($set);

		return true;
	}

	public function isStaging() {
		/** @var Setting $value */
		$value = $this->getBy(['key'=>'staging']);
		return ($value->value == 'yes');
	}

	public function getNotificationEmail()
	{
		/** @var Setting $setting */
		$setting = $this->getBy(["key"=>self::KEY_NOTIFICATION_EMAIL]);
		return $setting->_getValue();
	}
}