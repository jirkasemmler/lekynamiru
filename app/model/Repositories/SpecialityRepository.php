<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 21.6.15
 * Time: 22:25
 */

namespace App\Model\Repositories;

use YetORM\Repository;
use App\Model\Entities\Speciality;

/**
 * Class SpecialityRepository
 * @table speciality
 * @entity Speciality
 */
class SpecialityRepository extends Repository {

    public function manage($data) {
        if ($data->id == 0) {
            //insert
            $speciality = $this->createEntity();
            $speciality->name = $data->name;
        } else {
            //update
            $speciality = $this->getByID($data->id);
            $speciality->name = $data->name;
        }

        try {
            $this->persist($speciality);
            return $speciality;
        } catch (\Exception $e) {
            return $e->getCode();
        }
    }

    public function upsertByName($name) {
        $obj = $this->getBy(['name' => $name]);
        $id = $obj ? $obj->id : 0;
        $data = (object)['id' => $id, 'name' => $name];
        return $this->manage($data);
    }

    public function getAll($arr = false) {
        if ($arr) {
            $ret = [];
            $all = $this->findAll();
            foreach ($all as $item) {
                $ret[$item->id] = $item->name;
            }
            return $ret;
        } else {
            return $this->findAll();
        }
    }


    public function delSpeciality($id) {
        $speciality = $this->getByID($id);

        try {
            $this->delete($speciality);
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }
}
