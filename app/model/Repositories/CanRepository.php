<?php

namespace App\Model\Repositories;

use YetORM\Repository;
use App\Model\Entities\Can;

/**
 * Class CanRepository
 * @table  can
 * @entity Can
 */
class CanRepository extends Repository
{
    /**
     * completely manage the Can Entity - add|update
     *
     * @param $data
     *
     * @return bool|int|mixed
     */
    public function manage($data)
    {
        if ($data->id == 0) //insert
        {
            $can = $this->createEntity();
            $can->name = $data->name;
            $can->medicineId = ($data->medicine == 0) ? null : $data->medicine;
        } else //update
        {
            $can = $this->getByID($data->id);
            $can->name = $data->name;
            $can->medicineId = ($data->medicine == 0) ? null : $data->medicine;
            if ($data->medicine > 0) //it is can
            {
                $this->getTable("medicine")->where(["id" => $data->medicine])->update(["place" => "can"]);
            } else {
                $this->getTable("medicine")->where(["id" => $data->medicineOld])->update(["place" => "fspmdu"]);
            }
        }

        try {
            $this->persist($can);
            return true;
        } catch (\Exception $e) {
            return $e->getCode();
        }
    }

    public function getAll()
    {
        return $this->findAll();
    }

    /**
     * get list of cans with medicines which are in them
     * @return array
     */
    public function getCansWithMedicines()
    {
        //“FSP”, “MDU” “FPS/MDU”
        $all = $this->findAll();
        $ret = []; // begining of the array are the first ones - implecite wones
        $ret[-1] = "FSP/MDU";
        $ret[-2] = "FSP";
        $ret[-3] = "MDU";
        foreach ($all as $item) {
            //for next cans get the medicine
            // Medicine:Cans <=> 1:N
            $ret[$item->id] = $item->name . " (" . $item->getMedicine() . ")";
        }

        return $ret;
    }

    public function delCan($id)
    {
        $can = $this->getByID($id);

        try {
            $this->delete($can);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
