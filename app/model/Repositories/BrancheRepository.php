<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 27.6.15
 * Time: 15:53
 */

namespace App\Model\Repositories;

use App\Model\Entities\User;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\PredikceRepository;
use App\Model\Entities\Branche;
use Nette\Neon\Exception;
use Tracy\Logger;
use YetORM\Repository;
use YetORM\Entity;

/**
 * Class BrancheRepository
 * @table branche
 * @entity Branche
 */
class BrancheRepository extends Repository
{
    /**
     * @param UserRepository $userRepository
     */
    public $userRepository;
    /**
     * @param PredikceRepository $predikceRepository
     */
    public $predikceRepository;

    /**
     * get list of departments of $id Branche
     *
     * @param int $id
     *
     * @return mixed - collection of departments
     */
    public function getDepartments($id = 0)
    {
        if ($id == 0) {
            //this vvv getDepartments if implemeted in Usage class(entity)
            return $this->getByID($_SESSION["branche"])->getDepartments(false);
        } else {
            return $this->getByID($id)->getDepartments(false);
        }
    }

    /**
     * get all the list of branches
     * @return \YetORM\EntityCollection
     */
    public function getBranches()
    {
        return $this->findAll();
    }

    /**
     * manage insert / update branche - everything with the users etc
     *
     * @param $data - data from form
     *
     * @return bool - success?
     */
    public function manage($data)
    {
        $this->userRepository = new UserRepository($this->database);
        $this->predikceRepository = new PredikceRepository($this->database);

        /**
         * @var $branche Branche
         */
        if ($data->id == 0) //new -> insert
        {
            $branche = $this->createEntity();
        } else //update
        {
            $branche = $this->getByID($data->id);
        }

        $branche->name = $data->name;
        if (isset($_POST["export"])) {
            $branche->export = true;
        } else {
            $branche->export = false;
        }

        if (isset($_POST["orders"])) {
            $branche->orders = true;
        } else {
            $branche->orders = false;
        }

        $this->persist($branche);
        $assocUsers = $_POST["associated_users"];
        $users_tmp = (isset($_POST["users"])) ? $_POST["users"] : [];
        $oldUsersTmp = (isset($_POST["oldUsers"])) ? $_POST["oldUsers"] : [];

        $newUsers = array_values($users_tmp);
        $oldUsers = array_values(json_decode($oldUsersTmp));
        $deletedUsers = array_diff($oldUsers, $newUsers); //tito byli odebrani -> zmenit na pozorovatele
        $addedUsers = array_diff($newUsers, $oldUsers); //tito byli pridani -> zmenit na branche
        if ($data->id == 0 || (isset($_POST["changeUser"]))) {
            $this->getTable("branche_user")
                 ->where(["user_id" => $deletedUsers, "branche_id" => $branche->id])
                 ->delete();

            foreach ($addedUsers as $addedUser) {
                $this->getTable("branche_user")->insert(["user_id" => $addedUser, "branche_id" => $branche->id]);
                if ($this->getTable("department")->select("*")->where(["user_id" => $addedUser])->fetch() != null) {
                    $type_to_change_to = "branche_department";
                } else {
                    $type_to_change_to = "branche";
                }
                $this->userRepository->changeType($addedUser, $type_to_change_to);
            }
            foreach ($deletedUsers as $deletedUser) {
//					$this->getTable("branche_user")->insert(["user_id"=>$deletedUser,"branche_id"=>$branche->id]);
                if ($this->getTable("department")->select("*")->where(["user_id" => $deletedUser])->fetch() != null) {
                    $type_to_change_to = "department";
                } else {
                    $type_to_change_to = "view";
                }
                $this->userRepository->changeType($deletedUser, $type_to_change_to);
            }
        }

        $this->getTable('associated_users')->where(["branch_id" => $branche->id])->delete();
        foreach ($assocUsers as $assocUser){
            $this->getTable("associated_users")->insert(["user_id" => $assocUser, "branch_id" => $branche->id]);
        }

        $this->predikceRepository->checkBranche($branche->id);

        return true;
    }

    //delete branche and change users
    public function delBranche($id)
    {
        $this->userRepository = new UserRepository($this->database);
        $branche = $this->getByID($id);

        $brancheUsers = $this->getTable("branche_user")->where(["branche_id" => $branche->id]);
        foreach ($brancheUsers as $brancheUser) {
            $this->userRepository->changeType($brancheUser["user_id"], "view");
            $this->getTable("branche_user")->where(["id" => $brancheUser["id"]])->delete();
        }
        try {

            $this->delete($branche);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * get one department of the branche
     *
     * @param $id - id of department to get
     *
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getDepartment($id)
    {
        return $this->getTable("department")->select("*")->where(["id" => $id])->fetch();
    }

    public function getDepartmentAr($id)
    {
        return $this->getTable("department")->select("*")->where(["id" => $id]);
    }

    public function manageDepartment($data)
    {
        if (isset($_POST["export"])) {
            $export = true;
        } else {
            $export = false;
        }

        if (isset($_POST["orders"])) {
            $orders = true;
        } else {
            $orders = false;
        }


        if ($data->id == 0) //insert
        {
            $row = $this->getTable("department")->insert([
                "name" => $data->name,
                "branche_id" => $data->idBranche,
                "export" => $export,
                "orders" => $orders,
            ]);
            if ($data->user != 0) //the user is set
            {
                $this->userRepository = new UserRepository($this->database);
                $this->getTable("department")->where(["id" => $row->id])->update(["user_id" => $data->user]);
                $this->userRepository->changeType($data->user, "department");
            }
        } else {
            $row = $this->getTable("department")->where(["id" => $data->id])->update([
                "name" => $data->name,
                "branche_id" => $data->idBranche,
                "export" => $export,
                "orders" => $orders,
            ]);

            if (isset($_POST["changeDepUser"])) //the user is set
            {
                if ($data->user != $data->userOld) {
                    if ($data->user == 0) {
                        $data->user = null;
                    }


                    $this->userRepository = new UserRepository($this->database);
                    $this->getTable("department")->where(["id" => $data->id])->update(["user_id" => $data->user]);


                    if ($this->getTable("branche_user")->select("*")->where(["user_id" => $data->userOld])->fetch() !=
                        null) {
                        if ($this->getTable("department")->select("*")->where(["user_id" => $data->userOld])->fetch() !=
                            null) {
                            $type_to_change_to_old = "branche_department";
                        } else {
                            $type_to_change_to_old = "branche";
                        }
                    } else {
                        if ($this->getTable("department")->select("*")->where(["user_id" => $data->userOld])->fetch() !=
                            null) {
                            $type_to_change_to_old = "department";
                        } else {
                            $type_to_change_to_old = "view";
                        }
                    }
                    $this->userRepository->changeType($data->userOld, $type_to_change_to_old);

                    if ($this->getTable("branche_user")->select("*")->where(["user_id" => $data->user])->fetch() !=
                        null) {
                        $type_to_change_to_new = "branche_department";
                    } else {
                        $type_to_change_to_new = "department";
                    }
                    $this->userRepository->changeType($data->user, $type_to_change_to_new);
                }
            }
        }
        return true;
    }

    public function delDepartment($id)
    {
        $this->userRepository = new UserRepository($this->database);
        $dep = $this->getTable("department")->select("*")->where(["id" => $id])->fetch();

        try {
            $this->userRepository->changeType($dep["user_id"], "view");
            $this->getTable("department")->where(["id" => $id])->delete();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function zabho()
    {
        $this->getTable("branche")->delete();
        $this->getTable("branche_user")->delete();
        $this->getTable("user")->where("id > ?", 1)->delete();
        $this->getTable("department")->delete();
        $this->getTable("speciality")->delete();
        $this->getTable("medicine")->delete();
        $this->getTable("speciality_medicine")->delete();
    }

    public function tmpInsert($data)
    {
        echo "11";

        $domov = $this->createEntity();
        $domov->name = $data["name"];
        $domov->export = false;

        $this->persist($domov);

        $user = $this->getTable("user")->insert([
            "email" => $data["email"],
            "name" => "Uměle vytvořený uživatel",
            "type" => "branche",
        ]);
        $this->getTable("branche_user")->insert(["branche_id" => $domov->id, "user_id" => $user->id]);

        $deps = explode(",", $data["dep"]);
        foreach ($deps as $dep) {
            $this->getTable("department")->insert(["name" => $dep, "branche_id" => $domov->id]);
        }
    }

    public function insertOdb($name)
    {

        $this->getTable("speciality")->insert(["name" => $name]);
    }

    public function insertLek($data)
    {

        $medicine = $this->getTable("medicine")->insert([
            "id" => $data["id"],
            "name" => $data["name"],
            "sukl" => $data["sukl"],
        ]);


        foreach ($data["baleni"] as $baleni) {
            if ($baleni != 0) {
                $this->getTable("package")->insert(["size" => $baleni, "medicine_id" => $medicine->id]);
            }
        }
        foreach ($data["spec"] as $spec) {
            if ($spec != "null") {
                $spec_db = $this->getTable("speciality")->select("*")->where("name", $spec)->fetch();
                $this->getTable("speciality_medicine")->insert([
                    "medicine_id" => $medicine->id,
                    "speciality_id" => $spec_db->id,
                ]);
            }
        }
    }

    public function gt($table)
    {
        return $this->getTable($table);
    }

    public function setEnabledDeps($enabledDeps)
    {
        try {
            $this->getTable('department')->where('1=1')->update(['eureka_export' => 0]);
            $this->getTable('department')->where(['id' => $enabledDeps])->update(['eureka_export' => 1]);
            return true;
        } catch (Exception $ex) {
            Logger::log($ex);
            return false;
        }
    }

    /**
     * @param $idBranch
     * @param User $user
     *
     * @return bool
     */
    public function isInBranch($idBranch, $user)
    {
        if ($user->isSuperAdmin()) {
            return true;
        } else {
            $isIn = $this->getTable("branche_user")
                         ->where(["user_id" => $user->id, "branche_id" => $idBranch])
                         ->count();
            return $isIn > 0;
        }
    }
}
