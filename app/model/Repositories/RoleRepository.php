<?php

namespace App\Model\Repositories;

use App\Model\Entities\Permission;
use YetORM\Repository;
use App\Model\Entities\Role;

/**
 * Class RoleRepository
 * @table  role
 * @entity Role
 */
class RoleRepository extends Repository
{
    public function manage($data)
    {
        /**
         * @var Role $role
         */
        $role = ($data->id == 0) ? $this->createEntity() : $this->getByID($data->id);


        $role->name = $data->name;
        $role->description = $data->description;
        $role->sendNotifications = $data->send_notifications;

        try {
            $this->persist($role);
            return true;
        } catch (\Exception $e) {
            return $e->getCode();
        }
    }

    public function delRole($id)
    {
        $can = $this->getByID($id);

        try {
            $this->delete($can);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getPermissionsForRole($id, &$allPermissions)
    {
        $role = $this->getByID($id);
        $perms = $role->toRecord()->related('role_permission');
        $allowedPerms = [];
        foreach ($perms as $perm){
            $allowedPerms[] = $perm['permission_id'];
        }

        /**
         * @var $perm Permission
         */
        foreach($allPermissions as $perm) {
            if(in_array($perm->id, $allowedPerms)){
                $perm->enabled = true;
            }
        }

        return $allPermissions;
    }
}
