<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 19.7.15
 * Time: 13:24
 */

namespace App\Model\Repositories;

use YetORM\Repository;
use App\Model\Entities\Predikce;

/**
 * Class PredikceRepository
 * @table predikce
 * @entity Predikce
 */
class PredikceRepository extends Repository {
    public function checkBranche($branche_id) {
        $specialities = $this->getTable("speciality")->select("*");

        foreach ($specialities as $speciality) {
            $predikce = $this->getBy(["speciality_id" => $speciality->id, "branche_id" => $branche_id]);

            if ($predikce === null) {
                // it doesnt exist -> create one
                $predikce = $this->createEntity();
                $predikce->brancheId = (int)$branche_id;
                $predikce->userId = null;
                $predikce->specialityId = $speciality->id;
                $predikce->days = 5;
                $this->persist($predikce);
            }
        }
    }


    /**
     * run checkBranche for all the branches. it fixes problem of creating predictions
     */
    public function checkAllBranches() {
        $branches = $this->getTable("branche")->select("*");
        foreach ($branches as $branch) {
            $this->checkBranche($branch->id);
        }
    }

    public function manage($values) {
        $branche = $values["brancheId"];
        foreach ($values as $valK => $valV) {

            $what = substr($valK, 0, 4);
            $id = substr($valK, 4, 10);
            if (in_array($what, ["days", "user"]) && is_numeric($id)) {
                $predikce = $this->getById($id);
                if ($what == "days") {
                    $predikce->days = (int)$valV;
                    $this->persist($predikce);
                } elseif ($what == "user") {
                    if ($valV == 0) {
                        $valV = null;
                    }
                    $predikce->userId = $valV;

                    $this->persist($predikce);
                }
            }
        }

        return true;
    }
}
