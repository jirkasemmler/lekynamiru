<?php

namespace App\Model\Repositories;

use YetORM\Repository;
use App\Model\Entities\Permission;

/**
 * Class PermissionRepository
 * @table  permission
 * @entity Permission
 */
class PermissionRepository extends Repository
{
    public function getAllPermissions()
    {
        return $this->findAll();
    }

    public function getAllPermissionsByID()
    {
        $perms = $this->getAllPermissions()->orderBy('data_order');
        $out = [];
        foreach($perms as $perm){
            $out[$perm->id] = $perm;
        }
        return $out;
    }

    public static function makeTreeStructure($all)
    {
        $out = [];
        $tmp = [];
        foreach ($all as $item) {
            if ($item->parentId) {
                if (isset($out[$item->parentId])) {
                    $out[$item->parentId]['children'][] = self::formatNode($item);
                } else {
                    $tmp[] = self::formatNode($item);
                }
            } else {
                $out[$item->id] = self::formatNode($item, true);
            }
        }
        foreach ($tmp as $item) {
            $out[$item['parentId']]['children'][] = $item;
        }
        return array_values($out);
    }

    private static function formatNode(Permission $node, $folder = false)
    {
        return [
            'title' => $node->name,
            'key' => $node->id,
            'parentId' => $node->parentId,
            'selected' => $node->enabled,
            'folder' => false,
            'expanded' => $folder,
            'children' => [],
        ];
    }

    public function enableToRole($roleId, $permissionId)
    {
        $this->getTable('role_permission')->insert(['role_id' => $roleId, 'permission_id' => $permissionId]);
    }

    public function disableToRole($roleId, $permissionId)
    {
        $this->getTable('role_permission')->where(['role_id' => $roleId, 'permission_id' => $permissionId])->delete();
    }
}
