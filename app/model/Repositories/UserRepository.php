<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 20.6.15
 * Time: 23:32
 */

namespace App\Model\Repositories;

use Nette\Neon\Exception;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\Passwords;
use Tracy\Debugger;
use YetORM\Repository;
use App\Model\Entities\User;
use YetORM\Entity;
use Nette\Utils\Random;

/**
 * Class UserRepository
 * @table  user
 * @entity User
 */
class UserRepository extends Repository implements IAuthenticator
{
    public $brancheRepository;

    public function getDepartment($user_id)
    {
        $dep = $this->getTable("department")->select("*")->where(["user_id" => $user_id]);
        if ($dep != null) {
            $ret = [];
            foreach ($dep as $oneDep) {
                $ret[] = $oneDep["id"];
            }
            return $ret;
        } else {
            return 0;
        }
    }

    public function getFirstBranche($userId)
    {
        /**
         * @var $user User
         */
        $user = $this->getByID($userId);
        $brancheRep = new BrancheRepository($this->database);
        if ($user->isAllowed('Api', ['action' => 'admin'])) {
            $branches = $brancheRep->findAll();
            foreach ($branches as $item) {
                return $item->id;
            }
            return 0;

        }

        $associatedUsers = $this->getBranchesForAssociatedUser($userId);
        if($associatedUsers){
            foreach ($associatedUsers as $user){
                return $user['branch_id'];
            }
        }
        switch ($user->type) {
            case "branche":
            case "branche_department":
                {
                    return $user->toRecord()->related("branche_user")->fetch()["branche_id"];
                };
                break;
            case "department":
                {
                    $dep = $this->getTable("department")->select("*")->where(["user_id" => $user->id])->fetch();
                    if ($dep == null) {
                        return 0;
                    } else {
                        return $dep->branche_id;
                    }
                };
                break;
            default:
            {
                return 0;
            }
        }
    }

    public function getBranchesForAssociatedUser($userId)
    {
        return $this->getTable('associated_users')->where(['user_id' => $userId])->fetchAll();
    }

    public function resetPass($data)
    {
        $user = $this->getBy(["email" => $data->email, "token" => $data->token]);
        if ($user == null) {
            return "notfound";
        } else {
            if (strlen($data->password1) >= 6) {
                if ($data->password1 == $data->password2) {
                    $user->pass = Passwords::hash($data->password1);
                    $user->token = null;
                    try {
                        $this->persist($user);
                        return "ok";
                    } catch (\Exception $e) {
                        return "fail";
                    }
                } else {
                    return "PASS";
                }
            }
        }
    }

    public function lostPassStep1($data)
    {

        $user = $this->getBy(["email" => $data->email]);
        if ($user == false) {
            return "notfound";
        }
        do {
            $token = Random::generate(10);
            $tmp = $this->getByToken($token);
        } while ($tmp != false);

        $user->token = $token;
        try {
            $this->persist($user);
            return $token;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function isTokenValid($token)
    {
        if ($this->getByToken($token) != null) {
            return true;
        } else {
            return false;
        }
    }

    public function getUsers()
    {
        return $this->findAll();
    }

    /**
     * Performs an authentication.
     *
     * @param array $credentials - email and password
     *
     * @return \Nette\Security\Identity
     * @throws \Nette\Security\AuthenticationException
     */
    public function authenticate(array $credentials)
    {

        list($email, $password) = $credentials;

        /** @var User $user */
        $user = $this->getByEmail($email);

        if ($user != null && Passwords::verify($password, $user->pass)) //		if (true)
        {
            $branche = $this->getTable("branche")->select("*")->limit(1)->fetch();
            return new Identity($user->id, $user->type, ["name" => $user->name, "branche" => $branche->id]);
        } else {
            throw new AuthenticationException('Kombinace hesla a emailu není správná.');
        }
    }

    /**
     * registers user to the system, sends email about the registration
     *
     * @param $values
     *
     * @return string
     */
    public function registerUser($values)
    {

        if ($values->id == 0) //new
        {
            /**
             * @param User $user
             */
            $user = new User(); //new entity user
            if ($this->findAll()->count() == 0) //the database is empty
            {
                $user->type = 'admin'; //user is complete
            } else {
                $user->type = 'view'; //user is complete
            }
            if (isset($values->showSecretData) && $values->showSecretData == true) {
                $user->showSecretData = true;
            } else {
                $user->showSecretData = false;
            }

            if (isset($values->showSums) && $values->showSums == true) {
                $user->showSums = true;
            } else {
                $user->showSums = false;
            }

            do {
                $token = Random::generate(10);
                $tmp = $this->getByToken($token);
            } while ($tmp != false);

            $user->token = $token;
            $user->pass = Passwords::hash(Random::generate(6));
        } else {
            $user = $this->getByID($values->id);
            if ($values->ownEdit == "true") {

                $editPass = $_POST["password_edit"];

                if (!Passwords::verify($editPass, $user->pass)) {
                    return "PASS1";
                }
            } else {
                if (isset($values->showSecretData) && $values->showSecretData == true) {
                    $user->showSecretData = true;
                } else {
                    $user->showSecretData = false;
                }


                if (isset($values->showSums) && $values->showSums == true) {
                    $user->showSums = true;
                } else {
                    $user->showSums = false;
                }
            }

            if (isset($_POST["changePass"]) &&
                strlen($values->password1) >= 6 &&
                $values->password1 == $values->password2) {
                $user->pass = Passwords::hash($values->password1);
            }
        }
        $user->roleId = $values->role;
        $user->email = $values->email;
        $user->name = $values->name;
        if (isset($values->sendReports) && $values->sendReports == true) {
            $user->sendReports = true;
        } else {
            $user->sendReports = false;
        }


        try {
            $row = $this->persist($user); //try to write to DB
        } catch (\Exception $e) //duplicate data on UNIQUE index -> email
        {
            if ($e->getCode() == 23000) {
                return "DUPLICATE"; //duplicate data
            }
            return "ERR";
        }

        if ($values->id == 0) {
            return ["name" => "okmail", "token" => $token]; //it is ok but send email from presenter
        } else {
            return "OK"; //everything is ok
        }
    }

    public function delUser($id)
    {
        try {
            $user = $this->getByID($id);

            if ($user->type != "admin") {
                $this->delete($user);
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            Debugger::log($e);
            return false;
        }
    }

    /**
     * get users who I can add to a role. it means they have "view" type
     */
    public function getFreeUsers($acceptableType = [], $branche = 0)
    {
        $ret = [];

        $ret[0] = " - nezařazeno - ";

        $types = array_merge(["view"], $acceptableType);

        $data = $this->findBy(["type" => $types]);

        foreach ($data as $item) {
            if ($branche != 0) {
                if ($item->type == "department" or $item->type == "branche_department") {
                    $skip = false;
                    $departments = $this->getTable("department")->select("*")->where(["user_id" => $item->id]);
                    foreach ($departments as $department) {
                        if ($department->branche_id != $branche) {
                            $skip = true;
                        }
                    }
                    if ($skip) {
                        continue;
                    }
                }
                if ($item->type == "branche" or $item->type == "branche_department") {
                    $skip = false;
                    $branches = $this->getTable("branche_user")->select("*")->where(["user_id" => $item->id]);
                    foreach ($branches as $tmp_branche) {
                        if ($tmp_branche->branche_id != $branche) {
                            $skip = true;
                        }
                    }
                    if ($skip) {
                        continue;
                    }
                }
            }
            $ret[$item->id] = $item->name;
        }
        return $ret;
    }

    public function getAllUsersAsArray()
    {
        $out = [];
        $all = $this->findAll();

        foreach ($all as $item){
            $out[$item->id] = $item->name;
        }

        return $out;
    }

    /**
     * @param $id - id of user
     * @param $type - to which type to change
     *
     * @return bool - succ?
     */
    public function changeType($id, $type)
    {
        if ($id == null || $id == 0) {
            return false;
        }
        $user = $this->getByID($id);
        $user->type = $type;

        $this->persist($user);
    }

    public function changeTypes($users, $type)
    {
        $this->getTable("user")->where(["id" => $users])->update(["type" => $type]);
    }
}
