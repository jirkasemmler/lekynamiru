<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 18.7.15
 * Time: 19:01
 */

namespace App\Model\Repositories;

use App\Model\Entities\Branche;
use App\Presenters\FileManager;
use Tracy\Debugger;
use YetORM\Repository;
use App\Model\Entities\Usage;

/**
 * Class UsageRepository
 * @table usage_meta
 * @entity Usage
 */
class UsageRepository extends Repository
{

    /** @var FileManager @inject */
    public $FileManager;

    public function setRevision () {
        $revs = $this->findSrcBy([]);
        foreach ($revs as $rev) {
            $rev->revisionSrc = $rev->id;
            $this->persist($rev);
        }
    }

    public function addReserve ($data, $addBy = null) {
        /** @var $usage Usage */
        $usage = $this->getByID($data->id_usage);

        if ($addBy === null) {
            $addBy = $_POST["add_by"];
        }

        if ($addBy == "pills") {
            $add = $data->pills;
        } elseif ($addBy == "pck") {
            $add = $data->pckgs * $data->pcksize;
        }

        $usage->number += $add;
        $this->persist($usage);
        $this->setDailyAverage($usage);

        $this->countRemaining($usage->id);

        return true;
    }

    public function hasUsage ($patient) {
        $usages = $this->findByPatientId($patient);
        foreach ($usages as $usage) {
            $tmp = $this->getTable("usage")
                        ->select("id")
                        ->where('( usage_meta_id = ?  AND week= ? AND ( morning != 0 OR noon!=0 OR evening != 0 OR night != 0))',
                            $usage->id, $usage->activeWeek)
                        ->count();
            if ($tmp > 0)
                return true;
        }
        return false;

    }

    public function recountNonActiveDays () {
        //patients who are sleeping, pasive or dead (everything but not active)
        $usages = $this->findBy(["patient.state !=" => "running"]);

        foreach ($usages as $usage) //count remaining days for all usages of these not active patients
        {
            $this->countRemaining($usage->id);
        }
    }


    public function changeWeekForPatients ($by_ar) {
        $branches = $this->getTable("branche")
                         ->select("*");
//        $this->begin();
        /** @var Branche $branche */
        foreach ($branches as $branche) {
            $by_ar["patient.branche_id"] = $branche->id;
            $usages = $this->findBy($by_ar);

            foreach ($usages as $usage) {
                $usage->changeActiveWeek();
                $this->persist($usage);
            }
        }
//        $this->commit();
        return true;
    }

    public function recount ($id, $path_prefix, $one_usage = false) {
        $thisDate = date("Y-m-d");
        $finished = true;
        $zbyva = null;
        $max = ini_get('max_execution_time');
        $start = microtime(true);
        $stop = microtime(true);
        $day = strtolower(date("D"));
        $url = $this->getTable("settings")
                    ->select("value")
                    ->where(["key" => "prepocet_url"])
                    ->fetch()["value"];
        $path = $this->getTable("settings")
                     ->select("value")
                     ->where(["key" => "prepocet_path"])
                     ->fetch()["value"];

        if ($one_usage == true) {
            $usages = $this->findBy(["patient.state" => "running", "usage_meta.id" => $id, "usage_meta.active" => 1])
                           ->orderBy("usage_meta.id");
        } else {
            //                vvvvv here is the difference :)
            $usages = $this->findBy(["patient.state" => "running", "usage_meta.id > " => $id, "usage_meta.active" => 1])
                           ->orderBy("usage_meta.id");
        }

        /** @var Usage $usage */
        foreach ($usages as $usage) {
            $stop = microtime(true);
            $usedTime = ($stop - $start);
            $zbyva = $max - ($stop - $start);

            if ($zbyva < 5) {
                $finished = false;

                $status = "continue";
                $last_id = $usage->id;
                $data = ["id" => $last_id, "status" => $status, "time" => $usedTime, "date" => $thisDate,
                         "remain" => $zbyva];

                file_put_contents($path_prefix . "recount.json", json_encode($data));

                break;
            }

            $today = $usage->countDay($day);
            if ($day == "sun") {
                echo "changed week\n";
                $usage->changeActiveWeek();
            }

            if (fmod($today, 0.25) != 0) {
                $today = round($today / (1 / 3)) / 3;
            }

            $usage->number = $usage->number - $today;

            if (fmod($usage->number, 0.25) != 0) {
                $usage->number = round($usage->number / (1 / 3)) * (1 / 3);
            }

            $this->persist($usage);
            $this->countRemaining($usage->id);

        }

        if ($finished) {
            $status = "justfinished";
            $last_id = $usage->id;
            $data = ["id" => $last_id, "status" => $status];
            file_put_contents($path_prefix . "recount.json", json_encode($data));
            $data["time"] = ($stop - $start);
            $data["remain"] = $zbyva;
            $data["date"] = $thisDate;
            file_put_contents($path_prefix . "recount" . date("Y-m-d") . ".json", json_encode($data));
            return "done";
        }
        return "continue";
    }

    public function countRemaining ($id = 0) {
        if ($id == 0) {
            $usages = $this->findBy(["patient.state" => "running"]);
        } else {
            $usages = $this->findBy(["id" => $id]);
        }

        foreach ($usages as $usage) {
            /** @var Usage $usage */
            $remainingDays = $usage->countRemainingDays();

            $usage->remaining = (int)$remainingDays;

            $predikce = $this->getTable("predikce")
                             ->where(["speciality_id" => $usage->specialityId,
                                      "branche_id" => $usage->toRecord()->patient->branche_id])
                             ->fetch();

            $state = "ok";
            if ($predikce->days >= $remainingDays && $remainingDays > 0) {
                $state = "warning";
            } elseif ($remainingDays <= 0) {
                $state = "critical";
            }
            $usage->state = $state;

            $this->persist($usage);
        }

    }


    public function getDefMedicine ($id) {
        if ($id == 0) {
            return 0;
        } else {
            $usage = $this->getByID($id);
            $record = $usage->toRecord();
            return [$record->speciality->name, $usage->specialityId . "-" . $usage->medicineId,
                    $record->medicine->name];
        }
    }

    public function del ($id_usage, $userID) {
        $usageStatRepository = new UsageStatRepository($this->database);
        $MCRepository = new MedicalCardRepository($this->database);

        try {
            /** @var Usage $item */
            $item = $this->getByID($id_usage);

            $item->deleted = true;
            $item->active = false;
            $this->persist($item);

            $usages_to_delete = $this->findSrcBy(["revision_source" => $item->revisionSrc]);
            foreach ($usages_to_delete as $usage_item) {
                $usage_item->deleted = true;
                $this->persist($usage_item);
            }
            $medicine = $item->medicineId;
            $department_id = $item->toRecord()->patient->department_id;

            $posledni_karta = $MCRepository->getLastCard($item->patientId);
            $nova_karta = $MCRepository->makeNewCard($posledni_karta, $userID, $id_usage,
                "Odebrání <b>" . $item->getMedicine()->name . "</b>", $item->id); // napojeni starych usagi


            $usageStatRepository->updateUsageStat($medicine, $department_id);

            return $nova_karta->getOneUsage();
        } catch (\Exception $e) {
            Debugger::log($e);
            return false;
        }
    }

    public function getOneUsage ($id) {
        $ret = ["first" => [], "second" => []];
        $usage_meta = $this->getByID($id);

        $usages = $usage_meta->toRecord()
                             ->related("usage");
        foreach ($usages as $usage) {
            $ret[$usage->week][$usage->day] = ["morning" => round($usage->morning, 2), "noon" => round($usage->noon, 2),
                                               "evening" => round($usage->evening, 2),
                                               "night" => round($usage->night, 2),];
        }

        return $ret;
    }

    /**
     * insert | update card
     * @param $values
     *
     * @return int ID nove karty
     */
    public function manageCard ($values) {
        /** @var Usage $usage_old */
        /** @var Usage $usage */

        $usageStatRepository = new UsageStatRepository($this->database);
        $MCRepository = new MedicalCardRepository($this->database);
        if ($values->id_usage == 0) //insert of USAGE, not card
        {
            $usage = $this->createEntity();
            /** @var $usage Usage */
            $usage->patientId = (int)$values->id_patient;
            $usage->startDate = date('Y-m-d');
            $usage->active = true;
            $usage->number = 0.0;
            $usage->weeks = (int)$values->weeks;
            if($values->activeWeekSelect){
                $usage->activeWeek = Usage::$weeks[$values->activeWeekSelect - 1];
            }

            $posledni_karta = $MCRepository->getLastCard($usage->patientId);
            $nova_karta = $MCRepository->makeNewCard($posledni_karta, $values->userID, $values->id_usage,
                $values->comment); // napojeni starych usagi

        } else {
            //update
            $last_usage = null;
            $usage_old = $this->getByID($values->id_usage);

            $usage_old->active = false;

            $this->persist($usage_old);

            $karta_id = $usage_old->getMedicalCard();

            $karta = $MCRepository->getByID($karta_id);
            $nova_karta = $MCRepository->makeNewCard($karta, $values->userID, $usage_old->revisionSrc,
                $values->comment); // napojeni starych usagi

            $usage = $this->createEntity();
            $usage->active = true;
            $usage->revisionSrc = $usage_old->revisionSrc;
            $usage->patientId = $usage_old->patientId;
            $usage->startDate = $usage_old->startDate;
            $usage->number = $usage_old->number;

            $usage->weeks = (int)$values->weeks;
            // checking if the activeWeek is in the limit of new weeks -> keep or reset
            if ((array_search($usage_old->activeWeek, Usage::$weeks) + 1) > $usage->weeks) {
                // over the limit -> reset, should NOT happen
                $usage->activeWeek = Usage::$weeks[0];
            } else {
                // keep the active week
                if($values->activeWeekSelect){
                    $usage->activeWeek = Usage::$weeks[$values->activeWeekSelect - 1];
                }
                else{
                    // wrong data, using old value
                    $usage->activeWeek = $usage_old->activeWeek;
                }
            }
        }

        if (isset($_POST["stav"])) {
            $number = (float)$_POST["stav"];
        } else {
            if ($values->id_usage == 0) {
                $number = 0.0;
            } else {
                $number = $usage->number;
            }
        }

        $usage->number = $number; //stavajici stav leku - kolik ho mm
        $lek = $values->medicineSelect; // lek-speciality
        $lek_ex = explode("-", $lek); //exploding [ <speciality> , <medicine> ]
        $medicine = $lek_ex[1];
        $spec = $lek_ex[0];
        $usage->medicineId = (int)$medicine; //save medicine
        $usage->specialityId = (int)$spec; //save speciality
        $usage->showTwoWeeks = (bool)$values->showTwoWeeks; // show two weeks in view
        /*
         * beacuse there are three posibilities
         * 1) all weeks are the same 1,1,..,1,1 -> show only one week -> $usage->showTwoWeeks = false
         * 2) first and second week are different (the even-odd)
         *          1-0-1-0-1-0-1
         *          0-1-0-1-0-1-0
         *          -> showTwoWeeks = true
         * 3) more then two weeks -> $usage->weeks = <3-12>
         */


        $this->persist($usage);
        $usage_mc_connection = $MCRepository->addUsage($nova_karta, $usage);

        if ($values->id_usage == 0) {

            $usage->revisionSrc = $usage->id;
            $this->persist($usage);

        }

        //prepare structure where to store data. to be able save it to db in loop
        $davky = ["morning" => 0, "noon" => 0, "evening" => 0, "night" => 0];

        $dny = ["mon" => $davky, "tue" => $davky, "wed" => $davky, "thu" => $davky, "fri" => $davky, "sat" => $davky,
                "sun" => $davky];

        $predata = [];
        $preweeks = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth',
                     'eleventh', 'twelfth', 'zarazka'];

        $weeks = $values->weeks;
        $weeksTmp = 0;
        while ($weeksTmp < $weeks) {
            $predata[$preweeks[$weeksTmp]] = $dny;
            $weeksTmp++;
        }

        $data = ["day" => "", "week" => "", "usage_meta_id" => 0, "morning" => 0, "noon" => 0, "evening" => 0,
                 "night" => 0];

        $weeksTmp = 0;

        foreach ($values as $valueK => $valueV) {
            // <part of the day> = $davky (CZ)
            //identification of the input is  <day>0 <part of the day> 0 <week>
            $exploded = explode("0", $valueK);

            if (count($exploded) == 3) {
                if ($exploded[2] == $preweeks[$weeks]) {
                    break;
                }
                $predata[$exploded[2]][$exploded[0]][$exploded[1]] = $valueV;
            }
            $weeksTmp++;
        }

        foreach ($predata as $weekK => $weekV) {
            foreach ($weekV as $dayK => $dayV) {
                $data["day"] = $dayK;
                $data["week"] = $weekK;
                $data["usage_meta_id"] = $usage->id;

                $data["morning"] = $dayV["morning"];
                $data["noon"] = $dayV["noon"];
                $data["evening"] = $dayV["evening"];
                $data["night"] = $dayV["night"];

                $this->getTable("usage")
                     ->insert($data);
            }
        }

        $this->countRemaining($usage->id);
        $this->setDailyAverage($usage);
        $usageStatRepository->updateUsageStat($usage->medicineId, $usage->toRecord()->patient->department_id);
        return $usage_mc_connection;
    }

    /**
     * count and store the daily average usage
     * @param $usage Usage
     */
    public function setDailyAverage ($usage) {
        $usage->countDailyAverage(); // count the daily average
        $this->persist($usage);  // save it
    }

    public function getSpecialities ($id_patient, $karta_id = NULL) {
        $ret = [];
        $tmp = ["speciality" => NULL, "usages" => NULL];
        $specialities = $this->getTable("speciality")
                             ->select("*");

        if ($karta_id == NULL) {
            $karta_id = $this->getTable("medical_card")
                             ->where(["patient_id" => $id_patient, "active" => true])
                             ->order("revision_number DESC")
                             ->limit(1)
                             ->fetch()->id;
        }


        $usages_ids = $this->getTable("medical_card_usage_meta")
                           ->where(["medical_card_id" => $karta_id])
                           ->fetchPairs(null, "usage_meta_id");

        $pocet_tydnu = $this->getTable("settings")
                            ->select("*")
                            ->where(["key" => "pocty_tablet-pocet_tydnu"])
                            ->fetch()["value"];

        $sumUsageComplete = [];

        for ($k = 0; $k < $pocet_tydnu; $k++) {
            $sumUsageComplete[] = new \SumUsage();
        }

        /** @var Usage $usage */
        foreach ($specialities as $speciality) {
            $usages = $this->findSrcBy(["id" => $usages_ids, "speciality_id" => $speciality->id]);
            $specialitySumUsages_ar = [];
            for ($j = 0; $j < $pocet_tydnu; $j++) {

                $specialitySumUsages_ar[] = new \SumUsage();
            }

            //put everything to this if!!!!
            if (count($usages) > 0) //just specialities which has a usage.
            {
                foreach ($usages as $usage) {
                    $usage->defineWeeks();
                    for ($j = 0; $j < $pocet_tydnu; $j++) {
                        $specialitySumUsages_ar[$j]->mergeWith($usage->getActiveWeekData($j)->sumUsage);
                    }
                }

                $tmp["speciality"] = $speciality;
                $tmp["usages"] = $usages;
                $ret[] = $tmp;

                for ($j = 0; $j < $pocet_tydnu; $j++) {
                    $sumUsageComplete[$j]->mergeWith($specialitySumUsages_ar[$j]);
                }

            }
        }
        return [$ret, $sumUsageComplete];
    }

    public function running () {
        return $this->getTable("usage_meta")
                    ->where(["patient.state" => ["running", "sleeping", "dead"], "patient.department.eureka_export" => 1,
                             "revision" => NULL, "deleted" => false, "active" => true])
                    ->order('patient.branche_id')
                    ->order('patient_id')
                    ->order('id');
    }


    /**
     * without deleted!
     * @param array $criteria
     * @return Usage[]|\YetORM\EntityCollection
     */
    function findBy (array $criteria) {
        $criteria["deleted"] = false;
        $criteria["active"] = true;
        $selection = $this->getTable()
                          ->where($criteria);
        return $this->createCollection($selection);
    }

    /**
     * without deleted!
     * @return \YetORM\EntityCollection|Usage[]
     */
    function findAll () {
        return $this->findBy(array());
    }

    /**
     * WITH DELETED
     * @param array $criteria
     * @return Usage[]|\YetORM\EntityCollection
     */
    function findSrcBy (array $criteria) {
        $selection = $this->getTable()
                          ->where($criteria);
        return $this->createCollection($selection);
    }


    /**
     * @param array $criteria - without deleted
     * @return NULL|Usage
     */
    function getBy (array $criteria) {
        $criteria["deleted"] = false;
        return parent::getBy($criteria);
    }

    /**
     * @param array $criteria - doesnt care about the deleted flag - get everything
     * @return NULL|Usage
     */
    function getSrcBy (array $criteria) {
        return parent::getBy($criteria);
    }
}


