<?php

namespace App\Presenters;


use App\Model\Entities\Permission;
use App\Model\Repositories\PermissionRepository;

class PermissionPresenter extends BasePresenter
{
    /** @var PermissionRepository @inject */
    public $permissionRepository;

    public function beforeRender()
    {
        parent::beforeRender();
    }

    public function renderDefault()
    {
        $this->template->mainTitle = "Operace";
        $this->template->subTitle = "Výpis dostupných operací";

        $this->template->data = $this->permissionRepository->getAllPermissions();
    }

    public function getAllPermissionData()
    {
        $all = $this->permissionRepository->getAllPermissions()->orderBy('data_order');
        return PermissionRepository::makeTreeStructure($all);
    }



    public function actionGetPermissionData()
    {
        $data = $this->getAllPermissionData();
        $this->sendResponse(new \Nette\Application\Responses\JsonResponse($data));
    }


}
