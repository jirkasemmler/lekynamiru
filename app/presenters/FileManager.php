<?php

namespace App\Presenters;

use App\Model\Entities\Branche;
use App\Model\Entities\Can;
use App\Model\Entities\Log;
use App\Model\Entities\Medicine;
use App\Model\Entities\Order;
use App\Model\Entities\OrderItem;
use App\Model\Entities\Patient;
use App\Model\Entities\Predikce;
use App\Model\Entities\Usage;
use App\Model\Entities\UsageStat;
use App\Model\Entities\User;
use App\Model\Repositories\LogRepository;
use App\Model\Repositories\OrderItemRepository;
use App\Model\Repositories\OrderRepository;
use App\Model\Repositories\UsageStatRepository;
use App\Model\Repositories\UsageRepository;
use Nette\Database\Context;
use PHPExcel_Worksheet;
use PHPExcel_Writer_PDF_mPDF;
use PHPExcel_IOFactory;
use PHPExcel_Settings;
use PHPExcel_Style_Border;
use App\Model\Repositories\PatientRepository;
use DateTime;
use App\Model\Repositories\BrancheRepository;
use App\Model\Repositories\MedicineRepository;
use PHPExcel_Worksheet_PageSetup;
use App\Model\Repositories\SpecialityRepository;
use App\Model\Repositories\PredikceRepository;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\CanRepository;
use App\Model\Repositories\SettingRepository;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;

class FileManager
{
    /**
     * @param BrancheRepository $brancheRepository
     */
    protected $brancheRepository;
    /**
     * @param OrderRepository $orderRepository
     */
    protected $orderRepository;
    /**
     * @param OrderItemRepository $orderItemRepository
     */
    protected $orderItemRepository;
    /**
     * @param SpecialityRepository $specialityRepository
     */
    protected $specialityRepository;
    /**
     * @param MedicineRepository $medicineRepository
     */
    protected $medicineRepository;
    /**
     * @param PredikceRepository $predikceRepository
     */
    protected $predikceRepository;
    /**
     * @param LogRepository $logRepository
     */
    protected $logRepository;
    /**
     * @param CanRepository $canRepository
     */
    protected $canRepository;
    /**
     * @param UsageRepository $usageRepository
     */
    protected $usageRepository;
    /**
     * @param PatientRepository $patientRepository
     */
    protected $patientRepository;
    /**
     * @param SettingRepository $settingRepository
     */
    protected $settingRepository;
    /**
     * @param UsageStatRepository $usageStatRepository
     */
    protected $usageStatRepository;
    /**
     * @param UserRepository $userRepository
     */
    protected $userRepository;
    private $database;

    public function __construct(Context $database)
    {
        $this->database = $database;
    }

    public function patientsData($path, $what, $format = "xls", $department = [])
    {
        $target_ar = ['running' => "Aktivní", 'sleeping' => "Pasivní", 'stop' => "Pozastaveno", 'dead' => "Zesnulý"];
        $state_colors = ['running' => "#FFFFFF", 'sleeping' => "#FF851B", 'stop' => "#FF4136", 'dead' => "#adadad"];

        $this->patientRepository = new PatientRepository($this->database);
        $this->brancheRepository = new BrancheRepository($this->database);

        $patients = $this->patientRepository->findAll();


        $dokument = $this->prepareDoc($path, "pat_tpl.xls", "Pacienti", "Výpis pacientů");
        $list = $dokument->getActiveSheet();

        $i = 5;

        foreach ($patients as $patient) {
            $source = $patient->birthDate;
            $date = new DateTime($source);
            $newDateString = $date->format('d.m.Y'); // 31.07.2012

            $list->setCellValue("A$i", $patient->getFullName());
            $list->setCellValue("B$i", $patient->getDepartment());
            $list->setCellValue("C$i", $patient->getBranche());

            $list->getStyle("A$i:C$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            $i++;
        }

        $dokument->getActiveSheet()->setAutoFilter('A4:C4');

        $this->writeDoc($path, $dokument, "pacienti_all." . $format, $format);

        return $path . '/../data/pacienti_all.' . $format;

    }


    public function order_stat($path, $format)
    {
        $tpl = "order_stat_tpl.xls";
        $name = "Statistika objednávek";
        $this->orderRepository = new OrderRepository($this->database);
        $this->brancheRepository = new BrancheRepository($this->database);
        $this->orderItemRepository = new OrderItemRepository($this->database);
        $document = $this->prepareDoc($path, $tpl, $name, "Statistika objednávek\"");
        $document->setActiveSheetIndex(0);
        $branchesWithOrders = $this->brancheRepository->findBy(["orders" => true]);
        $dataAr = [];
        $tmp = [
            'order_normal' => 0,
            'order_extra' => 0,
            'urgent' => 0,
            'accepted' => 0,
            'changed' => 0,
            'canceled' => 0,
            'pharmacy' => 0,
            'branch' => 0,
        ];

        /**
         * @var $branchWithOrder Branche
         */
        foreach ($branchesWithOrders as $branchWithOrder) {
            $orders = $branchWithOrder->findOrders();
            if (!isset($dataAr[$branchWithOrder->id])) {
                $dataAr[$branchWithOrder->id] = [];
            }

            /**
             * @var $order Order
             */
            foreach ($orders as $order) {
                $closingDate = new \DateTime($order->_getClosingDate());
                $key = $closingDate->format("Y-m");

                if (!isset($dataAr[$order->brancheId][$key])) {
                    $dataAr[$order->brancheId][$key] = $tmp; // empty array
                }

                if ($order->isIsExtra()) {
                    $dataAr[$order->brancheId][$key]['order_extra'] += 1;
                } else {
                    $dataAr[$order->brancheId][$key]['order_normal'] += 1;
                }

                $dataAr[$order->brancheId][$key]['urgent'] += $this->orderItemRepository->getUrgentItems($order->id)
                                                                                        ->count();
                $dataAr[$order->brancheId][$key]['accepted'] += $this->orderItemRepository->getAcceptedItems($order->id)
                                                                                          ->count();
                $dataAr[$order->brancheId][$key]['changed'] += $this->orderItemRepository->getChangedItems($order->id)
                                                                                         ->count();
                $dataAr[$order->brancheId][$key]['canceled'] += $this->orderItemRepository->getCanceledItems($order->id)
                                                                                          ->count();
                $dataAr[$order->brancheId][$key]['pharmacy'] += $this->orderItemRepository->getPharmacyItems($order->id)
                                                                                          ->count();
                $dataAr[$order->brancheId][$key]['branch'] += $this->orderItemRepository->getBranchItems($order->id)
                                                                                        ->count();
            }
        }
        $document->setActiveSheetIndex(0);

        $currentBranchIndex = 1;
        foreach ($branchesWithOrders as $branchWithOrder) {
            $document->setActiveSheetIndex($currentBranchIndex - 1);
            $list = $document->getActiveSheet();
            $list->setTitle($branchWithOrder->getFullName());

            if ($currentBranchIndex != $branchesWithOrders->count()) {
                $copy = $list->copy();
                $copy->setTitle("tmpName");
                $document->addSheet($copy);
            }


            $list->setCellValue("B2", $branchWithOrder->getFullName());
            $list->setCellValue("B3", (new DateTime())->format("d.m.Y"));
            $list->setCellValue("B4", $branchWithOrder->getOrderSettings()->count());

            $i = 7;
            krsort($dataAr[$branchWithOrder->id]);

            foreach ($dataAr[$branchWithOrder->id] as $key => $periodData) {
                $list->setCellValue("A" . $i, $key);
                $list->setCellValue("B" . $i, '=C' . $i . '+D' . $i);
                $list->setCellValue("C" . $i, $periodData['order_normal']);
                $list->setCellValue("D" . $i, $periodData['order_extra']);
                $list->setCellValue("E" . $i, '=F' . $i . '+H' . $i . '+G' . $i);
                $list->setCellValue("F" . $i, $periodData['accepted']);
                $list->setCellValue("G" . $i, $periodData['changed']);
                $list->setCellValue("H" . $i, $periodData['canceled']);
                $list->setCellValue("I" . $i, $periodData['urgent']);
                $list->setCellValue("J" . $i, $periodData['pharmacy']);
                $list->setCellValue("K" . $i, $periodData['branch']);

                $i++;
            }
            $currentBranchIndex++;
            $list->setAutoFilter('A6:K6');
        }
        $document->setActiveSheetIndex(0);


        $this->writeDoc($path, $document, "delivery_stat." . $format, $format);
        return $path . '/../data/delivery_stat.' . $format;
    }

    /**
     * delivery list for orders
     *
     * @param $path
     * @param $orderId
     * @param $format
     *
     * @return string
     */
    public function delivery($path, $orderId, $format)
    {
        $tpl = "delivery_tpl.xls";
        $name = "Dodací list";


        $this->orderRepository = new OrderRepository($this->database);
        $document = $this->prepareDoc($path, $tpl, $name, "Dodací list\"");
        $document->setActiveSheetIndex(0);
        $list = $document->getActiveSheet();


        /** @var Order $order */
        $order = $this->orderRepository->getByID($orderId);

        $orderItems = $order->getOrderItems([
                                                OrderItem::STATE_APPROVED,
                                                OrderItem::STATE_POSTPONED,
                                                OrderItem::STATE_CHANGED,
                                                OrderItem::STATE_CANCELED,
                                            ], true);  // accepted orderItems only
        $this->writeOrderData($list, $order, $orderItems);

        $document->setActiveSheetIndex(1);
        $list = $document->getActiveSheet();

        $orderItems = $order->getOrderItems([
                                                OrderItem::STATE_APPROVED,
                                                OrderItem::STATE_POSTPONED,
                                                OrderItem::STATE_CHANGED,
                                                OrderItem::STATE_CANCELED,
                                            ], "pharmacy");  // accepted orderItems only
        $this->writeOrderData($list, $order, $orderItems);

        $document->setActiveSheetIndex(0);


        $this->writeDoc($path, $document, "delivery." . $format, $format);
        return $path . '/../data/delivery.' . $format;
    }

    /**
     * @param $list  PHPExcel_Worksheet
     * @param $order Order
     * @param $orderItems
     */
    protected function writeOrderData($list, $order, $orderItems)
    {
        $i = 9;
        $orderName = $order->_getClosingDate()->format('d/m/Y');
        if ($order->isIsExtra()) {
            $orderName .= " - ZVLÁŠTNÍ!";
        }
        $list->setCellValue("B2", $orderName);
        $list->setCellValue("B3", $order->getBranche()->getFullName());
        $list->setCellValue("B4", $order->_getClosingDate()->format('d. m. Y H:i'));
        $list->setCellValue("B5", $order->_getDeliveryDate()->format('d. m. Y H:i'));
        $list->setCellValue("B6", (new \DateTime())->format('d. m. Y H:i'));

        foreach ($orderItems as $item) {

            /** @var $item OrderItem */
            $patient = $item->getPatient();
            $list->setCellValue("A$i", $patient->getFullName());
            $list->setCellValue("B$i", $patient->getDepartment());
            $list->setCellValue("C$i", $item->getMedicineName());

            if ($item->isCustom()) {
                $list->mergeCells('D' . $i . ':E' . $i . '');
                $list->setCellValue("D$i", $item->_getCustomMedicineAmount());
            } else {
                $list->setCellValue("D$i", $item->_getPackages());
                $list->setCellValue("E$i", $item->getPackageSize());
            }

            $list->setCellValue("F$i",
                                $item->getFormattedState() .
                                (($item->_getState() == OrderItem::STATE_POSTPONED) ?
                                    " - " . $item->getFollowerOrderName() : ""));

            if ($item->_getState() == OrderItem::STATE_CHANGED) {
                $lastChange = $item->getLastChange();
                $i++;
                $list->setCellValue("A$i", "Zaměněno za : ");
                $list->setCellValue("C$i", $lastChange->getMedicineFromName());
                $list->setCellValue("E$i", $lastChange->_getMedicineAmountFrom());
            }
            $i++;
        }
        $list->setAutoFilter('A8:E8');
    }

    public function patients($path, $what, $format = "xls", $department = [], $user)
    {
        $target_ar = ['running' => "Aktivní", 'sleeping' => "Pasivní", 'stop' => "Pozastaveno", 'dead' => "Zesnulý"];
        $state_colors = ['running' => "#FFFFFF", 'sleeping' => "#FF851B", 'stop' => "#FF4136", 'dead' => "#adadad"];

        $this->patientRepository = new PatientRepository($this->database);
        $this->brancheRepository = new BrancheRepository($this->database);

        if ($department == []) {
            $patients = $this->patientRepository->findBy(["branche_id" => $_SESSION["branche"]]);
        } else {
            $patients = $this->patientRepository->findBy([
                                                             "branche_id" => $_SESSION["branche"],
                                                             "department_id" => $department,
                                                         ]);
        }

        $branche = $this->brancheRepository->getByID($_SESSION["branche"]);

        $source = date("Y-m-d");
        $date = new DateTime($source);
        $newDateString = $date->format('d.m.Y');

        $isAllowed = ($user->isAllowed('Patient', ['action' => 'default', 'do' => 'show_rc']));
        $fileName = $isAllowed ? "pacienti_tpl.xls" : "pacienti_tpl_no_rc.xls";
        $dokument = $this->prepareDoc($path, $fileName, "Pacienti", "Výpis pacientů");
        $list = $dokument->getActiveSheet();

        $i = 5;

        $list->setCellValue("B2", $newDateString);
        $list->setCellValue("B3", $branche->name);

        foreach ($patients as $patient) {
            $source = $patient->birthDate;
            $date = new DateTime($source);
            $newDateString = $date->format('d.m.Y'); // 31.07.2012

            $list->setCellValue("A$i", $patient->getFullName());
            $list->setCellValue("B$i", $target_ar[$patient->state]);
            $this->setColor($list, $state_colors[$patient->state], "B$i");
            $list->setCellValue("C$i", $newDateString);
            /**
             * @var $user User
             */
            if ($isAllowed) {
                $list->setCellValue("D$i", $patient->rc);
                $list->setCellValue("E$i", $patient->pojistovna);
                $list->setCellValue("F$i", $patient->getPatientsDepartment());
                $list->getStyle("A$i:F$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            } else {
                $list->setCellValue("D$i", $patient->pojistovna);
                $list->setCellValue("E$i", $patient->getPatientsDepartment());
                $list->getStyle("A$i:E$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            }


            $i++;
        }

        if ($isAllowed) {
            $dokument->getActiveSheet()->setAutoFilter('A4:F4');
        } else {
            $dokument->getActiveSheet()->setAutoFilter('A4:E4');
        }

        $this->writeDoc($path, $dokument, "pacienti." . $format, $format);

        return $path . '/../data/pacienti.' . $format;
    }

    public function medicine($path, $format)
    {
        $this->medicineRepository = new MedicineRepository($this->database);
        $this->canRepository = new CanRepository($this->database);
        $data = $this->medicineRepository->getMedicines();

        $dokument = $this->prepareDoc($path, "leky_tpl.xls", "Výpis léků", "Výpis léků");
        $list = $dokument->getActiveSheet();
        $list->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $list->getPageSetup()->setFitToPage(true);
        $list->getPageSetup()->setFitToWidth(1);
        $list->getPageSetup()->setFitToHeight(0);
        $list->getPageMargins()->setRight(0.4);
        $list->getPageMargins()->setLeft(0.4);
        $list->getPageMargins()->setBottom(0.3);
        $list->getPageMargins()->setTop(0.3);

        $source = date("Y-m-d");
        $date = new DateTime($source);
        $newDateString = $date->format('d.m.Y');
        $list->setCellValue("A2", "Datum: " . $newDateString);
        $list->getStyle("B1:F1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);
        $list->getStyle("B2:F2")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);


        $i = 4;
        /** @var Medicine $item */
        foreach ($data as $item) {
            $list->setCellValue("A$i", $item->name);
            $list->setCellValue("B$i", $item->getCan());
            $list->setCellValue("C$i", $item->sukl);
            $list->setCellValue("D$i", $item->getPackage());
            $list->setCellValue("E$i", $item->getSpecialities());
            $list->setCellValue("F$i", $item->isDangerous(true));
            $this->setAlign($list, "A$i:F$i", "left");
            $list->getStyle("A$i:F$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            $i++;
        }

        $dokument->getActiveSheet()->setAutoFilter('A3:F3');

        $this->writeDoc($path, $dokument, "leky." . $format, $format);

        return $path . '/../data/leky.' . $format;
    }

    public function can($path, $format)
    {
        $this->canRepository = new CanRepository($this->database);

        $data = $this->canRepository->getAll();

        $dokument = $this->prepareDoc($path, "can_tpl.xls", "Výpis kanystrů", "Výpis kanystrů");
        $list = $dokument->getActiveSheet();

        $source = date("Y-m-d");
        $date = new DateTime($source);
        $newDateString = $date->format('d.m.Y');
        $list->setCellValue("A2", "Datum: " . $newDateString);

//		$list->getStyle("B1:F1")->getBorders()->getVertical()->setBorderStyle(PHPExcel_Style_Border::NONE);
        $list->getStyle("B1:B1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);
        $list->getStyle("B2:B2")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);


        $i = 4;
        foreach ($data as $item) {
            /** @var $item Can */
            $list->setCellValue("A$i", $item->name);
            $this->setAlign($list, "A$i", "center");
            $list->setCellValue("B$i", $item->getMedicine());

//			$this->setAlign($list, "A$i:B$i", "left");
            $list->getStyle("A$i:B$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            $i++;
        }


        $dokument->getActiveSheet()->setAutoFilter('A3:B3');

        $this->writeDoc($path, $dokument, "kanystry." . $format, $format);

        return $path . '/../data/kanystry.' . $format;
    }

    public function branches($path, $format)
    {
        $this->brancheRepository = new BrancheRepository($this->database);
        $this->specialityRepository = new SpecialityRepository($this->database);
        $this->predikceRepository = new PredikceRepository($this->database);

        $data = $this->brancheRepository->getBranches();

        $document = $this->prepareDoc($path, "domovy_tpl.xls",
                                      "Výpis domovů, oddělení a odborností",
                                      "Výpis  domovů, oddělení a odborností");

        $sheet = $document->getActiveSheet();

        $source = date("Y-m-d");
        $date = new DateTime($source);
        $newDateString = $date->format('d.m.Y');
        $sheet->setCellValue("B2", $newDateString);

        $sheet->getStyle("A1:D1")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);
        $sheet->getStyle("A2:D2")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);
        $specialities = $this->specialityRepository->findAll();


        $i = 4;
        foreach ($data as $item) {
            /** @var $item Branche */
            $sheet->setCellValue("A$i", $item->name);
            $sheet->setCellValue("B$i", "Oddělení");
            $sheet->setCellValue("C$i", "Staniční sestra");
            $sheet->setCellValue("D$i", "Exp");
            $this->setBold($sheet, "A$i:D$i");
            $sheet->getStyle("A$i:D$i")
                  ->getBorders()
                  ->getTop()
                  ->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

            $i++;
            foreach ($item->getDepartments() as $dep) {
                $usr = str_replace("<i>", "-", $dep["user"]);
                $usr = str_replace("</i>", "-", $usr);

                $sheet->setCellValue("B$i", $dep["name"]);
                $sheet->setCellValue("C$i", $usr);
                $export = ($dep["export"]) ? "Ano" : "Ne";

                $sheet->setCellValue("D$i", $export);
                $sheet->getStyle("A$i:D$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                $i++;
            }

            $sheet->setCellValue("B$i", "Odbornost");
            $sheet->setCellValue("C$i", "Lékař");
            $sheet->setCellValue("D$i", "Dní");
            $this->setBold($sheet, "B$i:D$i");
            $sheet->getStyle("A$i:D$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
            $i++;
            foreach ($specialities as $speciality) {
                /** @var Predikce $predikce */
                $predikce = $this->predikceRepository->getBy([
                                                                 "speciality_id" => $speciality->id,
                                                                 "branche_id" => $item->id,
                                                             ]);
                if ($predikce) {
                    $sheet->setCellValue("C$i", $predikce->getUser());
                    $sheet->setCellValue("D$i", $predikce->days);
                }

                $sheet->setCellValue("B$i", $speciality->name);
                $this->setAlign($sheet, "D$i", "left");

                $sheet->getStyle("A$i:D$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                $i++;
            }
        }

        $this->writeDoc($path, $document, "domovy." . $format, $format);
        return $path . '/../data/domovy.' . $format;
    }

    /**
     * @param $path
     * @param $patient_id
     * @param User $user
     * @param string $format
     * @param bool $response
     *
     * @return string
     * @throws \PHPExcel_Exception
     *
     */
    public function mk($path, $patient_id, $user, $format = "xls", $response = true)
    {
        $this->patientRepository = new PatientRepository($this->database);
        $this->usageRepository = new UsageRepository($this->database);
        $dokument = $this->prepareDoc($path, "mc2_tpl.xls", "Medikační karta", "Medikační karta");
        $dokument->getActiveSheet()->getPageSetup()
                 ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $list = $dokument->getActiveSheet();

        $data = $this->usageRepository->getSpecialities($patient_id);
        $specialities = $data[0];
        $patient = $this->patientRepository->getByID($patient_id);
        $list->setCellValue("B2", $patient->getFullName());
        $list->mergeCells('B2:H2');
        $list->mergeCells('B3:H3');
        $list->mergeCells('B4:H4');
        $list->mergeCells('B5:H5');

        $list->setCellValue("B3", $patient->getDate());
        $canRC = $user->isAllowed('Patient', ['action' => 'default', 'do'=>'show_rc']);
        $list->setCellValue("B4", $canRC ? $patient->rc : '-');
        $list->setCellValue("B5", $patient->pojistovna);

        $list->setCellValue("K3", $patient->getBranche());
        $list->setCellValue("K2", $patient->getDepartment());
        $list->mergeCells('K2:N2');
        $list->mergeCells('K3:N3');
        $list->mergeCells('K4:N4');
        $source = date("Y-m-d");
        $date = new DateTime($source);
        $newDateString = $date->format('d.m.Y');

        $list->setCellValue("K4", $newDateString);

        $i = 7;
        $days_dict = ["mon" => "B", "tue" => "C", "wed" => "D", "thu" => "E", "fri" => "F", "sat" => "G", "sun" => "H"];
        $weeks = [
            'first',
            'second',
            'third',
            'fourth',
            'fifth',
            'sixth',
            'seventh',
            'eighth',
            'ninth',
            'tenth',
            'eleventh',
            'twelfth',
        ];
        $cells = [
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z",
            "AA",
            "AB",
            "AC",
        ];

        foreach ($specialities as $speciality) {

            $list->setCellValue("A$i", $speciality["speciality"]->name);

            $list->getStyle("A$i")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            $list->getStyle("A$i")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            $list->getStyle("A$i")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
            $this->setBold($list, "A$i");
            $list->getStyle("B$i:AC$i")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);
            $list->getStyle("AC$i")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            $i++;
            foreach ($speciality["usages"] as $usage) {
                /**
                 * @var $usage Usage
                 */
                $list->getStyle("A$i")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);

                if ($usage->getMedicine()->dangerous) {
                    $this->setColor($list, "critical", "A$i");
                }

                $list->setCellValue("A$i", $usage->getMedicine()->name);
                $list->getStyle("A$i")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                $list->getStyle("A$i")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                if ($usage->showTwoWeeks || $usage->weeks > 2) {
                    $j = 1;
                } else {
                    $j = 2;
                }
                for ($j; $j <= $usage->weeks; $j++) {
                    if (($usage->showTwoWeeks || $usage->weeks > 2) && $usage->activeWeek == $weeks[$j - 1]) {
                        $this->setColor($list, "active", "B$i:AC$i");
                    }
                    $k = 1;
                    foreach ($usage->getWeek($weeks[$j - 1]) as $day) {
                        $l = $k;
                        if (fmod($day->morning, 1) == 0) {
                            $morning = $day->morning;
                        } else {
                            $morning = round($day->morning, 2);
                        }

                        $list->setCellValue($cells[$k++] . $i, $morning);
                        $list->getStyle($cells[$k] . $i)->getBorders()->getLeft()
                             ->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
                        if (fmod($day->noon, 1) == 0) {
                            $noon = $day->noon;
                        } else {
                            $noon = round($day->noon, 2);
                        }
                        $list->setCellValue($cells[$k++] . $i, $noon);
                        $list->getStyle($cells[$k] . $i)->getBorders()->getLeft()
                             ->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
                        if (fmod($day->evening, 1) == 0) {
                            $evening = $day->evening;
                        } else {
                            $evening = round($day->evening, 2);
                        }
                        $list->setCellValue($cells[$k++] . $i, $evening);
                        $list->getStyle($cells[$k] . $i)->getBorders()->getLeft()
                             ->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
                        if (fmod($day->night, 1) == 0) {
                            $night = $day->night;
                        } else {
                            $night = round($day->night, 2);
                        }
                        $list->setCellValue($cells[$k++] . $i, $night);
                        $list->getStyle($cells[--$k] . $i)->getBorders()->getLeft()
                             ->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);

                        $list->getStyle($cells[$l] . $i . ":" . $cells[$k] . $i)->getBorders()->getLeft()
                             ->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
                        $k++;
                        $this->setAlign($list, "B$i:AC$i", "center");
                    }

                    $list->getStyle("A$i:AC$i")->getBorders()->getTop()
                         ->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                    $list->getStyle("AC$i")->getBorders()->getRight()
                         ->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
                    $this->setAlign($list, "B$i:AC$i", "center");
                    $list->getStyle("A$i")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);

                    $i++;
                }
            }
            $i--;
            $list->getStyle("A$i:AC$i")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            $i++;
        }


        $sumUsages = $data[1];
        $parts = ["one" => "Celé", "half" => "Půlky", "quater" => "Čtvrtky", "third" => "Třetiny"];
// nechtely to domovy - kdyz se nasledujici radky odkomentuji tak bude mozno videt sumy i v exportech
//		foreach($sumUsages as $sumUsage)
//		{
//			$list->setCellValue($cells[0] . $i, $sumUsage->translateWeekOffset());
//			$list->getStyle("A$i:AC$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
//			$list->getStyle("A$i:AC$i")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);
//			$list->getStyle("A$i")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
//			$list->getStyle("AC$i")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
//			$i++;
//			foreach($parts as $partXK => $partXV)
//			{
//				$j = 1;
//				$list->setCellValue($cells[0] . $i, $partXV);
//				$this->setAlign($list, $cells[0] . $i, "right");
//
//
//				foreach($sumUsage->data as $dayV)
//				{
//					$list->getStyle($cells[$j] . $i)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
//					foreach($dayV as $partK => $partV)
//					{
//						$list->setCellValue($cells[$j] . $i, $partV[$partXK]);
//
//
//						$list->getStyle($cells[$j] . $i)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);
//						$j++;
//
//					}
//
//				}
//				$this->setAlign($list,"B$i:AC$i", "center");
//
//				$list->getStyle("A$i:AC$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
//				$list->getStyle("A$i:AC$i")->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
//				$list->getStyle("AC$i")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
//			$i++;
//			}
//
//
//		}
//		$list->getStyle("A$i:AC$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);

        $this->writeDoc($path, $dokument, "karta." . $format, $format);
        return $path . '/../data/karta.' . $format;
    }

    // existovala jeste jedna MK. podivej se do gitu na verzi starsi nez 2.12.2015. ta ale delala hnusne MK. podivej se do API na otazku MK vs. MK2


    public function usage($path, $branche = 0, $department = null, $period = 2, $format = "xls", $showBD = false, $showIns = false, $showRc = false)
    {
        $this->patientRepository = new PatientRepository($this->database);
        $this->usageRepository = new UsageRepository($this->database);
        $this->medicineRepository = new MedicineRepository($this->database);
        $this->brancheRepository = new BrancheRepository($this->database);
        $this->usageStatRepository = new UsageStatRepository($this->database);

        if ($branche == 0) {
            $branche_name = "všechny";
        } else {
            $branche_name = $this->brancheRepository->getByID($branche)->name;
        }

        if ($department == 0) {
            $department_name = "všechny";
        } else {
            $department_name = $this->brancheRepository->getDepartment($department)->name;
        }

        $document = $this->prepareDoc($path, "spotreba_tpl.xls", "Spotřeba", "Výpis spotřeby léků");
        $sheet = $document->getActiveSheet();
        $pills = $this->usageStatRepository->getUsageStat($branche, $department, $period);

        $i = 7;
        $source = date("Y-m-d");
        $date = new DateTime($source);
        $newDateString = $date->format('d.m.Y');

        $sheet->setCellValue("C2", $newDateString);
        $sheet->setCellValue("C3", $branche_name);
        $sheet->setCellValue("C4", $department_name);
        $sheet->setCellValue("C5", $period);

        foreach ($pills as $pill) {
            /**
             * @var $pill UsageStat
             */
            $sheet->setCellValue("A$i", $pill->getCan());
            $sheet->setCellValue("B$i", $pill->getMedicineName());
            $sheet->setCellValue("C$i", $pill->fulls);
            $sheet->setCellValue("D$i", $pill->halfs);
            $sheet->setCellValue("E$i", $pill->quaters);
            $sheet->setCellValue("F$i", $pill->thirds);
            $sheet->setCellValue("G$i", $pill->getSum());

            $pckgs = [];
            $pckgs_usg = [];
            foreach ($pill->getPackageList() as $pck) {
                $pckgs[] = $pck->size;
                $pckgs_usg[] = round($pill->getSum() / $pck->size, 2);
            }

            $pck_string = join(" - ", $pckgs);
            $pckgs_usg_string = join(" - ", $pckgs_usg);

            $sheet->setCellValue("H$i", $pck_string);
            $sheet->setCellValue("I$i", $pckgs_usg_string);
            $sheet->getStyle("A$i:I$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);

            $i++;
        }

        $document->getActiveSheet()->setAutoFilter('A6:I6');

        $this->writeDoc($path, $document, "spoteba." . $format, $format);

        return $path . '/../data/spoteba.' . $format;
    }

    /**
     * vyroba.xls - in API:Production
     *
     * @param        $path
     * @param int    $branche
     * @param null   $department
     * @param int    $period
     * @param string $format
     *
     * @return string
     */
    public function production($path, $branche = 0, $department = null, $period = 2, $format = "xls")
    {
        $this->patientRepository = new PatientRepository($this->database);
        $this->usageRepository = new UsageRepository($this->database);
        $this->medicineRepository = new MedicineRepository($this->database);
        $this->brancheRepository = new BrancheRepository($this->database);

        $ar = ["state" => "running"];

        if ($branche == 0) {
            $branche_name = "všechny";
        } else {
            $ar["branche_id"] = $branche;
            $branche_name = $this->brancheRepository->getByID($branche)->name;
        }

        if ($department == 0) {
            $department_name = "všechny";
        } else {
            $ar["department_id"] = $department;
            $department_name = $this->brancheRepository->getDepartment($department)->name;
        }

        $document = $this->prepareDoc($path, "production_tpl.xls", "Výroba", "Výroba");
        $sheet = $document->getActiveSheet();


        $source = date("Y-m-d");
        $date = new DateTime($source);
        $newDateString = $date->format('d.m.Y');

        $sheet->setCellValue("B2", $newDateString);
        $sheet->setCellValue("B3", $branche_name);
        $sheet->setCellValue("B4", $department_name);


        $sheet->getStyle("A1:G1")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);
        $sheet->getStyle("A2:G2")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_NONE);
        $patients = $this->patientRepository->findBy($ar)->orderBy([
                                                                       "branche_id" => "branche_id",
                                                                       "last_name" => "last_name",
                                                                   ]);

        $j = 0;  // index one less that usage but ++$j is used
        $i = 5;

        foreach ($patients as $patient) {
            /**
             * @var $patient Patient
             */
            if (!$this->usageRepository->hasUsage($patient->id)) {
                continue;
            }
            ++$i; // the increment MUST be after checking of usage
            ++$j;

            $sheet->setCellValue("A$i", $j);
            $sheet->setCellValue("B$i", $patient->getFullName());
            $sheet->setCellValue("C$i", $patient->getBranche());
            $sheet->setCellValue("D$i", $patient->getDepartment());
            $this->setAlign($sheet, "A$i", "center");
            $this->setAlign($sheet, "D$i", "center");
            $sheet->getStyle("A$i:G$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            $sheet->getStyle("E$i:G$i")->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            $sheet->getStyle("D$i:G$i")
                  ->getBorders()
                  ->getVertical()
                  ->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            $sheet->getStyle("G$i")->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
        }

        $document->getActiveSheet()->setAutoFilter('A5:D5');

        $i++; // add one more line padding

        // vvv signature part bellow the data

        // block "vyrobene kontejnery"
        $sheet->setCellValue("B" . ($i + 2), "kontejnerů VYROBENO");
        $sheet->mergeCells('B' . ($i + 2) . ':B' . ($i + 3));
        $sheet->getStyle('A' . ($i + 2))->getBorders()->getAllBorders()
              ->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
        $sheet->setCellValue("A" . ($i + 2), $j);
        $sheet->mergeCells('A' . ($i + 2) . ':A' . ($i + 3));

        // block "vracene kontejnery"
        $sheet->setCellValue("B" . ($i + 4), "kontejnerů VRÁCENO");
        $sheet->mergeCells('B' . ($i + 4) . ':B' . ($i + 5));
        $sheet->setCellValue("A" . ($i + 4), "");
        $sheet->mergeCells('A' . ($i + 4) . ':A' . ($i + 5));
        $sheet->getStyle('A' . ($i + 4) . ':A' . ($i + 5))->getBorders()->getAllBorders()
              ->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
        $sheet->mergeCells('A' . ($i + 4) . ':A' . ($i + 5));

        // vertical and horizontal align for "kontejnery"
        $this->setAlign($sheet, 'A' . ($i + 2) . ':A' . ($i + 5), PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->setAlign($sheet, 'B' . ($i + 2) . ':B' . ($i + 5), PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                        PHPExcel_Style_Alignment::VERTICAL_CENTER);

        // "datum" block
        $sheet->setCellValue("C" . ($i + 1), "Datum");
        $this->setAlign($sheet, "C" . ($i + 1), PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->mergeCells('C' . ($i + 1) . ':C' . ($i + 3));
        $sheet->mergeCells('D' . ($i + 1) . ':F' . ($i + 3));
        $sheet->getStyle('D' . ($i + 1) . ':D' . ($i + 3))->getBorders()->getBottom()
              ->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);

        // "jmeno" block
        $sheet->setCellValue("C" . ($i + 4), "Jméno");
        $sheet->mergeCells('C' . ($i + 4) . ':C' . ($i + 6));
        $sheet->mergeCells('D' . ($i + 4) . ':F' . ($i + 6));
        $this->setAlign($sheet, "C" . ($i + 4), PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('D' . ($i + 4) . ':D' . ($i + 6))->getBorders()->getBottom()
              ->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);

        // "podpis" block
        $sheet->setCellValue("C" . ($i + 7), "Podpis");
        $sheet->mergeCells('C' . ($i + 7) . ':C' . ($i + 9));
        $sheet->mergeCells('D' . ($i + 7) . ':F' . ($i + 9));
        $this->setAlign($sheet, "C" . ($i + 7), PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('D' . ($i + 7) . ':D' . ($i + 9))->getBorders()->getBottom()
              ->setBorderStyle(PHPExcel_Style_Border::BORDER_DOTTED);

        $this->writeDoc($path, $document, "vyroba." . $format, $format);

        return $path . '/../data/vyroba.' . $format;
    }

    public function logChunk($path, $fromDate, $toDate, $userID)
    {

        $fromDateFormated = date('Y-m-d', strtotime($fromDate));
        $toDateFormated = date('Y-m-d', strtotime($toDate));

        $this->logRepository = new LogRepository($this->database);
        $this->userRepository = new UserRepository($this->database);
        $this->logRepository = new LogRepository($this->database);

        // filtering for the log
        $filter = ["date >= ?" => $fromDateFormated, "date < ?" => $toDateFormated];

        /** @var User $user */

        $user = $this->userRepository->getByID($userID);
        $role = $user->getRole();

        if ($role == "branche" OR $role == "branche_department") {
            $branches = $user->getBranche();

            $filter["patient.branche_id"] = [];
            foreach ($branches as $branche) {
                $filter["patient.branche_id"][] = $branche->branche_id;
            }
        } elseif ($role == "department") {
            $deps = $user->getDepartments();

            $filter["patient.department_id"] = [];
            foreach ($deps as $dep) {
                $filter["patient.department_id"] = $dep->id;
            }
        }
        // ^^  filtering for the log
        $data = $this->logRepository->findBy($filter);

        $dokument = $this->prepareDoc($path, "log_tpl.xls", "Historie", "Historie");
        $list = $dokument->getActiveSheet();

        $list->setCellValue("C2", date("d.m.Y"));
        $list->setCellValue("C3", $fromDate);
        $list->setCellValue("C4", $toDate);

        $i = 7;
        /** @var Log $item */
        foreach ($data as $item) {
            $list->setCellValue("A$i", $item->translate());
            $list->setCellValue("B$i", $item->getUserName());
            $list->setCellValue("C$i", $item->getBranchName());
            $list->setCellValue("D$i", $item->getPatientName());
            $list->setCellValue("E$i", $item->date);
            $list->setCellValue("F$i", $item->dataTranslate(false));

            $i++;
        }

        $dokument->getActiveSheet()->setAutoFilter('A6:F6');

        $this->writeDoc($path, $dokument, "historie_" . $fromDateFormated . "_" . $toDateFormated . ".xls", "xls");

        return $path . "/../data/" . "historie_" . $fromDateFormated . "_" . $toDateFormated . ".xls";
    }

    /**
     * @param $list PHPExcel_Worksheet
     * @param $color
     * @param $cells
     */
    private function setColor(&$list, $color, $cells)
    {
        $color_ar = ["critical" => "FFC8C8", "warning" => "FFDDA6", "active" => "B1FFFA"];

        if (array_key_exists($color, $color_ar)) {
            $color = $color_ar[$color];
        } else {
            if ($color[0] == "#") {
                $color = str_replace("#", "", $color);
            } else {
                $color = "FFFFFF";
            }
        }
        $list->getStyle($cells)->getFill()->applyFromArray([
                                                               'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                                               'startcolor' => [
                                                                   'rgb' => $color,
                                                               ],
                                                           ]);
        $this->setAlign($list, $cells, "center");
    }

    /**
     * @param $list PHPExcel_Worksheet
     * @param $cells
     */
    private function setBold(&$list, $cells)
    {
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
        ];
        $list->getStyle($cells)->applyFromArray($styleArray);
    }

    /**
     * @param      $list PHPExcel_Worksheet
     * @param      $cells
     * @param      $align
     * @param null $vertical
     */
    private function setAlign(&$list, $cells, $align, $vertical = null)
    {
        switch ($align) {
            case "center":
                {
                    $al = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
                    break;
                }
            case "justify":
                {
                    $al = PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY;
                    break;
                }
            case "right":
                {
                    $al = PHPExcel_Style_Alignment::HORIZONTAL_RIGHT;
                    break;
                }
            default:
                {
                    $al = PHPExcel_Style_Alignment::HORIZONTAL_LEFT;
                    break;
                }
        }
        $style = [
            'alignment' => [
                'horizontal' => $al,
            ],
        ];
        if ($vertical) {
            $style['alignment']['vertical'] = $vertical;
        }
        $list->getStyle($cells)->applyFromArray($style);
    }

    public function prescription(
        $path,
        $branche = 0,
        $department = [],
        $format = "xls",
        $speciality = 0,
        $patient_id = 0,
        $to = null,
        $showBd = false,
        $showIns = false,
        $showRc = false
    )
    {
        $by_ar = ["patient.state" => "running"];
        $this->patientRepository = new PatientRepository($this->database);
        $this->usageRepository = new UsageRepository($this->database);
        $this->medicineRepository = new MedicineRepository($this->database);
        $this->specialityRepository = new SpecialityRepository($this->database);
        $this->brancheRepository = new BrancheRepository($this->database);
        $this->settingRepository = new SettingRepository($this->database);

        $document = $this->prepareDoc($path, "prescription_tpl.xls", "Stavy léků", "Výpis stavů léků");
        $sheet = $document->getActiveSheet();

        if ($branche == 0) {
            $branche_name = "všechny";
        } else {
            $branche_name = $this->brancheRepository->getByID($branche)->name;
            $by_ar["patient.branche_id"] = $branche;
        }

        if ($speciality == 0) {
            $speciality_name = "všechny";
        } else {
            $by_ar["speciality_id"] = $speciality;
            $speciality_obj = $this->specialityRepository->getByID($speciality);
            $speciality_name = $speciality_obj->name;
        }

        if ($department != []) {
            $by_ar["patient.department_id"] = $department;
            $department_name = "";
            $department_names = $this->brancheRepository->getDepartmentAr($department);
            foreach ($department_names as $dep) {
                $department_name .= $dep->name . " ";
            }
        } else {
            $department_name = "všechny";
        }

        if ($patient_id == 0) {
            $data = $this->usageRepository->findBy($by_ar)->orderBy("remaining_days");
        } else {
            $data = $this->usageRepository->findBy(["patient_id" => $patient_id])->orderBy("remaining_days");
        }


        $source = date("Y-m-d");
        $date = new DateTime($source);
        $newDateString = $date->format('d.m.Y');
        $sheet->setCellValue("B2", $branche_name);
        $sheet->setCellValue("B3", $department_name);
        $sheet->setCellValue("B4", $speciality_name);
        $sheet->setCellValue("B5", $newDateString);
        $i = 7;

        if ($to) {
            $toDateObj = new DateTime($to);
            $diffDays = (int)$toDateObj->diff(new DateTime(), true)->format("%a") + 1;
        } else {
            $diffDays = 14;
        }
        $sheet->setCellValue("H4", $diffDays);


        foreach ($data as $item) {
            /* @var $item Usage */
            if ($item->state != "ok") {
                $this->setColor($sheet, $item->state, "A$i:L$i");
                $this->setAlign($sheet, "A$i:L$i", "left");
            }

            $sheet->setCellValue("A$i", $item->getPatient());
            $sheet->setCellValue("B$i", $item->getPatientDepartment());
            $sheet->setCellValue("C$i", $item->getMedicine()->name);

            $sheet->setCellValue("D$i", $item->getSpeciality());
            $sheet->setCellValue("E$i", $item->getPills(($format == "pdf")));
            $sheet->setCellValue("F$i", $item->getRemainingDays(($format == "pdf")));
            $sheet->setCellValue("G$i", $item->getRemainingDate());

            $sheet->setCellValue("H$i", $item->getDailyUsage(true));
            $sheet->setCellValue("I$i", '=H' . $i . '*H$4-E' . $i);  // the $ is because of locked reference
            // set formula https://stackoverflow.com/questions/21302389/how-can-insert-formula-in-excel-sheet-using-phpexcel

            $sheet->setCellValue("J$i", $item->getPatientDate());
            $sheet->setCellValue("K$i", $item->getPatientIns());
            $sheet->setCellValue("L$i", $item->getPatientRc());

            $sheet->getStyle("A$i:L$i")->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_HAIR);
            $i++;
        }
//		$dokument->getActiveSheet()->setAutoFilter('A6:H6');
        // ^^ toto pouzij pokud je tam countToDate()

        $filter = 'K';

        $extraColumns = ['L' => $showRc, 'K' => $showIns, 'J' => $showBd];

        $enabledColumns = 0;
        foreach ($extraColumns as $column => $enabled){
            // going backwards
            if(!$enabled){
                $sheet->removeColumn($column); // bd
            }
            else{
                $enabledColumns++;
            }
        }

        $columnsInNormal = array_reverse(array_keys($extraColumns));

        if ($enabledColumns) {
            $maxFilter = $columnsInNormal[$enabledColumns - 1];
        } else {
            $maxFilter = 'I';
        }

        $document->getActiveSheet()->setAutoFilter('A6:' . $maxFilter . '6');


        if (!is_array($department)) {
            $department = [];
        }
        $this->writeDoc($path, $document,
                        "prescription" . $branche . implode("-", $department) . $speciality . "." . $format, $format);

        return $path . '/../data/prescription' . $branche . implode("-", $department) . $speciality . '.' . $format;
    }

    private function prepareDoc($path, $tpl, $title, $desc)
    {
        $dokument = PHPExcel_IOFactory::load($path . '/../data/tpl/' . $tpl);

        $author = "";

        $dokument->getProperties()->setDescription($desc)
                 ->setLastModifiedBy($author)
                 ->setModified(time())
                 ->setTitle($title);

        $dokument->setActiveSheetIndex(0);


        $vlastnosti = $dokument->getProperties();
        $vlastnosti->setCreated(time());
        $vlastnosti->setCreator('Lekynamiru.cz');

        $list = $dokument->getActiveSheet();
        $list->getPageSetup()->setFitToPage(true);
        $list->getPageSetup()->setFitToWidth(1);
        $list->getPageSetup()->setFitToHeight(0);
        $list->getPageMargins()->setRight(0.4);
        $list->getPageMargins()->setLeft(0.4);
        $list->getPageMargins()->setBottom(0.3);
        $list->getPageMargins()->setTop(0.3);

        return $dokument;
    }

    private function writeDoc($path, $dokument, $filename, $format = "pdf")
    {
        if ($format == "pdf") {
            $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
            $rendererLibraryPath = $path . '/../vendor/mpdf/mpdf/';

            PHPExcel_Settings::setPdfRenderer(
                $rendererName,
                $rendererLibraryPath
            );
            $writer = new PHPExcel_Writer_PDF_mPDF($dokument);
        } else {
            $writer = PHPExcel_IOFactory::createWriter($dokument, 'Excel5');
        }
        $writer->save($path . '/../data/' . $filename);
    }
}