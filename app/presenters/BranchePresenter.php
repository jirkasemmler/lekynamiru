<?php

namespace App\Presenters;

use App\Forms\BrancheFormFactory;
use App\Forms\OrderFormFactory;
use App\Model\Repositories\BrancheRepository;
use App\Model\Repositories\OrderSettingRepository;
use App\Model\Repositories\SpecialityRepository;
use App\Model\Repositories\PredikceRepository;
use model\Exceptions\DataException;
use Nette\Forms\Form;

class BranchePresenter extends BasePresenter {
    private $user;

    /** @var BrancheRepository @inject */
    public $brancheRepository;

    /** @var BrancheFormFactory @inject */
    public $brancheFormFactory;

    /** @var OrderFormFactory @inject */
    public $orderFormFactory;

    /** @var SpecialityRepository @inject */
    public $specialityRepository;

    /** @var PredikceRepository @inject */
    public $predikceRepository;

    /** @var OrderSettingRepository @inject */
    public $orderSettingRepository;


    protected $id;
    protected $idDepartment;
    protected $idBranche;
    protected $orderSettingId;

    public function beforeRender() {
        parent::beforeRender();
        try {
            $this->user = $this->getUser();

            if (!$this->user->isLoggedIn()) {
                $this->redirect("Login:out");
            }

        } catch (\Exception $e) {
            $this->redirect("Login:out");
        }
    }

    public function renderDefault() {
        $this->template->data = $this->brancheRepository->getBranches();
        $this->template->mainTitle = "Domovy";
        $this->template->subTitle = "Výpis domovů a oddělení";
    }


    public function handleAddOrderSetting($id, $orderSettingId = null) {
        $this->id = $id;
        $this->orderSettingId = $orderSettingId;
        $this->template->showOrderSettingForm = true; //show components or title
        $this->template->showBrancheForm = false; //show components or title
        $this->template->showTitle = true;

        if ($id == 0) {
            $this->template->modalTitle = "Přidat nastavení";
        } else //update
        {
            $this->id = $id; //saving id
            $this->template->modalTitle = "Editovat nastavení";
        }

        if ($this->presenter->isAjax()) {
            $this->redrawControl('listSnippet');
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    public function createComponentOrderSettingForm() {
        $form = $this->orderFormFactory->createOrderSetting($this->id, $this->orderSettingId);
        $form->onSuccess[] = array($this, 'orderSettingSuccess');
        return $form;
    }

    /**
     * @param $form Form
     */
    public function orderSettingSuccess($form) {
        $values = $form->getValues();
        $ret = false;
        try {
            $ret = $this->orderSettingRepository->manage($values);

            if ($ret) {
                $this->flashMessage('Nastavení bylo uloženo!', "success");
            } else {
                $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
            }
        } catch (DataException $e) {
            $this->flashMessage('Datum a den již jsou nastaveny, nelze nastavit dvakrát!', "danger");
        }


        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            if($ret){
                $this->template->showOrderSettingForm = false; //show components or title
                $this->redrawControl('formSnippet');
            }
            $this->redrawControl('listSnippet');
            $this->redrawControl('flashes');
        }
    }

    public function handleManageBranche($id) {
        $this->id = $id;
        $this->template->showDepartmentForm = false; //show components or title
        $this->template->showBrancheForm = true; //show components or title
        $this->template->showTitle = true;

        if ($id == 0) //new grade
        {
            $this->template->modalTitle = "Přidat domov";
        } else //update
        {
            $this->id = $id; //saving id
            $this->template->modalTitle = "Editovat domov" . $this->id;
        }

        if ($this->presenter->isAjax()) {
            $this->redrawControl('listSnippet');
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    public function handleManageSpec($id) {
        $this->template->specialities = $this->specialityRepository->findAll();

        $this->id = $id;
        $this->template->predikce = $this->predikceRepository->findBy(["branche_id" => $this->id]);
        $this->template->showDepartmentForm = false; //show components or title
        $this->template->showBrancheForm = false; //show components or title
        $this->template->showSpecForm = true; //show components or title
        $this->template->showTitle = true;

        if ($id == 0) //new grade
        {
            $this->template->modalTitle = "Přidat domov";
        } else //update
        {
            $this->id = $id; //saving id
            $this->template->modalTitle = "Editovat odbornosti domova";
        }

        if ($this->presenter->isAjax()) {
            $this->redrawControl('listSnippet');
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    protected function createComponentSpecForm() {

        $form = $this->brancheFormFactory->createSpecialities($this->id);
        $form->onSuccess[] = array($this, 'specSuccess');

        return $form;
    }

    public function specSuccess($form, $values) {
        $ret = $this->predikceRepository->manage($values);

        if ($ret === true) {
            $this->flashMessage('Predikce byly uloženy!', 'success');
        } else {
            $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            $this->redrawControl('listSnippet');
            $this->redrawControl('flashes');
            $form->setValues(array(), true); //clear form for next usage
        }
    }

    protected function createComponentForm() {
        $form = $this->brancheFormFactory->create($this->id);
        $form->onSuccess[] = array($this, 'brancheSuccess');

        return $form;
    }

    /**
     * method to login user
     * @param $form
     * @internal param $values - data from form
     */
    public function brancheSuccess($form) {

        $values = $form->getValues();
        $ret = $this->brancheRepository->manage($values);

        if ($ret === true) {
            $this->flashMessage('Domov byl uložen!', 'success');
        } else {
            $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            $this->redrawControl('listSnippet');
            $this->redrawControl('flashes');
            $form->setValues(array(), true); //clear form for next usage
        }
    }

    public function handleDelOrderSetting($id) {
        if ($this->orderSettingRepository->del($id)) {
            $this->payload->status = "ok";
        } else {
            $this->payload->status = "err";
        }
        $this->sendPayload();
    }

    public function handleDelBranche($id) {
        if ($this->brancheRepository->delBranche($id)) {
            $this->payload->status = "ok";
        } else {
            $this->payload->status = "err";
        }
        $this->sendPayload();
    }

    public function handleManageDepartment($idBranche, $idDepartment = 0) {
        $this->idBranche = $idBranche;
        $this->idDepartment = $idDepartment;
        $this->template->showDepartmentForm = true; //show components or title
        $this->template->showBrancheForm = false; //show components or title
        $this->template->showTitle = true;

        if ($idDepartment == 0) {
            //new department
            $this->template->modalTitle = "Přidat oddělení do domova";
        } else //update
        {
            $this->template->modalTitle = "Editovat oddělení domova";
        }

        if ($this->presenter->isAjax()) {
            $this->redrawControl('listSnippet');
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }


    protected function createComponentDepartmentForm() {
        $form = $this->brancheFormFactory->createDepartment($this->idBranche, $this->idDepartment);
        $form->onSuccess[] = array($this, 'departmentSuccess');

        return $form;
    }

    /**
     * method to login user
     * @param $form
     * @internal param $values - data from form
     */
    public function departmentSuccess($form, $values) {
        $ret = $this->brancheRepository->manageDepartment($values);

        if ($ret === true) {
            $this->flashMessage('Oddělení bylo uloženo!', 'success');
        } else {
            $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            $this->redrawControl('listSnippet');
            $this->redrawControl('flashes');
            $form->setValues(array(), true); //clear form for next usage
        }
    }

    public function handleDelDepartment($id) {
        if ($this->brancheRepository->delDepartment($id)) {
            $this->payload->status = "ok";
        } else {
            $this->payload->status = "err";
        }
        $this->sendPayload();
    }


}
