<?php

namespace App\Presenters;

use App\Model\Entities\Patient;
use App\Model\Entities\Usage;
use App\Model\Repositories\PatientRepository;
use App\Forms\PatientFormFactory;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\PlanRepository;
use App\Model\Repositories\UsageRepository;
use App\Model\Repositories\LogRepository;
use Nette\Application\UI\Form;
use Nette\Diagnostics\Debugger;
use App\Model\Repositories\SettingRepository;

class PatientPresenter extends BasePresenter
{
    protected $id;
    protected $id_card;
    protected $id_usage;
    protected $patient_id;
    public $user;
    public $showCard = false;
    /** @var PatientRepository @inject */
    public $patientRepository;
    /** @var Mailer @inject */
    public $mailer;
    /** @var PlanRepository @inject */
    public $planRepository;
    /** @var UsageRepository @inject */
    public $usageRepository;
    /** @var PatientFormFactory @inject */
    public $patientFormFactory;
    /** @var LogRepository @inject */
    public $logRepository;
    /** @var UserRepository @inject */
    public $userRepository;
    /** @var SettingRepository @inject */
    public $settingRepository;
    /** @var FileManager @inject */
    public $FileManager;
    private $department = null;

    public function startup()
    {
        parent::startup();

        try {
            $this->user = $this->getUser();

            if (!$this->user->isLoggedIn()) {
                $this->redirect("Login:out");
            } else {
                $this->department = $this->userRepository->getDepartment($this->user->id);

                $this->template->showSecretData = $this->userRepository->getByID($this->user->id)->showSecretData;
            }
        } catch (\Exception $e) {
            $this->redirect("Login:out");
        }
    }

    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->mainTitle = "Pacienti";
    }

    protected function getFilterPre($show, $branche)
    {
        if ($show == "all") {
            $by_ar = [];
        } elseif ($show == "warning") {
            $by_ar = ["state != ?" => "ok"];
        } elseif ($show == "critical") {
            $by_ar = ["state" => "critical"];
        }

        if ($this->getUser()
                 ->getRoles()[0] == "department"
        ) {
            $deps = $this->userRepository->getDepartment($this->getUser()->id);
            $by_ar["patient.department_id"] = $deps;
        }

        $by_ar["patient.branche_id"] = $branche;
        $by_ar["patient.state != ?"] = 'dead';

        return $by_ar;
    }

    public function actionPrescription($show = "all")
    {
        $this->template->mainTitle = "Stavy";
        $this->template->subTitle = "Pacienti a léky";

        $by_ar = $this->getFilterPre($show, $_SESSION['branche']);

        $data = $this->usageRepository->findBy($by_ar)
                                      ->orderBy("remaining_days");

        $this->template->data = $data;
        $this->template->date = date('Y-m-d', strtotime("+14 days"));
    }

    protected function getFilter($branche)
    {
        $by_ar = [];

        if ($this->getUser()
                 ->getRoles()[0] == "department"
        ) {
            $deps = $this->userRepository->getDepartment($this->getUser()->id);
            $by_ar["patient.department_id"] = $deps;
        }

        $by_ar["patient.branche_id"] = $branche;

        return $by_ar;
    }

    /**
     * ajax handler to change the content in Batching action
     *
     * @param $from - just a placeholder bcause of compatibility with log presenter
     * @param $to - date where pills are counted to
     */
    public function handleChangeBatchingRange($from, $to)
    {
        $toF = date('Y-m-d', strtotime($to));

        $filter = $this->getFilter($_SESSION["branche"]);
        $this->template->date = $toF;
        $this->template->data = $this->usageRepository->findBy($filter)
                                                      ->orderBy("medicine_id");

        $this->redrawControl('listSnippet');
    }

    /**
     * pridani pills pacientovi
     *
     * @param $id_usage
     */
    public function handleAddPills($id_usage)
    {
        $plans = $this->usageRepository->getByID($id_usage);

        $this->id_usage = $id_usage;
        $usage = $this->usageRepository->getByID($id_usage);
        $this->template->usage = $usage;
        $this->template->showPillsForm = true; //show components or title
        $this->template->showTitle = true;

        $this->template->modalTitle = "Přidat tablety";

        if ($this->presenter->isAjax()) {
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    /**
     * form na single pridani
     * @return \Nette\Application\UI\Form
     */
    public function createComponentPillsForm()
    {
        $form = $this->patientFormFactory->createPills($this->id_usage);
        $form->onSuccess[] = [$this, 'addPillsSuccess'];

        return $form;
    }

    /**
     * handler na multi pridavani tablet/baleni
     *
     * @param $id
     */
    public function handleMultiPills($id)
    {

        $this->patient_id = $id;
        $this->template->patient = $this->patientRepository->getByID($id);
        $this->template->showMultiPillsForm = true; //show components or title

        $this->template->showTitle = true;

        $this->template->modalTitle = "Přidat tablety";

        if ($this->presenter->isAjax()) {
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    /**
     * multiformular na pills - vykresluje se rucne
     * @return \Nette\Application\UI\Form
     */
    public function createComponentMultiPillsForm()
    {
        $form = $this->patientFormFactory->createMultiPills($this->patient_id);
        $form->onSuccess[] = [$this, 'addMultiPillsSuccess'];

        return $form;
    }

    /**
     * zpracovani multipills
     *
     * @param $form Form
     * @param $values
     */
    public function addMultiPillsSuccess($form, $values)
    {

        try {
            foreach ($values->pills as $usageId => $dataSet) {

                $this->usageRepository->addReserve($dataSet, $_POST[$usageId . "add_by"]);
                $this->addPills($dataSet, $_POST[$usageId . "add_by"]); // spina a hnus
            }

            $this->flashMessage("Stav zásoby byl upraven", 'success');
        } catch (\Exception $ex) {
            \Tracy\Debugger::log($ex);
            $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
        }


        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            $this->redrawControl('flashes');
            $form->setValues([], true); //clear form for next usage
        }
    }

    /**
     * single zpracovani multipills
     *
     * @param $data
     * @param $addBy
     */
    protected function addPills($data, $addBy)
    {
        /**
         * @var Usage $usage
         */
        $usage = $this->usageRepository->getByID($data->id_usage);
        $patient = $usage->getPatient();
        $medicine = $usage->getMedicine();

        if ($addBy == "pills") {
            $log_type = "added_pills";
            $msg = "Pacient: <b>" .
                $patient .
                "</b> , lék: <b>" .
                $medicine->name .
                "</b>. Přidáno <b>" .
                $data->pills .
                "</b> tablet";
        } elseif ($addBy == "pck") {

            $log_type = "added_pck";
            $msg = "Pacient: <b>" .
                $patient .
                "</b> , lék: <b>" .
                $medicine->name .
                "</b>. Přidáno <b>" .
                $data->pckgs .
                "</b> balení po <b>" .
                $data->pcksize .
                "</b> tabletách (" .
                $data->pckgs * $data->pcksize .
                " tablet)";
        }

        $this->logRepository->create_log_addpills($log_type, $this->getUser()->id, $msg, $usage->patientId);
    }

    public function addPillsSuccess($form, $data)
    {

        try {
            $this->usageRepository->addReserve($data);
            $this->addPills($data, $_POST["add_by"]);

            $this->flashMessage("Stav zásoby byl upraven", 'success');
        } catch (\Exception $ex) {
            \Tracy\Debugger::log($ex);
            $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            $this->redrawControl('flashes');
            $form->setValues([], true); //clear form for next usage
        }
    }

    public function actionDefault()
    {
        $this->template->showCardManualy = $this->showCard;
        $this->template->data = $this->patientRepository->getPatients($_SESSION["branche"], $this->department,
            Patient::NON_DEAD_STATES);
    }

    public function renderDead()
    {
        $this->setView("default");
        $this->template->showCardManualy = $this->showCard;
        $this->template->data = $this->patientRepository->getPatients($_SESSION["branche"], 0, [Patient::STATE_DEAD]);
    }

    public function handlePlan($id)
    {
        $plans = $this->planRepository->findByPatientId($id);
        $this->template->plans = $plans;;
        $this->patient_id = $id;

        $this->template->addToCard = false; //show components or title
        $this->template->showMedCard = false; //show components or title
        $this->template->addToCardEnable = false; //show components or title
        $this->template->showPlanForm = true; //show components or title
        $this->template->showPatientForm = false; //show components or title

        $this->template->showTitle = true;

        $this->template->modalTitle = "Plánování pacienta";


        if ($this->presenter->isAjax()) {
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    public function handleManagePatient($id)
    {
        $this->id = $id;

        $this->template->addToCard = false; //show components or title
        $this->template->showMedCard = false; //show components or title
        $this->template->addToCardEnable = false; //show components or title
        $this->template->showPlanForm = false; //show components or title
        $this->template->showPatientForm = true; //show components or title

        $this->template->showTitle = true;

        if ($id == 0) //new grade
        {
            $this->template->modalTitle = "Přidat pacienta";
        } else //update
        {
            $this->id = $id; //saving id
            $this->template->modalTitle = "Editovat pacienta";
        }

        if ($this->presenter->isAjax()) {
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    /**
     * @param $form Form
     */
    public function cardSuccess($form)
    {
        $values = $form->getValues();
        $values->userID = $this->getUser()->id;
        $ret = $this->usageRepository->manageCard($values);
        // ret = medical_card_usage_meta_id - ID ve vazbe
        if ($ret != false) {
            if ($values->id_usage == 0) {
                $log_type = "usage_created";
            } else {
                $log_type = "usage_changed";
            }
            $this->logRepository->create_log_mc($log_type, $this->getUser()->id, $ret, $ret->usage_meta->patient_id);

            if ($this->getUserEntity()->getUserRole()->sendNotifications) {
                $file = $this->FileManager->mk($this->context->parameters['wwwDir'], $values->id_patient,
                    $this->getUserEntity(), "pdf");

                /**
                 * @var $usageMeta Usage
                 */
                $usageMeta = $this->usageRepository->getByID($ret["usage_meta_id"]);
                $email = $this->settingRepository->getByKey("notifikacni_email")->value;
                $user_name = $this->userRepository->getByID($this->getUser()->id)->name;

                $patient_name = $usageMeta->getPatient();
                $medicine_name = $usageMeta->getMedicine()->name;

                $data = [
                    "user" => $user_name,
                    "patient" => $patient_name,
                    "medicine" => $medicine_name,
                    "date" => date("Y-m-d H:i:s"),
                ];

                $this->mailer->mk_change($file, $email, $data);
            }
            $this->flashMessage('Karta byla uložena!', 'success');
        } else {
            $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
//            $this->redrawControl('listSnippet');
            $this->redrawControl('flashes');
            $form->setValues([], true); //clear form for next usage
        }
    }

    public function handleShowCard($id, $mc_id = null)
    {
        $this->showCard = true;
        $this->template->addToCard = false; //show components or title
        $this->template->showMedCard = true; //show components or title
        $this->template->addToCardEnable = false; //show components or title
        $this->template->showPlanForm = false; //show components or title
        $this->template->showPatientForm = false; //show components or title
        if ($this->getUserEntity()->showSums()) {
            $this->template->showSums = true;
        } else {
            $this->template->showSums = false;
        }
        $this->template->showTitle = true;
        $this->template->modalTitle = "Medikační karta";

        $speciality_data = $this->usageRepository->getSpecialities($id, $mc_id);
        $this->template->specialities = $speciality_data[0];
        $this->template->sumUsages = $speciality_data[1];
        $this->template->parts = ["one" => "Celé", "half" => "Půlky", "quater" => "Čtvrtky", "third" => "Třetiny"];


        /** @var Patient $patient */
        $patient = $this->patientRepository->getByID($id);

        if ($mc_id == null) {
            $mc_id = $patient->getActiveMC()->id;
        }
        $this->template->revize = $mc_id;

        $this->template->patient = $patient;
        $karty = $patient->toRecord()
                         ->related("medical_card")
                         ->order("revision_number DESC");
        $this->template->mc_cards = $karty;
        if ($this->presenter->isAjax()) {
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    public function handleDelUsage($id_usage)
    {

        /**
         * @var $usage Usage
         */
        $usage = $this->usageRepository->getByID($id_usage);

        $ret = $this->usageRepository->del($id_usage, $this->getUser()->id);
        if ($ret != false) {
            $this->payload->status = "ok";
            $this->logRepository->create_log_mc("card_deleted", $this->getUser()->id, $ret, $usage->patientId,
                "Odebrání léku ");
        } else {
            $this->payload->status = "err";
        }
        $this->sendPayload();
    }

    public function handleDelPlan($id)
    {

        if ($this->planRepository->del($id)) {
            $this->payload->status = "ok";
        } else {
            $this->payload->status = "err";
        }
        $this->sendPayload();
    }

    public function handleAddToCard($id, $id_card = 0)
    {

        $this->id = $id;
        $this->id_card = $id_card;

        $this->template->addToCard = true; //show components or title
        $this->template->showMedCard = false; //show components or title
        $this->template->addToCardEnable = false; //show components or title
        $this->template->showPlanForm = false; //show components or title
        $this->template->showPatientForm = false; //show components or title

        $this->template->showTitle = true;

        $davky = ["morning", "noon", "evening", "night"];
        $dny = [
            "mon" => "Pondělí",
            "tue" => "Úterý",
            "wed" => "Středa",
            "thu" => "Čtvrtek",
            "fri" => "Pátek",
            "sat" => "Sobota",
            "sun" => "Neděle",
        ];
        $this->template->davky = $davky;
        $this->template->dny = $dny;
        $this->template->tydny = [
            "first" => ["name" => "První týden", "class" => "prvni_tyden"],
            "second" => ["name" => "Druhý týden", "class" => "dalsi_tyden druhy_tyden"],
            "third" => ["name" => "Třetí týden", "class" => "dalsi_tyden treti_tyden"],
            "fourth" => ["name" => "Čtvrtý týden", "class" => "dalsi_tyden ctvrty_tyden"],
            "fifth" => ["name" => "Pátý týden", "class" => "dalsi_tyden paty_tyden"],
            "sixth" => ["name" => "Šestý týden", "class" => "dalsi_tyden sesty_tyden"],
            "seventh" => ["name" => "Sedmý týden", "class" => "dalsi_tyden sedmy_tyden"],
            "eighth" => ["name" => "Osmý týden", "class" => "dalsi_tyden osmy_tyden"],
            "ninth" => ["name" => "Devátý týden", "class" => "dalsi_tyden devaty_tyden"],
            "tenth" => ["name" => "Desátý týden", "class" => "dalsi_tyden desaty_tyden"],
            "eleventh" => ["name" => "Jedenáctý týden", "class" => "dalsi_tyden jedenacty_tyden"],
            "twelfth" => ["name" => "Dvanáctý týden", "class" => "dalsi_tyden dvanacty_tyden"],
        ];

        if ($id == 0) {
            $this->template->modalTitle = "Přidat medikaci do medikační karty";
        } else {
            $this->id = $id; //saving id
            $this->template->modalTitle = "Editovat medikaci v medikační kartě";
        }

        if ($this->presenter->isAjax()) {
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    protected function createComponentPatientForm()
    {
        $form = $this->patientFormFactory->create($this->id);
        $form->onSuccess[] = [$this, 'patientSuccess'];

        return $form;
    }

    protected function createComponentCardForm()
    {
        $canChangeState = $this->getUserEntity()
                               ->isAllowed('Patient', ['action' => 'default', 'do' => 'changeNumber', 'params' => 'changeNumber']);
        $form = $this->patientFormFactory->createCard($this->id, $this->id_card, $canChangeState);
        $form->onSuccess[] = [$this, 'cardSuccess'];

        return $form;
    }

    protected function createComponentPlanForm()
    {
        $form = $this->patientFormFactory->createPlan($this->patient_id);
        $form->onSuccess[] = [$this, 'planSuccess'];

        return $form;
    }

    /**
     * @param $form Form
     */
    public function planSuccess($form)
    {

        $values = $form->getValues();
        $ret = $this->planRepository->planAdd($values);

        if ($ret == true) {
            $patient_name = $this->patientRepository->getByID($values->id_patient);
            $change_date = $values->date;
            $target_type = $values->target;
            $user_name = $this->userRepository->getByID($this->getUser()->id)->name;

            $this->logRepository->create_log_plan("plan_created", $this->getUser()->id, $user_name,
                $patient_name->firstName . " " . $patient_name->lastName, $change_date, $target_type,
                $patient_name->id);

            if ($this->getUserEntity()->getUserRole()->sendNotifications) {

                $email = $this->settingRepository->getByKey("notifikacni_email")->value;

                $data = [
                    "user" => $user_name,
                    "patient" => $patient_name->firstName . " " . $patient_name->lastName,
                    "change_date" => $change_date,
                    "date" => date("Y-m-d H:i:s"),
                    "target" => $this->logRepository->translate_state($target_type),
                ];

                $this->mailer->plan($email, $data);
            }
            $this->flashMessage('Změna pacienta byla naplánována!', 'success');
        } else {
            $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            $this->redrawControl('listSnippet');
            $this->redrawControl('flashes');
//			$this->redrawControl('gradeFormSnippet');
            $form->setValues([], true); //clear form for next usage
        }
    }

    /**
     * method to login user
     *
     * @param $form Form
     *
     * @internal param $values - data from form
     */
    public function patientSuccess($form)
    {

        $values = $form->getValues();
        $values->userID = $this->getUser()->id;
        $ret = $this->patientRepository->manage($values);

        if ($ret !== false) {
            $patient_name = $this->patientRepository->getByID($ret);
            $target_type = $values->state;
            $department = $values->department;
            $user_name = $this->userRepository->getByID($this->getUser()->id)->name;
            if ($values->id == 0) {
                $this->logRepository->create_edit_patient("patient_created", $this->getUser()->id, $user_name,
                    $patient_name->firstName . " " . $patient_name->lastName, $target_type,
                    $this->brancheRepository->getDepartment($department)->name, $patient_name->id);
            } else {
                $this->logRepository->create_edit_patient("patient_edited", $this->getUser()->id, $user_name,
                    $patient_name->firstName . " " . $patient_name->lastName, $target_type,
                    $this->brancheRepository->getDepartment($department)->name, $patient_name->id);

                if ($values->state == "dead") {
                    $pac = $this->patientRepository->getByID($values->id);

                    $file = $this->FileManager->prescription($this->context->parameters['wwwDir'], $pac->brancheId,
                        $pac->departmentId, "xls", 0, $pac->id);

                    //$mailer = new Mailer;
                    $email = $this->settingRepository->getByKey("notifikacni_email")->value;

                    $data = ["user" => $user_name, "patient" => $pac->getFullName(), "date" => date("Y-m-d H:i:s")];

                    $this->mailer->dead_patient($file, $email, $data);
                }
            }

            if ($this->getUserEntity()->getUserRole()->sendNotifications) {
                $email = $this->settingRepository->getByKey("notifikacni_email")->value;

                $data = [
                    "user" => $user_name,
                    "patient" => $patient_name->firstName . " " . $patient_name->lastName,
                    "date" => date("Y-m-d H:i:s"),
                    "target" => $this->logRepository->translate_state($target_type),
                    "department" => $this->brancheRepository->getDepartment($department)->name,
                ];

                $this->mailer->edit_patient($email, $data);
            }
            $this->flashMessage('Pacient byl uložen!', 'success');
        } else {
            $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            $this->redrawControl('listSnippet');
            $this->redrawControl('flashes');
            $form->setValues([], true); //clear form for next usage
        }
    }

    public function handleDelPatient($id)
    {

        if ($this->patientRepository->del($id)) {
            $this->payload->status = "ok";
        } else {
            $this->payload->status = "err";
        }
        $this->sendPayload();
    }
}

