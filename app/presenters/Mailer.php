<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 25.7.15
 * Time: 18:44
 */

namespace App\Presenters;

use App\Model\Entities\Order;
use App\Model\Entities\OrderItem;
use Nette;
use Nette\Mail\SendmailMailer;
use Nette\Mail\Message;
use Latte;
use Nette\Application\LinkGenerator;
use Nette\Application\Application;
use App\Model\Repositories\SettingRepository;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\PatientRepository;
use App\Model\Repositories\UsageRepository;


class Mailer {
    /** @inject Nette\Application\LinkGenerator */
    public $linkGenerator;

    /** @var SettingRepository @inject */
    public $settingRepository;

    /** @var UserRepository @inject */
    public $userRepository;

    /** @var PatientRepository @inject */
    public $patientRepository;

    /** @var UsageRepository @inject */
    public $usageRepository;

    /** @var Nette\Bridges\ApplicationLatte\ILatteFactory */
    private $latteFactory;

    private $presenter;


    public function __construct(SettingRepository $settingRepository) {
        $this->settingRepository = $settingRepository;
    }

    public function sendEmail($to_raw, $subject, $tpl, $data, $file = false) {
        $to = [];
        foreach ($to_raw as $one_raw) {
            if (filter_var($one_raw, FILTER_VALIDATE_EMAIL)) {
                $to[] = $one_raw;

            } else {
                echo "<br>invalid email adress : " . $one_raw . "<br>";
            }
        }

        if ($to == []) // there is no reciever
        {
            return true;
        }

        $latte = new Latte\Engine;

        Nette\Bridges\ApplicationLatte\UIMacros::install($latte->getCompiler());

        $predata = [
            '_presenter' => $this->presenter, // kvůli makru {plink}
            '_control' => $this->presenter    // kvůli makru {link}
        ];
        $tpl_data = array_merge($predata, $data);
        $html = $latte->renderToString(__DIR__ . '/templates/Emails/' . $tpl . '.latte', $tpl_data);

        $mail = new Message;

        $mail->setFrom('Lekynamiru.cz <info@lekynamiru.cz>')
             ->setReturnPath('-finfo@lekynamiru.cz')
             ->setSubject("Lekynamiru.cz - " . $subject)
             ->setHtmlBody($html);

        if ($file != false) {
            $mail->addAttachment($file);
        }


        if ($this->settingRepository->getBy(["key" => "staging"])->value == "yes" AND isset($this->settingRepository->getBy(["key" => "test_email"])->value)) {
            $mail->addTo($this->settingRepository->getBy(["key" => "test_email"])->value);
        } else {
            foreach ($to as $one) {
                $mail->addTo($one);
            }
        }


        $mailer = new SendmailMailer;
        $mailer->commandArgs = "-finfo@lekynamiru.cz";
        $mailer->send($mail);
    }

    public function sendTest($to) {
        $this->sendEmail($to, "test", "success", ["msg" => "hello"]);
    }

    public function registrace($pres, $in) {
        $this->presenter = $pres;
        $to = [$in["email"]];
        $subject = "Byl Vám zřízen účet!";
        $tpl = "newReg";
        $data = $in;
        $this->sendEmail($to, $subject, $tpl, $data);
    }

    public function lostPass($pres, $in) {
        $this->presenter = $pres;
        $to = [$in["email"]];
        $subject = "Obnova hesla";
        $tpl = "lostPass";
        $data = $in;
        $this->sendEmail($to, $subject, $tpl, $data);
    }

    public function mk_change($file, $email, $data) {
        $this->sendEmail([$email], "Změna medikační karty", "changeMC", $data, $file);
    }

    public function change_patient($file, $email, $data) {
        $this->sendEmail([$email], "Přesun pacienta do zesnulých", "change_state_pac", $data, $file);
    }

    public function branche_report($file, $emails, $data) {
        $this->sendEmail($emails, "Přehled stavů léků pro domov " . $data["branche_name"], "branche_report", $data,
            $file);
    }

    public function db($file, $emails, $data) {
//		$date = ;
        $this->sendEmail($emails, "DB report " . date("d.m.Y"), "db", $data, $file);
    }


    public function fail_report($email, $msg) {
        $this->sendEmail([$email], "Chybové hlášení", "failure", ["msg" => $msg]);
    }

    public function success_report($email, $msg) {
        $this->sendEmail([$email], "Hlášení o úspěšné operaci", "success", ["msg" => $msg]);
    }

    public function deparment_report($file, $email, $data) {
        $this->sendEmail([$email], "Přehled stavů léků pro oddělení " . $data["department_name"], "department_report",
            $data, $file);
    }

    public function speciality_report($file, $email, $data) {
        $this->sendEmail([$email], "Přehled stavů léků pro odbornost " . $data["speciality_name"], "speciality_report",
            $data, $file);
    }

    public function admin_report($file, $email, $data) {
        $this->sendEmail([$email], "Přehled stavů léků", "admin_report", $data, $file);
    }

    public function plan($email, $data) {
        $this->sendEmail([$email], "Naplánování změny stavu pacienta", "plan", $data);
    }

    public function edit_patient($email, $data) {
        $this->sendEmail([$email], "Editace pacienta", "edit_patient", $data);
    }

    public function dead_patient($file, $email, $data) {
        $this->sendEmail([$email], "Úmrtí pacienta", "dead_pac", $data, $file);
    }

    /**
     * @param $order Order
     * @param bool $auto - was is send automatically?
     * @return bool
     */
    public function sendSentOrder($order, $auto = false) {
        $email = $this->settingRepository->getNotificationEmail();
        $data = ['domov' => $order->getBranche(), 'order' => $order, 'auto' => $auto];
        $this->sendEmail([$email], "Zaslání objednávky", "order_sent", $data);

        if ($auto) {
            $brancheUsers = $order->getBranche()->getUsersForReport();
            $data = ['domov' => $order->getBranche(), 'order' => $order, 'auto' => $auto];
            $this->sendEmail($brancheUsers, "Automatické odeslání objednávky", "order_sent_branch", $data);
        }

        return true;
    }


    public function sendClosedOrder($order) {
        /**
         * @var $order Order
         */
        $brancheUsers = $order->getBranche()->getUsersForReport();
        $data = ['order' => $order];
        $this->sendEmail($brancheUsers, "Uzavření objednávky", "order_closed_branch", $data);
    }

    /**
     * new urgent item is in the order
     * @param $orderItem OrderItem
     */
    public function sendUrgentOrderItem($orderItem) {
        $email = $this->settingRepository->getNotificationEmail();
        $data = ['item' => $orderItem, 'order' => $orderItem->getOrder()];
        $this->sendEmail([$email], "Přidání urgentní položky", "order_urgent_item", $data);
    }
}
