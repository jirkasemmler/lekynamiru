<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 26.7.15
 * Time: 21:52
 */

namespace App\Presenters;

use App\Model\Entities\Branche;
use App\Model\Entities\Medicine;
use App\Model\Entities\OrderFile;
use App\Model\Entities\Patient;
use App\Model\Entities\Predikce;
use App\Model\Entities\Speciality;
use App\Model\Entities\User;
use App\Model\Repositories\LogRepository;
use App\Model\Repositories\MedicalCardRepository;
use App\Model\Repositories\OrderFileRepository;
use App\Model\Repositories\OrderRepository;
use Nette\Application\UI\Presenter;
use App\Model\Repositories\UsageRepository;
use PdfResponse\PdfResponse;
use Nette\Application\Responses;
use PHPExcel;

use App\Model\Repositories\PatientRepository;
use App\Model\Repositories\BrancheRepository;
use App\Model\Repositories\MedicineRepository;
use App\Model\Repositories\PredikceRepository;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\SettingRepository;
use App\Model\Repositories\PlanRepository;
use App\Model\Repositories\SpecialityRepository;
use App\Model\Repositories\UsageStatRepository;
use App\Presenters\Mailer;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

class ApiPresenter extends Presenter
{

    /** @var Mailer @inject */
    public $mailer;

    public $workingDir;
    //<editor-fold desc="injects">
    /** @var UsageRepository @inject */
    public $usageRepository;

    /** @var OrderFileRepository @inject */
    public $orderFileRepository;

    /** @var LogRepository @inject */
    public $logRepository;

    /** @var OrderRepository @inject */
    public $orderRepository;

    /** @var PatientRepository @inject */
    public $patientRepository;

    /** @var PlanRepository @inject */
    public $planRepository;

    /** @var UserRepository @inject */
    public $userRepository;

    /** @var SettingRepository @inject */
    public $settingRepository;

    /** @var PredikceRepository @inject */
    public $predikceRepository;

    /** @var SpecialityRepository @inject */
    public $specialityRepository;

    /** @var FileManager @inject */
    public $FileManager;

    /** @var BrancheRepository @inject */
    public $brancheRepository;

    /** @var MedicineRepository @inject */
    public $medicineRepository;

    /** @var UsageStatRepository @inject */
    public $usageStatRepository;

    /** @var MedicalCardRepository @inject */
    public $MCRepository;

    //</editor-fold>

    public function startup()
    {
        $this->workingDir = $this->context->parameters['wwwDir'] . "/../run/";
        parent::startup();

    }

    /**
     * @return User
     */
    public function getUserEntity()
    {
        return $this->userRepository->getByID($this->getUser()->getId());
    }


    public function actionCreateCards()
    {
        // zablokovani
        exit();
        $patients = $this->patientRepository->findAll();

        /**
         * @var Patient $patient
         */
        foreach ($patients as $patient) {
            $cards = $patient->toRecord()->related("medical_card");
            if ($cards->count() == 0) // there is no card for this patient
            {
                $usages = $patient->toRecord()->related("usage_meta");
                $this->MCRepository->createFirstCard($patient->id, $usages);
            }
        }

        echo "ok";
        $this->terminate();
    }


    public function actionRevision()
    {
        // zablokovani
        exit();
        $this->usageRepository->setRevision();
        echo "ok";
        $this->terminate();
    }


    public function actionLogChange()
    {
        echo "zablokovano";
        exit();
        $this->logRepository->setMedicalCards();
        echo "ok";
        $this->terminate();
    }

    //<editor-fold desc="cron-actions">

    public function actionSendDB()
    {
        if (!$this->context->parameters['consoleMode']) {
            echo "this is CLI action only";
            exit();
        }

        //$mailer = new Mailer;
        $prefix = $this->context->parameters['wwwDir'] . "/../db/dump/";
        $files = scandir($prefix);
        $date = date("Y-m-d");
        $file_names = preg_grep("/dump_" . $date . ".*/", $files);
        $users = ["jirka.semmler@gmail.com"]; // TEST

        foreach ($file_names as $file) {
            $this->mailer->db($prefix . $file, $users, ["msg" => $file]);
        }
        echo "DB zaslana\n";
        $this->terminate();
    }

    /**
     * recounting of remaining days for non active patients
     */
    public function actionNonActive()
    {
        if (!$this->context->parameters['consoleMode']) {
            echo "this is CLI action only";
            exit();
        }

        $this->usageRepository->recountNonActiveDays();

//		$this->payload->status = "ok";
//		$this->sendPayload();

        $this->writeResult("non-active", "done");
        $this->terminate();
    }

    /**
     * change active week for non active patients because they are not in recount()
     * @param string $who
     * @throws \Nette\Application\AbortException
     */
    public function actionChangeWeek($who = "sleeping")
    {
        if (!$this->context->parameters['consoleMode']) {
            echo "this is CLI action only";
            exit();
        }


        $by_ar = [];

        $by_ar["patient.state"] = $who;

        if ($this->usageRepository->changeWeekForPatients($by_ar)) {
            $this->writeResult("change-week", "done");
        } else {
            $this->writeResult("change-week", "failed");
        }
//		$this->sendPayload();
        $this->terminate();
    }

    public function actionRecount()
    {
        if (!$this->context->parameters['consoleMode']) {
            echo "this is CLI action only";
            exit();
        }

        $result = $this->usageRepository->recount(0, $this->workingDir, false);
        $this->writeResult("recount", $result);

        $this->terminate();
    }

    public function actionTest()
    {
        //$mailer = new Mailer;

        $this->mailer->sendTest(["test-lnm@seznam.cz", "jirka.semmler@gmail.com"]);

        $this->terminate();

    }

    public function actionMailer()
    {
        if (!$this->context->parameters['consoleMode']) {
            echo "this is CLI action only";
            exit();
        }

        $branches = $this->brancheRepository->getBranches();
        /** @var Branche $branche */
        foreach ($branches as $branche) {
            if ($branche->export) {
                $fileBd = $this->FileManager->prescription($this->context->parameters['wwwDir'], $branche->id, [], "xls", 0, 0, null, true, false);
                $fileIns = $this->FileManager->prescription($this->context->parameters['wwwDir'], $branche->id, [], "xls", 0, 0, null, false, true);
                $fileBdIns = $this->FileManager->prescription($this->context->parameters['wwwDir'], $branche->id, [], "xls", 0, 0, null, true, true);

                $fileBdRc = $this->FileManager->prescription($this->context->parameters['wwwDir'], $branche->id, [], "xls", 0, 0, null, true, false, true);
                $fileInsRc = $this->FileManager->prescription($this->context->parameters['wwwDir'], $branche->id, [], "xls", 0, 0, null, false, true, true);
                $fileBdInsRc = $this->FileManager->prescription($this->context->parameters['wwwDir'], $branche->id, [], "xls", 0, 0, null, true, true, true);


                $file = $this->FileManager->prescription($this->context->parameters['wwwDir'], $branche->id, [], "xls");

                $users = $branche->getUsersObjectForReport();

                $usersBd = array_values(array_map(function ($user) {
                    return $user->email;
                }, array_filter($users, function (User $user) {
                    return $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_bd'])
                        && !$user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_ins']);
                })));
                $usersIns = array_values(array_map(function ($user) {
                    return $user->email;
                }, array_filter($users, function (User $user) {
                    return !$user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_bd'])
                        && $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_ins']);
                })));
                $usersBdIns = array_values(array_map(function ($user) {
                    return $user->email;
                }, array_filter($users, function (User $user) {
                    return $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_bd'])
                        && $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_ins']);
                })));

                $usersBdRc = array_values(array_map(function ($user) {
                    return $user->email;
                }, array_filter($users, function (User $user) {
                    return $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_bd'])
                        && !$user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_ins'])
                        && $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_rc'])
                        ;
                })));
                $usersInsRc = array_values(array_map(function ($user) {
                    return $user->email;
                }, array_filter($users, function (User $user) {
                    return !$user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_bd'])
                        && $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_ins'])
                        && $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_rc'])
                        ;
                })));
                $usersBdInsRc = array_values(array_map(function ($user) {
                    return $user->email;
                }, array_filter($users, function (User $user) {
                    return $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_bd'])
                        && $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_ins'])
                        && $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_rc'])
                        ;
                })));

                $usersNothing = array_values(array_map(function ($user) {
                    return $user->email;
                }, array_filter($users, function (User $user) {
                    return !$user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_bd'])
                        && !$user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_ins'])
                        && !$user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_rc'])
                        ;
                })));

                $data = ["date" => date("Y-m-d H:i:s"), "branche_name" => $branche->name];

                $this->mailer->branche_report($fileBd, $usersBd, $data);
                $this->mailer->branche_report($fileIns, $usersIns, $data);
                $this->mailer->branche_report($fileBdIns, $usersBdIns, $data);

                $this->mailer->branche_report($fileBdRc, $usersBdRc, $data);
                $this->mailer->branche_report($fileInsRc, $usersInsRc, $data);
                $this->mailer->branche_report($fileBdInsRc, $usersBdInsRc, $data);

                $this->mailer->branche_report($file, $usersNothing, $data);

                echo "   " . json_encode($usersBd) . " : BD branche report sent\n";
                echo "   " . json_encode($usersIns) . " : INS branche report sent\n";
                echo "   " . json_encode($usersBdIns) . " : BD INS branche report sent\n";


                echo "   " . json_encode($usersBdRc) . " : BD RC branche report sent\n";
                echo "   " . json_encode($usersInsRc) . " : INS RC branche report sent\n";
                echo "   " . json_encode($usersBdInsRc) . " : BD INS RC branche report sent\n";


                echo "   " . json_encode($usersNothing) . " : - branche report sent\n";
            }

            $deps = $branche->getDepartmentsReport();

            foreach ($deps as $dep) {
                $showBd = $dep['user']->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_bd']);
                $showIns = $dep['user']->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_ins']);
                $showRc = $dep['user']->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_rc']);
                $file = $this->FileManager->prescription($this->context->parameters['wwwDir'], $branche->id, $dep["id"],
                    "xls", 0, 0, null, $showBd, $showIns, $showRc);

                $data = [
                    "date" => date("Y-m-d H:i:s"),
                    "branche_name" => $branche->name,
                    "department_name" => $dep["name"]
                ];
                $this->mailer->deparment_report($file, $dep["mail"], $data);
                echo "   " . $dep["mail"] . " : department report sent\n";
            }

            $specialities = $this->predikceRepository->findBy([
                "branche_id" => $branche->id,
                "user.send_reports" => true
            ]);

            foreach ($specialities as $speciality) {
                $user = new User($speciality->toRecord()->user);
                $showBd = $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_bd']);
                $showIns = $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_ins']);
                $showRc = $user->isAllowed('Api', ['action' => 'mailer', 'do' => 'usage', 'params' => 'show_rc']);
                $file = $this->FileManager->prescription($this->context->parameters['wwwDir'], $branche->id, 0, "xls",
                    $speciality->specialityId, 0, null, $showBd, $showIns, $showRc);

                $data = [
                    "date" => date("Y-m-d H:i:s"),
                    "branche_name" => $branche->name,
                    "speciality_name" => $speciality->toRecord()->speciality->name
                ];

                $mail = $user->email;

                $this->mailer->speciality_report($file, $mail, $data);
                echo $mail . " : speciality report sent\n";
            }
        }

        $fileAdmin = $this->FileManager->prescription($this->context->parameters['wwwDir']);
        $admins = $this->userRepository->findAll();

        $data = ["date" => date("Y-m-d H:i:s")];
        $this->mailer->admin_report($fileAdmin, "jirka.semmler@gmail.com", $data); //test
        foreach ($admins as $admin) {
            /**
             * @var $admin User
             */
            if ($admin->isAllowed('Api', ['action' => 'full-export'])) {
                $this->mailer->admin_report($fileAdmin, $admin->email, $data);
                echo $admin->email . " : admin report sent\n";
            }
        }

        $this->writeResult("mailer", "done");

        $this->terminate();
    }

    public function actionChangeStatus()
    {
        if ($this->planRepository->runTodaysPlan()) {
            $this->writeResult("status", "done");
        } else {
            $this->writeResult("status", "failed");
        }
        $this->terminate();
    }

    //<editor-fold desc="orders">

    /**
     * sends new orders which are after time and no extra
     * @cron : each minute
     */
    public function actionSendOrders()
    {
        // closing
        $sentOrders = $this->orderRepository->sendNewOrdersAuto();
        $this->notifyAboutAutoSending($sentOrders);
        $this->logAutoOrders($sentOrders);

        $this->terminate();
    }

    /**
     * archives old closed orders
     * @cron : 00am
     */
    public function actionArchiveOrders()
    {
        $orders = $this->orderRepository->archiveClosedOrders();
        $this->logAutoOrders($orders, 'archive');

        // notification for archive is not needed

        $this->terminate();
    }


    /**
     * sends notification about closed order
     * @param $closedOrders
     */
    private function notifyAboutAutoSending($closedOrders)
    {
        foreach ($closedOrders as $closedOrder) {
            $this->mailer->sendSentOrder($closedOrder, true);
        }
    }

    private function logAutoOrders($orders, $what = "send")
    {
        $ids = [];
        foreach ($orders as $order) {
            $ids[] = $order->id;
        }
        $time = date("Y-m-d H:i:s");
        $data = file_get_contents($this->workingDir . "../reports/" . $what . "Orders.json");
        $decoded = json_decode($data, true);
        $decoded[] = ['datetime' => $time, 'orders' => $ids];
        if (count($decoded) > 43200) {
            array_shift($decoded);
        }
        $encoded = json_encode($decoded, JSON_PRETTY_PRINT);

        file_put_contents($this->workingDir . "../reports/" . $what . "Orders.json", $encoded);
    }

    //</editor-fold>

    public function actionUsageStat()
    {
        $periods = [2, 4, 6, 8, 10, 12, 14, 16];

        $branches = $this->brancheRepository->findAll();

        $this->usageStatRepository->flushTable();
        /**
         * @var $branch Branche
         */
        foreach ($branches as $branch) {
            foreach ($branch->getDepartmentsAll() as $department) {
                foreach ($periods as $period) {
                    $this->usageStatRepository->countDepartment($department["id"], $period);
                }
            }
        }
        $this->writeResult("usage-stat", "done");

        $this->terminate();
    }
    //</editor-fold>

    //<editor-fold desc="eureka">
    public function actionEureka($token)
    {
        $token_setting = $this->settingRepository->getByKey("eureka_export_token");
        $sysmail = $this->settingRepository->getByKey("sys_mail");
        if ($token_setting != null && $token != $token_setting->value) {
            if ($sysmail == null) {
                echo "<h3>Forbidden access!</h3>";
            } else {
                echo "<h3>Forbidden access! Contant admin : " . $sysmail->value . "</h3>";
            }
            exit();
        }
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename=export.csv');

        $weeks_setting = $this->settingRepository->getByKey("pocet_tydnu_exportu");
        if ($weeks_setting == null) {
            $weeks_num = 2;
        } else {
            $weeks_num = $weeks_setting->value;
        }
        $usages = $this->usageRepository->running();
        $i = 0;

        foreach ($usages as $usage) {
            echo $usage->patient->branche_id . ";";
            echo $usage->patient->branche->name . ";";
            echo "domov" . $usage->patient->branche_id . "@lekynmiru.cz;";
            echo $usage->patient_id . ";";
            echo $usage->patient->last_name . ";";
            echo $usage->patient->first_name . ";";
            $datum2 = explode(" ", $usage->patient->birth_date);
            $datum3 = explode("-", $datum2[0]);
            if (count($datum3)) {
                echo $datum3[2] . $datum3[1] . $datum3[0] . ";";
            } else {

                echo "00000000;";
            }
            echo $usage->patient->rc . ";";
            if ($usage->patient->department == null) {
                echo "unknown;";
            } else {
                echo $usage->patient->department->name . ";";
            }
            echo ($usage->patient->state == "running") ? "1;" : "0;";
            echo $usage->speciality->name . ";";
            echo $usage->id . ";";
            echo $usage->medicine->id . ";";
            echo $usage->medicine->sukl . ";";
            echo $usage->medicine->name . ";";
            $datum2 = explode(" ", $usage->start_date);
            $datum3 = explode("-", $datum2[0]);
            echo $datum3[2] . $datum3[1] . $datum3[0] . ";";
            echo $usage->number . ";";
            echo "0;"; //weekly usage

            for ($j = 0; $j < $weeks_num; $j++) {
                $week = $this->getweek($usage->active_week, $j, $usage->weeks);

                $pills = $usage->related("usage")->where(["week" => $week])->order("usage.week, usage.day");
                foreach ($pills as $pill) {
                    echo $pill->morning . ";";
                    echo $pill->noon . ";";
                    echo $pill->evening . ";";
                    echo $pill->night . ";";
                }

            }
            echo "\n";
            $i++;
        }
        exit();
    }
    //</editor-fold>

    //<editor-fold desc="helpers">
    public function writeResult($key, $value)
    {
        $data = file_get_contents($this->workingDir . "status.json");
        $decoded = json_decode($data, true);

        $decoded[$key] = $value;
        $encoded = json_encode($decoded);

        file_put_contents($this->workingDir . "status.json", $encoded);
        $this->terminate();
    }

    public function actionToDate()
    {
        echo "povol akci ActionToDate";
        exit();
//		$this->usageRepository->countToDate();
    }

    public function actionPrepare()
    {
        $data = file_get_contents($this->workingDir . "status.json");
        file_put_contents($this->workingDir . "status_yesteday.json", $data);
        $decoded = json_decode($data, true);

        foreach ($decoded as $key => $val) {
            if ($val != "done") {
                $this->failreport(">>> " . $key . " <<< failed before recount");
            }

            $decoded[$key] = "ready";
        }
        $encoded = json_encode($decoded);

        file_put_contents($this->workingDir . "status.json", $encoded);
        $this->terminate();
    }

    public function actionCheck()
    {
        $data = file_get_contents($this->workingDir . "status.json");
        $decoded = json_decode($data, true);

        $failed = false;
        foreach ($decoded as $key => $val) {
            if ($val != "done") {
                $failed = true;
            }
        }

        if ($failed) {
            $this->failreport("recount failed\n\n " . $data);
        } else {
            $this->successReport("recount success\n\n " . $data);
        }

        $this->terminate();

    }

    function getweek($week, $offset, $max)
    {
        if ($offset == 0) {
            return $week;
        }

        $weeks = [
            'first',
            'second',
            'third',
            'fourth',
            'fifth',
            'sixth',
            'seventh',
            'eighth',
            'ninth',
            'tenth',
            'eleventh',
            'twelfth'
        ];

        $index = array_search($week, $weeks);
        $new = ($index + $offset) % $max;  // 4

        return $weeks[$new];
    }

    private function failreport($what)
    {
        //$mailer = new Mailer;
        $mail = $this->settingRepository->getByKey("sys_mail");
        if ($mail == null) {
            $mail_address = "info@lekynamiru.cz";
        } else {
            $mail_address = $mail->value;
        }
        $this->mailer->fail_report($mail_address, $what);
        echo "error : $what";
    }

    private function successReport($what)
    {
        //$mailer = new Mailer;
        $mail = $this->settingRepository->getByKey("sys_mail");
        if ($mail == null) {
            $mail_address = "info@lekynamiru.cz";
        } else {
            $mail_address = $mail->value;
        }
        $this->mailer->success_report($mail_address, $what);
        echo "success : $what";
    }

    //</editor-fold>


    public function actionSpecialityReport($branche_id, $predikce_id, $what)
    {

        $predikce = $this->predikceRepository->getByID($predikce_id);
        $branche = $this->brancheRepository->getByID($branche_id);

        $file = $this->FileManager->prescription($this->context->parameters['wwwDir'], $branche->id, [], "xls",
            $predikce->specialityId);

        $data = [
            "date" => date("Y-m-d H:i:s"),
            "branche_name" => $branche->name,
            "speciality_name" => $predikce->toRecord()->speciality->name
        ];

        if ($what == "response") {
            $this->sendResponse(new Responses\FileResponse($file, 'Report.' . "xls", 'application/octet-stream'));
        } elseif ($what == "mail") {
            //$mailer = new Mailer;
            $mail = $predikce->getUserMail();
            $this->mailer->speciality_report($file, $mail, $data);
            echo "ok";
            exit();
        }

    }

    // tato metoda delala hnusne medikační karty. puvodne se jmenovala MK, pak jsem vytvoril MK2 (ta dela hezke mk) a pak jsem tu novou hezkou MK2 přejmenoval na MK
    //to same i ve file manageru
//	public function actionMK($patient_id, $format = "xls", $response = true)
//	{
//		$file = $this->FileManager->mk($this->context->parameters['wwwDir'], $patient_id, $format);
//
//		$this->sendResponse(new Responses\FileResponse($file, 'Medikační karta.'.$format, 'application/octet-stream'));
//	}

    //<editor-fold desc="files">
    public function actionMK($patient_id, $format = "xls", $response = true)
    {
        $file = $this->FileManager->mk($this->context->parameters['wwwDir'], $patient_id, $this->getUserEntity(), $format);

        $this->sendResponse(new Responses\FileResponse($file, 'Medikační karta.' . $format,
            'application/octet-stream'));
    }

    public function actionPatients($what, $format = "xls")
    {

        $user = $this->getUser();
        if ($user->getRoles()[0] == "department") {
            $department = $this->userRepository->getDepartment($this->getUser()->id);
        } else //		elseif(in_array($user->getRoles()[0], ["branche", "branche_department"]))
        {
            $department = [];
        }

        $file = $this->FileManager->patients($this->context->parameters['wwwDir'], $what, $format, $department, $this->getUserEntity());

        $this->sendResponse(new Responses\FileResponse($file, 'Pacienti.' . $format, 'application/octet-stream'));
    }

    public function actionPatientsData($what, $format = "xls")
    {

        $user = $this->getUser();
        if ($user->getRoles()[0] == "department") {
            $department = $this->userRepository->getDepartment($this->getUser()->id);
        } else //		elseif(in_array($user->getRoles()[0], ["branche", "branche_department"]))
        {
            $department = [];
        }

        $file = $this->FileManager->patientsData($this->context->parameters['wwwDir'], $what, $format, $department);

        $this->sendResponse(new Responses\FileResponse($file, 'Pacienti všichni.' . $format, 'application/octet-stream'));
    }

    public function actionUsage($branche = 0, $department = null, $period = 2, $format = "xls")
    {
        $user = $this->getUserEntity();
        $showBD = $user->isAllowed('Api', ['action' => 'patient', 'do' => 'usage', 'params' => 'show_bd']);
        $showIns = $user->isAllowed('Api', ['action' => 'patient', 'do' => 'usage', 'params' => 'show_ins']);
        $showRc = $user->isAllowed('Api', ['action' => 'patient', 'do' => 'usage', 'params' => 'show_rc']);

        $file = $this->FileManager->usage($this->context->parameters['wwwDir'], $branche, $department, $period,
            $format, $showBD, $showIns, $showRc);

        $this->sendResponse(new Responses\FileResponse($file, 'Spotřeba léků.' . $format, 'application/octet-stream'));
    }

    public function actionOrderFile($id)
    {
        /**
         * @var $orderFile OrderFile
         */
        $orderFile = $this->orderFileRepository->getByID($id);
        $file = $this->context->parameters['wwwDir'] . '/../data/uploads/' . $orderFile->hash;

        $this->sendResponse(new Responses\FileResponse($file, $orderFile->filename, 'application/octet-stream'));
    }

    public function actionOrderStats()
    {
        $file = $this->FileManager->order_stat($this->context->parameters['wwwDir'], 'xls');

        $this->sendResponse(new Responses\FileResponse($file, 'Statistika objednavek.xls', 'application/octet-stream'));
    }

    public function actionProduction($branche = 0, $department = null, $period = 2, $format = "xls")
    {
        $file = $this->FileManager->production($this->context->parameters['wwwDir'], $branche, $department, $period,
            $format);

        $this->sendResponse(new Responses\FileResponse($file, 'Výroba.' . $format, 'application/octet-stream'));
    }

    /**
     * delivery list for orders
     * @param $orderId
     * @param string $format
     */
    public function actionDelivery($orderId, $format = "xls")
    {
        $file = $this->FileManager->delivery($this->context->parameters['wwwDir'], $orderId, $format);

        $this->sendResponse(new Responses\FileResponse($file, 'Dodaci-list.' . $format, 'application/octet-stream'));
    }

    public function actionPrescription($format = "xls", $from, $to)
    {
        $user = $this->getUser();
        if ($user->getRoles()[0] == "department") {
            $department = $this->userRepository->getDepartment($this->getUser()->id);
        } else //		elseif(in_array($user->getRoles()[0], ["branche", "branche_department"]))
        {
            $department = [];
        }
        $showBD = $this->getUserEntity()->isAllowed('Api', ['action' => 'patient', 'do' => 'prescription', 'params' => 'show_bd']);
        $showIns = $this->getUserEntity()->isAllowed('Api', ['action' => 'patient', 'do' => 'prescription', 'params' => 'show_ins']);
        $showRc = $this->getUserEntity()->isAllowed('Api', ['action' => 'patient', 'do' => 'prescription', 'params' => 'show_rc']);
        $file = $this->FileManager->prescription($this->context->parameters['wwwDir'], $_SESSION["branche"],
            $department, $format, 0, 0, $to, $showBD, $showIns, $showRc);

        $this->sendResponse(new Responses\FileResponse($file, 'Stavy léků.' . $format, 'application/octet-stream'));
    }

    public function actionMedicine($format = "xls")
    {
        $file = $this->FileManager->medicine($this->context->parameters['wwwDir'], $format);

        $this->sendResponse(new Responses\FileResponse($file, 'Přehled léků.' . $format, 'application/octet-stream'));
    }

    public function actionHistory($format, $from, $to)
    {
        $file = $this->FileManager->logChunk($this->context->parameters['wwwDir'], $from, $to, $this->getUser()->id);

        $this->sendResponse(new Responses\FileResponse($file, 'Historie .' . $format, 'application/octet-stream'));
    }

    public function actionCan($format = "xls")
    {
        $file = $this->FileManager->can($this->context->parameters['wwwDir'], $format);

        $this->sendResponse(new Responses\FileResponse($file, 'Přehled kanystrů.' . $format,
            'application/octet-stream'));
    }

    public function actionBranche($format = "xls")
    {
        $file = $this->FileManager->branches($this->context->parameters['wwwDir'], $format);

        $this->sendResponse(new Responses\FileResponse($file, 'Přehled domovů.' . $format, 'application/octet-stream'));
    }

    //</editor-fold>

    public function actionStartChunk()
    {
        echo "pokud jsi toto pouzil tak me zajima proc, napis na jirka.semmler@gmail.com";
        exit();
        $last = $this->logRepository->findAll()->orderBy("id")->limit(1)->toArray()[0];
//		$this->file();

        $this->FileManager->logChunk($this->context->parameters['wwwDir'], $last->date,
            date('Y-m-d H:i:s', strtotime("+3 months", strtotime($last->date))), 1);

        $this->terminate();
    }

    public function actionGetPackagesForMedicine($id)
    {
        /**
         * @var Medicine $medicine
         */
        $medicine = $this->medicineRepository->getByID($id);
        $packages = $medicine->getPackageList();
        $out = [];
        foreach ($packages as $package) {
            $out[] = ["id" => $package->id, "size" => $package->size];
        }
//		$this->payload->out = $out;
        $this->sendJson($out);
    }

    // kod funguje, ale z bezpecnostniho hlediska je to zakomentovane

    public function actionImport()
    {
        exit();
        $row = 1;
        $lekyAr = [];
        $specialitiesObj = [];
//        if (($handle = fopen(__DIR__ . '/../../data/leky2.csv', "r")) !== FALSE) {
//            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
//                $lekyAr[$data[0]] = ['name' => $data[1], 'sukl'=> $data[2], 'bal1'=>$data[3], 'bal2' => $data[4], 'bal3' => $data[5] , 'spec' => $data[6]];
//
//            }
//            $speciality = array_unique(array_map(function ($item) {return $item['spec'];} , $lekyAr));
//            foreach($speciality as $spec){
//                $s = $this->specialityRepository->getBy(['name' => $spec]);
//                if(!$s){
//                    $s = new Speciality();
//                    $s->name = $spec;
//                    $this->specialityRepository->persist($s);
//
//                    $p1 = $this->predikceRepository->getBy(['speciality_id' => $s->id, 'branche_id' => 1]);
//                    if(!$p1){
//                        $p1 = new Predikce();
//                        $p1->brancheId = 1;
//                        $p1->days = 5;
//                        $p1->specialityId = $s->id;
//                        $this->predikceRepository->persist($p1);
//                    }
//
//                    $p1 = $this->predikceRepository->getBy(['speciality_id' => $s->id, 'branche_id' => 2]);
//                    if(!$p1){
//                        $p1 = new Predikce();
//                        $p1->days = 5;
//                        $p1->brancheId = 2;
//                        $p1->specialityId = $s->id;
//                        $this->predikceRepository->persist($p1);
//                    }
//                }
//                $specialitiesObj[$spec] = $s;
//            }
//            fclose($handle);
//        }


        $dokument = PHPExcel_IOFactory::load(__DIR__ . '/../../data/import.xlsx');
        $dokument->setActiveSheetIndex(0);
        $list = $dokument->getActiveSheet();

//4195
        $i = 6;
        while ($i < 898) {
            $value = $list->getCell("B" . $i)->getValue();

            // create patient
            if ($value == 'Rodné číslo:') {
                $odd = $list->getCell("L" . ($i))->getValue();
                $branch = $list->getCell("L" . ($i - 1))->getValue();

                $datumNar = $list->getCell("D" . ($i + 1))->getValue();
                $datumNar = date('d.m.Y', PHPExcel_Shared_Date::ExcelToPHP($datumNar));
                $rc = $list->getCell("D" . $i)->getValue();
                $name = $list->getCell("B" . ($i - 1))->getValue();
                $nameAr = explode(' ', $name);
                $first = $nameAr[1];
                $last = $nameAr[0];

                $branchObj = $this->brancheRepository->getByName($branch);
                if (!$branchObj) {
                    $branchObj = new Branche();
                    $branchObj->name = $branch;
                    $this->brancheRepository->persist($branchObj);
                }

                $row = $this->brancheRepository->getTable("department")->select('id')->where([
                    "name" => $odd,
                    "branche_id" => $branchObj->id
                ]);
                if (count($row) == 0) {
                    $this->brancheRepository->getTable("department")->insert([
                        "name" => $odd,
                        "branche_id" => $branchObj->id
                    ]);

                    $row = $this->brancheRepository->getTable("department")->select('id')->where([
                        "name" => $odd,
                        "branche_id" => $branchObj->id
                    ]);
                }

                $depId = $row->fetch()['id'];


                $values = (object)[
                    'id' => 0,
                    'birthDate' => $datumNar,
                    'firstName' => ucfirst(mb_strtolower($first)),
                    'lastName' => ucfirst(mb_strtolower($last)),
                    'rc' => $rc,
                    'pojistovna' => '111',
                    'state' => 'running',
                    'department' => $depId,
                    'userID' => 1
                ];
                $patientObj = $this->patientRepository->getBy(['rc' => $rc]);
                if (!$patientObj) {
                    $patient_id = $this->patientRepository->manage($values, 1);
                } else {
                    $patient_id = $patientObj->id;
                }

                $i = $i + 5;

                $indexOfUsage = $list->getCell("B" . $i)->getValue();

                while ($indexOfUsage) {
                    /*
                    $lekNazevOr = $list->getCell("C" . $i)->getValue();
                    $lekNazev = (isset($lekyAr[$lekNazevOr]))? $lekyAr[$lekNazevOr]['name']: $lekNazevOr . "-PUVODNI";
                    $sukl = (isset($lekyAr[$lekNazevOr]))? $lekyAr[$lekNazevOr]['sukl']: '0';
                    $lekObj = $this->medicineRepository->getByName($lekNazev);

                    if (!$lekObj) {
                        $specs = (isset($lekyAr[$lekNazevOr])) ? [$specialitiesObj[$lekyAr[$lekNazevOr]['spec']]->id] : [];
//                        $specs = [];
                        $data = (object) ['name' => $lekNazev, 'sukl' => $sukl, 'can' => '-1', 'specialities' => $specs];
                        $lekId = $this->medicineRepository->insert($data);
                        if($specs){
                            $pckgs = [$lekyAr[$lekNazevOr]['bal1']];
                            if($lekyAr[$lekNazevOr]['bal2']){
                                $pckgs[] = $lekyAr[$lekNazevOr]['bal2'];
                            }
                            if($lekyAr[$lekNazevOr]['bal3']){
                                $pckgs[] = $lekyAr[$lekNazevOr]['bal3'];
                            }
                            $this->medicineRepository->setPackages($lekId, $pckgs);
                        }


                    }else{
                        $lekId = $lekObj->id;
                    }
                    $rano = $list->getCell("E" . $i)->getValue();
                    $poledne = $list->getCell("F" . $i)->getValue();
                    $vecer = $list->getCell("H" . $i)->getValue();
                    $noc = $list->getCell("I" . $i)->getValue();

                    $specId = isset($lekyAr[$lekNazevOr]) ? $specialitiesObj[$lekyAr[$lekNazevOr]['spec']]->id : '1';
                    $values = [
                        'id_patient' => $patient_id,
                        'weeks' => 2,
                        'activeWeekSelect' => false,
                        'id_usage' => 0,
                        'comment' => 'init',
                        'userID' => 1,
                        'showTwoWeeks' => false,
                        'medicineSelect' => $specId . '-' . $lekId
                    ];
                    $dny = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
                    $weeks = ['first' , 'second'];
                    foreach($weeks as $week){
                        foreach($dny as $den){
                            $values[$den . '0morning0' .$week] = $rano;
                            $values[$den . '0noon0' .$week] = $poledne;
                            $values[$den . '0evening0' .$week] = $vecer;
                            $values[$den . '0night0' .$week] = $noc;
                        }
                    }
                    $this->usageRepository->manageCard((object)$values);
                    */
                    $i++;
                    $indexOfUsage = $list->getCell("B" . $i)->getValue();

                }

            }

            $i++;
        }
    }

    public function actionImport2()
    {
        $dokument = PHPExcel_IOFactory::load(__DIR__ . '/../../data/tpl/chynov2.xls');
        $dokument->setActiveSheetIndex(0);
        $list = $dokument->getActiveSheet();

        $i = 1;
        $imported = 0;
        while ($i <= 40) {
            $firstName = $list->getCell("A" . $i)->getValue();
            $lastName = $list->getCell("B" . $i)->getValue();
            $rc = $list->getCell("C" . $i)->getValue();
            $dep = $list->getCell("D" . $i)->getValue();
            $ins = $list->getCell("E" . $i)->getValue();

            $birthDateRcPart = explode('/', $rc)[0];
            $year = (int)substr($birthDateRcPart, 0, 2);
            $month = (int)substr($birthDateRcPart, 2, 2);
            $day = (int)substr($birthDateRcPart, 4, 2);
            if ($month > 50) {
                $month = $month - 50;
            }
            $birthDate = "{$day}.{$month}.19{$year}";

            $row = $this->brancheRepository->gt("department")->select('id')->where([
                "name" => $dep,
                "branche_id" => 3
            ]);
            if (count($row) == 0) {
                $this->brancheRepository->gt("department")->insert([
                    "name" => $dep,
                    "branche_id" => 3
                ]);

                $row = $this->brancheRepository->gt("department")->select('id')->where([
                    "name" => $dep,
                    "branche_id" => 3
                ]);
            }
            $depId = $row->fetch()['id'];

            $values = (object)[
                'id' => 0,
                'birthDate' => $birthDate,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'rc' => $rc,
                'pojistovna' => (string)$ins,
                'state' => 'running',
                'department' => $depId,
                'userID' => 1
            ];
            $patientObj = $this->patientRepository->getBy(['rc' => $rc]);

            if (!$patientObj) {
                $imported++;
                $this->patientRepository->manage($values, 3);
            }

            $i++;
        }
        echo 'imported ' . $imported;
        exit();
    }

    private function getSpecObjs($specialities, &$specObjs)
    {
        $out = [];
        foreach ($specialities as $spec) {
            if (isset($specObjs[$spec])) {
                $out[] = $specObjs[$spec];
            } else {
                $s = new Speciality();
                $s->name = $spec;
                $this->specialityRepository->persist($s);
                $specObjs[$spec] = $s;
                $out[] = $s;

                $p1 = $this->predikceRepository->getBy(['speciality_id' => $s->id, 'branche_id' => 1]);
                if (!$p1) {
                    $p1 = new Predikce();
                    $p1->brancheId = 1;
                    $p1->days = 5;
                    $p1->specialityId = $s->id;
                    $this->predikceRepository->persist($p1);
                }
            }
        }
        return $out;
    }

    public function actionImport3()
    {
        $lekyAr = [];
        $handle = fopen(__DIR__ . '/../../data/tpl/lekyImport.csv', "r");
        $specObjs = [];
        while (($data = fgetcsv($handle, 1000, ";")) !== false) {
            $specialities = explode(',', $data[3]);
            $specialities = array_map('trim', $specialities);


            $lekyAr[] = [
                'name' => trim($data[1]),
                'sukl' => $data[2],
                'bal1' => $data[4],
                'bal2' => $data[5],
                'bal3' => $data[6],
                'spec' => $this->getSpecObjs($specialities, $specObjs)
            ];
        }

        $updated = [];
        $modifiedSukl = [];
        $created = [];
        foreach ($lekyAr as $lekData) {
            /**
             * @var $lekDb Medicine
             */

            $lekDb = $this->medicineRepository->getBy(['sukl' => $lekData['sukl']]);

            if (!$lekDb) {
                $lekDb = $this->medicineRepository->getBy(['name' => $lekData['name']]);
            }

            if ($lekDb) {
                if ($lekDb->sukl != $lekData['sukl']) {
                    $modifiedSukl[] = "{$lekData['name']} SUKL {$lekDb->sukl} -> {$lekData['sukl']}";
                }
                $updated[] = $lekData['name'];
                $canId = $lekDb->getCan(true);
                $lekDb->name = $lekData['name'];
                $lekDb->sukl = $lekData['sukl'];
            } else {
                $created[] = $lekData['name'];

                $lekDb = new Medicine();
                $lekDb->sukl = $lekData['sukl'];
                $lekDb->name = $lekData['name'];
                $canId = -1;
            }

            $specIds = array_map(function ($item) {
                return $item->id;
            }, $lekData['spec']);
            // persist saves even the specialities
            $this->medicineRepository->persist($lekDb, $canId, $specIds);

            // add packages
            $sizes = [];
            if ($lekData['bal1']) {
                $sizes[] = $lekData['bal1'];
            }
            if ($lekData['bal2']) {
                $sizes[] = $lekData['bal2'];
            }
            if ($lekData['bal3']) {
                $sizes[] = $lekData['bal3'];
            }

            $this->medicineRepository->setPackages($lekDb->id, $sizes);
            $this->medicineRepository->changeSpecialityOfUsages($lekDb->id, $lekData['spec']);

        }


        fclose($handle);
    }
}
