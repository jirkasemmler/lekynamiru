<?php

namespace App\Presenters;

use App\Model\Entities\Log;
use App\Model\Entities\User;
use App\Model\Repositories\LogRepository;
use App\Forms\BrancheFormFactory;
use App\Forms\LoginFormFactory;
use App\Model\Repositories\SettingRepository;
use App\Model\Repositories\UsageRepository;
use App\Model\Repositories\PatientRepository;
use App\Model\Repositories\UserRepository;
use Nette\Application\UI\Form;

class LogPresenter extends BasePresenter {


    /** @var LogRepository @inject */
    public $logRepository;

    /** @var SettingRepository @inject */
    public $settingRepository;

    /** @var UserRepository @inject */
    public $userRepository;

    /** @var PatientRepository @inject */
    public $patientRepository;

    /** @var UsageRepository @inject */
    public $usageRepository;

    /** @var BrancheFormFactory @inject */
    public $brancheFormFactory;

    /** @var LoginFormFactory @inject */
    public $loginFormFactory;
    private $id;

    protected function getFilter($branche, $fromF, $toF) {
        $filter = [
            "date >= ?" => $fromF
        ];

        if ($toF != null) {
            $filter["date <= ?"] = $toF;
        }

        if ($this->getUser()->getRoles()[0] == "branche" OR $this->getUser()->getRoles()[0] == "branche_department") {
            $filter["patient.branche_id"] = $branche;
        } elseif ($this->getUser()->getRoles()[0] == "department") {
            $filter["patient.department_id"] = $this->departmentID;
        }

        return $filter;
    }

    public function actionDefault() {

        $this->template->mainTitle = "Historie";

        $filter = $this->getFilter($_SESSION["branche"],
            date('Y-m-d', strtotime("-1 months", strtotime(date('d.m.Y')))), null);

        $this->template->data = $this->logRepository->findBy($filter)->orderBy("date DESC");
        //show only data from last 3 months

        $this->template->from = date('d.m.Y', strtotime("-1 months", strtotime(date('d.m.Y'))));
        $this->template->subTitle = $this->template->from . " - dnes";
        $this->template->fromXLS = date('d.m.Y', strtotime("-2 months", strtotime(date('d.m.Y'))));
        $this->template->toXLS = $this->template->from;

    }

    public function actionCards() {
        $data = $this->logRepository->findBy(["event" => ["card_changed", "card_created"]]);

        /** @var $item Log */
        foreach ($data as $item) {
            foreach ($item->toRecord()->usage_meta->related("medical_card_usage_meta") as $tmp) {
                $mc = $tmp->id;
                break;
            }

            $item->mc = $mc;
            $item->event = $item->event == "card_changed" ? "usage_created" : "usage_changed";
            $this->logRepository->persist($item);
        }

        $this->terminate();
    }


    public function actionTest($part = 1) {

        $data = $this->logRepository->findAll()->limit(5000, ($part - 1) * 5000);

        /** @var $item Log */
        foreach ($data as $item) {
            $pacient = null;
            echo $item->id . " : ";
            echo $item->event . " : ";
            echo "<br>\n";
            if (in_array($item->event, ["usage_created", "usage_changed", "card_deleted"])) {
                $pacient = $item->toRecord()->medical_card_usage_meta->medical_card->patient_id;
            } else {
                if ($item->mc) {
                    $pacient = $item->mc->medical_card->patient_id;
                } else {

                    switch ($item->event) {
                        case "card_created":
                        case "card_changed": {
                            $usage = $item->toRecord()->usage_meta;
                            if ($usage == null) {
                                $usage_meta_id = $item->data;

                                $usage = $this->usageRepository->getByID($usage_meta_id);
                                if ($usage != null) {
                                    $pacient = $usage->patient_id;
                                } else {
                                    $pacient = null;
                                }
                            } else {
                                $pacient = $usage->patient_id;;

                            }
                        };
                            break;
                        case "added_pck":
                        case "added_pills": {
                            $tmp = str_replace("<b>", "<xxx>", $item->data);
                            $tmp2 = str_replace("</b>", "<yyy>", $tmp);

                            preg_match_all("/\w*: <xxx>(.*) (.*)<yyy> \, .*/", $tmp2, $output_array);

                            $last = $output_array[1];
                            $first = $output_array[2];

                            $pacienti = $this->patientRepository->findBy([
                                "first_name" => $first, "last_name" => $last
                            ]);
                            $pocet_vysledku = $pacienti->count();
                            if ($pocet_vysledku == 0) {
                                echo "nenalezeno";
                            } elseif ($pocet_vysledku > 1) {
                                echo "duplicita";
                            } else {
                                $pacienti->rewind();
                                $pacient = $pacienti->current()->id;
                            }
                        };
                            break;
                        case "changed_patient_state": {

                            $tmp = str_replace("<b>", "<xxx>", $item->data);
                            $tmp2 = str_replace("</b>", "<yyy>", $tmp);

                            preg_match_all("/\w*<xxx>(.*) (.*)<yyy> .*/", $tmp2, $output_array);

                            $last = $output_array[1];
                            $first = $output_array[2];

                            $pacienti = $this->patientRepository->findBy([
                                "first_name" => $first, "last_name" => $last
                            ]);
                            $pocet_vysledku = $pacienti->count();
                            if ($pocet_vysledku == 0) {
                                echo "nenalezeno";
                            } elseif ($pocet_vysledku > 1) {
                                echo "duplicita";
                            } else {
                                $pacienti->rewind();
                                $pacient = $pacienti->current()->id;
                            }
                        };
                            break;
                        case "plan_created": {
                            $tmp = str_replace("<b>", "<xxx>", $item->data);
                            $tmp2 = str_replace("</b>", "<yyy>", $tmp);

                            preg_match_all("/\w*<xxx>(.*)<yyy> .*<xxx>(.*) (.*)<yyy>.*<xxx>(.*)<yyy>.*/", $tmp2,
                                $output_array);

                            $last = $output_array[3];
                            $first = $output_array[2];

                            $pacienti = $this->patientRepository->findBy([
                                "first_name" => $first, "last_name" => $last
                            ]);
                            $pocet_vysledku = $pacienti->count();
                            if ($pocet_vysledku == 0) {
                                echo "nenalezeno";
                            } elseif ($pocet_vysledku > 1) {
                                echo "duplicita";
                            } else {
                                $pacienti->rewind();
                                $pacient = $pacienti->current()->id;
                            }

                        };
                            break;
                        case "patient_created":
                        case "patient_edited": {

                            $tmp = str_replace("<b>", "<xxx>", $item->data);
                            $tmp2 = str_replace("</b>", "<yyy>", $tmp);

                            preg_match_all("/\w*<xxx>(.*)<yyy> .*<xxx>(.*) (.*)<yyy>.*<xxx>(.*)<yyy>.*<xxx>(.*)<yyy>.*/",
                                $tmp2, $output_array);

                            $last = $output_array[3];
                            $first = $output_array[2];

                            $pacienti = $this->patientRepository->findBy([
                                "first_name" => $first, "last_name" => $last
                            ]);
                            $pocet_vysledku = $pacienti->count();

                            if ($pocet_vysledku == 0) {
                                echo "nenalezeno";
                            } elseif ($pocet_vysledku > 1) {
                                echo "duplicita";
                            } else {
                                $pacienti->rewind();
                                $pacient = $pacienti->current()->id;
                            }

                        };
                            break;

                    }


                }
            }

            if ($pacient != null) {
                $item->patient = $pacient;
                $this->logRepository->persist($item);
            }

            echo "\n";
        }
        $this->terminate();
    }


    public function renderSetting() {
        /**
         * @var $user User
         */
        $user = $this->userRepository->getByID($this->user->id);
        $this->template->superadmin = $user->isSuperAdmin();
        $this->template->mainTitle = "Nastavení";
        $this->template->data = $this->settingRepository->findAll();
    }


    public function renderEurekaExport() {
        $this->template->mainTitle = "Nastavení domova k exportu";
    }


    public function handleEdit($id) {
        $this->id = $id;
        $this->template->showForm = true; //show components or title
        $this->template->showTitle = true;

        $this->template->modalTitle = "Editovat systémové nastavení";

        if ($this->presenter->isAjax()) {

            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    protected function createComponentBranchEurekaExport() {
        $form = $this->brancheFormFactory->createBranchEurekaExport();
        $form->onSuccess[] = array($this, 'eurekaExportSuccess');

        return $form;
    }

    public function eurekaExportSuccess($form, $data) {
        $enabledDeps = [];
        foreach ($data as $itemK => $itemV) {
            $enabledDeps = array_merge($enabledDeps, array_values($itemV));
        }
        if($this->brancheRepository->setEnabledDeps($enabledDeps)){
            $this->flashMessage('Nastavení bylo uloženo!');
        }
        else{
            $this->flashMessage('Došlo k chybě', 'error');
        }


        $this->redirect('this');
    }


    protected function createComponentForm() {
        $form = $this->loginFormFactory->createSetting($this->id);
        $form->onSuccess[] = array($this, 'settingSuccess');

        return $form;
    }

    /**
     * @param $form Form
     */
    public function settingSuccess($form) {
        $values = $form->getValues();
        $this->id = $values->id;
        $ret = $this->settingRepository->manage($values);
        if ($ret === true) {
            $this->flashMessage('Nastavení bylo uloženo!');
        } else {
//
            $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");

        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            $this->redrawControl('list');
            $this->redrawControl('flashes');

            $form->setValues(array(), true); //clear form for next usage
        }
    }


    public function handleChangeRange($from, $to) {
        $fromF = date('Y-m-d', strtotime($from));
        $toF = date('Y-m-d', strtotime($to));

        $filter = $this->getFilter($_SESSION["branche"], $fromF, $toF);
        $this->template->data = $this->logRepository->findBy($filter)->orderBy("date DESC");
        // show by range

        $this->template->subTitle = $from . " - " . $to;

        $this->redrawControl('logList');
        $this->redrawControl('pageTitleSnippet');
    }


}