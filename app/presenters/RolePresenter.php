<?php

namespace App\Presenters;

use App\Forms\RoleFormFactory;
use App\Model\Repositories\PermissionRepository;
use App\Model\Repositories\RoleRepository;

class RolePresenter extends BasePresenter
{
    /** @var RoleRepository @inject */
    public $roleRepository;
    /** @var PermissionRepository @inject */
    public $permissionRepository;
    /** @var RoleFormFactory @inject */
    public $roleFormFactory;
    protected $id;

    public function actionDefault()
    {
        $this->template->mainTitle = "Role";
        $this->template->subTitle = "Uživatelské role v systému";
        $this->template->data = $this->roleRepository->findAll();
    }

    public function handleManageRole($id = null)
    {
        $this->id = $id;
        $this->template->showForm = true; //show components or title
        $this->template->showTitle = true;

        if ($id == 0) {
            $this->template->modalTitle = "Přidat roli";
        } else {
            //update
            $this->id = $id; //saving id
            $this->template->modalTitle = "Editovat roli ";
        }

        if ($this->presenter->isAjax()) {

            $this->redrawControl('roleFormSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    public function handleDelRole($id = null)
    {
        if ($this->roleRepository->delRole($id)) {
            $this->payload->status = "ok";
        } else {
            $this->payload->status = "err";
        }
        $this->sendPayload();
    }

    public function createComponentRoleForm()
    {
        $form = $this->roleFormFactory->createRole($this->id);

        $form->onSubmit[] = [$this, 'manageRoleSuccess'];

        return $form;
    }

    public function manageRoleSuccess($form)
    {
        $ret = $this->roleRepository->manage($form->getValues());
        if ($ret === true) {
            $this->flashMessage('Role byla uložena!', "success");
        } else {

            $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            $this->redrawControl('listOfRoles');
            $this->redrawControl('flashes');
            $form->setValues([], true); //clear form for next usage
        }
    }

    public function actionPermissions($id)
    {
        $role = $this->roleRepository->getByID($id);
        $this->template->mainTitle = "Role";
        $this->template->roleId = $id;
        $this->template->subTitle = "Nastavení role <b>" . $role->name . "</b>";
    }

    public function actionPermissionsForRole($id)
    {
        $all = $this->permissionRepository->getAllPermissions()->orderBy('data_order');

        $permsWithAllowed = $this->roleRepository->getPermissionsForRole($id, $all);

        $data = PermissionRepository::makeTreeStructure($permsWithAllowed);

        $this->sendResponse(new \Nette\Application\Responses\JsonResponse($data));
    }

    public function actionSavePermission($id)
    {
        $all = $this->permissionRepository->getAllPermissions()->orderBy('parent_id');

        $postData = $this->getHttpRequest()->getPost();
        $flattenData = self::flatten($postData['data']);
        $extractedData = [];
        foreach ($flattenData as $item) {
            $extractedData[(int)$item['key']] = $item['selected'] === 'true';
        }

        $permsWithAllowed = $this->roleRepository->getPermissionsForRole($id, $all);

        foreach ($permsWithAllowed as $perm) {
            if ($perm->enabled !== $extractedData[$perm->id]) {
                if ($extractedData[$perm->id]) {
                    // add permission
                    $this->permissionRepository->enableToRole($id, $perm->id);
                } else {
                    // remove permission
                    $this->permissionRepository->disableToRole($id, $perm->id);
                }
            }
        }

        $this->sendResponse(new \Nette\Application\Responses\JsonResponse(['status' => 'ok']));
    }

    public static function flatten($array)
    {
        $out = [];
        foreach ($array as $item) {
            if (isset($item['children'])) {
                $out = array_merge($out, $item['children']);
                unset($item['children']);
            }
            $out[] = $item;
        }
        return $out;
    }
}
