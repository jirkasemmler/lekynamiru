<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 15.9.15
 * Time: 17:57
 */

namespace App\Presenters;
use App\Model\Repositories\PredikceRepository;

class ReportPresenter extends BasePresenter {

	/** @var PredikceRepository @inject */
	public $predikceRepository;

	public function renderDefault()
	{
		$this->template->mainTitle = "Reporty";
		$this->template->subTitle = "Zasílání reportů dle specializací";


		$predikce = $this->predikceRepository->findBy(["branche_id"=>$_SESSION["branche"]]);
		$this->template->predikce = $predikce;
	}
}