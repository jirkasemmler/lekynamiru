<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2017
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 15.8.2017
 * Time: 22:55
 */

namespace App\Presenters;

use App\Forms\OrderFormFactory;
use App\Model\Entities\Order;
use App\Model\Entities\OrderFile;
use App\Model\Entities\OrderItem;
use App\Model\Repositories\MedicineRepository;
use App\Model\Repositories\OrderFileRepository;
use App\Model\Repositories\OrderItemChangeRepository;
use App\Model\Repositories\OrderItemRepository;
use App\Model\Repositories\OrderRepository;
use App\Model\Repositories\OrderSettingRepository;
use App\Model\Repositories\PatientRepository;
use Latte\Engine;
use model\Exceptions\DataException;
use Nette\Application\UI\Form;
use Nette\Neon\Exception;
use Nette\Utils\Random;
use Tracy\Debugger;

class OrderPresenter extends BasePresenter
{

    private $user;
    private $orderId;
    private $orderItemId;
    private $dateTime;
    private $urgent;

    /** @var OrderRepository @inject */
    public $orderRepository;

    /** @var OrderSettingRepository @inject */
    public $orderSettingRepository;

    /** @var OrderItemRepository @inject */
    public $orderItemRepository;

    /** @var OrderItemChangeRepository @inject */
    public $orderItemChangeRepository;

    /** @var OrderFormFactory @inject */
    public $orderFormFactory;

    /** @var MedicineRepository @inject */
    public $medicineRepository;

    /** @var PatientRepository @inject */
    public $patientRepository;

    /** @var OrderFileRepository @inject */
    public $orderFileRepository;

    /** @var Mailer @inject */
    public $mailer;

    public function startup()
    {
        try {
            $this->user = $this->getUser();
            $user = $this->getUserEntity();
            if (!$user || !$user->isAllowed('Order', ['action' => 'default'])) {
                $this->redirect("Login:out");
            }

        } catch (\Exception $e) {
            $this->redirect("Login:out");
        }

        parent::startup();
    }

    /**
     * custom flashes in the modal
     * @param string
     * @param string
     * @return \stdClass
     */
    public function flashModalMessage($message, $type = 'info')
    {
        $id = $this->getParameterId('flashModal');
        $messages = $this->getPresenter()->getFlashSession()->$id;
        $messages[] = $flash = (object)[
            'message' => $message,
            'type' => $type,
        ];
        $this->getTemplate()->modalFlashes = $messages;
        $this->getPresenter()->getFlashSession()->$id = $messages;
        return $flash;
    }

    private function redrawHandlerComponents()
    {
        if ($this->isAjax()) {
            $this->redrawControl('formSnippet');
            $this->redrawControl('header');
            $this->redrawControl('list');
            $this->redrawControl('flashes');
        } else {
            $this->redirect('this');
        }
    }


    public function beforeRender()
    {
        if (!$this->getUserEntity()->allowOrders()) {
            $this->flashMessage("Pro práci s objednávkami nemáte oprávnění.", "warning");
            $this->redirect("Patient:default");
        }

        parent::beforeRender();
    }


    public function actionDefault($id = 0, $archived = false)
    {
        $branche = $id;
        if ($this->getUserEntity()->isAdmin()) {
            $this->setView("default_admin");
            $this->template->branches = $this->brancheRepository->getBranches();
            $this->template->selectedBrancheID = $branche;
            $brancheName = ($branche == 0) ?
                "<b>všechny domovy</b>" :
                "domov <b>" . $this->brancheRepository->getByID($branche)
                    ->getFullName() . "</b>";
            $this->template->selectedBrancheTitle = $brancheName;
            $this->template->archived = $archived;

            $this->template->orders = $this->orderRepository->findOrdersForAdmin($id ?: null, $archived);
        } else {
            $this->setView("default_branche");
            $this->template->slots = $this->orderSettingRepository->getFollowingSlots($this->getBrancheID());
            $this->template->extras = $this->orderRepository->findExtra($this->getBrancheID());
        }

        $this->template->mainTitle = "Objednávky";
        $this->template->subTitle = "<a href='#' class='tomodal'
															data-toggle='modal'
															data-target='helpModal'><i class='fa fa-question-circle'></i></a>";
        $this->template->user = $this->getUserEntity();

        $latte = new Engine;

        $latte->setTempDirectory(__DIR__ . '/../../temp');

        $html = $latte->renderToString(__DIR__ . '/templates/Order/Components/help.latte',
            ["basePath" => $this->template->basePath]);
        $this->template->help = $html;

        return $html;
    }


    //<editor-fold desc="branche-view">
    public function createComponentOrderItemForm()
    {
        $form = $this->orderFormFactory->createOrderItem($this->orderId, $this->orderItemId, $this->dateTime,
            $this->getUserEntity(), $this->getBrancheID(), $this->urgent);
        $form->onSuccess[] = array($this, 'orderItemSuccess');
        return $form;
    }


    public function createComponentOrderFileForm()
    {
        $form = $this->orderFormFactory->createOrderFile($this->orderId);
        $form->onSuccess[] = array($this, 'orderFileSuccess');
        return $form;
    }

    public function orderFileSuccess($form)
    {
        /**
         * @var $orderFile OrderFile
         */
        $orderFile = $this->orderFileRepository->createEntity();
        $values = $form->getValues();

        if ($this->getHttpRequest()->isMethod('POST')) {
            $orderFile->title = $values->title;
            $orderFile->id_order = (int)$values->id_order;
            $orderFile->filename = $values->file->getName();
            $orderFile->date = new \DateTime();
            $orderFile->hash = Random::generate(10, 'a-z');

            $path = $this->context->parameters['wwwDir'] . '/../data/uploads/' . $orderFile->hash;
            $values->file->move($path);


            $this->orderFileRepository->persist($orderFile);
        }
    }

    public function handleOrderFile($orderId)
    {
        $this->orderId = $orderId;
        $this->template->orderFiles = $this->orderFileRepository->findForOrder($orderId);
        $this->template->showTitle = true;
        $this->template->orderFile = true;

        $this->template->modalTitle = 'Soubory';


        $this->redrawControl('formSnippet');
        $this->redrawControl('titleSnippet');
    }

    /**
     * @param $form Form
     */
    public function orderItemSuccess($form)
    {
        $values = $form->getValues();

        $this->orderId = $values->orderId;
        $this->orderItemId = $values->orderItemId;
        $orderHash = $values->orderHash;
        $parsedHash = explode("#", $orderHash);
        $values->urgent = ($values->urgent == "1");

        if ($this->orderId && count($parsedHash) == 3 && $this->orderId != $parsedHash[1])  // original orderId != new orderId -> moving orderItem from order A to order B
        {
            $this->orderId = $parsedHash[1];
        }

        if ($this->orderId) {
            $order = $this->orderRepository->getByID($this->orderId);
        } else {
            if ($parsedHash[1] == "") {
                $order = $this->orderRepository->createOrder($parsedHash[0], $parsedHash[2], $this->getBrancheID());
            } else {
                $order = $this->orderRepository->getByID((int)$parsedHash[1]);
            }
        }


        $orderItem = $this->orderItemRepository->manage($values, $order);
        $msg = "Výborně! Vaše " . (($values->urgent) ? "URGENTNÍ " : "") . 'položka byla přidána!';
        if ($orderItem) {
            $this->flashMessage($msg, "success");
        } else {
            $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
        }

        if ($values->urgent) {
            $this->mailer->sendUrgentOrderItem($orderItem);
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } elseif ($form["send"]->isSubmittedBy()) {

            $this->template->showOrderItemForm = false;
            $this->template->showTitle = false;
            $this->redrawControl('formSnippet');
            $this->flashModalMessage($msg, "success");
            $this->redrawControl('list');
            $this->redrawControl('flashModal');
        } else {
            $msg .= " Přidejte další nebo fomulář zavřete.";
            $this->flashModalMessage($msg, "success");
            $this->redrawControl('list');
            $this->redrawControl('flashModal');
            $form->setValues(array(), true); //clear form for next usage
        }
    }

    public function handleManageOrderItem($orderId, $orderItemId, $dateTime, $urgent = false)
    {
        $this->dateTime = $dateTime;
        $this->orderItemId = $orderItemId;
        $this->orderId = $orderId;
        $this->urgent = $urgent;
        $this->template->showOrderItemForm = true;
        $this->template->showTitle = true;

        if ($orderItemId == 0) {
            $this->template->modalTitle = "Přidat položku do objednávky";
        } else {
            $this->template->modalTitle = "Editovat položku";
        }

        if ($this->presenter->isAjax()) {
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
//            $this->redrawControl('list');
        }
    }

    public function handleSummarizeOrder($orderId)
    {
        /**
         * @var Order $order
         */
        $order = $this->orderRepository->getByID($orderId);
        $this->template->order = $order;
        $this->template->deliveryDate = $order->getDeliveryDateTime();
        $this->template->orderDate = new \DateTime();

        $this->template->modalTitle = "Odeslat objednávku";
        $this->template->showSummaryDialog = true;
        $this->template->showTitle = true;


        if ($this->presenter->isAjax()) {
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    public function actionSendOrder($orderId, $isAndSetAsExtra = false)
    {
        /**
         * @var Order $order
         */
        $order = $this->orderRepository->getByID($orderId);
        if ($isAndSetAsExtra)  // updating closing date. $isAndSetAsExtra is in true only for branche
        {
            $this->orderRepository->setAsExtra($order);
        }
        if ($this->sendOrder($order)) //sending email and changing status
        {
            $this->flashMessage('Objednávka byla odeslána!', "success");
        } else {
            $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
        }

        $this->redirect("Order:");
    }

    /**
     * @param $order Order
     * @return bool
     */
    private function sendOrder($order)
    {
        return ($this->orderRepository->sendOrder($order) AND $this->mailer->sendSentOrder($order));
    }

    public function handleDelOrderItem($orderItemId)
    {
        $this->payload->status = ($this->orderItemRepository->del($orderItemId)) ? "ok" : "err";
        $this->sendPayload();
    }

    //</editor-fold>

    //<editor-fold desc="admin-item-operations">
    /**
     * shows the modal and it's item for proceed the order item
     * @param $orderItemId
     * @param string $operation
     */
    public function handleProceedOrderItem($orderItemId, $operation = "approve")
    {
        $this->template->showTitle = true;
        $this->template->proceedOrderItemForm = true;
        $this->orderItemId = $orderItemId;

        $this->template->orderItemObj = $this->orderItemRepository->getByID($orderItemId);

        $title = "Zprocesovat položku";
        switch ($operation) {
            case "approve":
                $title = "Schválit položku";
                break;
            case "change":
                $title = "Zaměnit položku";
                break;
            case "postpone":
                $title = "Přesunout položku";
                break;
            case "cancel":
                $title = "Odmítnout položku";
                break;
        }

        $this->template->modalTitle = $title;
        $this->template->operation = $operation;


        $this->redrawControl('formSnippet');
        $this->redrawControl('titleSnippet');
    }

    /**
     * handler for approving order item
     * @param $orderItemId
     */
    public function handleApproveOrderItem($orderItemId)
    {
        try {
            $this->orderItemRepository->approve($orderItemId);
            $this->flashMessage('Položka byla úspešně SCHVÁLENA!', 'success');
        } catch (Exception $e) {
            Debugger::log($e);
            $this->flashMessage('Došlo k neznámé chybě, prosím kontaktujte administrátora', 'danger');
        }

        $this->redrawHandlerComponents();
    }

    /**
     * component for changing order item
     * @return Form
     */
    public function createComponentChangeOrderItemForm()
    {
        $form = $this->orderFormFactory->createChangeOrderItem($this->orderItemId);
        $form->onSuccess[] = array($this, 'changeOrderItemSuccess');
        return $form;
    }

    /**
     * handler for changing order item
     * @param $form Form
     */
    public function changeOrderItemSuccess($form)
    {
        try {
            $values = $form->getValues();
            $this->orderItemId = $values->order_item_id;
            // to be able to get data from orderItem before the modification. used in changeOrderItem
            $orderItemBeforeModification = $this->orderItemRepository->getByID($this->orderItemId);
            $orderItem = $this->orderItemRepository->changeOrderItem($values);
            $this->orderItemChangeRepository->createChangeForChangeOrderItem($orderItem, $values,
                $this->getUserEntity()->id, $orderItemBeforeModification);

            $this->flashMessage("Položka byla úspěšně ZAMĚNĚNA.", "success");
        } catch (Exception $e) {
            Debugger::log($e);
            $this->flashMessage("Došlo k chybě", "danger");
        }
        $this->redrawHandlerComponents();
    }

    /**
     * component with form for postponing order item
     * @return Form
     */
    public function createComponentPostponeOrderItemForm()
    {
        $form = $this->orderFormFactory->createPostponeOrder($this->orderItemId);
        $form->onSuccess[] = array($this, 'postponeOrderItemSuccess');
        return $form;
    }

    /**
     * @param $form Form
     */
    public function postponeOrderItemSuccess($form)
    {
        $values = $form->getValues();

        // <settingID> # <orderID> # <datetimehash>
        $orderHashParsed = explode("#", $values->orderHash);

        if ($orderHashParsed[1] == "") {
            $targetOrder = $this->orderRepository->createOrder($orderHashParsed[0], $orderHashParsed[2],
                $this->getBrancheID());
        } else {
            $targetOrder = $this->orderRepository->getByID((int)$orderHashParsed[1]);
        }

        /**
         * @var OrderItem $orderItem
         */
        // archiving the previous
        $orderItem = $this->orderItemRepository->getByID($values->order_item_id);

        //create the follower and copy data
        $followerOrderItem = $this->orderItemRepository->postponeOrderItem($orderItem, $targetOrder);

        $this->orderItemChangeRepository->createChangeForPostponeOrderItem($orderItem, $followerOrderItem, $targetOrder,
            $values, $this->getUserEntity()->id);

        $this->flashMessage("Položka byla úspěšně přesunuta.", "success");

        $this->redrawControl('formSnippet');
        $this->redrawControl('header');
        $this->redrawControl('list');
        $this->redrawControl('flashes');
    }

    /**
     * component with form for canceling order item
     * @return Form
     */
    public function createComponentCancelOrderItemForm()
    {
        $form = $this->orderFormFactory->createCancelOrderItem($this->orderItemId);
        $form->onSuccess[] = array($this, 'cancelOrderItemSuccess');
        return $form;
    }

    /**
     * @param $form Form
     */
    public function cancelOrderItemSuccess($form)
    {
        try {
            $values = $form->getValues();
            $orderItem = $this->orderItemRepository->cancelOrderItem($values);
            $this->orderItemChangeRepository->createChangeForCancelItem($orderItem, $values->comment,
                $this->getUserEntity()->id);

            $this->flashMessage("Položka byla úspěšně ODMÍTNUTA.", "success");
        } catch (Exception $e) {
            Debugger::log($e);
            $this->flashMessage("Došlo k chybě", "danger");
        }
        $this->redrawHandlerComponents();
    }

    //</editor-fold>

    //<editor-fold desc="admin-order-operations">
    /**
     * changes the status of the order
     * @param $orderId
     * @param $newStatus
     */
    public function handleChangeOrderState($orderId, $newStatus)
    {
        /**
         * @var $order Order
         */
        $order = $this->orderRepository->getByID($orderId);

        try {
            $this->orderRepository->changeState($orderId, $newStatus);
            if ($newStatus == Order::STATE_SENT) {
                $this->orderRepository->openByAdmin($orderId);
                $msg = "OTEVŘENA";
            } else {
                $msg = "UZAVŘENA";
            }
            $this->flashMessage("Objednávka byla " . $msg, "success");
        } catch (DataException $e) {
            Debugger::log($e);
            $this->flashMessage("Došlo k chybě", "danger");
        }

        $this->redrawHandlerComponents();
    }

    /**
     * component for closing (status) ORDER
     * @return Form
     */
    public function createComponentCloseOrderForm()
    {
        $form = $this->orderFormFactory->createCloseOrder($this->orderId);
        $form->onSuccess[] = array($this, 'closeOrderSuccess');
        return $form;
    }

    /**
     * @param $form Form
     */
    public function closeOrderSuccess($form)
    {
        try {
            $values = $form->getValues();
            $this->orderId = $values->order_id;
            $order = $this->orderRepository->changeState($values->order_id, Order::STATE_CLOSED,
                $values->delivery_date);
            $this->flashMessage("Objednávka byla uzavřena", "success");

            $this->mailer->sendClosedOrder($order);
            $this->template->showClosingForm = false;
        } catch (Exception $e) {
            Debugger::log($e);
            $this->flashMessage("Došlo k chybě", "danger");
        }

        $this->redrawHandlerComponents();
    }

    /**
     * shows the modal for changing status of the ORDER
     * @param $orderId
     * @param $newStatus
     */
    public function handleChangeOrderStateOpenDialog($orderId, $newStatus)
    {
        $order = $this->orderRepository->getByID($orderId);
        $this->orderId = $orderId;
        if ($newStatus == "close" and $this->getUserEntity()->isAdmin()) {
            $this->template->modalTitle = "Uzavřít objednávku";
            $this->template->showClosingForm = true;
            $this->template->showTitle = true;
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        } else {
            $this->template->orderObj = $order;
            $this->template->dialogAction = $newStatus;

            switch ($newStatus) {
                case "close":
                    $title = "Uzavřít";
                    break;
                case "open":
                    $title = "Otevřít";
                    break;
                default:
                    $title = "Archivovat";
            }

            $this->template->modalTitle = $title . " objednávku";
            $this->template->showOpenCloseDialog = true;
            $this->template->showTitle = true;
            $this->redrawControl('formSnippet');
            $this->redrawControl('titleSnippet');
        }
    }
    //</editor-fold>

    //<editor-fold desc="branche-order-operations">
    /**
     * ajax handler for closing order
     * @param $orderId
     */
    public function handleCloseOrder($orderId)
    {
        if ($this->orderRepository->changeState($orderId, Order::STATE_SENT)) {
            $this->payload->status = "ok";
        } else {
            $this->payload->status = "err";
        }

        $this->sendPayload();
    }

    /**
     * opens the dialog to create new Extra order
     */
    public function handleExtraOrderOpenDialog()
    {
        $this->template->modalTitle = "Zvláštní objednávka";
        $this->template->showExtraOrderDialog = true;
        $this->template->showTitle = true;
        $this->redrawControl('formSnippet');
        $this->redrawControl('titleSnippet');
    }

    /**
     * creates extra order
     */
    public function handleCreateExtraOrder()
    {
        try {
            $order = $this->orderRepository->createOrder(null, null, $this->getBrancheID());
            $this->orderRepository->setAsExtra($order);

            $this->flashMessage("Zvláštní objednávka byla vytvořena", "success");

            $this->template->slots = $this->orderSettingRepository->getFollowingSlots($this->getBrancheID());
            $this->template->extras = $this->orderRepository->findExtra($this->getBrancheID());
        } catch (Exception $e) {
            $this->flashMessage("Došlo k chybě", "danger");
        }

        $this->redrawHandlerComponents();
    }

    /**
     * deletes extra order
     * @param $orderId
     */
    public function handleDelExtraOrder($orderId)
    {
        $this->payload->status = ($this->orderRepository->del($orderId)) ? "ok" : "err";
        $this->sendPayload();
    }

    /**
     * deletes extra order
     * @param $orderFileId
     */
    public function handleDelOrderFile($orderFileId)
    {
        $this->payload->status = ($this->orderFileRepository->del($orderFileId)) ? "ok" : "err";
        $this->sendPayload();
    }
    //</editor-fold>

    /**
     * shows history
     * @param $orderItemId
     */
    public function handleShowChanges($orderItemId)
    {
        /**
         * @var $orderItem OrderItem
         */
        $orderItem = $this->orderItemRepository->getByID($orderItemId);
        $this->template->changes = $orderItem->getChanges();
        $this->template->modalTitle = "Změny položky objednávky";
        $this->template->showChanges = true;
        $this->template->showTitle = true;
        $this->redrawControl('formSnippet');
        $this->redrawControl('titleSnippet');
    }

}