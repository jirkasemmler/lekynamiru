<?php

namespace App\Presenters;


class ChangelogPresenter extends BasePresenter {

    public function renderDefault() {
        $this->template->mainTitle = "Changelog";
        $this->template->subTitle = "Záznam provedených změn";
    }
}