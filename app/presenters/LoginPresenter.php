<?php

namespace App\Presenters;

use App\Model\Repositories\UserRepository;
use App\Model\Repositories\BrancheRepository;
use App\Forms\LoginFormFactory;
use Nette;

class LoginPresenter extends BasePresenter {

    /** @var LoginFormFactory @inject */
    public $loginFactory;

    /** @var Mailer @inject */
    public $mailer;

    /** @var UserRepository @inject */
    public $userRepository;

    /** @var BrancheRepository @inject */
    public $brancheRepository;
    private $token;

    private function isInBranche($id_branche, $user) {
        return $this->brancheRepository->isInBranch($id_branche, $user);
    }

    public function actionChangeBranche($id) {
        $user = $this->getUserEntity();
        $_SESSION["branche"] = $id;

        if ($user->isAllowed('Patient', ['action' => 'default']) || $this->isInBranche($id, $user)) {
            $this->redirect("Patient:");
        } else {
            $this->redirect("Default:");
        }
    }

    public function renderDefault() {
        if ($this->user->isLoggedIn()) {
            $user = $this->getUserEntity();
            if($user->isAllowed('Patient', ['action' => 'default'])){
                $this->redirect("Patient:");
            }
            else{
                $this->redirect("Default:");
            }

        }
        $this->template->mainTitle = "Přihlášení";
        $this->template->subTitle = "Přihlašte se do systému pomocí emailu a hesla";
        $this->template->data = "";
    }

    public function renderLost() {
//just to show the component and content of the template. no reason to send there any more data
    }

    /**
     * Login form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentLostPassForm() {
        $form = $this->loginFactory->createLostPass();
        $form->onSuccess[] = array($this, 'lostPassSuccess');

        return $form;
    }

    /**
     * @param $form Nette\Application\UI\Form
     */
    public function lostPassSuccess($form) {
        $data = $form->getValues();
        $ret = $this->userRepository->lostPassStep1($data);

        switch ($ret) {
            case false: {
                $this->flashMessage("Došlo k neočekávané chybě.", "warning");
            };
                break;
            case "notfound": {
                $this->flashMessage("Pro zadaný email nebyla nalezena žádná shoda :(", "warning");
            };
                break;
            default: {
                $in = ["email" => $data->email, "token" => $ret];
                // $mailer = new Mailer;
                $this->mailer->lostPass($this->getPresenter(), $in);
                $this->flashMessage("Výborně! Na váš email byly zaslány informace pro daší postup.", 'success');
            };
                break;
        }
    }

    public function renderResetPass($token) {
        if (!$this->userRepository->isTokenValid($token)) {
            $this->template->notValid = "Tento odkaz pro obnovu hesla není platný!";
        } else {
            $this->token = $token;
            $this->template->title = "Obnova hesla";
        }

    }

    public function createComponentResetPass() {
        $form = $this->loginFactory->createResetPass($this->token);
        $form->onSuccess[] = array($this, 'resetPassSuccess');

        return $form;
    }

    /**
     * @param $form Nette\Application\UI\Form
     */
    public function resetPassSuccess($form) {
        $data = $form->getValues();
        $ret = $this->userRepository->resetPass($data);

        switch ($ret) {
            case "ok": {
                $this->flashMessage("Výborně, bylo vám vytvořeno nové heslo!", "success");
                $this->redirect("Login:");
            };
                break;
            case "notfound": {
                $this->flashMessage("Bohužel nebyl nalezen účet se zadaným emailem pro obnovu hesla.", "warning");
            };
                break;
            case "pass": {
                $this->flashMessage("Hesla jsou příliš krátká nebo se neshodují.", "warning");
            };
                break;
            default: {
                $this->flashMessage("Došlo k neočekávané chybě.", "danger");
            };
        }

    }

    /**
     * Login form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentLoginForm() {
        $form = $this->loginFactory->create();
        $form->onSuccess[] = array($this, 'loginSuccess');

        return $form;
    }


    /**
     * method to login user
     * @param $form Nette\Application\UI\Form
     * @internal param $values - data from form
     */
    public function loginSuccess($form) {

        $values = $form->getValues();


        try {
            $this->getUser()->login($values->email, $values->password);
            $roles = $this->getUser()->getRoles();


            // todo : prepsat pro domovy aby se jim ukazal jen ten jejich
            $user = $this->userRepository->getByID($this->getUser()->id);

            $branche = $this->userRepository->getFirstBranche($user->id);

            if ($branche == 0) {
                $_SESSION["branche"] = 0;
                $this->flashMessage("Váš účet nemá dané zařazení.");
                $this->redirect("User:profile");
            } else {
                $_SESSION["branche"] = $branche;
                $user = $this->getUserEntity();
                $this->flashMessage("Vítejte v systému Lekynamiru.cz");
                if($user->isAllowed('Patient', ['action' => 'default'])){
                    $this->redirect("Patient:");
                }
                else{
                    $this->redirect("Default:");
                }
            }


        } catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage($e->getMessage(), "warning");
        }
    }

    protected function createComponentRegForm() {
        $form = $this->loginFactory->createReg();
        $form->onSuccess[] = array($this, 'regSuccess');

        return $form;
    }

    /**
     * method to login user
     * @param $form
     * @internal param $values - data from form
     */
    public function regSuccess($form, $values) {

        $ret = $this->userRepository->registerUser($values);
    }

    /**
     * action to logout
     */
    public function actionOut() {
        $this->getUser()->logout();

        $this->flashMessage('Byl(a) jsi úspěšně odhlášen(a).');
        $this->redirect('Login:');
    }


}
