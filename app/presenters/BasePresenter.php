<?php

namespace App\Presenters;

use App\Model\Entities\Role;
use Nette,
    App\Model;
use App\Model\Repositories\BrancheRepository;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\SettingRepository;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var BrancheRepository @inject */
    public $brancheRepository;
    /** @var SettingRepository @inject */
    public $settingRepository;
    /** @var Model\Repositories\PermissionRepository @inject */
    public $permissionRepository;
    /** @var UserRepository @inject */
    public $userRepository;
    public $departmentID;

    public function startup()
    {

        parent::startup();

        if ($this->getUser()->isLoggedIn()) {
            $role = $this->getUser()->getRoles()[0];

            if ($role == 'department') {
                $this->departmentID = $this->userRepository->getDepartment($this->getUser()->id);
            }
        }

        if (!$this->isLoginOperation()) {
            $user = $this->getUserEntity();

            $paramsCopy = $this->params;
            unset($paramsCopy['action']);
            unset($paramsCopy['do']);
            if (array_key_exists('id', $paramsCopy) && $paramsCopy['id'] == null) {
                unset($paramsCopy['id']);
            }

            foreach ($paramsCopy as $k => $v) {
                if (substr($k, 0, 1) == '_') {
                    unset($paramsCopy[$k]);
                }
            }

            $keys = array_keys($paramsCopy);
            $serializedKeys = implode(',', $keys);
            $toPass = $this->params;

            $toPass['params'] = $serializedKeys;


            if (!$user->isAllowed($this->name, $toPass) && !$this->isLoginOperation()) {
                throw new Nette\Application\ForbiddenRequestException('Nepovolena operace');
            }
        }
    }

    private function isLoginOperation()
    {
        return $this->name == 'Login' ||
            ($this->name == 'Login' && in_array($this->params['action'], ['reset-pass', 'default']))
            || $this->name == 'Api'
            || $this->name == 'Changelog'
            || ($this->name == 'User' && $this->params['action'] == 'profile');
    }

    /**
     * @return Model\Entities\User
     */
    public function getUserEntity()
    {
        return $this->userRepository->getByID($this->getUser()->getId());
    }

    public function getBrancheID()
    {
        return $_SESSION['branche'];
    }

    public function getBrancheEntity()
    {
        return $this->brancheRepository->getByID($this->getBrancheID());
    }

    public function beforeRender()
    {
        $user = $this->getUserEntity();

        $this->template->isSuperAdmin = ($user) ? $user->isSuperAdmin() : false;
        parent::beforeRender();


        $this->template->logged = $this->getUser()->isLoggedIn();
        $this->template->user = $user;


        if ($this->getUser()->isLoggedIn()) {
            $this->template->userName = $this->getUser()->identity->data['name'];
        }


        $menu = [];
        /*
          $menuItem = [
            'name' => '',
            'link' => '',

            'tooltip' => '',
            'icon' => '', //fa-icon without 'fa-' prefix
            'allowedto' => [] //fa-icon without 'fa-' prefix
        ];
         */


        if ($this->getUser()->isLoggedIn()) {
            $config = [
                'Medicine:#' => [
                    'name' => 'Léky',
                    'link' => '#',
                    'icon' => 'medkit',
                    'help' => 'Správa léků, kanystrů a odborností',
                ],
                'Medicine:default' => [
                    'name' => 'Léky',
                    'link' => 'Medicine:default',
                    'icon' => 'medkit',
                    'help' => 'Organizovat všechny léky v systému',
                ],
                'Medicine:cans' =>
                    [
                        'name' => 'Kanystry',
                        'link' => 'Medicine:cans',
                        'icon' => 'server',
                        'help' => 'Organizace kanystrů',
                    ],
                'Medicine:speciality' =>
                    [
                        'name' => 'Odbornosti',
                        'link' => 'Medicine:speciality',
                        'icon' => 'user-md',
                        'help' => 'Správa číselníku odborností',
                    ],
                'Branche:default' => [

                    'name' => 'Domovy',
                    'link' => 'Branche:',
                    'icon' => 'hospital-o',
                    'help' => 'Správa domovů',
                ],
                'Patient:default' => [
                    'name' => 'Pacienti',
                    'link' => 'Patient:',
                    'icon' => 'wheelchair',
                    'help' => 'Pacienti',
                ],
                'Patient:dead' => [
                    'name' => '&#10013;',
                    'link' => 'Patient:dead',
                    'icon' => '',
                    'help' => 'Pacienti',
                ],

                'Patient:prescription' => [
                    'name' => 'Stavy',
                    'link' => 'Patient:prescription',
                    'icon' => 'bar-chart',
                    'help' => 'Recepty',
                ],
                'Report:default' => [

                    'name' => 'Reporty',
                    'link' => 'Report:',
                    'icon' => 'book',
                    'help' => 'Reporty',
                ],
                'Usage:default' => [

                    'name' => 'Spotřeba',
                    'link' => 'Usage:',
                    'icon' => 'balance-scale',
                    'help' => 'Přehled spotřeb',
                ],
                'Order:default' => [
                    'name' => 'Objednávky',
                    'link' => 'Order:',
                    'icon' => 'shopping-cart',
                    'help' => 'Objednávky',
                ],

                'Settings:#' => [
                    'name' => 'Systém',
                    'link' => 'Log:',
                    'icon' => 'cogs',
                    'help' => 'Nastavení a historie',
                ],
                'User:default' => [
                    'name' => 'Uživatelé',
                    'link' => 'User:',
                    'icon' => 'users',
                    'help' => 'Správa uživatelů',
                ],
                'Log:default' => [
                    'name' => 'Historie',
                    'link' => 'Log:',
                    'icon' => 'history',
                    'help' => 'Historie',
                ],
                'Log:setting' => [
                    'name' => 'Nastavení',
                    'link' => 'Log:setting',
                    'icon' => 'wrench',
                    'help' => 'Nastavení',
                ],
                'Log:eurekaExport' => [
                    'name' => 'Export',
                    'link' => 'Log:eureka-export',
                    'icon' => 'exchange',
                    'help' => 'Nastavení exportu',
                ],
                'Role:default' => [
                    'name' => 'Role',
                    'link' => 'Role:default',
                    'icon' => 'address-book',
                    'help' => '',
                ],

                'Permission:default' => [
                    'name' => 'Oprávnění',
                    'link' => 'Permission:default',
                    'icon' => 'key',
                    'help' => '',
                ],
            ];
            if (!$this->isLoginOperation()) {
                $role = $user->getUserRole();
                $perms = ($user->isSuperAdmin()) ? $this->permissionRepository->getAllPermissionsByID() : $role->getPermissions();
                $menu = Role::generateMenu($config, $perms);
            }
            $activeBranche = $this->brancheRepository->getByID($_SESSION['branche']);
            if ($activeBranche) {
                $this->template->activeBranche = $activeBranche;
            } else {
                if ($user->isAllowed('Api', ['action' => 'admin'])) {
                    $this->template->activeBranche = $this->userRepository->getFirstBranche($user->id);
                } else {
                    $this->redirect('Login:out');
                }
            }
            $this->template->role = $this->getUser()->getRoles()[0];
            $assocUsers = $this->userRepository->getBranchesForAssociatedUser($user->id);
            if ($user->isAllowed('api', ['action' => 'admin']) || $assocUsers || $user->isAllowed('login', ['action' => 'changeBranche', 'params' => 'id'])) {
                if ($user->isAllowed('api', ['action' => 'admin'])) {
                    $this->template->branches = $this->brancheRepository->findAll();
                }
                else{
                    $brancheIds = array_map(function ($item){return $item['branch_id'];}, $assocUsers);
                    $branchs = $this->brancheRepository->findBy(['id' => $brancheIds]);
                    $this->template->branches = $branchs;
                }
            }
        }

        $this->template->menu = $menu;

        $versionNames = [
            'Actinium',
            'Aluminum',
            'Americium',
            'Antimony',
            'Argon',
            'Arsenic',
            'Astatine',
            'Barium',
            'Berkelium',
            'Beryllium',
            'Bismuth',
            'Bohrium',
            'Boron',
            'Bromine',
            'Cadmium',
            'Calcium',
            'Californium',
            'Carbon',
            'Cerium',
            'Cesium',
            'Chlorine',
            'Chromium',
            'Cobalt',
            'Copper',
            'Curium',
            'Dubnium',
            'Dysprosium',
            'Einsteinium',
            'Erbium',
            'Europium',
            'Fermium',
            'Fluorine',
            'Francium',
            'Gadolinium',
            'Gallium',
            'Germanium',
            'Gold',
            'Hafnium',
            'Hassium',
            'Helium',
            'Holmium',
            'Hydrogen',
            'Indium',
            'Iodine',
            'Iridium',
            'Iron',
            'Krypton',
            'Lanthanum',
            'Lawrencium',
            'Lead',
            'Lithium',
            'Lutetium',
            'Magnesium',
            'Manganese',
            'Meitnerium',
            'Mendelevium',
            'Mercury',
            'Molybdenum',
            'Neodymium',
            'Neon',
            'Neptunium',
            'Nickel',
            'Niobium',
            'Nitrogen',
            'Nobelium',
            'Osmium',
            'Oxygen',
            'Palladium',
            'Phosphorus',
            'Platinum',
            'Plutonium',
            'Polonium',
            'Potassium',
            'Praseodymium',
            'Promethium',
            'Protactinium',
            'Radium',
            'Radon',
            'Rhenium',
            'Rhodium',
            'Rubidium',
            'Ruthenium',
            'Rutherfordium',
            'Samarium',
            'Scandium',
            'Seaborgium',
            'Selenium',
            'Silicon',
            'Silver',
            'Sodium',
            'Strontium',
            'Sulfur',
            'Tantalum',
            'Technetium',
            'Tellurium',
            'Terbium',
            'Thallium',
            'Thorium',
            'Thulium',
            'Tin',
            'Titanium',
            'Tungsten',
            'Uranium',
            'Vanadium',
            'Xenon',
            'Ytterbium',
            'Yttrium',
            'Zinc',
            'Zirconium',
        ];
        $versionNumber = (string)$this->getContext()->parameters['appVersion'];
        $this->template->version = $versionNumber;
        $versionIndex = explode('.', $versionNumber);
        $index = (int)$versionIndex[1];
        if (array_key_exists($index, $versionNames)) {
            $versionName = $versionNames[$index];
        } else {
            $versionName = ' - n/a - ';
        }
        $this->template->versionName = $versionName;


        $staticSuffixTmp = (string)$this->getContext()->parameters['staticSuffix'];
        $staticSuffix = $staticSuffixTmp ? $staticSuffixTmp : '';
        $this->template->suffix = $staticSuffix;

        $this->template->staging = $this->settingRepository->isStaging();
    }
}
