<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system 
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 27.6.15
 * Time: 16:01
 */
namespace App\Presenters;
use App\Model\Repositories\UsageStatRepository;
use App\Model\Repositories\UserRepository;
use App\Forms\LoginFormFactory;
use App\Model\Repositories\MedicineRepository;
use App\Presenters\Mailer;
use Nextras;
class UsagePresenter extends BasePresenter {

	/** @var UserRepository @inject */
	public $userRepository;

	/** @var MedicineRepository @inject */
	public $medicineRepository;

	/** @var UsageStatRepository @inject */
	public $usageStatRepository;

	/** @var LoginFormFactory @inject */
	public $loginFactory;

	protected $id;
	protected $user;
	protected $admin;
	public function beforeRender()
	{
		parent::beforeRender();
		try{
			$this->user = $this->getUser();

			if(!$this->user->isLoggedIn())
			{
				$this->redirect("Login:out");
			}
		}
		catch(\Exception $e)
		{
			$this->redirect("Login:out");
		}
	}

	/**
	 * @param int  $branche  - for what branche , 0 = all
	 * @param null $department - for what department in branche NULL = all
	 * @param int  $period - for how long period
	 */
	public function renderDefault($branche,$department = NULL, $period = 2)
	{
		$this->template->mainTitle = "Spotřeba";
		$this->template->subTitle = "Celková, v domovech, na odděleních";

		if($department == 0)
		{
			$department = NULL;
		}
		$this->template->selectedUsageBranche = $branche;
		$this->template->selectedUsageDep = $department;
		$this->template->branches = $this->brancheRepository->getBranches();
		$this->template->usages = $this->usageStatRepository->getUsageStat($branche,$department, $period);

        $this->template->now  = $period;
        $this->template->next  = $period+2;
        $this->template->previous  = $period-2;

		if($branche != 0)
		{
			$deps = $this->brancheRepository->getDepartments($branche);
			$deps[NULL] = "Všechna"; //all

			$this->template->deps = $deps;
		}
	}

	protected function createComponentGrid()
	{
		$grid = new Nextras\Datagrid\Datagrid();
		$grid->addColumn('name',"Název")->enableSort(Nextras\Datagrid\Datagrid::ORDER_ASC);
		$grid->addColumn('kanystr', 'Account created');

		$grid->setDatasourceCallback(function($filter, $order)
		{
			$filters = array();
			foreach ($filter as $k => $v)
			{
				if ($k == 'id' || is_array($v))
					$filters[$k] = $v;
				else
					$filters[$k. ' LIKE ?'] = "%$v%";
			}

			$selection = $this->medicineRepository->magic($filters);

			if ($order[0])
			{
				$selection->order("name");
			}

			return $selection;
		});

		return $grid;
	}


	public function renderTest()
	{

	}

	/**
	 * the same input as in the render. just to change the content
	 * @param int  $branche
	 * @param null $department
	 * @param int  $period
	 */
	public function handleChangeUsage($branche, $department, $period = 2, $page = 1)
	{
		if($period == 18)
		{
			return false;
		}
		$this->template->mainTitle = "Spotřeba";
		$this->template->subTitle = "Celková, v domovech, na odděleních";

		if($department == 0)
		{
			$department = NULL;
		}
		$this->template->selectedUsageBranche = $branche;
		$this->template->selectedUsageDep = $department;
		$this->template->branches = $this->brancheRepository->getBranches();
//		$this->template->pills = $this->medicineRepository->countPillsMagic($branche, $department, $period, $page);
		$this->template->usages = $this->usageStatRepository->getUsageStat($branche,$department, $period);

		$this->template->now  = $period;
		$this->template->next  = $period+2;
		$this->template->previous  = $period-2;

		if($branche != 0)
		{
			$deps = $this->brancheRepository->getDepartments($branche);
			$deps[NULL] = "Všechna";

			$this->template->deps = $deps;
		}

		$this->redrawControl('list');
	}


}
