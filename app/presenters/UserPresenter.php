<?php
/**
 * Created by PhpStorm.
 * Lekynamiru.cz system
 * Author: Jiri Semmler, 2015
 * Contact: Jiri Semmler jirka.semmler@gmail.com
 * Date: 27.6.15
 * Time: 16:01
 */

namespace App\Presenters;

use App\Model\Repositories\UserRepository;
use App\Forms\LoginFormFactory;
use App\Presenters\Mailer;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class UserPresenter extends BasePresenter {

    /** @var UserRepository @inject */
    public $userRepository;

    /** @var LoginFormFactory @inject */
    public $loginFactory;

    /** @var Mailer @inject */
    public $mailer;

    protected $id;
    protected $user;
    protected $admin;

    public function renderDefault() {
        $data = $this->userRepository->getUsers()->toArray();
        if(!$this->getUserEntity()->isSuperAdmin()){
            // filter out super admins
            $data = \array_filter($data, function($item){return !$item->isSuperAdmin();});
        }
        $this->template->data = $data;
        $this->template->mainTitle = "Uživatelé";
        $this->template->subTitle = "Výpis uživatelů systému";
    }


    public function handleManageUser($id = null, $admin = false) {

        $this->admin = $admin;
        $this->id = $id;
        $this->template->showRegisterForm = true; //show components or title
        $this->template->showTitle = true;

        if ($id == 0) //new grade
        {
            $this->template->modalTitle = "Registrovat uživatele";
        } else //update
        {
            $this->id = $id; //saving id
            $this->template->modalTitle = "Editovat uživatele ";
        }

        if ($this->presenter->isAjax()) {
            $this->redrawControl('registerFormSnippet');
            $this->redrawControl('titleSnippet');
        }
    }


    protected function createComponentRegForm() {
        // TODO fix toho admina
        $form = $this->loginFactory->createReg($this->id, true);
        $form->onSuccess[] = array($this, 'regSuccess');

        return $form;
    }

    /**
     * method to login user
     * @param $form Form
     * @internal param $values - data from form
     */
    public function regSuccess($form, $values) {

        try {
            $ret = $this->userRepository->registerUser($values);

            if ($ret === "OK") {

                $this->flashMessage('Změny byly v pořádku uloženy', "success");
            } elseif ($ret === "DUPLICATE") {
                $this->flashMessage('Zadaný email už je v systému evidován', "warning");
            } elseif (is_array($ret) && $ret["name"] == "okmail") {
                $in = ["email" => $values->email, "token" => $ret["token"]];
                $this->mailer->registrace($this->getPresenter(), $in);

                $this->flashMessage('Uživatel byl registrován a byly mu zaslány kroky pro vstup do systému.',
                    "success");

            } elseif ($ret === "PASS1") {
                $this->flashMessage('Hesla se neshodují nebo jsou krátká.', "warning");
            } else {
                $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
            }
        } catch (\Exception $e) {
            Debugger::log($e);
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            $this->redrawControl('listSnippet');
            $this->redrawControl('flashes');
            $form->setValues(array(), true); //clear form for next usage
        }
    }


    public function handleDelUser($id) {
        if ($this->userRepository->delUser($id)) {
            $this->payload->status = "ok";
        } else {
            $this->payload->status = "err";
        }
        $this->sendPayload();
    }

    public function renderProfile($id = 0) {
        if ($id == 0) {
            $user_id = $this->getUser()->id;
            $showButton = true;
        } else {
            $user_id = $id;
            $showButton = false;
        }

        $user = $this->userRepository->getByID($user_id);
        $this->template->user = $user;
        $this->template->showButton = $showButton;
    }

    public function handleEdit() {

    }
}
