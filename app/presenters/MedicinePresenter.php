<?php

namespace App\Presenters;

use App\Forms\MedicineFormFactory;
use App\Model\Entities\Medicine;
use App\Model\Entities\Speciality;
use App\Model\Repositories\CanRepository;
use App\Model\Repositories\MedicineRepository;
use App\Model\Repositories\PredikceRepository;
use App\Model\Repositories\SpecialityRepository;
use Nette\Application\UI\Form;
use Nette\Neon\Exception;
use PHPExcel_IOFactory;

class MedicinePresenter extends BasePresenter
{
    private $user;
    /** @var MedicineFormFactory @inject */
    public $medicineFormFactory;
    /** @var MedicineRepository @inject */
    public $medicineRepository;
    /** @var CanRepository @inject */
    public $canRepository;
    /** @var SpecialityRepository @inject */
    public $specialityRepository;
    protected $id;
    private $imported;
    private $importSucceded;
    private $importFailed;
    /** @var PredikceRepository @inject */
    public $predikceRepository;

    public function beforeRender()
    {
        parent::beforeRender();
        try {
            $this->user = $this->getUser();

            if (!$this->user->isLoggedIn()) {
                $this->redirect("Login:out");
            }
        } catch (\Exception $e) {
            $this->redirect("Login:out");
        }
    }

    public function renderDefault()
    {
        $this->template->mainTitle = "Léky";
        $this->template->subTitle = "Výpis léků";

        $this->template->data = $this->medicineRepository->getMedicines();
    }

    public function handleManageMedicine($id)
    {
        $this->id = $id;
        $this->template->showMedicineForm = true; //show components or title
        $this->template->showTitle = true;

        if ($id == 0) {
            $this->template->modalTitle = "Přidat lék";
        } else {
            $this->id = $id; //saving id
            $this->template->modalTitle = "Editovat lék ";
        }

        if ($this->presenter->isAjax()) {
            $this->redrawControl('medicineFormSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    public function createComponentMedicineForm()
    {
        $form = $this->medicineFormFactory->create($this->id);

        $form->onSubmit[] = [$this, 'manageMedicineSuccess'];

        return $form;
    }

    public function manageMedicineSuccess($form)
    {
        $values = $form->getValues();
        $this->id = $values->id;
        $ret = $this->medicineRepository->manage($values);
        if ($ret instanceof Medicine) {
            $this->flashMessage('Lék byl uložen!', "success");
        } else {
            switch ($ret) {
                case 23000:
                    {
                        $this->flashMessage('Zadaný SUKL kód je již používán, zvolte jiný', "danger");
                    };
                    break;
                default:
                    {
                        $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
                    };
            }
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            $this->redrawControl('listOfMedicine');
            $this->redrawControl('flashes');
            $form->setValues([], true); //clear form for next usage
        }
    }

    public function handleDelMedicine($id)
    {

        if ($this->medicineRepository->delMedicine($id)) {
            $this->payload->status = "ok";
        } else {
            $this->payload->status = "err";
        }
        $this->sendPayload();
    }
    // ^^^^ MEDICINE ^^^^ //

    // vvvv CANS vvvv //
    public function renderCans()
    {
        $this->template->mainTitle = "Kanystry";
        $this->template->subTitle = "Výpis kanystrů a přiřazených léků";
        $this->template->data = $this->canRepository->getAll();
    }

    public function handleManageCan($id)
    {
        $this->id = $id;
        $this->template->showCanForm = true; //show components or title
        $this->template->showTitle = true;

        if ($id == 0) {
            $this->template->modalTitle = "Přidat kanystr";
        } else {
            //update
            $this->id = $id; //saving id
            $this->template->modalTitle = "Editovat kanystr ";
        }

        if ($this->presenter->isAjax()) {

            $this->redrawControl('canFormSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    public function handleDelCan($id)
    {

        if ($this->canRepository->delCan($id)) {
            $this->payload->status = "ok";
        } else {
            $this->payload->status = "err";
        }
        $this->sendPayload();
    }

    public function createComponentCanForm()
    {
        $form = $this->medicineFormFactory->createCan($this->id);

        $form->onSubmit[] = [$this, 'manageCanSuccess'];

        return $form;
    }

    public function manageCanSuccess($form)
    {
        $ret = $this->canRepository->manage($form->getValues());
        if ($ret === true) {
            $this->flashMessage('Kanystr byl uložen!', "success");
        } else {
            switch ($ret) {
                case 23000:
                    {
                        $this->flashMessage('Kanystr nelze vytvořit s přiřazeným lékem, protože lék už je obsažen v jiném kanystru',
                            "danger");
                    };
                    break;
                default:
                    {
                        $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
                    };
            }
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            if ($ret === true) {
                $this->template->showCanForm = false;
                $this->redrawControl('canFormSnippet');
            }
            $this->redrawControl('listOfCans');
            $this->redrawControl('flashes');
            $form->setValues([], true); //clear form for next usage
        }
    }
    // ^^^^ CANS ^^^^ //

    // vvvv SPECIALITY vvvv //
    public function renderSpeciality()
    {
        $this->template->mainTitle = "Odbornosti";
        $this->template->subTitle = "Výpis odborností";

        $this->template->data = $this->specialityRepository->getAll();
    }

    public function handleManageSpeciality($id)
    {
        $this->id = $id;
        $this->template->showSpecialityForm = true; //show components or title
        $this->template->showTitle = true;

        if ($id == 0) {
            //new grade
            $this->template->modalTitle = "Přidat odbornost";
        } else {
            //update
            $this->id = $id; //saving id
            $this->template->modalTitle = "Editovat odbornost";
        }

        if ($this->presenter->isAjax()) {

            $this->redrawControl('specialityFormSnippet');
            $this->redrawControl('titleSnippet');
        }
    }

    public function handleDelSpeciality($id)
    {
        if ($this->specialityRepository->delSpeciality($id)) {
            $this->payload->status = "ok";
        } else {
            $this->payload->status = "err";
        }
        $this->sendPayload();
    }

    public function createComponentSpecialityForm()
    {
        $form = $this->medicineFormFactory->createSpeciality($this->id);

        $form->onSubmit[] = [$this, 'manageSpecialitySuccess'];

        return $form;
    }

    public function manageSpecialitySuccess($form)
    {
        $ret = $this->specialityRepository->manage($form->getValues());
        if ($ret instanceof Speciality) {
            $this->predikceRepository->checkAllBranches();

            $this->flashMessage('Odbornost byla uložena!', "success");
        } else {
            switch ($ret) {
                case 23000:
                    {
                        $this->flashMessage('Odbornost nelze vytvořit, protože není unikátní.',
                            "danger");
                    };
                    break;
                default:
                    {
                        $this->flashMessage('Bohužel došlo k neznámé chybě ', "warning");
                    };
            }
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        } else {
            $this->template->showSpecialityForm = false; //show components or title
            $this->redrawControl('specialityFormSnippet');
            $this->redrawControl('list');
            $this->redrawControl('flashes');
            $form->setValues([], true); //clear form for next usage
        }
    }

    protected function createComponentImportForm()
    {
        $form = $this->medicineFormFactory->createImportForm();
        $form->onSuccess[] = [$this, 'importSuccess'];

        return $form;
    }

    public function importSuccess(Form $form)
    {
        $values = $form->getValues();
        $path = implode(DIRECTORY_SEPARATOR, [__DIR__, '..', '..', 'data', 'tmp', 'tmpMedicineImport.xls']);
        $values->file->move($path);

        $doc = PHPExcel_IOFactory::load($path);
        $doc->setActiveSheetIndex(0);
        $sheet = $doc->getActiveSheet();
        $i = 2;
        $success = [];
        $fail = [];
        while ($medicineName = $sheet->getCell("A{$i}")->getValue()) {
            try {
                $medicineName = $sheet->getCell("A{$i}")->getValue();
                $suklCode = $sheet->getCell("B{$i}")->getValue();
                $specialitiesCSV = $sheet->getCell("C{$i}")->getValue();
                $specs = [];
                if ($specialitiesCSV) {
                    $specStr = explode(',', $specialitiesCSV);
                    $specs = [];
                    foreach ($specStr as $spec) {
                        $specObj = $this->specialityRepository->upsertByName($spec);
                        if ($specObj instanceof Speciality) {
                            $specs[] = $specObj->id;
                        }
                    }
                }
                $packagesCSV = $sheet->getCell("D{$i}")->getValue();

                $suklCode = $suklCode ?: '0';
                $medicineObj = $this->medicineRepository->getBy(['name' => $medicineName]);
                $data = (object)[
                    'id' => ($medicineObj) ? $medicineObj->id : 0,
                    'name' => $medicineName,
                    'sukl' => (string)$suklCode,
                    'can' => '-1',
                    'specialities' => $specs,
                ];

                $medicineObj = $this->medicineRepository->manage($data);

                if ($packagesCSV) {
                    $pckgs = explode(',', $packagesCSV);
                    $this->medicineRepository->setPackages($medicineObj->id, $pckgs);
                }
                $success[] = $medicineName;
            } catch (Exception $ex) {
                $fail[] = $medicineName;
            }

            $i++;
        }
        unlink($path);
        $this->template->imported = true;
        $this->template->importSucceded = $success;
        $this->template->importFailed = $fail;
    }

    public function actionImport()
    {
        $this->template->imported = false;
    }
}
