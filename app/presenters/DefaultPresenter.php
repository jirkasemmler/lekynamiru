<?php

namespace App\Presenters;

class DefaultPresenter extends BasePresenter {

    public function renderDefault() {
        $this->template->mainTitle = "Léky na míru";
        $this->template->subTitle = "";
    }
}
